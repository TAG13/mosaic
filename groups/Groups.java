package groups;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import app.Array;
import app.Module;
import app.Request;
import app.Roles;
import app.Site;
import app.Text;
import app.pages.Page;
import calendar.EventProvider;
import calendar.MonthView;
import db.DBConnection;
import db.Filter;
import db.LinkValueRenderer;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.MultiColumnRenderer;
import db.column.OptionsColumn;
import db.column.PersonColumn;
import db.column.RolesColumn;
import db.column.TextAreaColumn;
import db.feature.Feature.Location;
import db.feature.TSVectorSearch;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Tabs;
import mosaic.Person;
import ui.NavBar;
import ui.TaskPane;
import web.HTMLWriter;

public class Groups extends Module {
	@JSONField(fields={"m_start_collapsed"})
	boolean		m_allow_subgroups;
	@JSONField(choices={ "calendar", "documents", "email", "lists", "mandate", "minutes", "people", "web sites" })
	String[]	m_items = new String[] { "calendar", "documents", "email", "mandate", "minutes", "people", "web sites" };
	@JSONField(after="double-click to rename",on_edit_updated="e => {net.post(context+'/Groups/rename_job','from='+e.detail.data.__originalData.value+'&to='+e.detail.data.value)}")
	String[]	m_jobs = new String[] { "convener" };
	@JSONField(required=true)
	String		m_mandate_label = "Mandate";
	@JSONField
	boolean		m_show_email_link = true;
	@JSONField
	boolean		m_show_reports = true;
	@JSONField
	boolean		m_start_collapsed;

	//--------------------------------------------------------------------------

	private String
	actionURL(int group_id, String action) {
		return "net.post(context+'/Groups/" + group_id + "/" + action + "',null,function(){net.replace('Groups/" + group_id + "')})";
	}

	//--------------------------------------------------------------------------

	private void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("groups")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("calendar", Types.BOOLEAN))
			.add(new JDBCColumn("calendar_access_policy", Types.VARCHAR).setDefaultValue("group members"))
			.add(new JDBCColumn("calendar_public", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("mandate", Types.VARCHAR))
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("private", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("role", Types.VARCHAR))
			.add(new JDBCColumn("self_joining", Types.BOOLEAN).setDefaultValue(true));
		if (m_allow_subgroups)
			table_def.add(new JDBCColumn("parent_group", Types.INTEGER));
		if (m_jobs != null)
			for (String job : m_jobs)
				table_def.add(new JDBCColumn(job, Types.INTEGER));
		addColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("group_links")
			.add(new JDBCColumn("groups"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("url", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	public void
	appendJobs(StringBuilder s, String value) {
		if (m_jobs != null)
			for (String job : m_jobs) {
				if (s.length() > 0)
					s.append(" OR ");
				s.append('"').append(job).append("\"=").append(value);
			}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;

		HTMLWriter writer = request.writer;
		if ("inactive_groups".equals(request.getPathSegment(1))) {
			Rows rows = new Rows(new Select("id,name").from("groups").where("NOT active").orderBy("lower(name)"), request.db);
			while (rows.next())
				writer.aOnClick(rows.getString(2), "document.location=context+'/Groups?group=" + rows.getInt(1) + "'").br();
			rows.close();
			return true;
		}
		int group_id = request.getPathSegmentInt(1);
		if (group_id == 0)
			return false;
		String item = request.getPathSegment(2);
		if (item == null)
			return false;
		if (item.equals("calendar")) {
			EventProvider calendar = (EventProvider)request.site.getModule("Group" + group_id + "Calendar");
			if (calendar != null)
				new MonthView(calendar, request).writeComponent(request);
		} else if (item.equals("mandate"))
			writer.write(request.db.lookupString(new Select("mandate").from("groups").whereIdEquals(group_id)));
		else
			switch(item) {
			case "documents":
				ViewState.setBaseFilter("group_documents", "groups_id=" + group_id, request);
				ViewState.setQuicksearch("group_documents", null, null, request);
				request.site.newView("group_documents", request).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
				break;
			case "lists":
				Rows rows = new Rows(new Select("id,name").from("lists").orderBy("name"), request.db);
				while (rows.next())
					request.writer.aOnClick(rows.getString(2), "this.style.display='none';net.replace(this.nextElementSibling,context+'/Views/list" + rows.getInt(1) + "/component')").write("<div></div>");
				rows.close();
				break;
			case "minutes":
				ViewState.setBaseFilter("group_minutes", "groups_id=" + group_id, request);
				ViewState.setQuicksearch("group_minutes", null, null, request);
				request.site.newView("group_minutes", request).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
				break;
			case "people":
				View groups = request.site.newView("groups", request);
				groups.selectByID(group_id);
				writeJobs(group_id, groups, request);
				writer.br();
				writeMembers(group_id, groups, request);
				break;
			case "web sites":
				ViewState.setBaseFilter("group_links", "groups_id=" + group_id, request);
				ViewState.setQuicksearch("group_links", null, null, request);
				request.site.newView("group_links", request).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			}
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		if ("rename_job".equals(request.getPathSegment(1))) {
			String from = request.getParameter("from");
			for (int i=0; i<m_jobs.length; i++)
				if (m_jobs[i].equals(from)) {
					String to = request.getParameter("to");
					m_jobs[i] = to;
					request.db.renameColumn("groups", from, to);
					store(request.db);
					break;
				}
			return true;
		}
		int group_id = request.getPathSegmentInt(1);
		String action = request.getPathSegment(2);
		String job = request.getPathSegment(3);
		if ("join".equals(action))
			((Person)request.getUser()).joinGroup(group_id, job, request.db);
		else if ("leave".equals(action))
			((Person)request.getUser()).leaveGroup(group_id, job, request.db);
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks() {
		return new String[][] { { "Groups", "/Groups/settings" } };
	}

	//--------------------------------------------------------------------------

	public List<String>
	getGroups(int people_id, DBConnection db) {
		return db.readValues(new Select("name").from("groups").join("groups", "people_groups").whereEquals("people_id", people_id));
	}

	//--------------------------------------------------------------------------
	// return list of group names and jobs for a person

	public List<String[]>
	getGroupsJobs(int people_id, DBConnection db) {
		ArrayList<String[]> group_jobs = new ArrayList<>();
		StringBuilder where = new StringBuilder();
		appendJobs(where, Integer.toString(people_id));
		if (m_jobs != null) {
			Rows rows = new Rows(new Select("*").from("groups").where(where.toString()), db);
			while (rows.next())
				for (String job : m_jobs)
					if (rows.getInt(job) == people_id)
						group_jobs.add(new String[] { rows.getString("name"), job });
			rows.close();
		}
		return group_jobs;
	}

	//--------------------------------------------------------------------------

	String
	getHasJobWhere(int people_id) {
		if (m_jobs == null)
			return null;
		StringBuilder s = new StringBuilder();
		for (int i=0; i<m_jobs.length; i++) {
			if (i > 0)
				s.append(" OR ");
			s.append('"').append(m_jobs[i]).append("\"=").append(people_id);
		}
		return s.toString();
	}

	//--------------------------------------------------------------------------

	public Map<String,Integer>
	getJobsPeople(int group_id, DBConnection db) {
		Map<String,Integer> jobs_people = new HashMap<>();
		if (m_jobs != null)
			for (String job : m_jobs)
				jobs_people.put(job, db.lookupInt(new Select(job).from("groups").whereIdEquals(group_id), -1));
		return jobs_people;
	}

	//--------------------------------------------------------------------------

	public boolean
	hasJob(int group_id, int people_id, DBConnection db) {
		if (m_jobs != null)
			return db.exists(new Select().from("groups").whereIdEquals(group_id).andWhere(getHasJobWhere(people_id)));
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		adjustTables(db);
		db.createManyToManyLinkTable("people", "groups");
		Rows groups = new Rows(new Select("id,name,calendar_public,calendar_access_policy").from("groups").where("active AND calendar"), db);
		String[] colors = new String[] { "#3D9970", "#2ECC40", "#01FF70", "#AAAAAA", "#DDDDDD" };
		int num = 0;
		while (groups.next()) {
			EventProvider event_provider = new GroupEventProvider(groups.getString(2), groups.getInt(1), groups.getBoolean(3), groups.getString(4), db);
			m_site.addModule(event_provider, db);
			if (event_provider.getColor() == null)
				event_provider.setColor(colors[num++ % colors.length]);
		}
		groups.close();
		Roles.add("groups");
		m_site.addUserDropdownItem(new Page("Edit " + getDisplayName(null), this){
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Edit Groups", request);
				request.site.newView("edit groups", request).writeComponent();
				request.close();
			}
		}.setRole("groups"), db);
		m_site.getPages().add(new Page("Groups", m_site) {
			@Override
			public void
			a(NavBar nav_bar, Request request) {
				Select select = new Select(m_allow_subgroups ? "id,name,parent_group" : "id,name").from("groups").where("active").orderBy("name");
				int people_id = request.getUser().getId();
				String where = "NOT private OR EXISTS(SELECT 1 FROM people_groups WHERE groups.id=groups_id AND people_id=" + people_id + ")";
				if (m_jobs != null)
					where += " OR " + getHasJobWhere(people_id);
				ArrayList<String[]> rows = request.db.readRows(select.andWhere(where));
				HTMLWriter writer = request.writer;
				nav_bar.dropdownOpen(getDisplayName(request), false);
				if (m_allow_subgroups) {
					String uuid = UUID.randomUUID().toString();
					writer.write("<div id=\"").write(uuid).write("\" style=\"padding:10px\"></div>");
					writer.scriptOpen();
					writer.write("new NavList([");
					boolean first = true;
					for (String[] group : rows)
						if (group[2] == null) {
							if (first)
								first = false;
							else
								writer.comma();
							writeGroup(group, rows, writer);
						}
					writer.write("],{has_subs:true");
					if (m_start_collapsed)
						writer.write(",start_collapsed:true");
					writer.write("}).render('").write(uuid).write("')");
					writer.scriptClose();
				} else
					for (String[] group : rows)
						nav_bar.aOnClick(group[1], "document.location=context+'/Groups?group=" + group[0] + "'");
				if (m_show_reports) {
					nav_bar.divider();
					nav_bar.aOnClick("View Membership By Group", "document.location=context+'/Groups?report=View Membership By Group'");
					nav_bar.aOnClick("View Membership By Person", "document.location=context+'/Groups?report=View Membership By Person'");
				}
				if (request.db.exists(new Select(null).from("groups").where("NOT active"))) {
					nav_bar.divider();
					nav_bar.aOnClick("View Inactive Group", "new Dialog({title:'Inactive Groups',url:context+'/Groups/inactive_groups'}).open()");
				}
				nav_bar.dropdownClose();
			}
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Groups", request.site.newHead(request).style(".nav li { white-space:nowrap; }").script("calendar-min"), request);
				String report = request.getParameter("report");
				if (report != null) {
					request.writer.write("<div class=\"container\">");
					if ("View Membership By Group".equals(report)) {
						request.writer.h2("View Membership By Group");
						request.site.newView("report groups", request).write();
					} else if ("View Membership By Person".equals(report)) {
						request.writer.h2("View Membership By Person");
						request.site.newView("report people", request).write();
						StringBuilder on = new StringBuilder("groups.id=people_groups.groups_id");
						appendJobs(on, "people_id");
						Rows rows = new Rows(new Select("c,COUNT(c)").from("(SELECT COUNT(*) AS c FROM (SELECT DISTINCT people_id,groups.id FROM people_groups JOIN groups ON " + on.toString() + " WHERE groups.active ORDER BY people_id,groups.id) a GROUP BY people_id ORDER BY people_id) b").groupBy("c").orderBy("c"), request.db);
						while (rows.next()) {
							int num_groups = rows.getInt(1);
							int num_people = rows.getInt(2);
							request.writer.write("<div>" + num_people + " " + (num_people == 1 ? "person is a member of " : "people are members of ") + num_groups + (num_groups == 1 ? " group" : " groups") + "</div>");
						}
						rows.close();
					}
					request.writer.write("</div>");
					return;
				}
				writeGroupPane(request.getInt("group", 0), request);
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	public boolean
	isMember(int group_id, int people_id, DBConnection db) {
		if (hasJob(group_id, people_id, db))
			return true;
		return db.rowExists("people_groups", "people_id=" + people_id + " AND groups_id=" + group_id);
	}

	//--------------------------------------------------------------------------

	final boolean
	isMemberOfSubgroup(int group_id, int people_id, DBConnection db) {
		if (!m_allow_subgroups)
			return false;
		return isMemberOfSubgroup(group_id, people_id, db, new HashSet<String>());
	}

	//--------------------------------------------------------------------------

	private boolean
	isMemberOfSubgroup(int group_id, int people_id, DBConnection db, Set<String> seen) {
		List<String> ids = db.readValues(new Select("id").from("groups").where("parent_group=" + group_id));
		for (String id : ids)
			if (!seen.contains(id)) {
				seen.add(id);
				int parent_group_id = Integer.parseInt(id);
				if (isMember(parent_group_id, people_id, db))
					return true;
				if (isMemberOfSubgroup(parent_group_id, people_id, db, seen))
					return true;
			} else
				m_site.log("parent group id " + id + " already seen", false);

		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("edit groups")) {
			ViewDef view_def = new ViewDef(name) {
				private void
				addEventProvider(String group_name, int group_id, boolean calendar_public, String access_policy, Request request) {
					EventProvider event_provider = new GroupEventProvider(group_name, group_id, calendar_public, access_policy, request.db);
					site.addModule(event_provider, request.db);
					event_provider.store(request.db);
				}
				@Override
				public void
				afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
					if (name_value_pairs.getBoolean("active") && name_value_pairs.getBoolean("calendar"))
						addEventProvider(name_value_pairs.getString("name"), id, name_value_pairs.getBoolean("calendar_public"), name_value_pairs.getString("calendar_access_policy"), request);
					super.afterInsert(id, name_value_pairs, request);
				}
				@Override
				public String
				beforeDelete(String where, Request request) {
					String calendar_name = "Group" + where.substring(3) + "Calendar"; // assuming where starts with id=
					Module event_provider = request.site.getModule(calendar_name);
					if (event_provider != null)
						event_provider.remove(request.site, request.db);
					return super.beforeDelete(where, request);
				}
				@Override
				public String
				beforeUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
					if (!name_value_pairs.getBoolean("active") || !name_value_pairs.getBoolean("calendar")) {
						Module event_provider = request.site.getModule("Group" + id + "Calendar");
						if (event_provider != null)
							event_provider.remove(request.site, request.db);
					}
					if (!name_value_pairs.getBoolean("active") && request.db.lookupBoolean(new Select("active").from("groups").whereIdEquals(id))) {
						request.db.delete("people_groups", "groups_id", id, false);
						if (m_jobs != null)
							for (String job : m_jobs)
								request.db.update("groups", "\"" + job + "\"=NULL", id);
						ArrayList<String> mail_list_ids = request.db.readValues(new Select("id").from("mail_lists").where("send_to='Group:" + id + "' AND active"));
						for (String mail_list_id : mail_list_ids)
							request.db.update("mail_lists", "active=false", "id=" + mail_list_id);
					}
					if (name_value_pairs.getBoolean("active") && name_value_pairs.getBoolean("calendar")) {
						GroupEventProvider event_provider = (GroupEventProvider)request.site.getModule("Group" + id + "Calendar");
						if (event_provider == null)
							addEventProvider(name_value_pairs.getString("name"), id, name_value_pairs.getBoolean("calendar_public"), name_value_pairs.getString("calendar_access_policy"), request);
						else {
							event_provider.setAccessPolicy("group members".equals(name_value_pairs.getString("calendar_access_policy")) ? new GroupAccessPolicy(id).add().delete().edit() : null);
							event_provider.setCalendarPublic(name_value_pairs.getBoolean("calendar_public"));
							event_provider.setDisplayName(name_value_pairs.getString("name"));
						}
					}
					return super.beforeUpdate(id, name_value_pairs, previous_values, request);
				}
			}
			.setAlternateRows(true)
			.setDefaultOrderBy("lower(name)")
			.setFrom("groups")
			.setRecordName(Text.singularize(getDisplayName(null)))
			.setColumn(new OptionsColumn("calendar_access_policy", "group members", "event poster").setDisplayName("who can edit events"))
			.setColumn(new Column("calendar_public").setDisplayName("show calendar to community"))
			.setColumn(new TextAreaColumn("mandate").setDisplayName(m_mandate_label).setIsRichText(true, false))
			.setColumn(new Column("name").setIsRequired(true))
			.setColumn(new RolesColumn("role", site))
			.setColumn(new Column("self_joining").setDisplayName("allow people to join/leave the group themselves"))
			.addRelationshipDef(new ManyToMany("edit groups people", "people_groups", "first,last"));
			if (Array.indexOf(m_items, "documents") != -1)
				view_def.addRelationshipDef(new OneToMany("edit documents"));
			if (Array.indexOf(m_items, "minutes") != -1)
				view_def.addRelationshipDef(new OneToMany("minutes"));
			if (Array.indexOf(m_items, "web sites") != -1)
				view_def.addRelationshipDef(new OneToMany("group_links"));
			if (m_allow_subgroups)
				view_def.setColumn(new LookupColumn("parent_group", "groups", "name").setAllowNoSelection(true));
			if (m_jobs != null)
				for (String job : m_jobs)
					view_def.setColumn(new PersonColumn(job, true, site.getPeopleFilter()).setAllowNoSelection(true));
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("name");
			column_names.add("active");
			if (m_allow_subgroups)
				column_names.add("parent_group");
			if (m_jobs != null)
				for (String job : m_jobs)
					column_names.add(job);
			view_def.setColumnNamesTable(column_names);
			column_names.add(2, "private");
			column_names.add(3, "self_joining");
			column_names.add("mandate");
			column_names.add("calendar");
			column_names.add("calendar_public");
			column_names.add("calendar_access_policy");
			column_names.add("role");
			view_def.setColumnNamesForm(column_names);
			return view_def;
		}
		if (name.equals("edit groups people"))
			return new ViewDef(name)
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete())
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setRecordName("Member")
				.setCenter(false)
				.setColumnNamesTable(new String[] { "person" })
				.setColumn(new Column("person").setValueRenderer(new MultiColumnRenderer(new String[] { "first", "last" }, true, false), false));
		if (name.equals("edit people groups"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy(){
					@Override
					public boolean
					showDeleteButtonForRow(View view, Request request) {
						return request.userIsAdmin() || view.data.getBoolean("self_joining");
					}
				}.add().delete())
				.setAddButtonText("join")
				.setDefaultOrderBy("lower(name)")
				.setDeleteButtonText("leave")
				.setFrom("groups")
				.setRecordName("Group")
				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "name" });
		if (name.equals("group_documents"))
			return site.getModule("Documents")._newViewDef("documents", site)
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT))
				.addSection(new Tabs("type", new OrderBy("type NULLS FIRST")).setNullValueLabel("Documents"))
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit())
				.setCenter(false)
				.setName(name)
				.setShowDoneLink(true)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setViewRole("admin"));
		if (name.equals("group_links"))
			return new ViewDef(name)
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit())
				.setCenter(false)
				.setDefaultOrderBy("title")
				.setRecordName("Web Site")
				.setShowDoneLink(true)
				.setTimestampRecords(true)
				.setColumnNamesTable(new String[] { "link" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"))
				.setColumn(new Column("link").setDisplayName("web site").setValueRenderer(new LinkValueRenderer().setHrefColumn("url").setTarget("_blank").setTextColumn("title"), false))
				.setColumn(new Column("_timestamp_").setDisplayName("added"));
		if (name.equals("group_lists"))
			return site.getModule("Lists")._newViewDef("lists", site)
				.setAccessPolicy(new AccessPolicy().view())
				.setCenter(false)
				.setName(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setViewRole("admin"));
		if (name.equals("group_minutes"))
			return site.getModule("Minutes")._newViewDef("minutes", site)
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit())
				.setCenter(false)
				.setName(name)
				.setShowDoneLink(true)
				.setViewClass(GroupMinutesView.class)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"));
		if (name.equals("groups")) {
			ViewDef view_def = new ViewDef(name)
				.setDefaultOrderBy("lower(name)")
				.setRecordName("Group")
				.setColumnNamesTable(new String[] { "name" })
				.setColumn(new Column("mandate").setDisplayName(m_mandate_label))
				.addRelationshipDef(new ManyToMany("groups people", "people_groups", "first,last").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM))
				.addRelationshipDef(new OneToMany("group_documents").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM))
				.addRelationshipDef(new OneToMany("group_minutes").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM))
				.addRelationshipDef(new OneToMany("group_links").setHideOnForms(Mode.ADD_FORM, Mode.EDIT_FORM, Mode.READ_ONLY_FORM));
			if (m_jobs != null)
				for (String job : m_jobs)
					view_def.setColumn(new PersonColumn(job, true, site.getPeopleFilter()));
			return view_def;
		}
		if (name.equals("groups people"))
			return new ViewDef(name)
				.setAccessPolicy(new GroupJobAccessPolicy(0).add().delete())
				.setCenter(false)
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setRecordName("Member")
				.setShowColumnHeads(false)
				.setShowDoneLink(true)
				.setColumnNamesTable(new String[] { "id" })
				.setColumn(new PersonColumn("id", true, site.getPeopleFilter()));
		if (name.equals("members"))
			return new ViewDef(name)
				.setAccessPolicy(new GroupJobAccessPolicy(0).add().delete())
				.setCenter(false)
				.setDefaultOrderBy("first,last")
				.setDeleteButtonText("remove")
				.setFrom("people_groups")
				.setRecordName("Member")
				.setSelectFrom("people_groups JOIN people ON people.id=people_id")
				.setShowColumnHeads(false)
				.setShowDoneLink(true)
				.setColumnNamesTable(new String[] { "people_id" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"))
				.setColumn(new PersonColumn("people_id", true, site.getPeopleFilter()).setDisplayName("person"));
		if (name.equals("people_groups"))
			return new ViewDef(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setFilter(new Filter(){
					@Override
					public boolean
					accept(Rows rows, Request request) {
						return rows.getBoolean("active") && rows.getBoolean("self_joining");
					}
				}).setDisplayName("group"))
				.setColumn(new LookupColumn("people_id", "people", "first,last").setFilter(site.getPeopleFilter()).setDisplayName("person"));
		if (name.equals("report groups"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setBaseFilter("active")
				.setDefaultOrderBy("lower(name)")
				.setFrom("groups")
				.setShowHead(false)
				.setCenter(false)
				.setColumnNamesTable(new String[] { "name", "people" })
				.setColumn(new Column("name").setDisplayName("group"))
				.setColumn(new Column("people"){
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						Set<String> people = new TreeSet<>();
						List<String[]> members = request.db.readRows(new Select("first,last").from("people_groups JOIN people ON people.id=people_groups.people_id").whereEquals("groups_id", view.data.getInt("id")).orderBy("first,last"));
						for (String[] person : members)
							people.add(Text.join(" ", person[0], person[1]));
						if (m_jobs != null)
							for (String job : m_jobs) {
								int id = view.data.getInt(job);
								if (id != 0) {
									String person = request.site.lookupName(id, request.db);
									if (person != null) {
										people.remove(person);
										people.add(person + " (" + job + ")");
									}
								}
							}
						request.writer.writeList(people, ", ");
						return true;
					}
				});
		if (name.equals("report people"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setBaseFilter("active")
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setShowHead(false)
				.setCenter(false)
				.setColumnNamesTable(new String[] { "person", "groups" })
				.setColumn(new Column("groups"){
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						String person_id = view.data.getString("id");
						StringBuilder on = new StringBuilder("groups.id=people_groups.groups_id");
						appendJobs(on, person_id);
						List<String> groups = request.db.readValues(new Select("name").distinct().from("people_groups JOIN groups ON " + on.toString()).where("groups.active AND people_id=" + person_id).orderBy("name"));
						boolean first = true;
						for (String group : groups) {
							if (first)
								first = false;
							else
								request.writer.write(", ");
							request.writer.write(group);
						}
						return true;
					}
				})
				.setColumn(new Column("person").setValueRenderer(new MultiColumnRenderer(new String[] { "first", "last" }, true, false), false));
		return null;
	}

	//--------------------------------------------------------------------------

	public void
	removePerson(int id, DBConnection db) {
		db.delete("people_groups", "people_id", id, false);
		if (m_jobs != null)
			for (String job : m_jobs)
				db.update("groups", "\"" + job + "\"=NULL", job + "=" + id);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request request) {
		super.updateSettings(request);
		adjustTables(request.db);
	}

	//--------------------------------------------------------------------------

	final void
	writeGroup(String[] group, List<String[]> groups, HTMLWriter writer) {
		String parent_id = group[0];
		writer.write("[").jsString(group[1]).comma().write("function(){document.location=context+'/Groups?group=" + group[0] + "'}");
		group[0] = null;
		if (m_allow_subgroups) {
			boolean first = true;
			for (String[] g : groups)
				if (parent_id.equals(g[2])) {
					if (first) {
						first = false;
						writer.write(",[");
					} else
						writer.comma();
					writeGroup(g, groups, writer);
				}
			if (!first)
				writer.write("]");
		}
		writer.write("]");
	}

	//--------------------------------------------------------------------------

	final void
	writeGroupPane(int group_id, Request request) {
		String name = request.db.lookupString(new Select("name").from("groups").whereIdEquals(group_id));
		if (name == null)
			return;
		HTMLWriter writer = request.writer;

		writer.write("<div class=\"container\"><div><h2 style=\"display:inline-block\">");
		writer.write(name);
		writer.write("</h2>");
		String[] email_list = request.db.readRow(new Select("id,name").from("mail_lists").where("send_to='Group:" + group_id + "' AND active"));
		if (m_show_email_link)
			if (email_list != null) {
				writer.setAttribute("style", "margin-left:20px;");
				String domain = request.site.getSettings().getString("mail list domain override");
				String address = domain == null ? request.site.getEmailAddress(email_list[1]) : email_list[1] + "@" + domain;
				writer.aOpen("mailto:" + address);
				writer.ui.icon("envelope").nbsp().write(address).tagClose();
			} else {
				Set<String> members = new TreeSet<>();
				Map<String,Integer> jobs_people = getJobsPeople(group_id, request.db);
				for (String job : jobs_people.keySet()) {
					String email = request.db.lookupString(new Select("email").from("people").whereIdEquals(jobs_people.get(job)));
					if (email != null)
						members.add(email);
				}
				Rows rows = new Rows(new Select("email").from("people").joinOn("people_groups", "(people.id=people_groups.people_id)").whereEquals("groups_id", group_id), request.db);
				while (rows.next()) {
					String email = rows.getString(1);
					if (email != null)
						members.add(email);
				}
				rows.close();
				if (members.size() > 0) {
					StringBuilder s = new StringBuilder();
					for (String member : members) {
						if (s.length() > 0)
							s.append(',');
						s.append(member);
					}
					writer.setAttribute("style", "margin-left:20px;");
					writer.aOpen("mailto:" + s.toString());
					writer.ui.icon("envelope").nbsp().write("Email Group Members").tagClose();
				}
			}
		writer.write("</div>");
		TaskPane task_pane = writer.ui.taskPane("Groups/" + group_id).selectFirstTask().setPills(true).setStacked(false).open();
		for (String item : m_items)
			switch(item) {
			case "calendar":
				if (request.db.lookupBoolean(new Select("calendar AND active").from("groups").whereIdEquals(group_id)))
					task_pane.task(item);
				break;
			case "email":
				if (email_list != null)
					task_pane.task(item, request.getContext() + "/MailLists/" + email_list[0] + "/archive");
				break;
			case "lists":
				if (request.db.exists(new Select("1").from("lists").whereEquals("groups_id", group_id)))
					task_pane.task(item);
				break;
			case "mandate":
				String mandate = request.db.lookupString(new Select("mandate").from("groups").whereIdEquals(group_id));
				if (mandate == null || mandate.length() == 0)
					continue;
				task_pane.task(m_mandate_label.toLowerCase(), request.getContext() + "/Groups/" + group_id + "/mandate");
				break;
			case "people":
				if (request.db.lookupBoolean(new Select("active").from("groups").whereIdEquals(group_id)))
					task_pane.task(item);
				break;
			default:
				task_pane.task(item);
			}
		task_pane.close();
		writer.write("</div>");
		request.setSessionInt("groups_id", group_id); // used by GroupAccessPolicy
	}

	//--------------------------------------------------------------------------

	private void
	writeJobs(int group_id, View groups, Request request) {
		if (m_jobs == null || m_jobs.length == 0)
			return;
		boolean self_joining = groups.data.getBoolean("self_joining");
		HTMLWriter writer = request.writer;
		int user_id = request.getUser().getId();
		writer.write("<div class=\"list_border_inner\" style=\"display:table;\"><table style=\"border-collapse:separate;border-spacing:0 10px\">");
		for (String job : m_jobs) {
			int person_id = groups.data.getInt(job);
			String job_label = job.replace('_', ' ');
			writer.write("<tr><td><span style=\"font-weight:bold\">").write(job_label.substring(0, 1).toUpperCase()).write(job_label.substring(1)).write("</span></td><td style=\"padding-left:20px\">");
			if (person_id == 0) {
				if (self_joining || request.userIsAdmin())
					writer.ui.buttonOnClick("sign up as " + job_label, actionURL(group_id, "join/" + job));
				else
					writer.write("nobody");
			} else {
				groups.writeColumnHTML(job);
				if (person_id == user_id && (self_joining || request.userIsAdmin()))
					writer.nbsp().nbsp().nbsp().ui.buttonOnClick("remove me as " + job_label, actionURL(group_id, "leave/" + job));
			}
			writer.write("</td></tr>");
		}
		writer.write("</table></div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeMembers(int group_id, View groups, Request request) {
		boolean self_joining = groups.data.getBoolean("self_joining") && !request.userIsGuest();
		HTMLWriter writer = request.writer;

		if (self_joining)
			writer.write("<div style=\"display:table;\">");
		ViewState.setBaseFilter("members", "groups_id=" + group_id, request);
		request.site.newView("members", request).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
		if (self_joining) {
			writer.write("<hr style=\"margin:2px 0;\" />");
			if (request.db.rowExists("people_groups", "people_id=" + request.getUser().getId() + " AND groups_id=" + group_id))
				writer.ui.buttonOnClick("leave group", actionURL(group_id, "leave"));
			else if (!isMember(group_id, request.getUser().getId(), request.db))
				writer.ui.buttonOnClick("join group", actionURL(group_id, "join"));
			writer.write("</div>");
		}
	}
}