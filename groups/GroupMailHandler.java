package groups;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import app.Request;
import app.Site;
import app.Text;
import db.DBConnection;
import db.Rows;
import db.Select;
import email.MailHandler;

public class GroupMailHandler extends MailHandler {
	private int m_id;

	//--------------------------------------------------------------------------

	public
	GroupMailHandler(int id, DBConnection db) {
		super((db.lookupBoolean(new Select("active").from("groups").whereIdEquals(id)) ? "Group:" : "Inactive Group:") + db.lookupString(new Select("name").from("groups").whereIdEquals(id)));
		m_id = id;
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	getDisplayName() {
		return m_display_name;
	}

	//--------------------------------------------------------------------------

	@Override
	protected ArrayList<InternetAddress>
	getRecipients(int list_id, Site site, DBConnection db) {
		ArrayList<InternetAddress> recipients = new ArrayList<>();
		Groups groups = (Groups)site.getModule("Groups");
		Set<String> email_addresses = new HashSet<>();
		Map<String,Integer> jobs_people = groups.getJobsPeople(m_id, db);
		for (String job : jobs_people.keySet()) {
			String email_address = db.lookupString(new Select("email").from("people").whereIdEquals(jobs_people.get(job)));
			if (email_address != null)
				email_addresses.add(email_address);
		}
		List<String> addresses = db.readValues(new Select("email").from("people JOIN people_groups ON (people.id=people_groups.people_id)").where("email IS NOT NULL AND groups_id=" + m_id + " AND NOT EXISTS(SELECT 1 FROM mail_lists_digest WHERE mail_lists_digest.people_id=people.id AND mail_lists_digest.mail_lists_id=" + list_id + ")"));
		for (String address : addresses)
			email_addresses.add(address);
		for (String email_address : email_addresses)
			try {
				recipients.add(new InternetAddress(email_address));
			} catch (AddressException e) {
				site.log(e);
			}
		return recipients;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isSubscribed(int person_id, Request request) {
		if (person_id == 0)
			return false;
		return ((Groups)request.site.getModule("Groups")).isMember(m_id, person_id, request.db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeSubscribers(Request request) {
		Set<String> subscribers = new TreeSet<>();
		Groups groups = (Groups)request.site.getModule("Groups");
		Map<String,Integer> jobs_people = groups.getJobsPeople(m_id, request.db);
		for (String job : jobs_people.keySet()) {
			String name = request.db.lookupString(new Select("first,last").from("people").whereIdEquals(jobs_people.get(job)));
			if (name != null)
				subscribers.add(name);
		}
		Rows rows = new Rows(new Select("first,last").from("people").joinOn("people_groups", "people.id=people_groups.people_id").whereEquals("groups_id", m_id).orderBy("first,last"), request.db);
		while (rows.next())
			subscribers.add(Text.join(" ", rows.getString(1), rows.getString(2)));
		rows.close();
		boolean first = true;
		for (String subscriber : subscribers) {
			if (first)
				first = false;
			else
				request.writer.br();
			request.writer.write(subscriber);
		}
		return !first;
	}
}
