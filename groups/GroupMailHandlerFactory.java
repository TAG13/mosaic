package groups;

import java.util.List;

import db.DBConnection;
import db.Rows;
import db.Select;
import email.MailHandler;
import email.MailHandlerFactory;
import ui.Option;

public class GroupMailHandlerFactory implements MailHandlerFactory {
	@Override
	public void
	addSendToOptions(List<Option> options, DBConnection db) {
		Rows rows = new Rows(new Select("id,name").from("groups").where("active").orderBy("name"), db);
		while (rows.next())
			options.add(new Option("Group:" + rows.getString(2), "Group:" + rows.getString(1)));
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public MailHandler
	getHandler(String send_to, DBConnection db) {
		if (send_to.startsWith("Group:"))
			return new GroupMailHandler(Integer.parseInt(send_to.substring(6)), db);
		return null;
	}
}
