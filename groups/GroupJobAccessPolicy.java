package groups;

import app.Request;
import mosaic.Person;

public class GroupJobAccessPolicy extends GroupAccessPolicy {
	public
	GroupJobAccessPolicy(int group_id) {
		super(group_id);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	isMember(Request request) {
		int group_id = m_group_id;
		if (group_id == 0)
			group_id = request.getSessionInt("groups_id", 0);
		if (group_id == 0)
			return false;
		Person user = (Person)request.getUser();
		if (user == null)
			return false;
		Groups groups = (Groups)request.site.getModule("Groups");
		if (groups.hasJob(group_id, user.getId(), request.db))
			return true;
		return false;
	}
}
