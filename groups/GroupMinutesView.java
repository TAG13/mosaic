package groups;

import app.Request;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.feature.TSVectorSearch;
import minutes.MinutesView;

public class GroupMinutesView extends MinutesView {
	int		m_current_group = -1;
	boolean	m_show_group_headers;

	//--------------------------------------------------------------------------

	public
	GroupMinutesView(ViewDef view_def, Request request) {
		super(view_def, request);
		ViewState state = (ViewState)request.getOrCreateState(ViewState.class, view_def.getName());
		String base_filter = state.getBaseFilter();
		if (base_filter != null)
			m_show_group_headers = base_filter.startsWith("show_on_main_minutes_page");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeRow(String[] columns) {
		if (m_mode != Mode.READ_ONLY_LIST) {
			super.writeRow(columns);
			return;
		}
		if (m_show_group_headers) {
			int groups_id = data.getInt("groups_id");
			if (groups_id != m_current_group) {
				m_writer.write("<div class=\"").write(m_writer.ui.div_primary).write("\" style=\"margin-bottom:0;padding:5px 10px;\">");
				if (groups_id == 0) {
					String title = m_request.site.getSettings().getString("main meeting");
					if (title == null)
						title = "Business Meeting";
					m_writer.write(title);
				} else
					m_writer.write(m_request.db.lookupString(new Select("name").from("groups").whereIdEquals(groups_id)));
				m_writer.write("</div>");
				m_current_group = groups_id;
			}
		}
		super.writeRow(columns);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write() {
		if (!m_request_processed) {
			processRequest(this, m_request).write();
			return;
		}
		if (getMode() != Mode.READ_ONLY_LIST) {
			super.write();
			return;
		}
		m_writer.write("<div style=\"display:table;\"><div style=\"text-align:right;\">");
		TSVectorSearch search = new TSVectorSearch(null, null);
		search.init(m_view_def);
		String quicksearch = getState().getQuicksearch();
		if (quicksearch != null && quicksearch.length() > 0) {
			m_request.writer.write("minutes");
			search.writeRowWindowControlsSpan(this, m_request);
		} else
			search.write(null, this, m_request);
		m_writer.write("</div>");
		super.write();
		m_writer.write("</div>");
	}
}
