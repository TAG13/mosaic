package groups;

import app.Request;
import db.Select;
import db.View;
import db.access.AccessPolicy;
import mosaic.Person;

public class GroupAccessPolicy extends AccessPolicy {
	protected int	m_group_id;

	//--------------------------------------------------------------------------

	public
	GroupAccessPolicy(int group_id) {
		m_group_id = group_id;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request request) {
		if (!m_delete)
			return false;
		if (m_group_id == 0) {
			String role = getRole(request);
			if (role != null)
				return request.userHasRole(role);
		}
		return isMember(request) || request.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request request) {
		if (!m_edit)
			return false;
		if (m_group_id == 0) {
			String role = getRole(request);
			if (role != null)
				return request.userHasRole(role);
		}
		return isMember(request) || request.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	private String
	getRole(Request request) {
		return request.db.lookupString(new Select("role").from("groups").whereIdEquals(request.getSessionInt("groups_id", 0)));
	}

	//--------------------------------------------------------------------------

	protected boolean
	isMember(Request request) {
		int group_id = m_group_id;
		if (group_id == 0)
			group_id = request.getSessionInt("groups_id", 0);
		if (group_id == 0)
			return false;
		Person user = (Person)request.getUser();
		if (user == null)
			return false;
		Groups groups = (Groups)request.site.getModule("Groups");
		if (groups.isMember(group_id, user.getId(), request.db))
			return true;
		return groups.isMemberOfSubgroup(group_id, user.getId(), request.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View view, Request request) {
		if (!m_add)
			return false;
		if (m_group_id == 0) {
			String role = getRole(request);
			if (role != null)
				return request.userHasRole(role);
		}
		return isMember(request) || request.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(View view, Request request) {
		if (!m_delete)
			return false;
		if (m_group_id == 0) {
			String role = getRole(request);
			if (role != null)
				return request.userHasRole(role);
		}
		return isMember(request) || request.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(View view, Request request) {
		if (!m_edit)
			return false;
		if (m_group_id == 0) {
			String role = getRole(request);
			if (role != null)
				return request.userHasRole(role);
		}
		return isMember(request) || request.userHasRole("groups");
	}
}
