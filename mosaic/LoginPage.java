package mosaic;

import java.io.File;
import java.lang.reflect.Field;

import org.apache.commons.fileupload.FileItem;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Module;
import app.Request;
import app.Time;
import calendar.CalendarView;
import calendar.CalendarView.View;
import calendar.EventProvider;
import calendar.ListView;
import calendar.MonthView;
import db.object.JSONField;
import ui.Select;
import web.HTMLWriter;

public class LoginPage extends Module {
	@JSONField(label="null", type="file")
	private String 				m_banner = "banner.png";
	private JsonObject	m_page_template = Json.parse("{\"blocks\":["
			+ "{\"type\":\"banner\"},"
			+ "{\"type\":\"blackboard\"},"
			+ "{\"type\":\"html\"},"
			+ "{\"type\":\"login form\"},"
			+ "{\"type\":\"upcoming\"},"
			+ "{\"type\":\"pictures\"}"
			+ "]}").asObject();
	@JSONField(choices = { "random", "recent" }, label="null")
	private String				m_pictures = "random";
	@JSONField(fields = {"m_banner"})
	private boolean				m_show_banner = true;
	@JSONField
	private boolean				m_show_blackboard;
	@JSONField(fields = {"m_pictures"})
	private boolean				m_show_pictures = true;
	@JSONField
	private boolean				m_show_remember_me_checkbox = true;
	@JSONField(fields = {"m_text_box"})
	private boolean				m_show_text_box;
	@JSONField(fields = {"m_upcoming_mode"})
	private boolean				m_show_upcoming = true;
	@JSONField(label="null", type="html")
	private String				m_text_box;
	@JSONField(admin_only=true)
	private boolean				m_upcoming_all_calendars;
	@JSONField
	private CalendarView.View	m_upcoming_mode = CalendarView.View.LIST;

	// --------------------------------------------------------------------------

	public
	LoginPage() {
		m_renameable = false;
		m_required = true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		writePage(request);
		return true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if ("Banner".equals(segment_one)) {
			FileItem file_item = request.getFileItem("file");
			String filename = file_item.getName();
			int index = filename.lastIndexOf('/');
			if (index != -1)
				filename = filename.substring(index + 1);
			index = filename.lastIndexOf('\\');
			if (index != -1)
				filename = filename.substring(index + 1);
			String path = request.site.getBasePathBuilder().append("images").append(filename).toString();
			try {
				file_item.write(new File(path));
			} catch (Exception e) {
				request.site.log(e);
			}
			m_banner = filename;
			store(request.db);
			request.writer.write("dom.$('banner').value='" + filename + "';dom.$('banner_img').src=dom.$('banner').value;");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks() {
		return new String[][] { { "Login Page", "/LoginPage/settings" } };
	}

	//--------------------------------------------------------------------------

	public boolean
	showBlackboard() {
		return m_show_blackboard;
	}

	//--------------------------------------------------------------------------

	protected boolean
	showRememberMeCheckbox() {
		return m_show_remember_me_checkbox;
	}

	//--------------------------------------------------------------------------

	private void
	writePage(Request request) {
		HTMLWriter writer = request.writer;
		writer.setAttribute("class", "login");
		request.site.newHead(request).close();
		writer.write("<div id=\"top\" style=\"height:2em;\"></div>");
		writer.js("if(dom.$('top').parentNode.tagName!='BODY')document.location=document.location;");
		writer.write("<div class=\"container\"><div class=\"row\">");
		JsonArray blocks = m_page_template.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			switch(block.getString("type", null)) {
			case "banner":
				if (m_show_banner && m_banner != null)
					writer.write("<div style=\"padding:10px;text-align:center;width:100%;\"><img src=\"").write(request.getContext()).write("/images/" + m_banner + "\" /></div>");
				break;
			case "blackboard":
				if (m_show_blackboard) {
					Blackboard blackboard = (Blackboard)request.site.getModule("Blackboard");
					if (blackboard != null && blackboard.isActive())
						blackboard.writeComponent(request);
				}
				break;
			case "html":
				if (m_show_text_box)
					writer.write("<div class=\"alert alert-secondary\" style=\"display:table;margin:10px auto;\">").write(m_text_box).write("</div></div><div class=\"row\">");
				break;
			case "login form":
				request.site.writeLoginForm(request);
				writer.br();
				break;
			case "pictures":
				if (m_show_pictures) {
					writer.write("<div class=\"col-md-6\">");
					new SlideShow().write(m_pictures, false, request);
					writer.write("</div>");
				}
				break;
			case "upcoming":
				if (m_show_upcoming) {
					writer.write("<div id=\"upcoming\" class=\"col-md-6\"><style>#upcoming > div {margin: 0 auto;}#upcoming > div > table {padding:0}</style>");
					request.setData("upcoming", Boolean.TRUE);
					if (m_upcoming_mode == View.MONTH)
						writer.write("<div style=\"text-align:center;\"><h3>").write(Time.formatter.getMonthNameLong(Time.newDate())).write("</h3></div>");
					EventProvider event_provider = (EventProvider)request.site.getModule("Main Calendar");
					if (event_provider != null)
						(m_upcoming_mode == View.MONTH ? new MonthView(event_provider, request) : new ListView(event_provider, request).setAllCalendars(m_upcoming_all_calendars)).writeComponent(false, request);
					else
						request.log("Main Calendar module not found", false);
					writer.write("</div>");
					request.setData("upcoming", null);
				}
				break;
			}
		}
		writer.write("</div></div>");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request request) {
		String field_name = field.getName();
		if ("m_banner".equals(field_name))
			request.writer.setAttribute("id", "banner_img").setAttribute("style", "max-height:100px").img(m_banner).space().aOnClick("replace", "Dialog.upload(context+'/LoginPage/Banner')");
		else if ("m_upcoming_mode".equals(field_name))
			new Select("upcoming_mode", CalendarView.View.class).setSelectedOption(m_upcoming_mode.toString(), null).write(request);
		else
			super.writeSettingsInput(field, request);
	}
}
