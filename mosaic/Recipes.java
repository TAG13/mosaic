package mosaic;

import java.io.IOException;
import java.sql.Types;

import app.Request;
import app.Site;
import db.DBConnection;
import db.OrderBy;
import db.Select;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import social.NewsProvider;

public class Recipes extends NewsProvider {
	public Recipes() {
		super("recipes", "recipe", "posted a recipe");
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Simple recipes list.";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("recipes")
			.add(new JDBCColumn("category", Types.VARCHAR))
			.add(new JDBCColumn("directions", Types.VARCHAR))
			.add(new JDBCColumn("ingredients", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", Types.INTEGER))
			.add(new JDBCColumn("title", Types.VARCHAR));
		addColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("recipes"))
			return addHooks(new ViewDef(name)
				.addSection(new Tabs("category", new OrderBy("category")))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit().view())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Recipe")
				.setColumnNamesForm(new String[] { "title", "ingredients", "directions", "category", "_owner_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(new Column("category").setShowPreviousValuesDropdown(true))
				.setColumn(new TextAreaColumn("directions").setIsExpanding(true))
				.setColumn(new TextAreaColumn("ingredients").setIsExpanding(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true)));
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	@Override
	public void
	writeAddButton(Request request) {
		writeAddButton("journal-text", "recipes", request);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, Request request) {
		request.writer.write("<a onclick=\"new Dialog({cancel:true,ok:{text:'done',click:function(){Dialog.top().close()}},cancel:false,title:'View Recipe',url:context+'/Views/recipes?db_key_value=")
			.write(item_id).write("&amp;db_mode=READ_ONLY_FORM',owner:C._(this),width:'auto'});return false;\" href=\"#\">")
			.write(request.db.lookupString(new Select("title").from("recipes").whereIdEquals(item_id)))
			.write("</a>");
	}
}
