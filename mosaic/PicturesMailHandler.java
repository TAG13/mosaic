package mosaic;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;

import db.DBConnection;
import email.MailHandler;
import web.Images;

public class PicturesMailHandler extends MailHandler {
	private String m_base_file_path;

	//--------------------------------------------------------------------------

	public
	PicturesMailHandler(String base_file_path) {
		super("Pictures");
		m_base_file_path = base_file_path;
	}

	//--------------------------------------------------------------------------

	private void
	addPicture(String filename, String subject, InputStream input_stream, int people_id, DBConnection db) {
		int id = db.insert("pictures", null, null);
		String new_filename = id + filename.substring(filename.lastIndexOf('.'));
		db.update("pictures", "file='" + new_filename +"',_owner_=" + people_id, id);
		try {
			String pictures_dir = m_base_file_path + "/pictures/";
			FileOutputStream fos = new FileOutputStream(pictures_dir + new_filename);
			int b = input_stream.read();
			while (b != -1) {
				fos.write(b);
				b = input_stream.read();
			}
			fos.close();
			Images.makeThumb(Images.load(pictures_dir + new_filename), pictures_dir, new_filename, 150, true);
			if (subject != null && subject.length() > 0)
				db.insert("pictures_pictures_tags", "pictures_id,pictures_tags_id", id + "," + getTagID(subject, db));
			db.insert("news", "provider,item_id,people_id", "'pictures'," + id + "," + people_id);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	private int
	getTagID(String tag, DBConnection db) {
		tag = DBConnection.string(tag);
		int id = db.lookupInt("id", "pictures_tags", "tag=" + tag, -1);
		if (id == -1)
			id = db.insert("pictures_tags", "tag", tag);
		return id;
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	handleMessage(Message message, DBConnection db) {
		try {
			int people_id = db.lookupInt("id", "people", "email ILIKE '" + DBConnection.escape(((InternetAddress)message.getFrom()[0]).getAddress()) + "%'", -1);
			if (people_id != -1)
				parsePart(message, people_id, message.getSubject(), db);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return true;
	}

	//--------------------------------------------------------------------------

	private void
	parsePart(Part part, int people_id, String subject, DBConnection db) {
		try {
			String type = part.getContentType().toLowerCase();
			if (type.startsWith("multipart")) {
				Multipart multipart = (Multipart)part.getContent();
				for (int i=0,n=multipart.getCount(); i<n; i++)
					parsePart(multipart.getBodyPart(i), people_id, subject, db);
			} else if (type.startsWith("image"))
				addPicture(part.getFileName(), subject, part.getInputStream(), people_id, db);
		} catch (IOException | MessagingException e) {
			throw new RuntimeException("PicturesMailHandler.parsePart", e);
		}
	}
}
