package mosaic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.PriorityQueue;

import app.Request;
import app.Text;
import calendar.Event;
import db.Rows;
import db.Select;
import db.access.AccessPolicy;
import db.object.JSONField;

public class BirthdaysEventProvider extends MosaicEventProvider {
	@JSONField private boolean m_show_on_login_page = true;

	//--------------------------------------------------------------------------

	public
	BirthdaysEventProvider() {
		super("Birthdays");
		m_check_overlaps = false;
		m_events_table = null;
		m_show_on_menu = true;
		setAccessPolicy(new AccessPolicy());
		setColor("#FF851B");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAllEvents(ArrayList<Event> events, Request request) {
		ResultSet rs = request.db.select(new Select("first,last,birthday").from("people").where("active AND show_birthday_on_calendar AND birthday IS NOT NULL"));
		try {
			while (rs.next())
				events.add(new Event(this, rs.getDate(3).toLocalDate(), Text.join(" ", "Birthday:", rs.getString(1), rs.getString(2))).setRepeat("every year"));
			rs.getStatement().close();
		} catch (SQLException e) {
			request.abort(e);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, String category, String location, Request request) {
		if (category != null && !category.equals("birthdays"))
			return;
		if (location != null)
			return;
		if (!m_show_on_login_page && request.getUser() == null)
			return;
		to = to.minusDays(1);
		String from_date = from.toString();
		String to_date = to.toString();
		int from_year = from.getYear();
		int to_year = to.getYear();
		String where = "active AND birthday IS NOT NULL AND show_birthday_on_calendar AND (birthday + (" + from_year + " - date_part('year',birthday)) * interval '1 year' BETWEEN '" + from_date + "' AND '" + to_date + "' OR birthday + (" + to_year + " - date_part('year',birthday)) * interval '1 year' BETWEEN '" + from_date + "' AND '" + to_date + "')";
		Rows rows = new Rows(new Select("first,last,birthday").from("people").where(where), request.db);
		while (rows.next()) {
			LocalDate date = rows.getDate(3);
			int month = date.getMonthValue();
			LocalDate date_with_from_year = date.withYear(from_year);
			date = date_with_from_year.isBefore(from) ? date.withYear(to_year) : date_with_from_year;
			if (date.getMonthValue() != month) // February 29th changed to March 1st
				date = date.minusDays(1);
			events.add(new Event(this, date, /*"<span class=\"calendar_time\">Birthday</span>" + */ Text.join(" ", rows.getString(1), rows.getString(2))));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate c, String category, String location, boolean all_calendars, Request request) {
		Rows rows = new Rows(new Select("first,last,birthday").from("people").where("active AND birthday IS NOT NULL AND show_birthday_on_calendar"), request.db);
		while (rows.next()) {
			LocalDate c2 = rows.getDate(3).withYear(c.getYear());
			int month = c2.getMonthValue();
			if (c2.isBefore(c))
				c2 = c2.plusYears(1);
			if (c2.getMonthValue() != month) // February 29th changed to March 1st
				c2 = c2.minusDays(1);
			events.add(new Event(this, c2, "<span class=\"calendar_time\">Birthday</span>" + Text.join(" ", rows.getString(1), rows.getString(2))));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate c, Request request) {
		return (m_show_on_login_page || request.getUser() != null) && request.db.exists("people", "birthday IS NOT NULL AND show_birthday_on_calendar") ? target_max : 0;
	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request request) {
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(Request request) {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, Request request) {
		super.writeSettingsForm(new String[] { "m_active", "m_color", "m_display_name", "m_show_on_login_page", "m_show_on_menu", "m_show_on_upcoming", "m_uuid" }, in_dialog, request);
	}
}
