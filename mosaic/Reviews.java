package mosaic;

import java.sql.Types;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Options;
import db.OrderBy;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import social.NewsProvider;

public class Reviews extends NewsProvider {
	private Options	m_categories;

	// --------------------------------------------------------------------------

	public
	Reviews() {
		super("reviews", "review", "posted a review");
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Share reviews of anything, including movies, books, plumbers, dentists, etc.";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("reviews")
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("categories_id", Types.INTEGER))
			.add(new JDBCColumn("contact", Types.VARCHAR))
			.add(new JDBCColumn("review", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		addColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createTable("reviews_categories", "text VARCHAR");
		m_categories = new Options(new Select("*").from("reviews_categories").orderBy("text"), m_site).setAllowEditing(true);
		m_site.addObjects(m_categories);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("reviews"))
			return addHooks(new ViewDef(name)
				.addSection(new Tabs("categories_id", new OrderBy("categories_id")))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit().view())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Review")
				.setColumnNamesForm(m_add_new_items_to_news_feed ? new String[] { "title", "categories_id", "review", "show_on_news_feed" } : new String[] { "title", "categories_id", "review" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(((Options)site.getObjects("reviews_categories")).newColumn("categories_id").setDisplayName("category"))
				.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM)));
		if (name.equals("reviews_categories"))
			return m_categories.newViewDef(name, site)
				.setRecordName("Category")
				.setColumn(new Column("text").setDisplayName("category").setIsRequired(true));
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, Request request) {
		String[] row = request.db.readRow(new Select("title,review").from("reviews").whereIdEquals(item_id));
		request.writer.h5(row[0]);
		request.writer.tag("p", row[1]);
	}
}
