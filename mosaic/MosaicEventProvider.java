package mosaic;

import app.Request;
import calendar.EventProvider;
import db.object.JSONField;

public class MosaicEventProvider extends EventProvider {
	@JSONField private boolean m_show_on_upcoming;

	//--------------------------------------------------------------------------

	public
	MosaicEventProvider(String name) {
		super(name);
		setShowOnUpcoming(true);
		setShowOnMenu(true);
	}

	//--------------------------------------------------------------------------

	public MosaicEventProvider
	setShowOnUpcoming(boolean show_on_upcoming) {
		m_show_on_upcoming = show_on_upcoming;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnMenu(Request request) {
		if (request.getData("upcoming") != null)
			return m_active && m_show_on_upcoming;
		return m_active && m_show_on_menu;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeControlButtons(Request request) {
		Person user = (Person)request.getUser();
		if (user != null) {
			request.writer.setAttribute("title", "subscribe to this calendar");
			request.writer.ui.buttonOnClick("ics", "Calendar.$().ics_dialog('" + request.site.getDomain() + "'," + user.getId() + ",'" + user.getUUID(request.db) + "')");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, Request request) {
		if (column_names == null)
			column_names = new String[] { "m_active", "m_check_overlaps", "m_color", "m_display_items", "m_end_date_label", "m_event_label", "m_events_can_repeat", "m_events_have_category", "m_events_have_color", "m_events_have_location", "m_events_have_start_time", "m_display_name", "m_ical_items", "m_reminder_subject_items", "m_role", "m_show_on_menu", "m_show_on_upcoming", "m_start_date_label", "m_support_attachments", "m_support_registrations", "m_support_reminders", "m_tooltip_items", "m_uuid" };
		super.writeSettingsForm(column_names, in_dialog, request);
	}
}
