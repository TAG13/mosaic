package mosaic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.Attachments;
import app.ClassTask;
import app.Mail;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import app.Time;
import app.pages.Page;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Reorderable;
import db.Rows;
import db.Select;
import db.Task;
import db.View;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Tabs;
import ui.Select.Type;
import ui.Table;
import ui.TaskPane;
import web.HTMLWriter;

public class Decisions extends SiteModule {
	@JSONField
	private String			m_agenda_head = "The agenda for the meeting is to address the following items in order as time allows:";
	private Attachments		m_attachments = new Attachments("decisions");
	@JSONField
	String					m_meetings_name = "General Meetings";
	@JSONField
	private String			m_parking_lot_name = "Parking Lot";
	@JSONField
	boolean					m_proposals_need_approval;
	@JSONField
	private String			m_proposals_pane_head;
	@JSONField(type="role")
	private String			m_proposals_role;
	@JSONField(admin_only=true)
	String[]				m_proposal_types;
	@JSONField
	boolean					m_send_agenda_email;
	@JSONField(type="mail list")
	String					m_send_announcements_to = "announce";
	private OptionsColumn	m_status_column;
	@JSONField
	private String[]		m_status_options = new String[] { "completed (no decision)", "did not pass", "dropped", "in process", "online vote", "owner vote", "passed", "pending", "tabled", "withdrawn" };
	@JSONField(fields={"m_meetings_name"})
	boolean					m_support_meetings;
	@JSONField(fields={"m_proposals_need_approval","m_proposals_pane_head","m_proposals_role"})
	boolean					m_support_proposals;
	@JSONField
	boolean					m_support_voting;
	@JSONField
	boolean					m_system_is_opt_in;
	private TextAreaColumn	m_text_column = new TextAreaColumn("text").setIsRichText(true, false);
	private Column			m_type_column = new Column("type").setViewRole("administrator");
	private Column			m_vote_totals_column;

	//--------------------------------------------------------------------------

	public
	Decisions() {
		m_status_column = new OptionsColumn("status", m_status_options).setDefaultValue("in process").setEditRole("decisions").setViewRole("decisions");
		m_vote_totals_column = new Column("vote_totals"){
			@Override
			public boolean
			writeValue(View view, Map<String,Object> data, Request request) {
				ResultSet rs = request.db.select(new Select("vote,COUNT(*)").from("votes").andWhere("votes.decisions_id=" + view.data.getString("id")).groupBy("vote").orderBy("vote DESC"));
				boolean first = true;
				try {
					while (rs.next()) {
						String vote = rs.getString(1);
						int num_votes = rs.getInt(2);
						if (first)
							first = false;
						else
							request.writer.write(", ");
						request.writer.write(vote).write(": ").write(num_votes);
					}
					rs.getStatement().close();
					if (first)
						return false;
				} catch (SQLException e) {
					request.abort(e);
				}
				return true;
			}
		}.setIsReadOnly(true);
	}

	//--------------------------------------------------------------------------

	private static int[]
	countVotes(int decisions_id, DBConnection db) {
		ResultSet counts = db.select("SELECT vote,COUNT(*) FROM votes WHERE decisions_id=" + decisions_id + " GROUP BY vote ORDER BY vote DESC");
		int[] votes = new int[3];
		try {
			while (counts.next()) {
				String vote = counts.getString(1);
				int count = counts.getInt(2);
				if (vote.equals("yes"))
					votes[0] = count;
				else if (vote.equals("no"))
					votes[1] = count;
				else
					votes[2] = count;
			}
			counts.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return votes;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if (segment_one == null)
			return false;
		request.writer.setAttribute("style", "margin-top:0").h2(segment_one);
		switch (segment_one) {
		case "Decisions":
			request.site.newView("decisions", request).writeComponent();
			return true;
		case "General Meetings":
			request.site.newView("general_meetings", request).writeComponent();
			return true;
		case "Old Proposals":
			request.site.newView("old proposals", request).writeComponent();
			return true;
		case "Opted In Members":
			writeOptedInMembersPane(request);
			return true;
		case "Proposals":
			writeProposalsPane(request);
			return true;
		case "Votes":
			String vote = request.getParameter("vote");
			if (vote != null) {
				NameValuePairs name_value_pairs = new NameValuePairs();
				String decision_id = request.getParameter("decision");
				name_value_pairs.set("decisions_id", decision_id);
				boolean owner_vote = request.db.lookupString(new Select("status").from("decisions").whereIdEquals(decision_id)).equals("owner vote");
				if (owner_vote)
					name_value_pairs.set("_owner_", ((mosaic.Person)request.getUser()).getHousehold(request).getId());
				else
					name_value_pairs.set("_owner_", request.getUser().getId());
				String where = name_value_pairs.getWhereString("votes", request.db);
				name_value_pairs.set("vote", vote);
				request.db.updateOrInsert("votes", name_value_pairs, where);
			}
			writeVotesPane(request);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		String action = request.getParameter("action");
		if (action == null)
			return false;
		int decisions_id = request.getInt("decisions_id", 0);
		int people_id = request.getUser().getId();
		switch (action) {
		case "agree":
			int id = request.db.lookupInt("id", "decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + people_id, -1);
			if (id == -1) {
				request.db.insert("decisions_agree", "decisions_id,people_id", decisions_id + "," + people_id);
				if (request.db.countRows("decisions_agree", "decisions_id=" + decisions_id) >= getQuorum(request.db))
					request.db.update("decisions", "status='in process'", decisions_id);
			}
			return true;
		case "close vote":
			int[] votes = countVotes(decisions_id, request.db);
			if (request.db.lookupString(new Select("status").from("decisions").whereIdEquals(decisions_id)).equals("owner vote")) {
				request.db.update("decisions", "type='owner vote'", decisions_id);
				int owner_households = request.db.countRows(new Select("COUNT(*)").from("families join people on people.families_id=families.id").where("participant AND people.active AND owner").groupBy("families.id"));
				if (votes[0] + votes[1] >= owner_households * 0.25 && votes[0] >= votes[1] * 2)
					request.db.update("decisions", "status='passed'", decisions_id);
				else
					request.db.update("decisions", "status='did not pass'", decisions_id);
			} else if (votes[0] + votes[1] >= getQuorum(request.db) && votes[0] >= votes[1] * 2)
				request.db.update("decisions", "status='passed'", decisions_id);
			else
				request.db.update("decisions", "status='did not pass'", decisions_id);
			return true;
		case "don't agree":
			request.db.delete("decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + people_id, false);
			if (request.db.countRows("decisions_agree", "decisions_id=" + decisions_id) < getQuorum(request.db))
				request.db.update("decisions", "status='pending'", decisions_id);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	private String
	getAgenda(Site site, DBConnection db) {
		Map<String,Integer> map = new HashMap<>();
		boolean annual_meeting = false;
		boolean budget_meeting = false;
		ResultSet rs = db.select(new Select("annual_meeting,budget_meeting").from("general_meetings").where("date>='" + Time.newDate().toString() + "'").orderBy("date").limit(1));
		try {
			if (rs.next()) {
				annual_meeting = rs.getBoolean(1);
				budget_meeting = rs.getBoolean(2);
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			System.out.println(e);
		}

		Select query = new Select("title,text,_owner_").from("decisions").where("status='in process'").orderBy("_timestamp_");
		if (m_proposal_types != null && m_proposal_types.length > 1)
			query.andWhere("type='Regular'");
		List<String[]> proposals = db.readRows(query);
		int num_proposals = proposals.size();
		List<String[]> ci_proposals = null;
		if (annual_meeting) {
			ci_proposals = db.readRows(new Select("title,text,_owner_").from("decisions").where(m_proposals_need_approval ? "status='pending' AND type='Capital Improvement'" : "status='in process' AND type='Capital Improvement'").orderBy("_timestamp_"));
			num_proposals += ci_proposals.size();
		}
		if (num_proposals == 0 && !annual_meeting && !budget_meeting)
			return null;

		query = new Select("title,people_id").from("decisions").join("decisions", "decisions_rank").where("status='in process'").orderBy("people_id,_order_ NULLS LAST,date");
		if (m_proposal_types != null && m_proposal_types.length > 1)
			query.andWhere("type='Regular'");
		rs = db.select(query);
		for (String[] proposal : proposals)
			map.put(proposal[0], new Integer(0));
		try {
			int previous_people_id = 0;
			int row = 0;
			while (rs.next()) {
				int people_id = rs.getInt(2);
				if (people_id != previous_people_id) {
					row = 0;
					previous_people_id = people_id;
				}
				String title = rs.getString(1);
				map.put(title, map.get(title) + proposals.size() - row);
				row++;
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		TreeMap<Integer,List<String>> ordered = new TreeMap<>();
		for (String title : map.keySet()) {
			Integer i = map.get(title);
			if (i > 0) {
				if (ordered.get(i) == null)
					ordered.put(i, new ArrayList<String>());
				ordered.get(i).add(title);
			}
		}
		StringBuilder agenda = new StringBuilder();
		agenda.append(m_agenda_head).append("<ol>");
		if (annual_meeting) {
			agenda.append("<li>Election of Officers</li>");
			agenda.append("<li>General Meeting Schedule Setting</li>");
			if (ci_proposals != null && ci_proposals.size() > 0)
				agenda.append("</ol>Capital Improvements Proposals<ol>");
				for (String[] proposal : ci_proposals) {
					agenda.append("<li>");
					agenda.append(proposal[0]);
					agenda.append("</li>");
				}
				agenda.append("</ol>");
				if (ordered.size() > 0 || proposals.size() > 0)
					agenda.append("Regular Proposals<ol>");
		} else if (budget_meeting)
			agenda.append("<li>Budget and Dues changes (if any) for next year</li>");
		for (Integer i : ordered.descendingKeySet()) {
			List<String> titles = ordered.get(i);
			for (String title : titles) {
				agenda.append("<li>");
				agenda.append(title);
				agenda.append("</li>");
			}
		}
		for (String[] proposal : proposals)
			if (map.get(proposal[0]) == 0) {
				agenda.append("<li>");
				agenda.append(proposal[0]);
				agenda.append("</li>");
			}
		agenda.append("</ol>");
		if (annual_meeting) {
			agenda.append("<hr /><h4>Election of Officers</h4>Annual election of President, Vice President, Secretary and Treasurer.");
			agenda.append("<hr /><h4>General Meeting Schedule Setting</h4>Selection of dates for general meetings this year.");
			if (ci_proposals != null)
				for (String[] proposal : ci_proposals) {
					agenda.append("<hr /><h4>");
					agenda.append(proposal[0]);
					agenda.append("</h4><div style=\"font-size:smaller\">Submitted by ");
					agenda.append(site.lookupName(Integer.parseInt(proposal[2]), db));
					agenda.append("</div>");
					agenda.append(proposal[1]);
				}
		}
		if (budget_meeting)
			agenda.append("<hr /><h4>Budget and Dues changes (if any) for next year</h4>Discussion and final agreement on the budget and HOA dues changes (if any) for next year. Passing of the budget and dues is in the form of an Owner vote by the end of the year.");
		for (Integer i : ordered.descendingKeySet()) {
			List<String> titles = ordered.get(i);
			for (String title : titles)
				for (String[] proposal : proposals)
					if (proposal[0].equals(title)) {
						agenda.append("<hr /><h4>");
						agenda.append(title);
						agenda.append("</h4><div style=\"font-size:smaller\">Submitted by ");
						agenda.append(site.lookupName(Integer.parseInt(proposal[2]), db));
						agenda.append("</div>");
						agenda.append(proposal[1]);
						break;
					}
		}
		for (String[] proposal : proposals)
			if (map.get(proposal[0]) == 0) {
				agenda.append("<hr /><h4>");
				agenda.append(proposal[0]);
				agenda.append("</h4><div style=\"font-size:smaller\">Submitted by ");
				agenda.append(site.lookupName(Integer.parseInt(proposal[2]), db));
				agenda.append("</div>");
				agenda.append(proposal[1]);
			}
		agenda.append("<hr />");
		return agenda.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "System for adding proposals, creating agendas by the community, online votes, recording decisions, and general meeting schedules";
	}

	//--------------------------------------------------------------------------

	private static int
	getNumParticipants(DBConnection db) {
		return db.countRows(new Select("1").from("people").where("active AND participant"));
	}

	//--------------------------------------------------------------------------

	static int
	getQuorum(DBConnection db) {
		return (int)Math.ceil(getNumParticipants(db) / 4.0);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("decisions")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("status", Types.VARCHAR))
			.add(new JDBCColumn("type", Types.VARCHAR).setDefaultValue("Regular"))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		addColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("decisions_agree")
			.add(new JDBCColumn("decisions"))
			.add(new JDBCColumn("people"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("decisions_rank")
			.add(new JDBCColumn("decisions_id", Types.INTEGER))
			.add(new JDBCColumn("people_id", Types.INTEGER))
			.add(new JDBCColumn("_order_", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("votes")
			.add(new JDBCColumn("decisions"))
			.add(new JDBCColumn("vote", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		JDBCTable.adjustTable(table_def, true, false, db);
		m_attachments.init(db);
		addPage(new Page("Decisions", m_site){
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Decisions", request);
				boolean old_proposals = request.db.exists("decisions", "status='completed (no decision)' OR status='did not pass' OR status='dropped' OR status='withdrawn'");
				boolean voting = request.db.exists("decisions", "status='online vote' OR status='owner vote'");
				if (m_support_proposals || m_support_meetings || m_system_is_opt_in || old_proposals || voting) {
					TaskPane task_pane = request.writer.ui.taskPane("Decisions").open();
					if (m_support_voting && request.userHasRole("decisions"))
						task_pane.taskOnClick("Add New Vote", "new Dialog({cancel:true,ok:{text:'add'},reload_page:true,title:'Add New Vote',url:context+'/Views/add vote/component?db_mode=ADD_FORM',width:'auto'});return false;");
					ArrayList<String> items = new ArrayList<>();
					if (m_support_proposals)
						items.add("Proposals");
					items.add("Decisions");
					if (m_support_meetings)
						items.add(m_meetings_name);
					if (old_proposals)
						items.add("Old Proposals");
					if (m_system_is_opt_in)
						items.add("Opted In Members");
					if (voting)
						items.add("Votes");
					task_pane.sortedTasks(items);
					task_pane.closeTasks();
					if (voting)
						task_pane.selectTask("Votes");
					else if (m_support_proposals) {
						int pending_id = request.getInt("pending", -1);
						String on_complete = null;
						if (pending_id != -1)
							on_complete = "function(){new Dialog({ok:{text:'done'},owner:C._(this),title:'View Proposal',url:context+'/Views/pending list/component?db_mode=READ_ONLY_FORM&db_key_value=" + pending_id + "',width:'auto'});}";
						task_pane.selectTask("Proposals", on_complete);
					} else
						task_pane.selectTask("Decisions");
					task_pane.close();
				} else
					request.site.newView("decisions", request).writeComponent();
				request.close();
			}
		}.setRole(m_site.getDefaultRole()), m_site, db);
		Roles.add("decisions");
		Roles.add("proposals");
		m_site.addModule(new GeneralMeetingsEventProvider(), db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("add proposal")) {
			ViewDef view_def = new ViewDef(name) {
					@Override
					public void
					afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
						if (m_send_announcements_to != null)
							if ((m_proposal_types == null || m_proposal_types.length <= 1 || "Regular".equals(name_value_pairs.getString("type"))) && "pending".equals(name_value_pairs.getString("status")))
								sendNewProposalEmail(id, name_value_pairs.getString("title"), request);
						super.afterInsert(id, name_value_pairs, request);
					}
				}
				.setAccessPolicy(m_proposals_role != null ? new RoleAccessPolicy(m_proposals_role).add() : null)
				.setAddButtonText("add new proposal")
				.setCenter(false)
				.setFrom("decisions")
				.setOnSuccess("Dialog.top().close();net.replace('Decisions',context+'/Decisions/Proposals');")
				.setRecordName("Proposal")
				.setShowHead(false)
				.setColumnNamesForm(m_proposal_types == null || m_proposal_types.length <= 1 ? new String[] { "title", "text", "status", "_owner_" } : new String[] { "type", "title", "text", "status", "_owner_" })
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true))
				.setColumn(new Column("status").setDefaultValue(m_proposals_need_approval ? "pending" : "in process").setIsHidden(true))
				.setColumn(m_text_column)
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany("decisions_attachments"));
			if (m_proposal_types != null && m_proposal_types.length > 1)
				view_def.setColumn(new OptionsColumn("type", m_proposal_types).setPostText("note: CI proposals are only for the January meeting"));
			return view_def;
		}
		if (name.equals("add vote"))
			return new ViewDef(name) {
					@Override
					public void
					afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
						sendVoteEmail(name_value_pairs.getString("title"), "owner vote".equals(name_value_pairs.getString("status")), request);
						super.afterInsert(id, name_value_pairs, request);
					}
				}
				.setAccessPolicy(m_proposals_role != null ? new RoleAccessPolicy(m_proposals_role).add() : null)
				.setAddButtonText("add new vote")
				.setCenter(false)
				.setFrom("decisions")
				.setRecordName("Vote")
				.setShowHead(false)
				.setColumnNamesForm("status", "title", "text", "_owner_")
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true))
				.setColumn(new OptionsColumn("status", "online vote", "owner vote").setDefaultValue("online vote").setDisplayName("type").setType(Type.RADIO))
				.setColumn(m_text_column)
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany("decisions_attachments"));
		if (name.equals("ci proposals"))
			return new ViewDef(name).setAccessPolicy(new AccessPolicy().view())
				.setBaseFilter("status='pending' AND type='Capital Improvement'")
				.setCenter(false)
				.setDefaultOrderBy("_timestamp_")
				.setFrom("decisions")
				.setRecordName("Proposal")
				.setOnSuccess("Dialog.top().close();net.replace('Decisions',context+'/Decisions/Proposals');")
				.setShowHead(false)
				.setColumnNamesForm(new String[] { "type", "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(new Column("date").setViewRole("decisions"))
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDisplayName("submitted by"))
				.setColumn(m_status_column)
				.setColumn(m_text_column)
				.setColumn(new Column("_timestamp_").setDisplayName("submitted"))
				.setColumn(m_type_column)
				.addRelationshipDef(new OneToMany("decisions_attachments"));
		if (name.equals("decisions")) {
			ViewDef view_def = new ViewDef(name)
				.addSection(new Tabs("date", new OrderBy("date")).setFormatYear(true))
				.setAccessPolicy(new RoleAccessPolicy("decisions").add().delete().edit().view())
				.setBaseFilter("status='passed'")
				.setDefaultOrderBy("title")
				.setRecordName("Decision")
				.setColumn(m_status_column)
				.setColumn(m_text_column)
				.addRelationshipDef(new OneToMany("decisions_attachments"));
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("title");
			column_names.add("date");
			addColumnNames(column_names);
			view_def.setColumnNamesTable(column_names);
			column_names.clear();
			if (m_proposal_types != null && m_proposal_types.length > 1)
				column_names.add("type");
			column_names.add("title");
			column_names.add("text");
			column_names.add("status");
			column_names.add("date");
			addColumnNames(column_names);
			if (m_support_voting)
				column_names.add("vote_totals");
			view_def.setColumnNamesForm(column_names);
			if (m_support_voting)
				view_def
					.setColumn(m_vote_totals_column)
					.addRelationshipDef(new OneToMany("votes"));
			return view_def;
		}
		if (name.equals("decisions_attachments"))
			return m_attachments._newViewDef(name, site);
		if (name.equals("old proposals")) {
			ViewDef view_def = new ViewDef(name).setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setBaseFilter("status='completed (no decision)' OR status='did not pass' OR status='dropped' OR status='withdrawn'")
				.setDefaultOrderBy("date DESC NULLS LAST,title")
				.setFrom("decisions")
				.setRecordName("Proposal")
				.setShowHead(false)
				.setColumnNamesForm(m_support_voting ? new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_", "vote_totals" } : new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title", "date", "status" })
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDisplayName("submitted by"))
				.setColumn(new Column("_timestamp_").setDisplayName("submitted"))
				.addRelationshipDef(new OneToMany("decisions_attachments"));
			if (m_support_voting)
				view_def
					.setColumn(m_vote_totals_column)
					.addRelationshipDef(new OneToMany("votes"));
			return view_def;
		}
		if (name.equals("parking lot"))
			return new ViewDef(name) {
					@Override
					public Reorderable
					getReorderable(Request request) {
						if (userIsParticipant(request))
							return super.getReorderable(request);
						return null;
					}
				}
				.setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setAllowSorting(false)
				.setBaseFilter("status='in process'")
				.setCenter(false)
				.setFrom("decisions")
				.setRecordName("Proposal")
				.setReorderable(new Reorderable("decisions_rank", "decisions_id").setInstructions("(click and drag to prioritize the proposals, highest priority at the top)")).setDefaultOrderBy("_order_,_timestamp_") // must be after setReorderable
				.setOnSuccess("Dialog.top().close();net.replace('Decisions',context+'/Decisions/Proposals');")
				.setSelectFrom("decisions LEFT JOIN decisions_rank ON decisions_rank.decisions_id=decisions.id AND people_id=$(@user id)")
				.setShowHead(false)
				.setColumnNamesForm(new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(new Column("date").setViewRole("decisions"))
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDisplayName("submitted by"))
				.setColumn(m_status_column)
				.setColumn(m_text_column)
				.setColumn(new Column("_timestamp_").setDisplayName("submitted"))
				.addRelationshipDef(new OneToMany("decisions_attachments"));
		if (name.equals("pending list"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setAllowSorting(false)
				.setBaseFilter(m_proposal_types == null || m_proposal_types.length <= 1 ? "status='pending'" : "status='pending' AND type='Regular'")
				.setCenter(false)
				.setDefaultOrderBy("title")
				.setFrom("decisions")
				.setOnSuccess("Dialog.top().close();net.replace('Decisions'),context+'/Decisions/Proposals');")
				.setRecordName("Proposal")
				.setShowHead(false)
				.setColumnNamesForm(m_proposals_need_approval ? new String[] { "type", "title", "_owner_", "text", "agree", "others", "status" } : new String[] { "type", "title", "_owner_", "text", "others", "status" })
				.setColumnNamesTable(m_proposals_need_approval ? new String[] { "title", "_owner_", "_timestamp_", "agree" } : new String[] { "title", "_owner_", "_timestamp_" })
				.setColumn(new Column("agree") {
					private void
					writeInputValue(View view, Request request) {
						String decisions_id = view.data.getString("id");
						int num_agree = request.db.countRows(new Select("1").from("decisions_agree").where("decisions_id=" + decisions_id));
						HTMLWriter writer = request.writer;
						int id = request.db.lookupInt("id", "decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + request.getUser().getId(), -1);
						if (id != -1) {
							if (view.getMode() == View.Mode.EDIT_FORM || view.getMode() == View.Mode.READ_ONLY_FORM || num_agree == 1)
								writer.write("you've agreed ");
							else
								writer.write(num_agree).write(" people have agreed, including you ");
							writer.ui.buttonOnClick("don't agree", "net.post(context+'/Decisions','action=don\\'t agree&decisions_id=" + decisions_id + "',function(){document.location=document.location})");
						} else {
							if (userIsParticipant(request))
								writer.ui.buttonOnClick("agree", "net.post(context+'/Decisions','action=agree&decisions_id=" + decisions_id + "',function(){document.location=document.location})");
							if (view.getMode() != View.Mode.EDIT_FORM && view.getMode() != View.Mode.READ_ONLY_FORM) {
								writer.write(" (").write(num_agree).write(" other");
								if (num_agree == 1)
									writer.write(" has");
								else
									writer.write("s have");
								writer.write(" agreed)");
							}
						}
					}
					@Override
					public void
					writeInput(View view, Form form, Request request) {
						writeInputValue(view, request);
					}
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						writeInputValue(view, request);
						return true;
					}
				}.setDisplayName("agree to discuss"))
				.setColumn(new LookupColumn("_owner_", "people", "first,last").setDisplayName("submitted by"))
				.setColumn(m_status_column)
				.setColumn(m_text_column)
				.setColumn(new Column("others") {
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						List<String> names = request.db.readValues(new Select("first||' '||last").from("decisions_agree JOIN people ON people.id=decisions_agree.people_id").where("decisions_id=" + view.data.getString("id") + " AND people_id!=" + request.getUser().getId()).orderBy("first,last"));
						for (int i=0; i<names.size(); i++) {
							if (i > 0)
								request.writer.write(", ");
							request.writer.write(names.get(i));
						}
						return names.size() > 0;
					}
				}.setDisplayName("others who've agreed").setIsReadOnly(true))
				.setColumn(new Column("_timestamp_").setDisplayName("submitted"))
				.setColumn(m_type_column)
				.addRelationshipDef(new OneToMany("decisions_attachments"))
				.addTask(new Task("send announcement", false, false) {
					@Override
					public void
					perform(String id, Request request) {
						sendNewProposalEmail(Integer.parseInt("id"), request.db.lookupString(new Select("title").from("decisions").whereIdEquals(Integer.parseInt("id"))), request);
					}
					@Override
					protected void
					writeLink(String view_def_name, String id, Request request) {
						if (request.userIsAdministrator())
							super.writeLink(view_def_name, id, request);
					}
				});
		if (name.equals("tabled proposals"))
			return new ViewDef(name)
				.setAccessPolicy(new RecordOwnerAccessPolicy().delete().edit().view())
				.setBaseFilter("status='tabled'")
				.setCenter(false)
				.setFrom("decisions")
				.setRecordName("Proposal")
				.setShowHead(false)
				.setColumnNamesForm(new String[] { "title", "text", "status" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(m_status_column)
				.setColumn(m_text_column)
				.addRelationshipDef(new OneToMany("decisions_attachments"));
		if (name.equals("votes"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setRecordName("Vote")
				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "_owner_", "vote" })
				.setColumn(new Column("_owner_"){
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						if ("owner vote".equals(request.db.lookupString(new Select("type").from("decisions").whereIdEquals(view.data.getInt("decisions_id")))))
							request.writer.write(request.db.lookupString(new Select("name").from("families").whereIdEquals(view.data.getInt("_owner_"))));
						else
							request.writer.write(request.site.lookupName(view.data.getInt("_owner_"), request.db));
						return true;
					}
				});
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	nightly(LocalDateTime now, Site site, DBConnection db) {
		if (!m_send_agenda_email)
			return;
		try {
			LocalDate three_days_from_now = now.toLocalDate().plusDays(3);
			ResultSet rs = db.select("id,start_time,end_time", "general_meetings", "date='" + three_days_from_now.toString() + "'");
			if (rs.next())
				sendAgendaEmail(rs.getInt(1), three_days_from_now, rs.getTime(2).toLocalTime(), rs.getTime(3).toLocalTime(), site, db);
			rs.getStatement().close();
			if (m_support_voting && db.exists("general_meetings", "date='" + now.toLocalDate().plusDays(5).toString() + "'"))
				sendPrioritizeEmail(site, db);
		} catch (Exception e) {
			site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask({"id","date","start time","end time"})
	public void
	sendAgendaEmail(int id, LocalDate date, LocalTime start, LocalTime end, Site site, DBConnection db) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");
		StringBuilder message = new StringBuilder("The next general meeting is scheduled for ")
			.append(date.getMonthValue()).append('/').append(date.getDayOfMonth()).append(' ').append(dtf.format(start)).append('-').append(dtf.format(end)).append(". ");
		int facilitator = db.lookupInt(new Select("facilitator").from("general_meetings").whereIdEquals(id), 0);
		if (facilitator != 0) {
			message.append("The meeting will be facilitated by ");
			message.append(site.lookupName(facilitator, db));
			message.append(". ");
		}
		String agenda = getAgenda(site, db);
		if (agenda == null)
			agenda = "There is no agenda for this meeting.";
		else if (m_system_is_opt_in) {
			int num = getNumParticipants(db);
			message.append(num);
			message.append(" people have opted in to the system so ");
			message.append(Math.round(num * 0.25));
			message.append(" are needed for a quorum. ");
		}
		db.update("general_meetings", "agenda=" + DBConnection.string(agenda), id);
		message.append(agenda);

		new Mail(site).send(m_send_announcements_to, m_send_announcements_to, "General Meeting", message.toString(), true);
	}

	//--------------------------------------------------------------------------

	void
	sendNewProposalEmail(int id, String title, Request request) {
		new Mail(request.site).send(m_send_announcements_to, m_send_announcements_to, "new proposal",
			"A new proposal with the title \"" + title +
				"\" has been posted. Click <a href=\"" + request.site.getURL("Decisions?pending=" + id) +
				"\">here</a> to view the proposal. After " + getQuorum(request.db) +
				" community members who have opted in have clicked the \"agree to discuss\" button, the proposal will be automatically moved to the Parking Lot where it will be included on future General Meeting agendas.",
			true);
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public static void
	sendPrioritizeEmail(Site site, DBConnection db) {
		boolean parking_lot = db.countRows("decisions", "status='in process'") > 1;
		int pending = db.countRows("decisions", "status='pending' AND type='Regular'");
		if (parking_lot || pending > 0) {
			String content = "This is a reminder that in two days the agenda will be set for the next general meeting.";
			if (pending > 0)
				if (pending == 1)
					content += " There is one proposal that is pending.";
				else
					content += " There are " + Integer.toString(pending) + " proposals that are pending.";
			content += " Please go to the <a href=\"" + site.getURL("Decisions") + "\">decisions page</a> if you would like to";
			if (parking_lot) {
				content += " prioritize the proposals in the Parking Lot";
				if (pending > 0)
					content += " and/or";
			}
			if (pending > 0)
				content += " check the list of pending proposals to see which ones you agree should come to a meeting";
			content += ".";
			ArrayList<String> emails = db.readValues(new Select("email").from("people").where("email IS NOT NULL AND participant AND active"));
			new Mail(site).send(emails.toArray(new String[emails.size()]),
				"General Meeting Reminder",
				content,
				true);
		}
	}

	//--------------------------------------------------------------------------

	void
	sendVoteEmail(String title, boolean owner, Request request) {
		String where = "email IS NOT NULL AND active";
		if (owner)
			where += " AND owner";
		else
			where += " AND participant";
		ArrayList<String> emails = request.db.readValues(new Select("email").from("people").where(where));
		new Mail(request.site).send(emails.toArray(new String[emails.size()]),
			owner ? "Owner Vote" : "Online Vote",
			"A new vote with the title \"" + title + "\" has been posted. Click <a href=\"" + request.site.getURL("Decisions") + "\">here</a> to cast a vote for your household.",
			true);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request request) {
		boolean system_is_opt_in = m_system_is_opt_in;
		super.updateSettings(request);
		if (system_is_opt_in != m_system_is_opt_in) {
			People people = (People)request.site.getModule("People");
			if (m_system_is_opt_in)
				people.addColumn(new JDBCColumn("participant", Types.BOOLEAN));
			else
				people.removeColumn("participant");
			people.adjustTables(request.db);
		}
	}

	//--------------------------------------------------------------------------

	static boolean
	userIsOwner(Request request) {
		return request.db.lookupBoolean(new Select("owner").from("people").whereIdEquals(request.getUser().getId()));
	}

	//--------------------------------------------------------------------------

	boolean
	userIsParticipant(Request request) {
		return !request.userIsGuest() && (!m_system_is_opt_in || request.db.lookupBoolean(new Select("participant").from("people").whereIdEquals(request.getUser().getId())));
	}

	//--------------------------------------------------------------------------

	void
	writeOptedInMembersPane(Request request) {
		HTMLWriter writer = request.writer;
		int user_id = request.getUser().getId();
		if (m_system_is_opt_in && !request.userIsGuest() && !userIsParticipant(request)) {
			LocalDate birthday = request.db.lookupDate(new Select("birthday").from("people").whereIdEquals(user_id));
			if (birthday == null)
				request.writer.write("You are not currently opted in. Please go to the Settings page, enter your birthday, and click Save.");
			else if (birthday.until(Time.newDate(), ChronoUnit.YEARS) >= 18)
				request.writer.ui.buttonOnClick("opt in", "net.post(context+'/db','db_cmd=update&db_view_def=account&db_key_value=" + user_id + "&participant=true',function(){net.replace('Decisions',context+'/Decisions/Opted In Members')})");
		}
		writer.write("<div><table><tr><td style=\"padding-right:50px;vertical-align: top;\">");
		Rows rows = new Rows(new Select("id,first,last").from("people").where("participant AND active").orderBy("first,last"), request.db);
		while (rows.next()) {
			writer.write(rows.getString(2)).write(' ').write(rows.getString(3));
			if (m_system_is_opt_in && user_id == rows.getInt(1))
				writer.write(' ').ui.buttonOnClick("opt out", "net.post(context+'/db','db_cmd=update&db_view_def=account&db_key_value=" + user_id + "&participant=false',function(){net.replace('Decisions',context+'/Decisions/Opted In Members')})");
			writer.br();
		}
		rows.close();
		writer.write("</td><td style=\"vertical-align: top;\"><h4 style=\"margin-top:0\">general meeting votes</h4>people: ").write(getNumParticipants(request.db)).br()
			.write("households: ").write(request.db.countRows(new Select("families_id").distinct().from("people").where("participant AND active").orderBy("families_id"))).br()
			.write("homes: ").write(request.db.countRows(new Select("number").distinct().from("people join families on families.id=people.families_id join homes on homes.id=families.homes_id").where("participant AND people.active"))).br()
			.write("quorum: ").write(getQuorum(request.db)).br();
		int owner_households = request.db.countRows(new Select("COUNT(*)").from("families join people on people.families_id=families.id").where("participant AND people.active AND owner").groupBy("families.id"));
		writer.write("<h4>owner votes</h4>households: ").write(owner_households).br()
			.write("quorum: ").write((int)(owner_households * 0.25)).br()
			.write("</td></tr></table></div>");
	}

	//--------------------------------------------------------------------------

	void
	writeProposalsPane(Request request) {
		HTMLWriter writer = request.writer;
		if (m_proposals_pane_head != null) {
			writer.write("<div class=\"alert alert-secondary\">");
			writeText(m_proposals_pane_head, request.db, writer);
			writer.write("</div>");
		}
		if (!request.userIsGuest())
			writer.ui.buttonOnClick("submit new proposal", "new Dialog({cancel:true,ok:{text:'add'},owner:C._(this),title:'Submit Proposal',url:context+'/Views/add proposal/component?db_mode=ADD_FORM',width:'auto'});return false;");
		if (m_proposals_need_approval) {
			writer.h3("Pending List");
			if (request.db.exists("decisions", "status='pending' AND type='Regular'")) {
				writer.br().br();
				View view = request.site.newView("pending list", request);
				String id = request.getParameter("pending");
				if (id != null) {
					String view_id = view.componentOpen();
					writer.tagClose();
					writer.js("C.push('" + view_id + "'),context+'/Views/pending list?db_key_value=" + id + "&db_mode=READ_ONLY_FORM','proposal');");
				} else
					view.writeComponent();
			}
		}
		if (request.db.exists("decisions", "status='in process'")) {
			writer.h3(m_parking_lot_name);
			request.site.newView("parking lot", request).writeComponent();
		}
		if (request.db.exists("decisions", "type='Capital Improvement' AND status='pending'")) {
			writer.h3("Capital Improvement Proposals");
			request.site.newView("ci proposals", request).writeComponent();
		}
		if (request.db.exists("decisions", "status='tabled'")) {
			writer.h3("Tabled Proposals");
			request.site.newView("tabled proposals", request).writeComponent();
		}
		if (request.userIsAdministrator()) {
			writer.write("<br /><div class=\"alert alert-secondary\">");
			writer.h3("Agenda");
			writer.write(getAgenda(request.site, request.db));
			writer.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeText(String text, DBConnection db, HTMLWriter writer) {
		int start = 0;
		int index = text.indexOf("$(");
		while (index != -1) {
			writer.write(text.substring(start, index));
			index += 2;
			int end = text.indexOf(')', index);
			String var = text.substring(index, end);
			if (var.equals("quorum"))
				writer.write(getQuorum(db));
			start = end + 1;
			index = text.indexOf("$(", start);
		}
		writer.write(text.substring(start));
	}

	//--------------------------------------------------------------------------

	void
	writeVotesPane(Request request) {
		HTMLWriter writer = request.writer;
		boolean user_is_owner = userIsOwner(request);
		boolean user_is_participant = userIsParticipant(request);
		Rows rows = new Rows(new Select("*").from("decisions").where("status='online vote' OR status='owner vote'").orderBy("title"), request.db);
		while (rows.next()) {
			int decision_id = rows.getInt("id");
			boolean owner_vote = rows.getString("status").equals("owner vote");
			writer.hr();
			writer.h2(rows.getString("title"));
			if (request.userHasRole("decisions")) {
				writer.ui.buttonOnClick("close vote", "net.post(context+'/Decisions','action=close vote&decisions_id=" + decision_id + "',function(){document.location=document.location})");
				writer.br();
			}
			writer.write("<div class=\"alert alert-secondary\">");
			writer.write(rows.getString("text"));
			writer.write("</div><div class=\"alert alert-secondary\">");
			if (owner_vote && user_is_owner || !owner_vote && user_is_participant) {
				String vote = request.db.lookupString(new Select("vote").from("votes").where("decisions_id=" + decision_id + " AND _owner_=" + (owner_vote ? ((mosaic.Person)request.getUser()).getHousehold(request).getId() : request.getUser().getId())));
				if (vote != null) {
					if (owner_vote)
						writer.write("Your household has voted with the vote <b>");
					else
						writer.write("You have voted with the vote <b>");
					writer.write(vote).write("</b>. You may change your vote if you want.<br /><br />");
				}
				writer.write("Vote: ");
				writer.ui.buttonOnClick("Yes", "net.replace('Decisions',context+'/Decisions/Votes?decision=" + decision_id + "&vote=yes')");
				writer.space();
				writer.ui.buttonOnClick("No", "net.replace('Decisions',context+'/Decisions/Votes?decision=" + decision_id + "&vote=no')");
				writer.space();
				writer.ui.buttonOnClick("Abstain", "net.replace('Decisions',context+'/Decisions/Votes?decision=" + decision_id + "&vote=abstain')");
			}
			writer.h4("Votes");
			int[] votes = null;
			ArrayList<String[]> voters = request.db.readRows(owner_vote ? new Select("name,vote").from("votes JOIN families ON families.id=votes._owner_").where("decisions_id=" + decision_id).orderBy("name") : new Select("first,last,vote").from("votes JOIN people ON people.id=votes._owner_").where("decisions_id=" + decision_id).orderBy("first,last"));
			if (voters.size() == 0)
				writer.write("no votes yet");
			else {
				Table table = writer.ui.table().borderSpacing(5);
				for (String[] row : voters) {
					table.tr();
					if (owner_vote)
						table.td(row[0]).td(row[1]);
					else
						table.td(row[0] + " " + row[1]).td(row[2]);
				}
				table.close();
				writer.h4("Totals");
				votes = countVotes(decision_id, request.db);
				writer.write("yes: ").write(votes[0]).br().write("no: ").write(votes[1]).br().write("abstain: ").write(votes[2]);
				if (votes[0] / 2 >= votes[1])
					writer.br().write("Currently at least 2/3 of yes or no votes are <b>yes</b> votes");
				else
					writer.br().write("Currently less than 2/3 of yes or no votes are <b>yes</b> votes");
			}
			writer.h4("Info");
			if (owner_vote) {
				int owner_households = request.db.countRows(new Select("COUNT(*)").from("families join people on people.families_id=families.id").where("people.active AND owner").groupBy("families.id"));
				writer.write("owner households: ").write(owner_households).br().write("quorum: ").write((int)Math.ceil(owner_households / 4.0));
			} else
				writer.write("participants: ").write(getNumParticipants(request.db)).br().write("quorum: ").write(getQuorum(request.db));
			writer.write("</div>");
		}
		rows.close();
	}
}
