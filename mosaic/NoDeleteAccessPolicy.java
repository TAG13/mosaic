package mosaic;

import app.Request;
import db.access.RoleAccessPolicy;

public class NoDeleteAccessPolicy extends RoleAccessPolicy {
	public
	NoDeleteAccessPolicy(String role) {
		super(role);
	}

	//--------------------------------------------------------------------------
	// called before honoring request to delete a row

	@Override
	public boolean
	canDeleteRow(String from, int id, Request request) {
		return request.userIsAdministrator();
	}
}
