package mosaic;

import java.util.Map;

import app.Request;
import app.Site;
import app.Themes;
import db.Form;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.Select;
import db.View;
import db.access.AccessPolicy;
import db.column.BirthdayColumn;
import db.column.Column;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.column.TextAreaColumn;
import email.MailLists;
import groups.Groups;
import meals.MealEventProvider;
import web.Stringify;

@Stringify
public class PeopleViewDef extends app.PeopleViewDef {
	public
	PeopleViewDef(String name, AccessPolicy access_policy, Site site) {
		super(name, access_policy, ((People)site.getModule("people")).m_user_pic_size);
		if (name.equals("account")) {
			setColumnNamesForm(new String[] { "additional_emails", "birthday", "show_birthday_on_calendar",  "password", "picture", "theme", "timezone", "top menu", "user_name" });
			setColumn(new BirthdayColumn("birthday", ((People)site.getModule("people")).m_birthday_is_date));
			addRelationshipDef(new OneToMany("people_skills"));
		} else {
			setCenter(false);
			setPrintScript("var t=document.querySelector('thead');t.firstChild.removeChild(t.firstChild.firstChild);t=t.nextElementSibling.firstChild;while(t){t.removeChild(t.firstChild);t=t.nextSibling;}");
			setRecordName("person");
			setShowPrintLinkTable(true);
			setColumnNamesForm(new String[] { "first", "last", "pronouns", "picture", "bio", "families_id", "owner", "resident", "phone", "email", "additional_emails", "can_post_to_mail_lists", "birthday", "show_birthday_on_calendar", "user_name", "password", "timezone", "theme", "active" });
			setColumnNamesTable(new String[] { "picture", "first", "last", "home", "phone", "email", "page" });
			setColumn(new Column("page").setDisplayName("").setValueRenderer(new ColumnValueRenderer() {
				@Override
				public void
				writeValue(View view, ColumnBase<?> column, Request request) {
					request.writer.ui.buttonOnClick("...", "Person.popup(" + view.data.getInt("id") + ")");
				}
			}, false));
			setColumn(new TextAreaColumn("bio").setIsRichText(true, false));
			setColumn(new BirthdayColumn("birthday", ((People)site.getModule("people")).m_birthday_is_date));
			setColumn(new Column("cm_account_active").setDisplayName("Common Meal Account Active").setViewRole("administrator"));
			setColumn(new LookupColumn("families_id", "families", "name").setAllowEditing().setAllowNoSelection(true)/*.setBaseURL("household.jsp?families_id=")*/.setDisplayName("household").setViewRole("people"));
			setColumn(new LookupColumn("home", "families JOIN homes ON families.homes_id=homes.id", "number")/*.setBaseURL("household.jsp?families_id=")*/.setOneColumn("families_id").setDisplayName("home"));
			MealEventProvider meal_event_provider = (MealEventProvider)site.getModule("Common Meals");
			if (meal_event_provider != null)
				setColumn(new OptionsColumn("meal_age", meal_event_provider.getAges()).setAllowNoSelection(true));
			setColumn(new Column("owner").setEditRole("people"));
			setColumn(new Column("resident").setEditRole("people"));
			setColumn(((Themes)site.getModule("Themes")).getColumn());
		}
		addRelationshipDef(new OneToMany("additional_emails").setSpanFormCols(false));
		if (site.getModule("Groups").isActive())
			addRelationshipDef(new ManyToMany("edit people groups", "people_groups", "name").setSpanFormCols(false).setViewRole(site.getDefaultRole()));
		if (site.getModule("MailLists").isActive()) {
			addRelationshipDef(new ManyToMany("people mail_lists", "mail_lists_people", "name").setSpanFormCols(false).setViewRole(site.getDefaultRole()));
			addRelationshipDef(new ManyToMany("people mail_lists_digest", "mail_lists_digest", "name").setSpanFormCols(false).setViewRole(site.getDefaultRole()));
		}
		if (m_filters == null)
			m_filters = new String[] { "None|active", "Owners|active AND owner", "Residents|active AND resident", "Past Members|NOT active AND user_name != 'admin'" };
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
		if (name_value_pairs.containsName("active") && !name_value_pairs.getBoolean("active") && request.db.lookupBoolean(new Select("active").from("people").whereIdEquals(id))) {
			((Groups)request.site.getModule("Groups")).removePerson(id, request.db);
			((MailLists)request.site.getModule("MailLists")).removePerson(id, request.db);
		}
		if (request.getUser().getId() == id)
			request.removeSessionAttribute("user");
		return super.beforeUpdate(id, name_value_pairs, previous_values, request);
	}

	//--------------------------------------------------------------------

	@Override
	public Form
	newForm(View view, Request request) {
		if (m_name.equals("account"))
			return new AccountForm(view, request);
		return super.newForm(view, request);
	}
}
