package mosaic;

import java.time.LocalDateTime;

import app.Module;
import app.Site;
import app.Time;
import calendar.EventProvider;
import db.DBConnection;
import email.MailLists;

public class FiveMinuteThread implements Runnable {
	private Site m_site;

	//--------------------------------------------------------------------------

	public
	FiveMinuteThread(Site site) {
		m_site = site;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	run() {
		try {
			Thread.sleep((long)(Math.random() * 300000L)); // wait a random percentage of 5 minutes to stagger sites
		} catch (InterruptedException e) {
			System.out.println("FiveMinuteThread sleep exception");
			m_site.log(e);
			m_site = null;
			return;
		}
		while (true) {
			try {
				Thread.sleep(300000L); // 5 minutes
			} catch (InterruptedException e) {
				System.out.println("FiveMinuteThread sleep exception");
				System.out.println(e.toString());
				m_site = null;
				return;
			}
			DBConnection db = new DBConnection(m_site);
			try {
				LocalDateTime date_time = Time.newDateTime();
				if (date_time.getMinute() < 5) {
					MailLists mail_lists = (MailLists)m_site.getModule("MailLists");
					if (mail_lists != null)
						mail_lists.checkForMail(db);
				}
				for (Module module : m_site.getModules(EventProvider.class))
					if (((EventProvider)module).supportsReminders())
						((EventProvider)module).sendReminders(date_time, m_site, db);
			} catch (Exception e) {
				m_site.log(e);
			}
			db.close();
		}
	}
}