package mosaic;

import java.sql.Types;

import app.Mail;
import app.Request;
import app.Site;
import app.Site.Message;
import app.SiteModule;
import app.Subscriber;
import app.Text;
import app.pages.Page;
import db.DBConnection;
import db.OrderBy;
import db.Rows;
import db.RowsTable;
import db.Select;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.PersonColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Dividers;
import social.Comments;
import ui.Table;
import web.HTMLWriter;

public class TimeBank extends SiteModule implements Subscriber {
	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		switch(request.getPathSegment(1)) {
		case "balances":
			Table table = request.writer.ui.table().addClass("table").addClass("table-striped").addClass("table-condensed").addStyle("margin-top", "10px").open();
			Rows rows = new Rows(new Select("first,last,coalesce((SELECT sum(hours) FROM timebank WHERE _owner_=people.id),0.0) -  coalesce((SELECT sum(hours) FROM timebank WHERE recipient=people.id),0.0)").from("people").where("people.id IN(SELECT _owner_ FROM timebank UNION SELECT recipient FROM timebank)").orderBy("first,last"), request.db);
			while (rows.next())
				table.tr().td(Text.join(" ", rows.getString(1), rows.getString(2))).td(Float.toString(rows.getFloat(3)));
			rows.close();
			table.close();
			return true;
		case "by date":
			request.site.newView("timebank", request).writeComponent();
			return true;
		case "by person":
			request.site.newView("timebank by person", request).writeComponent();
			return true;
		case "skills":
			RowsTable.write(new Rows(new Select("skill,string_agg(first || ' ' || last, ', ' ORDER BY first,last)").from("people").join("people", "people_skills").groupBy("skill"), request.db), request.writer);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Track time that members volunteer in tasks for each other";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("timebank")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("recipient", "people"))
			.add(new JDBCColumn("hours", Types.REAL))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("task", Types.VARCHAR));
		addColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		addPage(new Page("Time Bank", m_site) {
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Time Bank", request);
				int id = request.getUser().getId();
				float balance = request.db.lookupFloat(new Select("coalesce((SELECT sum(hours) FROM timebank WHERE _owner_=" + id + "),0.0) -  coalesce((SELECT sum(hours) FROM timebank WHERE recipient=" + id + "),0.0)"), 0);
				HTMLWriter writer = request.writer;
				writer.write("<div class=\"container\" style=\"padding-top:10px;\"><div style=\"display:table;margin:auto;\"><div class=\"alert alert-secondary\">Your current time bank balance is ").write(balance).space().write(Text.pluralize("hour", balance));
				writer.write("</div><div id=\"button_nav\">");
				writer.write("<div id=\"by date div\">");
				request.site.newView("timebank", request).writeComponent();
				writer.write("</div></div></div></div>");
				writer.js("new ButtonNav(dom.$('button_nav'),[{text:'By Date',icon:'calendar',div:dom.$('by date div'),url:'/TimeBank/by date'}," +
					"{text:'By Person',icon:'person',url:'/TimeBank/by person'}," +
					"{text:'Balances',icon:'card-list',url:'/TimeBank/balances'}," +
					"{text:'Skills',icon:'wrench',url:'/TimeBank/skills'}])");
			}
		}.setRole(m_site.getDefaultRole()), m_site, db);
		m_site.addModule(new Comments("timebank", m_site, db), db);
		m_site.subscribe("timebank", this);
		m_site.subscribe("timebank_comments", this);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("timebank"))
			return new ViewDef(name)
				.setAccessPolicy(new Or(new RecordOwnerAccessPolicy().add().delete().edit(), new RecordOwnerAccessPolicy().setColumn("recipient").add().delete().edit()))
				.setDefaultOrderBy("date")
				.setRecordName("entry")
				.setColumnNamesForm("_owner_", "recipient", "task", "date", "hours")
				.setColumnNamesTable("date", "_owner_", "recipient", "task", "hours")
				.setColumn(new Column("date").setIsRequired(true))
				.setColumn(new PersonColumn("recipient", false, site.getPeopleFilter()))
				.setColumn(new Column("hours").setIsRequired(true))
				.setColumn(new PersonColumn("_owner_", false, site.getPeopleFilter()).setDefaultToUserId().setDisplayName("provider").setIsReadOnly(true))
				.setColumn(new Column("task").setIsRequired(true));
		if (name.equals("timebank by person"))
			return new ViewDef(name)
				.addSection(new Dividers("name", new OrderBy("name")))
				.setAccessPolicy(new AccessPolicy())
				.setDefaultOrderBy("date")
				.setFrom("timebank")
				.setRecordName("entry")
				.setSelectColumns("first||' '||last AS name,date,_owner_,recipient,task,hours,timebank.id")
				.setSelectFrom("people JOIN timebank ON people.id=_owner_ OR people.id=recipient")
				.setColumnNamesTable("date", "_owner_", "recipient", "task", "hours")
				.setColumn(new PersonColumn("recipient", false, site.getPeopleFilter()))
				.setColumn(new PersonColumn("_owner_", false, site.getPeopleFilter()).setDefaultToUserId().setDisplayName("provider").setIsReadOnly(true));
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, Site site, DBConnection db) {
		if (object.equals("timebank") && message.equals(Message.INSERT)) {
			String email = db.lookupString(new Select("email").from("people").whereIdEquals(db.lookupInt(new Select("recipient").from("timebank").whereIdEquals((int)data), 0)));
			if (email != null)
				new Mail(site).send(email, site.getDisplayName() + " Time Bank entry added", "Hello,\n\na new entry with you as the recipient has been added to the " + site.getDisplayName() + " Time Bank. Click <a href=\"" + site.getURL("Time Bank?edit=" + (int)data) + "\">here</a> to view the entry.", true);
			return;
		}
		if (object.equals("timebank_comments") && message.equals(Message.INSERT)) {
			int timebank_id = db.lookupInt(new Select("timebank_id").from("timebank_comments").whereIdEquals((int)data), 0);
			int comment_owner = db.lookupInt(new Select("_owner_").from("timebank_comments").whereIdEquals((int)data), 0);
			int provider = db.lookupInt(new Select("_owner_").from("timebank").whereIdEquals(timebank_id), 0);
			if (comment_owner != provider) {
				String email = db.lookupString(new Select("email").from("people").whereIdEquals(provider));
				if (email != null)
					new Mail(site).send(email, site.getDisplayName() + " Time Bank comment added", "Hello,\n\na new comment for a time bank entry with you as the provider has been added. Click <a href=\"" + site.getURL("Time Bank?edit=" + timebank_id) + "\">here</a> to view the entry.", true);
			}
			int recipient = db.lookupInt(new Select("recipient").from("timebank").whereIdEquals(timebank_id), 0);
			if (comment_owner != recipient) {
				String email = db.lookupString(new Select("email").from("people").whereIdEquals(recipient));
				if (email != null)
					new Mail(site).send(email, site.getDisplayName() + " Time Bank comment added", "Hello,\n\na new comment for a time bank entry with you as the recipient has been added. Click <a href=\"" + site.getURL("Time Bank?edit=" + timebank_id) + "\">here</a> to view the entry.", true);
			}
		}
	}
}