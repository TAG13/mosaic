package mosaic;

import java.time.ZoneId;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import app.FileManager;
import app.Module;
import app.Request;
import app.Site;
import app.SiteModule;
import app.Time;
import app.pages.Page;
import calendar.EventProvider;
import db.DBConnection;
import db.Validation;
import households.HouseholdEventProvider;
import ui.Card;
import ui.Select;
import ui.TaskPane;
import web.HTMLWriter;
import web.JS;

public class EditSite extends Module {
	@Override
	public boolean
	doGet(Request request) {
		switch (request.getPathSegment(1)) {
		case "Calendars":
			HTMLWriter writer = request.writer;
			String segment_two = request.getPathSegment(2);
			if (segment_two != null) {
				request.site.getModule(segment_two).writeSettingsForm(null, false, request);
				return true;
			}
			String id = UUID.randomUUID().toString();
			writer.setAttribute("id", id);
			writer.tag("div", null);
			writer.scriptOpen();
			writer.write("new TaskPane('EditSite/Calendars').button('Add',function(){new Dialog({cancel:true,title:'Add Calendar'}).add_ok('ok',function(){net.post(context+'/EditSite/Calendars/add','name='+this.get_value('name')+'&type='+this.get_value('type'),function(){Dialog.top().close();select_nav_item_by_name('EditSite','Calendars')});}).new_row('Name').add_input('name','text').new_row('Type').add_select('type',['Regular','Guest Room']).open();})");
			Map<String,Module> calendars = new TreeMap<>();
			for (Module module: request.site.getModules())
				if (module instanceof EventProvider && !(module instanceof HouseholdEventProvider))
					if (module.getDisplayName(request) != null)
						calendars.put(module.getDisplayName(request).toLowerCase(), module);
			for (Module calendar: calendars.values())
				writer.write(".task(").jsString(calendar.getDisplayName(request)).write(",context+'/EditSite/Calendars/" + HTMLWriter.URLEncode(calendar.getName()) + "')");
			writer.write(".render(dom.$('").write(id).write("'));");
			writer.scriptClose();
			return true;
		case "File Manager":
			((FileManager)request.site.getModule("FileManager")).setRoot(request.site.getSettings().getString("file manager root")).write(request);
			return true;
		case "Modules":
			writeModulesPane(request);
			return true;
		case "Site":
			ui.Form form = new ui.Form(m_name, request.getContext() + "/EditSite/Site", request.writer).open();
			form.rowOpen("time zone");
			Select select = new Select("time zone", ZoneId.getAvailableZoneIds()).setOnChange("if(this.nextSibling)this.nextSibling.remove()").setAllowNoSelection(true);
			String time_zone = request.site.getSettings().getString("time zone");
			if (time_zone != null)
				select.setSelectedOption(time_zone, null);
			select.write(request);
			request.writer.write(" Server Time: ").writeTime(Time.newTime());
			form.rowOpen("title");
			request.writer.setAttribute("required", "yes");
			request.writer.textInput("title", null, request.site.getDisplayName());
			form.close();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		switch (request.getPathSegment(1)) {
		case "Calendars":
			String segment_two = request.getPathSegment(2);
			if ("add".equals(segment_two)) {
				String name = request.getParameter("name");
				String type = request.getParameter("type");
				EventProvider event_provider = "Guest Room".equals(type) ? new GuestRoomEventProvider(name) : new MosaicEventProvider(name);
				event_provider.setCanBeDeleted(true);
				request.site.addModule(event_provider, request.db);
				event_provider.store(request.db);
			}
			return true;
		case "Modules":
			String module_name = request.getPathSegment(2);
			SiteModule module = (SiteModule)request.site.getModule(module_name);
			module.setActive(request.getParameter("active").equals("true"), request.site, request.db);
			return true;
		case "Site":
			request.writer.write("Form.show_saved('" + Validation.getValidIdentifier("db_submit_id", request) + "')");
			request.site.setTimeZone(request.getParameter("time zone"), request.db);
			request.site.getSettings().set(Site.SITE_DISPLAY_NAME, request.getParameter("title"), request.db);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		m_site.addUserDropdownItem(new Page("Edit Site", this){
			@Override
			public void
			write(Request request) {
				Map<String,String> config_tasks = new TreeMap<>();
				config_tasks.put("Calendars", null);
				String root = request.site.getSettings().getString("file manager root");
				if (root != null)
					config_tasks.put("File Manager", null);
				config_tasks.put("Modules", null);
				config_tasks.put("Site", null);
				for (Module module : request.site.getModules())
					if (module.isActive()) {
						String[][] module_config_tasks = module.getConfigTasks();
						if (module_config_tasks != null)
							for (String[] label_path : module_config_tasks)
								config_tasks.put(label_path[0], label_path[1]);
					}
				request.site.writePageOpen("Edit Site", request.site.newHead(request).script("calendar-min"), request);
				TaskPane task_pane = request.writer.ui.taskPane("EditSite").open();
				for (String label : config_tasks.keySet()) {
					String path = config_tasks.get(label);
					if (path == null)
						task_pane.task(label, request.getContext() + "/EditSite/" + label);
					else
						task_pane.task(label, request.getContext() + path);
				}
				task_pane.close();
				request.close();
			}
		}.setRole("admin"), db);
	}

	//--------------------------------------------------------------------------

	private void
	writeModulesPane(Request request) {
		HTMLWriter writer = request.writer;
		Collection<Module> modules = request.site.getModules();
		for (Module module : modules)
			if (module instanceof SiteModule) {
				Card card = writer.ui.card();
				card.header(module.getDisplayName(request))
					.bodyOpen();
				writer.write(((SiteModule) module).getDescription())
					.br()
					.write("<input type=\"checkbox\" onchange=\"net.post(context+'/EditSite/Modules/")
					.write(module.getName())
					.write("','active=' + (this.checked ? 'true' : 'false'))\"");
				if (((SiteModule) module).isActive())
					writer.write(" checked");
				writer.write("/> active ")
					.setAttribute("style", "margin-left:10px;")
					.ui.buttonOnClick("settings", "new Dialog({cancel:true,ok:true,title:'" + JS.escape(module.getDisplayName(request)) + " Settings',url:context+'/" + module.getName() + "/settings?dialog=true'})");
				card.close();
			}
	}
}
