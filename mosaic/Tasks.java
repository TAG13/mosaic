package mosaic;

import java.sql.Types;

import app.Module;
import app.Site;
import db.DBConnection;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;

public class Tasks extends Module {
	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("tasks")
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("num_people_needed", Types.VARCHAR))
			.add(new JDBCColumn("workers", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("tasks"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setRecordName("Task")
				.setColumnNamesForm(new String[] { "description", "num_people_needed", "workers", "notes", "_owner_" })
				.setColumnNamesTable(new String[] { "description", "num_people_needed", "workers", "notes", "_owner_" })
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDisplayName("posted by").setDefaultToUserId().setIsReadOnly(true));
		return null;
	}
}
