package mosaic;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import app.Request;
import app.Site;
import app.pages.Page;
import db.DBConnection;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.PictureColumn;
import db.object.JSONField;
import web.HTMLWriter;

public class People extends app.People {
	@JSONField(admin_only = true)
	JsonObject	m_page_template = Json.parse("{\"blocks\":["
			+ "{\"type\":\"icon\",\"icon\":\"person\",\"edit_only\":true},"
			+ "{\"type\":\"img\",\"style\":\"margin-right:.25em;max-height:100px;\",\"view\":\"account\",\"edit_only\":true,\"label\":\"your profile picture\"},"
			+ "{\"type\":\"text\",\"column\":\"first\",\"view\":\"account\",\"edit_only\":true,\"label\":\"first name\"},"
			+ "{\"type\":\"text\",\"column\":\"last\",\"view\":\"account\",\"edit_only\":true,\"label\":\"last name\"},"
			+ "{\"type\":\"text\",\"column\":\"pronouns\",\"view\":\"account\",\"edit_only\":true},"
			+ "{\"type\":\"text\",\"icon\":\"envelope\",\"column\":\"email\",\"view\":\"account\"},"
			+ "{\"type\":\"text\",\"icon\":\"phone\",\"column\":\"phone\",\"view\":\"account\",\"last\":true},"
			+ "{\"type\":\"household\"},"
			+ "{\"type\":\"html\",\"title\":\"Bio\",\"column\":\"bio\",\"view\":\"account\"},"
			+ "{\"type\":\"pictures\"},"
			+ "{\"type\":\"skills\"},"
			+ "{\"type\":\"groups\"},"
			+ "{\"type\":\"mail lists\"}"
			+ "]}").asObject();

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		Person person = new Person(request.getPathSegmentInt(1), request.db);
		person.writePage(request);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		super.init(db);
		db.createManyTable("people", "people_skills", "skill VARCHAR");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	initEditPage(DBConnection db) {
		m_site.addUserDropdownItem(new Page("Edit People", this){
			@Override
			public void
			write(Request request) {
				HTMLWriter writer = request.writer;
				request.site.writePageOpen("Edit People", request);
				writer.write("<table style=\"border-left: solid transparent 20px; width: 100%;\"><tr><td style=\"vertical-align: top;\">");
				ViewState.setBaseFilter("people edit", "active", request);
				request.site.newView("people edit", request).writeComponent();
				if (request.site.getModule("Households").isActive()) {
					writer.write("</td><td style=\"vertical-align: top;\">");
					ViewState.setBaseFilter("families", "active", request);
					request.site.newView("families", request).writeComponent();
					writer.write("</td><td style=\"vertical-align: top;\">");
					request.site.newView("homes", request).writeComponent();
				}
				writer.write("</td></tr></table>");
				request.close();
			}
		}.setRole("people"), db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("account"))
			return new mosaic.PeopleViewDef(name, new AccessPolicy().edit(), site);
		if (name.equals("people"))
			return new mosaic.PeopleViewDef(name, new AccessPolicy(), site);
		if (name.equals("people edit"))
			return new mosaic.PeopleViewDef(name, new NoDeleteAccessPolicy("people").add().edit(), site)
				.setAlternateRows(true)
				.setFilters(site.getViewDef("people", null).getFilters())
				.setFrom("people")
				.setColumnNamesTable(new String[] { "picture", "first", "last", "home", "phone", "email" });
		if (name.equals("people_bio_pictures")) {
			ViewDef view_def = new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setCenter(false)
				.setRecordName("Picture")
				.setShowColumnHeads(false)
				.setColumnNamesTable("filename")
				.setColumn(new Column("people_id").setDefaultToUserId().setIsHidden(true));
			view_def.setColumn(new PictureColumn("filename", view_def, "people", 200, 1024).setImageSize(100).setDirColumn("people_id"));
			return view_def;
		}
		if (name.equals("people_skills"))
			return new ViewDef(name)
				.setCenter(false)
				.setRecordName("Skill")
				.setShowColumnHeads(false)
				.setColumnNamesForm("people_id", "skill")
				.setColumnNamesTable("skill")
				.setColumn(new Column("people_id").setDefaultToUserId().setIsHidden(true))
				.setColumn(new Column("skill").setIsRequired(true));
		return super._newViewDef(name, site);
	}
}
