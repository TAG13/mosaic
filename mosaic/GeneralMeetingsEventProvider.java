package mosaic;

import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.PriorityQueue;

import app.Request;
import app.Site;
import calendar.Event;
import calendar.EventProvider;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.feature.Feature.Location;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import ui.SelectOption;

public class GeneralMeetingsEventProvider extends EventProvider {
	@JSONField
	protected String m_extra_text;

	//--------------------------------------------------------------------------

	public
	GeneralMeetingsEventProvider() {
		super("General Meetings");
		m_events_table = "general_meetings";
		m_events_have_start_time = true;
		setAccessPolicy(new AccessPolicy());
		setColor("#FFDC00");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAllEvents(ArrayList<Event> events, Request request) {
		Rows rows = new Rows(new Select("date,start_time,end_time,annual_meeting,budget_meeting,agenda,id").from("general_meetings").where("date>=current_date"), request.db);
		while (rows.next()) {
			Event event = new Event(this, rows.getDate(1), eventText(rows.getBoolean(4), rows.getBoolean(5), rows.getString(6) == null ? 0 : rows.getInt(7))).setStartTime(rows.getTime(2)).setEndTime(rows.getTime(3));
			if (m_calendar_location_id != 0)
				event.setLocations(new SelectOption[] { ((EventProvider)request.site.getModule("Main Calendar")).getLocationOptions().getOptionByValue(Integer.toString(m_calendar_location_id)) });
			events.add(event);
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, String category, String location, Request request) {
		if (category != null && !category.equals("general meetings"))
			return;
		if (location != null)
			return;
		Select query = new Select("date,start_time,end_time,annual_meeting,budget_meeting,agenda,id").from("general_meetings").where("date>='" + from.toString() + "'");
		if (to != null)
			query.andWhere("date<'" + to.toString() + "'");
		Rows rows = new Rows(query, request.db);
		while (rows.next()) {
			Event event = new Event(this, rows.getDate(1), eventText(rows.getBoolean(4), rows.getBoolean(5), rows.getString(6) == null ? 0 : rows.getInt(7))).setStartTime(rows.getTime(2)).setEndTime(rows.getTime(3));
			if (m_calendar_location_id != 0)
				event.setLocations(new SelectOption[] { ((EventProvider)request.site.getModule("Main Calendar")).getLocationOptions().getOptionByValue(Integer.toString(m_calendar_location_id)) });
			events.add(event);
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("general_meetings")
			.add(new JDBCColumn("agenda", Types.VARCHAR))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("end_time", Types.TIME))
			.add(new JDBCColumn("start_time", Types.TIME))
			.add(new JDBCColumn("facilitator", Types.INTEGER))
			.add(new JDBCColumn("annual_meeting", Types.BOOLEAN))
			.add(new JDBCColumn("budget_meeting", Types.BOOLEAN));
		JDBCTable.adjustTable(table_def, true, false, db);
		adjustReminders(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate date, Request request) {
		return request.db.countRows(new Select().from("general_meetings").where("date>='" + date.toString() + "'"));
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if ("agenda".equals(request.getPathSegment(2))) {
			request.writer.write(request.db.lookupString(new Select("agenda").from("general_meetings").whereIdEquals(request.getPathSegmentInt(1))));
			return true;
		}
		return super.doGet(request);
	}

	// --------------------------------------------------------------------------

	private String
	eventText(boolean annual_meeting, boolean budget_meeting, int id) {
		String title = annual_meeting ? "General Meeting - election of officers, capital improvements funding" : budget_meeting ? "General Meeting - annual budget discussion" : "General Meeting";
		StringBuilder s = new StringBuilder(title);
		if (id != 0)
			s.append("<br/><a href=\"#\" onclick=\"new Dialog({ok:true,title:'" + title + "',url:context+'/General Meetings/" + id + "/agenda'}).open()\">agenda</a>");
		if (m_extra_text != null)
			s.append("<p>").append(m_extra_text).append("</p>");
		return s.toString();

	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request request) {
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("general_meetings"))
			return new ViewDef(name)
				.addFeature(new YearFilter(name, Location.TOP_LEFT))
				.setAccessPolicy(new RoleAccessPolicy("decisions").add().delete().edit())
				.setDefaultOrderBy("date")
				.setRecordName("General Meeting")
				.setColumnNamesForm(new String[] { "date", "start_time", "end_time", "facilitator", "annual_meeting", "budget_meeting", "agenda" })
				.setColumnNamesTable(new String[] { "date", "start_time", "end_time", "facilitator" })
				.setColumn(new Column("start_time").setDefaultToPrevious())
				.setColumn(new Column("end_time").setDefaultToPrevious())
				.setColumn(new LookupColumn("facilitator", "people", "first,last").setAllowNoSelection(true).setFilter(site.getPeopleFilter()));
		return super._newViewDef(name, site);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(Request request) {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, Request request) {
		super.writeSettingsForm(new String[] { "m_active", "m_color", "m_display_item_labels", "m_display_items", "m_events_have_start_time", "m_ical_items", "m_display_name", "m_support_reminders", "m_tooltip_item_labels", "m_tooltip_items" }, in_dialog, request);
	}
}
