package mosaic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.OrderBy;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import households.HouseholdIdColumn;

public class Contacts extends SiteModule {
	@Override
	public String
	getDescription() {
		return "Lists of contacts public for the whole site, groups, and/or private to households";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("contacts")
			.add(new JDBCColumn("families"))
			.add(new JDBCColumn("first", Types.VARCHAR))
			.add(new JDBCColumn("last", Types.VARCHAR))
			.add(new JDBCColumn("relationship", Types.VARCHAR))
			.add(new JDBCColumn("phone", Types.VARCHAR))
			.add(new JDBCColumn("email", Types.VARCHAR))
			.add(new JDBCColumn("address", Types.VARCHAR))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("public", Types.BOOLEAN))
			.add(new JDBCColumn("category", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("contacts"))
			return new ViewDef(name)
				.addSection(new Tabs("category", new OrderBy("category")))
				.setDefaultOrderBy("first,last")
				.setRecordName("Contact")
				.setColumnNamesForm(new String[] { "families_id", "first", "last", "relationship", "phone", "email", "address", "notes", "category", "public" })
				.setColumnNamesTable(new String[] { "families_id", "first", "last", "relationship", "phone", "email", "address", "notes", "public" })
				.setColumn(new HouseholdIdColumn())
				.setColumn(new Column("public").setDisplayName("show on public household page"));
		return null;
	}

	//--------------------------------------------------------------------------

	public void
	writeHouseholdRecords(int household_id, Request request) {
		ViewState.setBaseFilter("contacts", "families_id=" + household_id, request);
		request.site.newView("contacts", request).writeComponent();
	}

	//--------------------------------------------------------------------------

	public void
	writePublicRecords(int household_id, Request request) {
		try {
			ResultSet rs = request.db.select("SELECT first,last,relationship,phone,email,address,notes FROM contacts WHERE families_id=" + household_id + " AND public");
			if (rs.isBeforeFirst()) {
				request.writer.h4("Contacts");
				while (rs.next()) {
					request.writer.write("<p><b>");
					request.writer.write(rs.getString(1));
					String relationship = rs.getString(2);
					if (relationship != null && relationship.length() > 0) {
						request.writer.write(" - ");
						request.writer.write(relationship);
					}
					request.writer.write("</b><br />");
					String phone = rs.getString(3);
					if (phone != null) {
						request.writer.write(phone);
						request.writer.write("<br />");
					}
					String email = rs.getString(4);
					if (email != null) {
						request.writer.write(email);
						request.writer.write("<br />");
					}
					String address = rs.getString(5);
					if (address != null) {
						request.writer.write(address);
						request.writer.write("<br />");
					}
					String notes = rs.getString(6);
					if (notes != null) {
						request.writer.write(notes);
						request.writer.write("<br />");
					}
					request.writer.write("</p>");
				}
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
