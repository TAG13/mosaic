package mosaic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.UUID;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import accounting.Accounting;
import app.Block;
import app.ClassTask;
import app.Mail;
import app.Module;
import app.People;
import app.Request;
import app.Roles;
import app.Site;
import app.Text;
import app.pages.Page;
import app.pages.Pages;
import calendar.CalendarPicker;
import calendar.Event;
import calendar.EventProvider;
import calendar.ReminderHandler;
import db.DBConnection;
import db.Filter;
import db.NameValuePairs;
import db.OrderBy;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.EmailAddressProvider;
import db.column.EmailColumn;
import db.column.FileColumn;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.column.PictureColumn;
import db.feature.TSVectorSearch;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import documents.Documents;
import email.MailLists;
import email.PeopleMailHandlerFactory;
import email.WebMail;
import groups.GroupEventProvider;
import groups.GroupMailHandlerFactory;
import groups.GroupMinutesView;
import groups.Groups;
import households.Household;
import households.HouseholdEventProvider;
import households.HouseholdIdColumn;
import households.Households;
import lists.Lists;
import meals.MealEventProvider;
import meetings.Meetings;
import minutes.Minutes;
import pictures.Pictures;
import sitemenu.SiteMenu;
import social.Comments;
import social.Discussions;
import social.News;
import social.NewsFeed;
import surveys.Surveys;
import todos.ToDos;
import ui.NavBar;
import ui.Table;
import ui.Tabs;
import web.Head;
import web.ImageMagick;
import web.PathBuilder;
import work.Work;

public class MOSAIC extends Site {
	public
	MOSAIC(String display_name, String base_directory) {
		super(display_name, base_directory, "home.jsp", "coho");
		m_people_filter = new Filter() {
			@Override
			public boolean
			accept(Rows rows, Request request) {
				return rows.getBoolean("active");
			}
		};
		m_themes.setDefaultTheme("cerulean");
	}

	//--------------------------------------------------------------------------

	protected void
	addGuestRoomModule(DBConnection db) {
		addModule(new GuestRoomEventProvider("Guest Room").setColor("#FC9").setOneEventPerDay(true), db);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"first","last","email"})
	public void
	addPerson(String first, String last, String email, DBConnection db) {
		NameValuePairs name_value_pairs = new NameValuePairs();
		name_value_pairs.set("name", last);
		int household_id = db.insert("families", name_value_pairs);
		name_value_pairs.clear();
		name_value_pairs.set("first", first);
		name_value_pairs.set("last", last);
		name_value_pairs.set("email", email);
		name_value_pairs.set("families_id", household_id);
		name_value_pairs.set("user_name", first + last);
		name_value_pairs.set("password", "md5(" + DBConnection.string(first + last) + ")");
		name_value_pairs.set("active", true);
		name_value_pairs.set("theme", m_themes.getDefaultTheme());
		int people_id = db.insert("people", name_value_pairs);
		db.insert("user_roles", "people_id,user_name,role", people_id + ",'" + first + last + "','coho'");
		if (db.countRows(new Select("1").from("people")) == 1) // give first person admin role
			db.insert("user_roles", "people_id,user_name,role", people_id + ",'" + first + last + "','admin'");
		db.update("families", "contact=" + people_id, household_id);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"id"})
	public void
	copyPersonToCoHoLink(int id, Request r) {
		try {
			ResultSet person = r.db.select(new Select("*").from("people").whereIdEquals(id));
			if (person.next()) {
				Connection c = DriverManager.getConnection("jdbc:postgresql:coholink?user=" + (r.site.isDevMode() ? "postgres" : "mosaic") + "&password=postgres");
				Statement s = c.createStatement();
				int groups_id = 0;
				ResultSet group = s.executeQuery("SELECT id FROM groups WHERE name=" + DBConnection.string(r.site.getDisplayName()));
				if (group.next()) {
					System.out.println("adding group " + r.site.getDisplayName());
					groups_id = group.getInt(1);
					if (groups_id == 0) {
						s.execute("INSERT INTO groups(mosaic_site,name,self_joining) VALUES(" + DBConnection.string(r.getContext()) + "," + DBConnection.string(r.site.getDisplayName()) + ",false");
						group = s.getResultSet();
						if (group.next())
							groups_id = group.getInt(1);
					}
				}
				String user_name = person.getString("user_name");
				s.execute("INSERT INTO people(first,last,email,user_name,password,active,theme) VALUES(" +
					DBConnection.string(person.getString("first")) + "," +
					DBConnection.string(person.getString("last")) + "," +
					DBConnection.string(person.getString("email")) + "," +
					DBConnection.string(user_name) + "," +
					DBConnection.string(person.getString("password")) + ",true,'cerulean') RETURNING id");
				person = s.getResultSet();
				if (person.next()) {
					int people_id = person.getInt(1);
					s.execute("INSERT INTO user_roles(people_id,user_name,role) VALUES(" + people_id + "," + DBConnection.string(user_name) + ",'coho')");
					s.execute("INSERT INTO people_groups(people_id,groups_id) VALUES(" + people_id + "," + groups_id + ")");
				} else
					System.out.println("person not added");
				s.close();
				c.close();
			} else
				System.out.println("person with id " + id + " not found");
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(HttpServletRequest http_request, HttpServletResponse http_response) throws IOException {
		String path = http_request.getServletPath();
		if (path.endsWith("/docs")) {
			Request request = new Request(http_request, http_response);
			writeDocs(request);
			request.release();
			return true;
		}
		if (path.endsWith(".ics")) {
			Request request = new Request(http_request, http_response);
			int user_id = request.getInt("u", 0);
			String uuid = request.getParameter("uuid");
			if (user_id != 0 && uuid != null && uuid.length() == 36 && uuid.indexOf(' ') == -1 && request.db.rowExists("people", "id=" + user_id + " AND uuid='" + uuid + "'")) {
				if (path.startsWith("/FamilyCalendar")) {
					Person user = new Person(user_id, request.db);
					Household household = user.getHousehold(request);
					if (household == null || household.getId() != Integer.parseInt(path.substring(15, path.length() - 4)))
						return true;
				}
				String file_path = request.site.getBasePathBuilder().append("calendars").append(path.substring(1)).toString();
				File file = new File(file_path);
				if (!file.exists()) {
					String module_name = path.substring(1, path.lastIndexOf('.'));
					EventProvider event_provider = (EventProvider)request.site.getModule(module_name);
					if (event_provider != null)
						event_provider.writeICS(request);
					else
						http_response.getWriter().write("calendar " + module_name + " not found\n");
				}
				if (file.exists()) {
					PrintWriter writer = http_response.getWriter();
					BufferedReader br = new BufferedReader(new FileReader(file_path));
					String line = br.readLine();
					while (line != null) {
						writer.println(line);
						line = br.readLine();
					}
					br.close();
				} else
					http_response.getWriter().write("file " + path.substring(1) + " not found\n");
            }
			request.release();
			return true;
		}
		if (path.endsWith("/receive_mail")) {
			Request request = new Request(http_request, http_response);
			String dir = getBasePathBuilder().append("inbox").append(request.getPathSegment(-2)).toString();
			Session session = Session.getInstance(new Properties(), null);
			((MailLists)getModule("MailLists")).readAndSend(new File(dir), session, request.db);
			request.release();
			return true;
		}
		return super.doGet(http_request, http_response);
	}
	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(HttpServletRequest http_request, HttpServletResponse http_response) throws IOException {
		if (http_request.getServletPath().startsWith("/ImageMagick/")) {
			Request request = new Request(http_request, http_response);
			PathBuilder file_path = request.site.getBasePathBuilder();
			String[] path_segments = request.getPathSegments();
			int n = path_segments.length - 2;
			String filename = path_segments[n];
			boolean is_thumb = "thumbs".equals(path_segments[n - 1]);
			if (is_thumb)
				--n;
			for (int i=1; i<n; i++)
				file_path.append(path_segments[i]);
			String new_filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf('.'));
			String pictures_dir = file_path.toString();
			ImageMagick.rotate(pictures_dir, filename, new_filename, path_segments[path_segments.length - 1].endsWith(" clockwise"), request.getInt("thumb_size", 0));
			int id = request.getInt("id", 0);
			if (id != 0) {
				NameValuePairs name_value_pairs = new NameValuePairs();
				name_value_pairs.set(request.getParameter("column"), new_filename);
				String table = request.getParameter("table");
				JDBCTable jdbc_table = request.db.getTable(table);
				if (jdbc_table.getColumn("width") != null) {
					int[] size = ImageMagick.getSize(pictures_dir + File.separatorChar + new_filename);
					if (size != null) {
						name_value_pairs.set("width", size[0]);
						name_value_pairs.set("height", size[1]);
					}
				}
				request.db.update(table, name_value_pairs , id);
			}
			http_response.setContentType("text/html");
			file_path = new PathBuilder(null);
			if (is_thumb)
				++n;
			for (int i=1; i<n; i++)
				file_path.append(path_segments[i]);
			file_path.append(new_filename);
			request.writer.write(file_path.toString());
			request.release();
			return true;
		}
		return super.doPost(http_request, http_response);
	}

	//--------------------------------------------------------------------------

	@Override
	public Class<?>[]
	getClasses() {
		return new Class<?>[] { FileColumn.class, PictureColumn.class };
	}

	//--------------------------------------------------------------------------

	protected Decisions
	getDecisionsModule() {
		return new Decisions();
	}

	//--------------------------------------------------------------------------

//	@AdminTask
//	public void
//	importPeople(Request request) {
//		DBConnection db = request.db;
//		db.delete("people", "id != 1");
//		db.delete("user_roles", "people_id != 1");
//		db.delete("families", null);
//		db.delete("homes", null);
//		db.delete("units", null);
//
//		CSVReader csv = new CSVReader(request.site.getBaseFilePath().append("people.csv").toString(), 24);
//
//		NameValuePairs nvp = new NameValuePairs();
//		nvp.set("active", true);
//		String[] line = csv.readLine();
//		while (line != null) {
//			if (db.exists("people", "first='" + line[1] + "' AND last='" + line[7] + "'"))
//				continue;
//			int household_id = db.lookupInt(new Select("id").from("families").where("name='" + line[7] + "'"), -1);
//			if (household_id == -1) {
//				String home_number = line[4];
//				if (!line[6].equals("0"))
//					home_number += " Apt " + line[6];
//				int homes_id = db.lookupInt(new Select("id").from("homes").where("number='" + home_number + "'"), -1);
//				if (homes_id == -1) {
//					int units_id = db.lookupInt(new Select("id").from("units").where("unit='" + line[5] + "'"), -1);
//					if (units_id == -1)
//						units_id = db.insert("units", "unit", "'" + line[5] + "'");
//					homes_id = db.insert("homes", "number,units_id", "'" + home_number + "'," + units_id);
//				}
//				household_id = db.insert("families", "name,homes_id", "'" + line[7] + "'," + homes_id);
//			}
////			First Name,Last Name,Username,Password,House Number,Unit Number,Apartment,Household,Cell Phone,Home Phone,Email,Birthday,Female / Male,Owner / Renter,Adult/Child,Custom 1,Custom 2,Custom 3,Custom 4,Custom 5,Current Resident,Move In Date,Move Out Date,Bio
//			nvp.set("families_id", household_id);
//			nvp.set("email", line[0]);
//			nvp.set("first", line[0]);
//			nvp.set("last", line[1]);
//			nvp.set("user_name", line[0] + line[1]);
//			nvp.set("password", "md5('" + line[0] + line[1] + "')");
//			nvp.set("cell_phone", line[8]);
//			nvp.set("phone", line[9]);
//			nvp.set("email", line[10]);
//			nvp.set("birthday", line[11]);
//			nvp.set("female/male", line[12]);
//			nvp.set("owner", "Owner".equals(line[13]));
//			nvp.set("adult/child", line[14]);
//			nvp.set("bio", line[23]);
//			int people_id = db.insert("people", nvp);
//			db.insert("user_roles", "people_id,role,user_name", people_id + ",'coho','" + line[0] + line[1] + "'");
//			line = csv.readLine();
//		}
//	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		addModule(new LoginPage(), db); // load first to avoid error on quick restarts
		Roles.add("admin");
		Roles.add("calendar editor");
		super.addModule(new EditSite(), db);
		db.createTable("units", "unit VARCHAR(30),hoa_dues REAL");
		JDBCTable.adjustTable(newHomesTable(db), true, false, db);
		JDBCTable.adjustTable(newHouseholdsTable(), true, false, db);
		db.createManyTable("families", "families_pictures", "filename VARCHAR");
		Decisions decisions = getDecisionsModule();
		addModule(decisions, db);
		app.People people = newPeople();
		if (decisions.m_system_is_opt_in)
			people.addColumn(new JDBCColumn("participant", Types.BOOLEAN));
		addModule(people, db);
		db.createManyTable("people", "people_bio_pictures", "filename VARCHAR");
		m_pages.add(new Page("People", "/people.jsp").setRole("coho"), db);

		News news = new News();
		addModule(news, db);

		Accounting accounting = new Accounting();
		addModule(accounting, db);
		addModule(new Backups(), db);
		addModule(new Blackboard(), db);
		addModule(new Contacts(), db);
		Discussions discussions = new Discussions();
		addModule(discussions, db);
		news.addProvider(discussions, this, db);
		((Comments)getModule(discussions.getTable() + "_comments")).setMaxHeight("none");
		Households households = new Households();
		if (households.isActive()) {
			String label  = accounting.getAccountLabel();
			households.addBlock(new Block(label == null ? "HOA" : label) {
				@Override
				public boolean
				handlesContext(String context, Request request) {
					return accounting.isActive();
				}
				@Override
				public void
				write(String context, Request request) {
					request.writer.h3(accounting.getAccountLabel());
					((Households)request.site.getModule("Households")).writeAccountHistory((Person)request.getUser(), request.getInt("id", 0), request);
				}
			});
			households.addBlock(new Block("Community Work") {
				@Override
				public boolean
				handlesContext(String context, Request request) {
					return request.site.getModule("Work").isActive() && request.userHasRole("coho");
				}
				@Override
				public void
				write(String context, Request request) {
					request.writer.h3("Community Work");
					ViewState.setBaseFilter("work_hours person", "_owner_=" + request.getUser().getId(), request);
					request.site.newView("work_hours person", request).writeComponent();
				}
			});
		}
		addModule(households, db);
		addModule(newGroups(), db);
		Documents documents = newDocuments();
		addModule(documents, db); // must be after groups
		news.addProvider(documents, this, db);
		addModule(new HomePage(), db);
		if (households.isActive())
			addModule(new HouseholdEventProvider(), db);
		Lists lists = (Lists)new Lists().addColumn(new JDBCColumn("groups"));
		addModule(lists, db);
		MailLists mail_lists = new MailLists();
		mail_lists.addMailHandlerFactory(new GroupMailHandlerFactory());
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Everyone"));
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Owners").setFilter("owner"));
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Residents").setFilter("resident"));
		mail_lists.addMailHandler("pictures", new PicturesMailHandler(m_base_path));
		addModule(mail_lists, db);
		EventProvider.setReminderHandler(new ReminderHandler(){
			@Override
			public boolean
			handleReminder(Mail mail, EventProvider event_provider, Event event, String from, String recipient, String subject, String content, Site site, DBConnection dbc) {
				if ("poster".equals(recipient)) {
					recipient = dbc.lookupString(new Select("email").from("people").whereIdEquals(event.getOwner()));
					if (event_provider instanceof GroupEventProvider) {
						String list = dbc.lookupString(new Select("name").from("mail_lists").where("send_to='Group:" + ((GroupEventProvider)event_provider).getGroupID() + "'"));
						if (list != null) {
							mail.send(site.getEmailAddress(list), recipient, "Reminder - " + subject, content, true);
							return true;
						}
					}
					mail.send(recipient, "Reminder - " + subject, content, true);
				} else {
					if (!recipient.endsWith(site.getDomain()))
						return false;
					String list = recipient.substring(0,  recipient.indexOf('@'));
					if (!dbc.exists("mail_lists", "name ILIKE '" + list + "'"))
						return false;
					try {
						mail.setFrom(from);
						mail.setContent(content, true);
						mail.setSubject("Reminder - " + subject);
						MimeMessage message = mail.getMessage();
						message.setRecipient(RecipientType.TO, new InternetAddress(recipient));
						((MailLists)site.getModule("MailLists")).handleMessage(list, message, false, true, dbc);
					} catch (MessagingException e) {
						site.log(e);
					}
				}
				return true;
			}
		});
		addModule(new Meetings(), db);
		Minutes minutes = (Minutes)new Minutes() {
			@Override
			protected String
			getItemTitle(int item_id, Request request) {
				int groups_id = request.db.lookupInt(new Select("groups_id").from("minutes").whereIdEquals(item_id), 0);
				if (groups_id != 0)
					return request.db.lookupString(new Select("name").from("groups").whereIdEquals(groups_id));
				return super.getItemTitle(item_id, request);
			}
		}.addColumn(new JDBCColumn("groups")).addColumn(new JDBCColumn("show_on_main_minutes_page", Types.BOOLEAN).setDefaultValue(true));
		addModule(minutes, db);
		news.addProvider(minutes, this, db);
		NewsFeed news_feed = new NewsFeed();
		addModule(news_feed, db);
		news.addProvider(news_feed, this, db);
		Pictures pictures = new Pictures();
		pictures.addColumn(new JDBCColumn("can_show_on_login_page", Types.BOOLEAN).setDefaultValue(true));
		addModule(pictures, db);
		news.addProvider(pictures, this, db);
		Recipes recipes = newRecipes();
		addModule(recipes, db);
		news.addProvider(recipes, this, db);
		Reviews reviews = new Reviews();
		addModule(reviews, db);
		news.addProvider(reviews, this, db);
		SiteMenu site_menu = new SiteMenu();
		addModule(site_menu, db);
		addModule(new Surveys(), db);
		addModule(new Tasks(), db);
		addModule(new TimeBank(), db);
		addModule(new ToDos(){
			@Override
			public boolean
			accept(Rows rows, Request request) {
				int household_id = rows.getInt("families_id");
				if (household_id != 0)
					return household_id == ((Person)request.getUser()).getHousehold(request).getId();
				return false;
			}
		}.setAssignTasks(true).setGroupByProject(true).setOneTable("families"), db);
		addModule(new WebMail(), db);
		addModule(new Work(), db);

		db.createTable("locations", "text VARCHAR");
		if (db.getTable("people").getColumn("birthday") != null)
			addModule(new BirthdaysEventProvider(), db);
		addModule(new MosaicEventProvider("Main Calendar")
			.addEventProvider((EventProvider)getModule("General Meetings")) // from Decisions module
			.setAllowViewSwitching(true)
			.setCalendarPicker(new CalendarPicker("Main Calendar"))
			.setColor("#F0F8A1")
			.setEventsCanRepeat(true)
			.setEventsHaveCategory(true)
			.setEventsHaveLocation(true)
			.setEventsHaveTime(true)
			.setSupportRegistrations(true)
			.setSupportReminders(), db);
		EventProvider common_meals = newMealEventProvider();
		addModule(common_meals, db);
		if (households.isActive())
			households.addBlock(new Block(common_meals.getDisplayName(null)) {
				@Override
				public boolean
				handlesContext(String context, Request request) {
					return common_meals.isActive();
				}
				@Override
				public void
				write(String context, Request request) {
					request.writer.h3(common_meals.getDisplayName(request));
					((MealEventProvider)common_meals).writeMealHistory(request);
				}
			});
		addGuestRoomModule(db);

		super.init(db);
		site_menu.setHelpURL("https://docs.cohousing.site").setHomePage(getHomePage());

		String smtp = getSettings().getString("smtp");
		if (smtp != null && smtp.length() > 0) {
			Thread thread = new Thread(new FiveMinuteThread(this));
			addThread(thread);
			thread.start();
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask()
	public static void
	listReminders(String date, Request request) {
		Table table = request.writer.ui.table().addClass("table table-condensed table-bordered");
		table.tr().th("id").th("note").th("event").th("repeat").th("end_date").th("num").th("unit").th("before").th("email").th("date");
		for (Module module : request.site.getModules())
			if (module instanceof EventProvider) {
				EventProvider event_provider = (EventProvider)module;
				if (event_provider.supportsReminders())
					event_provider.listReminders(null, table, request);
			}
		table.close();
	}

	//--------------------------------------------------------------------------

	protected Documents
	newDocuments() {
		return (Documents)new Documents().setDirColumn("groups_id").addColumn(new JDBCColumn("groups")).addColumn(new JDBCColumn("show_on_main_docs_page", Types.BOOLEAN).setDefaultValue(true));
	}

	//--------------------------------------------------------------------------

	protected JDBCTable
	newHomesTable(DBConnection db) {
		return new JDBCTable("homes")
			.add(new JDBCColumn("number", Types.VARCHAR).setUnique(true))
			.add(new JDBCColumn("units_id", Types.INTEGER))
			.add(new JDBCColumn("pays_hoa", "families").setOnDeleteSetNull(true))
			.add(new JDBCColumn("pays_water", "families").setOnDeleteSetNull(true));
	}

	//--------------------------------------------------------------------------

	protected JDBCTable
	newHouseholdsTable() {
		return Households.getTable();
	}

	//--------------------------------------------------------------------------

	protected Groups
	newGroups() {
		return new Groups();
	}

	//--------------------------------------------------------------------------

	protected EventProvider
	newMealEventProvider() {
		return new MealEventProvider(false).setColor("#7FDBFF").setOneEventPerDay(true);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Pages
	newPages() {
		return super.newPages().setSupportPagesTable(true);
	}

	//--------------------------------------------------------------------------

	protected app.People
	newPeople() {
		return (People)new mosaic.People()
			.addColumn(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.addColumn(new JDBCColumn("address", Types.VARCHAR))
			.addColumn(new JDBCColumn("bio", Types.VARCHAR))
			.addColumn(new JDBCColumn("birthday", Types.DATE))
			.addColumn(new JDBCColumn("can_post_to_mail_lists", Types.BOOLEAN).setDefaultValue(true))
			.addColumn(new JDBCColumn("emergency_contacts", Types.VARCHAR))
			.addColumn(new JDBCColumn("families_id", Types.INTEGER))
			.addColumn(new JDBCColumn("middle", Types.VARCHAR))
			.addColumn(new JDBCColumn("owner", Types.BOOLEAN))
			.addColumn(new JDBCColumn("phone", Types.VARCHAR))
			.addColumn(new JDBCColumn("resident", Types.BOOLEAN))
			.addColumn(new JDBCColumn("show_birthday_on_calendar", Types.BOOLEAN).setDefaultValue(true));
	}

	//--------------------------------------------------------------------------

	@Override
	public app.Person
	newPerson(Request request) {
		return new Person(request);
	}

	//--------------------------------------------------------------------------

	@Override
	public app.Person
	newPerson(int id, DBConnection db) {
		return new Person(id, db);
	}

	//--------------------------------------------------------------------------

	protected Recipes
	newRecipes() {
		return (Recipes)new Recipes() {
				@Override
				public void
				afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
					if (m_add_new_items_to_news_feed) {
						boolean show_on_news_feed = name_value_pairs.getBoolean("coho");
						if (show_on_news_feed)
							addNewsItem(id, name_value_pairs, request);
					}
				}
				@Override
				public String
				beforeUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
					if (name_value_pairs.valueChanged("coho", m_table, id, request.db))
						if (name_value_pairs.getBoolean("coho"))
							addNewsItem(id, name_value_pairs, request);
						else
							request.db.delete("news", "provider='" + m_name + "' AND item_id=" + id, false);
					return null;
				}
				@Override
				public String
				getDescription() {
					return "Store recipes for households that can also be shared with the community.";
				}
			}
			.addColumn(new JDBCColumn("coho", Types.BOOLEAN))
			.addColumn(new JDBCColumn("families_id", "families"));
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	newViewDef(String name) {
		if (name.equals("docs"))
			return newViewDef("documents")
				.addSection(new Dividers("groups_id", new OrderBy("groups_id", false, true)).setNullValueLabel(null))
				.setBaseFilter("groups_id IS NULL OR (SELECT active FROM groups WHERE groups.id=groups_id)")
				.setName("docs")
				.setShowColumnHeads(false)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDisplayName("Group"));
		if (name.equals("edit documents"))
			return super.newViewDef(name)
				.setBaseFilter("groups_id IS NULL OR (SELECT active FROM groups WHERE groups.id=groups_id)")
				.setDefaultOrderBy("groups_id NULLS FIRST,filename")
				.setColumnNamesTable(new String[] { "groups_id", "filename", "type", "_timestamp_" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDisplayName("Group"));
		if (name.equals("homes"))
			return new ViewDef(name)
				.setDefaultOrderBy("naturalsort(number)")
				.setRecordName("Home")
				.setColumnNamesForm(new String[] { "number", "units_id", "pays_hoa", "pays_water" })
				.setColumnNamesTable(new String[] { "number", "units_id" })
				.setColumn(new Column("number").setIsUnique(true))
				.setColumn(new LookupColumn("pays_hoa", "families", "name", "active", "name").setAllowNoSelection(true))
				.setColumn(new LookupColumn("pays_water", "families", "name", "active", "name").setAllowNoSelection(true).setViewRole("administrator"))
				.setColumn(new LookupColumn("units_id", "units", "unit").setAllowEditing().setAllowNoSelection(true).setDisplayName("unit"));
		if (name.equals("lostfound"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setRecordName("Item")
				.setColumn(new Column("date").setDefaultToShortDate())
				.setColumn(new OptionsColumn("what", "lost", "found"));
		if (name.equals("minutes"))
			return super.newViewDef(name)
				.setDefaultOrderBy("groups_id NULLS FIRST,date")
				.setViewClass(GroupMinutesView.class)
				.setColumnNamesTable(new String[] { "date", "groups_id", "summary" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDisplayName("group"));
		if (name.equals("movies")) {
			SelectRenderer select_renderer = new SelectRenderer(new Select("trim(first) || ' ' || trim(last)").from("people").orderBy("first,last"), null, null).setAllowNoSelection(true);
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setDefaultOrderBy("title")
				.setRecordName("Movie")
				.setColumn(new OptionsColumn("format", "DVD", "VHS"))
				.setColumn(new OptionsColumn("genre", "Action/Adventure", "Classic", "Comedy", "Drama", "Hong Kong", "Horror/Suspense", "Japanese Animation", "Kids/Family", "Romance", "SciFi/Fantasy", "Thriller", "Other/Misc"))
				.setColumn(new Column("on_loan_to").setInputRenderer(select_renderer))
				.setColumn(new Column("owner").setInputRenderer(select_renderer));
		}
		if (name.equals("recipes"))
			return super.newViewDef(name)
				.setColumnNamesForm(new String[] { "families_id", "title", "ingredients", "directions", "category", "coho", "_owner_" })
				.setColumn(new Column("coho").setDisplayName("share with community").setHideOnForms(Mode.READ_ONLY_FORM))
				.setColumn(new HouseholdIdColumn());
		if (name.equals("recommendations"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setRecordName("Recommendation");
		if (name.endsWith("_reminders")) {
			ViewDef view_def = super.newViewDef(name);
			EmailColumn email_column = (EmailColumn)view_def.getColumn("email");
			if (email_column != null)
				email_column.setAddressProvider(new EmailAddressProvider() {
					@Override
					public List<String>
					getAddresses(Request request) {
						ArrayList<String> addresses = request.db.readValues(new Select("name").from("mail_lists").where("active").orderBy("name"));
						for (int i=0; i<addresses.size(); i++)
							addresses.set(i, request.site.getEmailAddress(addresses.get(i)));
						if ("Common Meals_reminders".equals(name)) {
							String[] jobs = ((MealEventProvider)request.site.getModule("Common Meals")).getJobs();
							for (int i=0; i<jobs.length; i++)
								addresses.add(i, jobs[i].replace('_', ' '));
							addresses.add(0, "cook");
							addresses.add(0, "diners");
							addresses.add(0, "workers");
						} else
							addresses.add(0, "poster");
						return addresses;
					}
				});
			return view_def;
		}
		if (name.endsWith("suggestions"))
			return new ViewDef(name)
				.setDefaultOrderBy("date")
				.setRecordName("Suggestion")
				.setColumn(new Column("date").setDefaultToShortDate().setIsSortable(true))
				.setColumn(new LookupColumn("people_id", "people", "first,last").setDisplayName("suggested by").setDefaultToUserId().setIsReadOnly(true));
		if (name.equals("surveys"))
			return super.newViewDef(name)
				.setRowTemplate(new String[] {
					"<p><div class=\"survey_block\"><div class=\"survey_title\">$(title)</div>",
					"<p style=\"width:600px\">$(description)</p><span style=\"font-size:9pt\">",
					"$(take)",
					" - $(show_answers)",
					"</span><div style=\"padding:5px;text-align:right\">$(@buttons)</div>",
					"</div></p>"
				});
		if (name.equals("todos"))
			return super.newViewDef("todos")
				.setColumn(new HouseholdIdColumn());
		if (name.equals("units"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("people").add().delete().edit())
				.setRecordName("Unit")
				.setDefaultOrderBy("unit")
				.setColumn(new CurrencyColumn("hoa_dues"));
		if (name.equals("values"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("docs").add().delete().edit());

		return super.newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	nightly(LocalDateTime now, DBConnection db) {
		super.nightly(now, db);
//		Reminders.cleanLog(now.toLocalDate(), db);
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public static void
	searchPeople(int id, Request request) {
		String tables[] = request.db.getTableNames(false);
		for (String table : tables) {
			JDBCTable t = request.db.getTable(table);
			if (t.getColumn("_owner_") != null && request.db.rowExists(table, "_owner_=" + id))
				request.writer.write(table);
			if (t.getColumn("people_id") != null && request.db.rowExists(table, "people_id=" + id))
				request.writer.write(table);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	showRememberMeCheckbox() {
		return ((LoginPage)getModule("LoginPage")).showRememberMeCheckbox();
	}

	//--------------------------------------------------------------------------

	public void
	writeDocs(Request request) {
		ViewState.setBaseFilter("documents", "groups_id IS NULL OR (SELECT active FROM groups WHERE groups.id=groups_id)", request);
		TreeSet<String> labels = new TreeSet<>();
		List<String> types = request.db.readValues(new Select("text").from("documents_types"));
		labels.addAll(types);

		View documents = request.site.newView("docs", request);
		request.writer.write("<div style=\"text-align:right\">");
		TSVectorSearch search = new TSVectorSearch(null, null);
		search.init(documents.getViewDef());
		String quicksearch = documents.getState().getQuicksearch();
		if (quicksearch != null && quicksearch.length() > 0) {
			request.writer.write("documents");
			search.writeRowWindowControlsSpan(documents, request);
		} else
			search.write(null, documents, request);
		request.writer.write("</div><div style=\"height:10px;\"></div>");
		Tabs tabs = request.writer.ui.tabs("documents").open();
		documents.setWhere("show_on_main_docs_page AND type IS NULL");
		documents.select();
		if (documents.next() && documents.data.getString("type") == null || types.size() == 0) {
			tabs.pane("Documents");
			documents.write();
		}
		for (String label : labels) {
			documents.setWhere("show_on_main_docs_page AND type=" + DBConnection.string(label));
			documents.select();
			if (documents.next()) {
				tabs.pane(Text.pluralize(label));
				documents.write();
			}
		}
		tabs.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writePageOpen(String current_page, Head head, Request request) {
		if (head != null)
			head.script("flatpickr.min", false).styleSheet("flatpickr.min", false).script("tags", false).close();
		request.writer.write("<div class=\"above_menu\"></div>");
		if ("household".equals(current_page)) {
			Person person = (Person)request.getUser();
			if (person != null) {
				Household household = person.getHousehold(request);
				if (household != null)
					current_page = household.getName();
			}
		}
		SiteMenu site_menu = (SiteMenu)getModule("SiteMenu");
		if (site_menu != null)
			site_menu.write(current_page, request);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeSettingsInput(Object object, Field field, Request request) {
		String name = field.getName();
		if (name.startsWith("m_"))
			name = name.substring(2);
		String type = field.getAnnotation(JSONField.class).type();
		switch(type) {
		case "expense account":
			try {
				Integer id  = (Integer)field.get(object);
				new RowsSelect(name, new Select("id,name").from("accounts").where("active AND (type=2 OR type=5)").orderBy("name"), "name", "id", request).addFirstOption(null, "0").setSelectedOption(null, id == null ? null : id.toString()).write(request);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "income account":
			try {
				Integer id  = (Integer)field.get(object);
				new RowsSelect(name, new Select("id,name").from("accounts").where("active AND (type=1 OR type=4)").orderBy("name"), "name", "id", request).addFirstOption(null, "0").setSelectedOption(null, id == null ? null : id.toString()).write(request);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "location":
			try {
				Integer id  = (Integer)field.get(object);
				new RowsSelect(name, new Select("id,text").from("main_calendar_events_locations").orderBy("text"), "text", "id", request).setAllowNoSelection(true).setSelectedOption(null, id == null ? null : id.toString()).write(request);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "mail list":
			try {
				new RowsSelect(name, new Select("name").from("mail_lists").where("active").orderBy("name"), "name", null, request).addFirstOption("webmail", null).setAllowNoSelection(true).setSelectedOption((String)field.get(object), null).write(request);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		}
		return super.writeSettingsInput(object, field, request);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeUserDropdownItems(NavBar nav_bar, Request request) {
		Module families = request.site.getModule("Households");
		if (families != null && families.isActive()) {
			Person person = (Person)request.getUser();
			if (person != null && person.getHousehold(request) != null) {
				nav_bar.a(person.getHousehold(request).getName(), request.getContext() + "/household.jsp");
				nav_bar.divider();
			}
		}
		super.writeUserDropdownItems(nav_bar, request);
	}
}
