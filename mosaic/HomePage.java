package mosaic;

import java.io.File;
import java.lang.reflect.Field;

import org.apache.commons.fileupload.FileItem;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Module;
import app.Request;
import calendar.EventProvider;
import calendar.ListView;
import db.object.JSONField;
import social.News;
import web.HTMLWriter;
import web.Head;

public class HomePage extends Module {
	@JSONField(label="null", type="file")
	private String		m_banner = "banner.png";
	private JsonObject	m_page_template = Json.parse("{\"blocks\":["
			+ "{\"type\":\"banner\"},"
			+ "{\"type\":\"blackboard\"},"
			+ "{\"type\":\"html\"},"
			+ "{\"type\":\"pictures\"},"
			+ "{\"type\":\"news feed\"},"
			+ "{\"type\":\"upcoming\"}"
			+ "]}").asObject();
	@JSONField(choices = { "random", "recent" }, label="null")
	private String		m_pictures = "random";
	@JSONField(fields = {"m_banner"})
	private boolean		m_show_banner;
	@JSONField
	private boolean		m_show_blackboard;
	@JSONField
	private boolean		m_show_news_feed = true;
	@JSONField(fields = {"m_pictures"})
	private boolean		m_show_pictures;
	@JSONField(fields = {"m_text_box"})
	private boolean		m_show_text_box;
	@JSONField
	private boolean		m_show_upcoming = true;
	@JSONField(label="null", type="html")
	private String		m_text_box;
	@JSONField(admin_only=true)
	private boolean		m_upcoming_all_calendars = true;

	// --------------------------------------------------------------------------

	public
	HomePage() {
		m_renameable = false;
		m_required = true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		writePage(request);
		return true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;

		switch(request.getPathSegment(1)) {
		case "Banner":
			FileItem file_item = request.getFileItem("file");
			String filename = file_item.getName();
			int index = filename.lastIndexOf('/');
			if (index != -1)
				filename = filename.substring(index + 1);
			index = filename.lastIndexOf('\\');
			if (index != -1)
				filename = filename.substring(index + 1);
			String path = request.site.getBasePathBuilder().append("images").append(filename).toString();
			try {
				file_item.write(new File(path));
			} catch (Exception e) {
				request.site.log(e);
			}
			m_banner = filename;
			store(request.db);
			request.writer.write("dom.$('banner').value='" + filename + "';dom.$('banner_img').src='" + request.getContext() + "/images/" + filename + "';");
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks() {
		return new String[][] { { "Home Page", "/HomePage/settings" } };
	}

	//--------------------------------------------------------------------------

	private void
	writePage(Request request) {
		HTMLWriter writer = request.writer;
		Head head = request.site.newHead(request);
		if (m_show_news_feed && request.site.getModule("NewsFeed").isActive())
			head.script("gallery-min").styleSheet("gallery-min");
		request.site.writePageOpen("Home", head, request);
		if (!request.userHasRole("coho"))
			return;
		writer.write("<div class=\"container\"><div class=\"row\">");
		JsonArray blocks = m_page_template.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			switch(block.getString("type", null)) {
			case "banner":
				if (m_show_banner && m_banner != null) {
					writer.write("<div style=\"padding:10px;text-align:center;width:100%;\"><img src=\"");
					writer.write(request.getContext());
					writer.write("/images/" + m_banner + "\" /></div>");
				}
				break;
			case "blackboard":
				if (m_show_blackboard) {
					Blackboard blackboard = (Blackboard)request.site.getModule("Blackboard");
					if (blackboard != null && blackboard.isActive())
						blackboard.writeComponent(request);
				}
				break;
			case "html":
				if (m_show_text_box)
					writer.write("<div class=\"alert alert-secondary\" style=\"display:table;margin:10px auto;\">").write(m_text_box).write("</div></div><div class=\"row\">");
				break;
			case "news feed":
				if (m_show_news_feed && request.site.getModule("NewsFeed").isActive()) {
					writer.write("<div class=\"col-md-6\">");
					News news = (News)request.site.getModule("News");
					news.writeStatusForm(request);
					news.writeItems(request);
					writer.write("</div>");
				}
				break;
			case "pictures":
				if (m_show_pictures) {
					writer.write("<div class=\"col-md-12\">");
					new SlideShow().write(m_pictures, false, request);
					writer.write("</div>");
				}
				break;
			case "upcoming":
				if (m_show_upcoming) {
					request.setData("upcoming", Boolean.TRUE);
					writer.write("<div id=\"upcoming\" class=\"col-md-6\"><style>#upcoming > div {margin: 0 auto;}#upcoming > div > table {padding:0}</style>");
					new ListView((EventProvider)request.site.getModule("Main Calendar"), request).setAllCalendars(m_upcoming_all_calendars).writeComponent(false, request);
					writer.write("</div>");
					request.setData("upcoming", null);
				}
				break;
			}
		}
		writer.write("</div></div>");
		writer.js("gallery=new Gallery({dir:'pictures',show_comments:true});");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request request) {
		if ("m_banner".equals(field.getName()))
			request.writer.setAttribute("id", "banner_img").setAttribute("style", "max-height:100px").img(m_banner).space().aOnClick("replace", "Dialog.upload(context+'/HomePage/Banner')");
		else
			super.writeSettingsInput(field, request);
	}
}