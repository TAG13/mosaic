package mosaic;

import java.sql.Types;
import java.time.LocalDateTime;

import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import ui.Table;
import web.HTMLWriter;

public class Blackboard extends SiteModule {
	@JSONField private String m_title = "Community Notices";

	//--------------------------------------------------------------------------

	public
	Blackboard() {
		super();
		m_is_secured = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		String add = request.getParameter("add");
		if (add != null)
			request.db.insert("blackboard", "text", DBConnection.string(add));
		int id = request.getInt("delete", 0);
		if (id != 0)
			request.db.delete("blackboard", id, false);
		write(request);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Provides a place on the login page where anyone can write a message. Messages are cleared before each day.";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("blackboard")
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	nightly(LocalDateTime now, Site site, DBConnection db) {
		LoginPage login_page = (LoginPage)site.getModule("LoginPage");
		if (login_page != null && !login_page.showBlackboard())
			return;
		db.truncateTable("blackboard");
	}

	//--------------------------------------------------------------------------

	public void
	write(Request request) {
		HTMLWriter writer = request.writer;
		writer.setAttribute("class", "bb");
		int mark = writer.tagOpen("div");
		writer.setAttribute("class", "bb_title");
		writer.tag("span", m_title);
		writer.setAttribute("class", "bb_frame");
		writer.tagOpen("div");
		writer.setAttribute("class", "bb_board");
		writer.tagOpen("div");
		Table table = writer.ui.table().addStyle("width", "100%").borderSpacing(10);
		Rows rows = new Rows(new Select("id,text").from("blackboard").orderBy("_timestamp_"), request.db);
		while (rows.next()) {
			if (rows.getRow() % 3 == 1)
				table.tr();
			writer.setAttribute("onmouseout", "this.lastChild.style.visibility='hidden';");
			writer.setAttribute("onmouseover", "this.lastChild.style.visibility='';");
			table.td();
			writer.write(rows.getString(2));
			writer.space();
			writer.setAttribute("style", "visibility:hidden;");
			writer.ui.buttonOnClick("delete", "Dialog.confirm('Delete this entry?',function(){net.replace(bb_id,context+'/Blackboard?delete=" + rows.getString(1) + "')})");
		}
		rows.close();
		table.close();
		writer.tagClose();
		writer.write("<input type=\"text\" id=\"bbtext\" size=\"80\" maxlength=\"80\" class=\"form-control input-sm\" style=\"display:inline;margin-top:5px;padding:2px;width:inherit;\"> <a onclick=\"net.replace(bb_id,context+'/Blackboard?add='+encodeURIComponent(dom.$('bbtext').value));return false;\" class=\"btn btn-default btn-sm\" href=\"#\">Add</a>");
		writer.tagsCloseTo(mark);
	}

	//--------------------------------------------------------------------------

	public void
	writeComponent(Request request) {
		HTMLWriter writer = request.writer;
		String id = writer.addStyle("margin:0 auto").componentOpen(request.getContext() + "/Blackboard", null);
		writer.js("var bb_id='" + id + "';");
		write(request);
		writer.tagClose();
	}
}
