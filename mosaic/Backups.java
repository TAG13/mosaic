package mosaic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.Array;
import app.Module;
import app.Request;
import app.Time;
import ui.NavList;
import web.HTMLWriter;

public class Backups extends Module {
	private void
	addFile(File file, ZipOutputStream zos, int base_length) {
		byte[] buffer = new byte[1024];
		String file_path = file.getAbsolutePath();
		FileInputStream in = null;
        try {
			ZipEntry zip_entry = new ZipEntry(file_path.substring(base_length));
			zip_entry.setTime(0);
			zos.putNextEntry(zip_entry);
			try {
				in = new FileInputStream(file_path);
				int len;
				while ((len = in.read(buffer)) > 0)
					zos.write(buffer, 0, len);
			} finally {
			   in.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	addFiles(File node, ZipOutputStream zos, int base_length, String[] skip) {
		if (node.isFile())
			addFile(node, zos, base_length);
		if (node.isDirectory()) {
			if (skip != null && Array.indexOf(skip, node.getName()) != -1)
				return;
			File[] files = node.listFiles();
			for (File file : files)
				addFiles(file, zos, base_length, skip);
		}
	}

	//--------------------------------------------------------------------------

	private void
	backup(String directory, String[] skip, Request request) {
		request.response.setContentType("application/octet-stream;");
		request.response.setHeader("Content-disposition", "attachment; filename=\"" + directory + ".zip\"");
		String base_path = request.site.getBasePathBuilder().toString();
		try {
			ZipOutputStream zos = new ZipOutputStream(request.response.getOutputStream());
			addFiles(new File(base_path + "/" + directory), zos, base_path.length(), skip);
			zos.close();
		} catch (IOException e) {
			request.abort(e);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(HttpServletRequest http_request, HttpServletResponse http_response) throws IOException {
		Request request = new Request(http_request, http_response, null);
		String segment_one = request.getPathSegment(1);
		if (segment_one != null) {
			if ("decisions".equals(segment_one))
				backup("decisions", null, request);
			else if ("documents".equals(segment_one))
				backup("documents", null, request);
			else if ("mail".equals(segment_one))
				backup("mail", null, request);
			else if ("minutes attachments".equals(segment_one))
				backup("minutes", null, request);
			else if ("people".equals(segment_one))
				backup("people", null, request);
			else if ("pictures".equals(segment_one))
				backup("pictures", new String[] { "thumbs" }, request);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		write(request);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks() {
		return new String[][] { { "Backups", "/Backups" } };
	}

	//--------------------------------------------------------------------------

	private void
	write(Request request) {
		HTMLWriter writer = request.writer;
		NavList nav_list = writer.ui.navList().open();
		String dir = request.site.getBasePathBuilder().toString();
		String[] files = new File(dir).list(new FilenameFilter(){
			@Override
			public boolean
			accept(File directory, String name) {
				return name.endsWith(".zip");
			}
		});
		if (files.length > 0) {
			nav_list.heading("Database Backups");
			for (String file : files)
				nav_list.a(file + " (" + Time.formatter.getDateTime(app.File.modified(new File(dir + '/' + file))) + ")" , request.getContext() + "/" + file);
		}
		nav_list.heading("File Backups");
		nav_list.a("decisions", request.getContext() + "/Backups/decisions");
		nav_list.a("documents", request.getContext() + "/Backups/documents");
		nav_list.a("minutes attachments", request.getContext() + "/Backups/minutes attachments");
		nav_list.a("mail list archives", request.getContext() + "/Backups/mail");
		nav_list.a("people icons", request.getContext() + "/Backups/people");
		nav_list.a("pictures", request.getContext() + "/Backups/pictures");
//		nav_list.a("site", request.getContext() + "/Backups/site");
		nav_list.close();
	}
}
