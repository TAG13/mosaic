package mosaic;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import accounting.Accounting;
import app.Array;
import app.Block;
import app.Person;
import app.Request;
import app.Site;
import app.Text;
import app.Time;
import biweekly.component.VEvent;
import calendar.Event;
import calendar.EventProvider;
import calendar.EventViewDef;
import calendar.MonthView;
import calendar.Reminder;
import db.AnnualReport;
import db.DBConnection;
import db.Form;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.DateColumn;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.column.PersonColumn;
import db.column.TimeColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.QuarterFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.object.JSONField;
import db.section.Dividers;
import households.Household;
import ui.Table;
import web.HTMLWriter;
import web.Stringify;

@Stringify
public class GuestRoomEventProvider extends MosaicEventProvider {
	private final class GuestRoomEvent extends Event {
		String					m_guest;
		boolean					m_invoiced;
		private LocalDate		m_original_start_date;
		private LocalDateTime	m_timestamp;

		//--------------------------------------------------------------------------

		GuestRoomEvent(GuestRoomEventProvider event_provider, Rows rows) {
			super(event_provider, rows);
		}

		//--------------------------------------------------------------------------

		@Override
		public void
		adjustTimezone(ZoneId zone_id) {
			if (m_start_time != null) {
				ZonedDateTime zoned_date_time = ZonedDateTime.of(m_start_date, m_start_time, Time.zone_id);
				zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
				m_start_date = zoned_date_time.toLocalDate();
				m_start_time = zoned_date_time.toLocalTime();
			}
			if (m_end_date != null && m_end_time != null) {
				ZonedDateTime zoned_date_time = ZonedDateTime.of(m_end_date, m_end_time, Time.zone_id);
				zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
				m_end_date = zoned_date_time.toLocalDate();
				m_end_time = zoned_date_time.toLocalTime();
			}
		}

		//--------------------------------------------------------------------------

		@Override
		public void
		appendEventHTML(LocalDate date, StringBuilder s, boolean black_text, Request request) {
			super.appendEventHTML(date, s, black_text, request);
			if (m_charge_for_use && m_invoice_account_id != 0 && request.getUser() != null && "Accounting".equals(request.getSessionAttribute("current page")))
				if (m_invoiced)
					s.append("<br />invoiced");
				else
					s.append("<br />").append(request.writer.ui.button("invoice").setOnClick("event.stopPropagation();net.post(context+'/" + m_event_provider.getName() + "/" + m_id + "/invoice',null,function(){net.replace('Accounting');});return false;").toString());
		}

		//--------------------------------------------------------------------------

		@Override
		protected void
		appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request request) {
			switch(item) {
			case "guest":
				super.appendItem("guest", m_guest, "calendar_event_text", black_text, html, ics, s);
				return;
			case "posted by":
				if ((((GuestRoomEventProvider)m_event_provider).showEventOwner() || m_event_text == null || m_event_text.length() == 0 || ics) && m_owner != 0) {
					/*if (html && ((GuestRoomEventProvider)m_event_provider).showEventOwnerOnMouseOver())
						s.append("<div onmouseover=\"this.firstChild.style.visibility='';\" onmouseout=\"this.firstChild.style.visibility='hidden';\"><div style=\"visibility:hidden;\">");
					else*/ if (s.length() > 0)
						s.append(ics ? " " : "\n");
					StringBuilder i = new StringBuilder();
					String label = ((GuestRoomEventProvider)m_event_provider).m_reserved_by_label;
					if (label != null)
						i.append(label).append(": ");
					i.append(request.site.lookupName(m_owner, request.db));
					super.appendItem(null, i.toString(), "calendar_event_text", black_text, html, ics, s);
//					if (html && ((GuestRoomEventProvider)m_event_provider).showEventOwnerOnMouseOver())
//						s.append("</div></div>");
				}
				return;
			case "time":
				if (m_start_time != null && !ics) {
					if (html)
						s.append("<span class=\"calendar_time\">");
					else if (s.length() > 0)
						s.append("\n");
					if (date == null) // when writing .ics
						s.append("check in: ").append(Time.formatter.getTime(m_start_time, true)).append("\n").append("check out: ").append(Time.formatter.getTime(m_end_time, true));
					else {
						boolean start = ChronoUnit.DAYS.between(date, m_original_start_date) == 0;
						boolean end = ChronoUnit.DAYS.between(date, m_end_date) == 0;
						if (start && end)
							Time.formatter.appendTimeRange(m_start_time, m_end_time, s);
						else if (start)
							s.append("check in: ").append(Time.formatter.getTime(m_start_time, true));
						else if (end)
							s.append("check out: ").append(Time.formatter.getTime(m_end_time, true));
					}
					if (html)
						s.append("</span>");
				}
				break;
			default:
				super.appendItem(item, black_text, html, ics, date, s, request);
			}
		}

		//--------------------------------------------------------------------------

		@Override
		public int
		compareTo(Event event) {
			if (!(event instanceof GuestRoomEvent))
				return super.compareTo(event);
			int relation = m_start_date.compareTo(event.getStartDate());
			if (relation == 0 && m_timestamp != null)
				relation = m_timestamp.compareTo(((GuestRoomEvent)event).m_timestamp);
			return relation;
		}

		//--------------------------------------------------------------------------

		@Override
		protected void
		load(Rows rows) {
			super.load(rows);
			m_notes = m_event_text;
			m_event_text = null;
			if (((GuestRoomEventProvider)m_event_provider).m_use_guest_field)
				m_guest = rows.getString("guest_name");
			m_invoiced = rows.getBoolean("invoiced");
			m_original_start_date = m_start_date;
			m_timestamp = rows.getTimestamp("_timestamp_");
		}

		//--------------------------------------------------------------------------

		@Override
		public VEvent
		newVEvent(Request request) {
			VEvent vevent = new VEvent();
			StringBuilder summary = new StringBuilder();
			for (String item: m_event_provider.getIcalItems())
				appendItem(item, true, false, true, null, summary, request);
			vevent.setSummary(summary.toString());
			ZoneId zone_id = ZoneId.of(request.site.getSettings().getString("time zone"));
			if (m_start_time != null)
				vevent.setDateStart(java.util.Date.from(ZonedDateTime.of(m_start_date, m_start_time, zone_id).toInstant()));
			else
				vevent.setDateStart(java.util.Date.from(m_start_date.atStartOfDay(zone_id).toInstant()), false);
			if (m_end_time != null)
				vevent.setDateEnd(java.util.Date.from(ZonedDateTime.of(m_end_date, m_end_time, zone_id).toInstant()));
			else
				vevent.setDateEnd(java.util.Date.from(m_end_date.plusDays(1).atStartOfDay(zone_id).toInstant()), false);
			return vevent;
		}
	}

	private String		m_controls;
	private ViewDef		m_accounting_view_def;
	@JSONField
	String[]			m_agreements;
	@JSONField(fields = {"m_fee_description","m_rate_categories","m_rates","m_invoice_account_id","m_nights_per_year","m_household_or_home"})
	boolean				m_charge_for_use;
	@JSONField
	private String		m_fee_description = "Guest Room Fee";
	@JSONField(choices = {"household","home/unit"}, label = "per")
	String				m_household_or_home;
	@JSONField(type = "income account")
	int					m_invoice_account_id;
	@JSONField
	int					m_nights_per_year;
	@JSONField(after = "(leave empty to let user enter amount)")
	int	[]				m_rates;
	@JSONField
	private String[]	m_rate_categories;
	@JSONField
	private boolean		m_reservation_report_by_quarter;
	@JSONField
	String				m_reserved_by_label = "reserved by";
	@JSONField
	private boolean		m_show_event_owner = true;
//	@JSONField
//	private boolean		m_show_event_owner_on_mouse_over;
	@JSONField(admin_only = true)
	private boolean		m_support_high_season;
	@JSONField
	boolean				m_use_guest_field;

	//--------------------------------------------------------------------------

	public
	GuestRoomEventProvider(String name) {
		super(name);
		setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit());
		setEventsCanRepeat(true);
		setShowOnMenu(true);
		m_display_items = new String[] { "location", "time", "notes", "posted by" };
		m_end_date_label = "last night";
		m_start_date_label = "first night";
		m_waiting_list_message = "There is another reservation that overlaps with this one. Your reservation has been added and for the nights that overlap you will be on a waiting list.";
	}

	//--------------------------------------------------------------------------
	// TODO: change to work with multiple (waiting list) reservations

	private void
	addInvoices(int id, DBConnection db) {
		if (!m_charge_for_use || m_invoice_account_id == 0)
			return;
		String columns = "id,date,end_date,_owner_";
		if (m_rates == null)
			columns += ",rate";
		if (m_rate_categories != null && m_rate_categories.length > 1)
			columns += ",rate_category";
		Select query = new Select(columns).from(m_events_table);
		if (id != 0)
			query.whereIdEquals(id);
		else
			query.where("end_date <='" + Time.newDate().minusDays(1).toString() + "' AND invoiced IS NOT TRUE");
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			LocalDate last_night = rows.getDate(3);
			int household_id = db.lookupInt(new Select("families_id").from("people").whereIdEquals(rows.getInt(4)), 0);
			int event_id = rows.getInt(1);
			int num_nights = countNights(new int[] { event_id }, rows.getDate(2), last_night, db);
			Accounting.addInvoice(m_invoice_account_id, household_id, last_night, m_fee_description, num_nights * getRate(rows), m_site, db);
			db.update(m_events_table, "invoiced=TRUE", event_id);
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	private void
	adjustColumns() {
		removeColumn("guest_name");
		removeColumn("invoiced");
		removeColumn("rate");
		removeColumn("rate_category");
		removeColumn("uasge");

		if (m_use_guest_field)
			addColumn(new JDBCColumn("guest_name", Types.VARCHAR));
		addColumn(new JDBCColumn("invoiced", Types.BOOLEAN));
		if (m_charge_for_use) {
			if (m_rates == null)
				addColumn(new JDBCColumn("rate", Types.INTEGER));
			if (m_rate_categories != null && m_rate_categories.length > 1)
				addColumn(new JDBCColumn("rate_category", Types.VARCHAR));
		}
		if (m_nights_per_year > 0)
			addColumn(new JDBCColumn("usage", Types.INTEGER));
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendReminderItem(String item, String label, Reminder reminder, StringBuilder sb, Site site, DBConnection db) {
		if ("guest".equals(item))
			appendReminderItem(item, label, ((GuestRoomEvent)reminder.getEvent()).m_guest, sb);
		else
			super.appendReminderItem(item, label, reminder, sb, site, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	buildReminderDate(Event event, LocalDate date, boolean last_event, Site site) {
		StringBuilder sb = new StringBuilder();
		String start_date = Time.formatter.getDateLong(date);
		String end_date = Time.formatter.getDateLong(event.getEndDate());
		sb.append(last_event ? end_date : start_date);

		LocalTime start_time = event.getStartTime();
		if (last_event) {
			if (start_time != null) {
				LocalTime end_time = event.getEndTime();
				if (end_time != null)
					sb.append(" at ").append(Time.formatter.getTime(event.getEndTime(), true));
			}
			return sb.toString();
		}

		if (start_time != null) {
			LocalTime end_time = event.getEndTime();
			if (end_time != null)
				sb.append(" ").append(Time.formatter.getTime(start_time, true)).append(" to ").append(end_date).append(" ").append(Time.formatter.getTime(event.getEndTime(), true));
			else
				sb.append(" at ").append(Time.formatter.getTime(start_time, true));
		} else
			sb.append(" to ").append(end_date);
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendReminderWhat(Reminder reminder, StringBuilder message, Site site, DBConnection db) {
		if (m_use_guest_field)
			appendReminderItem("guest", "Guest", reminder, message, site, db);
		else
			appendReminderItem("posted by", m_reserved_by_label, reminder, message, site, db);
	}

	//--------------------------------------------------------------------------

	int
	countNights(int[] ids, LocalDate from, LocalDate to, DBConnection db) {
		PriorityQueue<Event> events = new PriorityQueue<>();
		ArrayList<Event> repeating_events = new ArrayList<>();
		Rows rows = new Rows(new Select("*").from(m_events_table).where("date<='" + to.toString() + "' AND end_date>='" + from.toString() + "'"), db);
		while (rows.next())
			repeating_events.add(loadEvent(rows));
		rows.close();
		while (!from.isAfter(to)) {
			addRepeatingEvents(events, repeating_events, from);
			from = from.plusDays(1);
		}
		setOverlaps(events);
		int num_days = 0;
		for (Event event: events)
			if (Array.indexOf(ids, event.getID()) != -1 && !event.overlaps())
				++num_days;
		return num_days;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if ("report".equals(request.getPathSegment(1))) {
			writeAccountingReport(request);
			return true;
		}
		return super.doGet(request);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		if ("invoice".equals(request.getPathSegment(2))) {
			addInvoices(request.getPathSegmentInt(1), request.db);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDayCSSClass(LocalDate date, LocalDate today) {
		if (ChronoUnit.DAYS.between(date, today) == 0)
			return "today";
		if (m_support_high_season) {
			int d = date.getDayOfMonth();
			int m = date.getMonthValue();
			if (m == 1 && d <= 3)
				return "peak";
			if (m == 11 && d >= 24)
				return "peak";
			if (m == 12 && d >= 21)
				return "peak";
		}
		return "day";
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request request) {
		Person user = request.getUser();
		if (user == null)
			return null;
		if (user.isAdministrator())
			return super.getEventEditJS(event, date, request);
		if (m_invoice_account_id != 0 && ((GuestRoomEvent)event).m_invoiced && !request.userIsAdmin() && !request.userHasRole("calendar editor") && !(m_role != null && request.userHasRole(m_role)))
			return null;
		return super.getEventEditJS(event, date, request);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		if ("m_reminder_subject_items".equals(field_name)) {
			if (m_use_guest_field)
				return getFieldOptions(new String[] { "category", "date", "event", "guest", "location" });
			return getFieldOptions(new String[] { "category", "date", "event", "location", "posted by" });
		}
		if (m_use_guest_field)
			return getFieldOptions(new String[] { "guest", "location", "notes", "posted by", "time" });
		return getFieldOptions(new String[] { "location", "notes", "posted by", "time" });
	}

	//--------------------------------------------------------------------------

	int
	getRate(Rows rs) {
		if (m_rates == null)
			return rs.getInt("rate");
		if (m_rate_categories != null && m_rate_categories.length > 1) {
			String rate_category = rs.getString("rate_category");
			for (int i=0; i<m_rate_categories.length; i++)
				if (m_rate_categories[i].equals(rate_category))
					return m_rates[i];
		}
		return m_rates[0];
	}

	//--------------------------------------------------------------------------

	private String
	getRateColumn() {
		StringBuilder rate_column = new StringBuilder();
		if (m_rates == null)
			rate_column.append("rate");
		else if (m_rate_categories == null || m_rate_categories.length == 1)
			rate_column.append(m_rates[0]);
		else {
			rate_column.append("CASE");
			for (int i=0; i<m_rate_categories.length; i++)
				rate_column.append(" WHEN rate_category=").append(DBConnection.string(m_rate_categories[i])).append(" THEN ").append(m_rates[i]);
			rate_column.append(" END");
		}
		return rate_column.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		adjustColumns();
		((Accounting)m_site.getModule("Accounting")).addBlock(new Block(m_name) {
			@Override
			public boolean
			handlesContext(String context, Request request) {
				return m_invoice_account_id != 0;
			}
			@Override
			public void
			write(String context, Request request) {
				switch(context) {
				case "accounting module":
					writeAccountingPane(request);
					return;
				case "accounting report":
					writeAccountingReportPicker(request);
					return;
				}
			}

		});
		super.init(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new GuestRoomEvent(this, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals(m_name)) {
			ViewDef view_def = new EventViewDef(this)
				.setAccessPolicy(m_access_policy)
				.setFrom(m_events_table)
				.setRecordName("Event");
			ArrayList<String> column_names_form = new ArrayList<>();
			if (m_nights_per_year > 0) {
				column_names_form.add("usage");
				view_def.setColumn(new Column("usage") {
					@Override
					public void
					writeInput(View view, Form form, Request request) {
						if (view.getMode() == Mode.ADD_FORM) {
							Household household = ((mosaic.Person)request.getUser()).getHousehold(request);
							if (household != null) {
								int household_id = ((mosaic.Person)request.getUser()).getHousehold(request).getId();
								LocalDate date = request.getDate("date");
								int year = date.getYear();
								String ids = request.db.lookupString(
									new Select("string_agg(distinct id::varchar, ',')")
										.from(m_events_table)
										.where("date<='" + year + "-12-31' AND end_date>='" + year + "-1-1'")
										.andWhere("household".equals(m_household_or_home) ?
											"_owner_ IN(SELECT id FROM people WHERE families_id=" + household_id + ")" :
											"_owner_ IN(SELECT id FROM people WHERE families_id IN(SELECT id FROM families WHERE homes_id=" + request.db.lookupInt(new Select("homes_id").from("famililes").whereIdEquals(household_id), 0) + "))"));
								if (ids != null) {
									int nights_so_far = countNights(Array.split(ids), date.withDayOfYear(1), date.withMonth(12).withDayOfMonth(31), request.db);
									request.writer.write("<div class=\"alert alert-info\" role=\"alert\">Your " + m_household_or_home + " has used " + nights_so_far + " out of " + m_nights_per_year + " nights in " + year + "</div>");
								}
							}
						}
					}
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						return false;
					}
				}.setDisplayName(""));
			}
			setLocationColumn(view_def, column_names_form);
			column_names_form.add("date");
			DateColumn start_date_column = new DateColumn("date").setDisplayName(m_start_date_label).setIsRequired(true);
			view_def.setColumn(start_date_column);
			if (m_events_have_start_time) {
				column_names_form.add("start_time");
				if (m_default_start_time != null) {
					view_def.setColumn(new TimeColumn("start_time").setDateColumn("date").setDefaultValue(m_default_start_time));
					start_date_column.setTimeColumn("start_time");
				}
			}
			column_names_form.add("end_date");
			DateColumn end_date_column = new DateColumn("end_date").setDisplayName(m_end_date_label).setIsRequired(true);
			view_def.setColumn(end_date_column);
			if (m_events_have_start_time) {
				column_names_form.add("end_time");
				if (m_default_end_time != null) {
					view_def.setColumn(new TimeColumn("end_time").setCheckOrder(false).setDateColumn("end_date").setDefaultValue(m_default_end_time));
					end_date_column.setTimeColumn("end_time");
				}
			}
			if (m_use_guest_field) {
				column_names_form.add("guest_name");
				view_def.setColumn(new Column("guest_name").setDisplayName("guest"));
			}
			if (m_charge_for_use) {
				if (m_rate_categories != null && m_rate_categories.length > 1) {
					column_names_form.add("rate_category");
					view_def.setColumn(new OptionsColumn("rate_category", m_rate_categories));
				}
				if (m_rates == null) {
					column_names_form.add("rate");
					view_def.setColumn(new Column("rate").setPostText(m_support_multiple_locations ? "per room per night" : "per night").setIsRequired(true));
				}
			}
			column_names_form.add("event");
			view_def.setColumn(new Column("event").setDisplayName("notes"));
			column_names_form.add("invoiced");
			view_def.setColumn(new Column("invoiced").setViewRole("administrator"));
			column_names_form.add("repeat");
			view_def.setColumn(new Column("repeat").setDefaultValue("every day").setIsHidden(true));
			column_names_form.add("_owner_");
			LookupColumn owner_column = new PersonColumn("_owner_", false, site.getPeopleFilter()).setDisplayName("posted by").setDefaultToUserId();
			if (m_role == null)
				owner_column.setIsReadOnly(true);
			view_def.setColumn(owner_column);
			if (m_support_reminders)
				view_def.addRelationshipDef(new OneToMany(m_name + "_reminders").setSpanFormCols(false));
			column_names_form.add("_timestamp_");
			view_def.setColumn(new Column("_timestamp_").setDisplayName("added").setIsReadOnly(true).setHideOnForms(Mode.ADD_FORM));
			if (m_agreements != null) {
				column_names_form.add("agreements");
				view_def.setColumn(new Column("agreements") {
					@Override
					public void
					writeInput(View view, Form form, Request request) {
						HTMLWriter writer = request.writer;
						writer.write(m_agreements[0]).write("</br>");
						for (int i=1; i<m_agreements.length; i++)
							writer.setAttribute("required", "true").ui.checkbox(null, m_agreements[i], "agreement " + i, view.getMode() != Mode.ADD_FORM, false, false);
					}
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						return false;
					}
				}.setDisplayName(""));
			}
			view_def.setColumnNamesForm(column_names_form);
			return view_def;
		}
		if ("reservations".equals(name)) {
			ViewDef view_def = new ViewDef("reservations")
				.addFeature(m_reservation_report_by_quarter ? new QuarterFilter(m_events_table, Location.TOP_LEFT) : new MonthFilter(m_events_table, Location.TOP_LEFT))
				.addFeature(new YearFilter(m_events_table, Location.TOP_LEFT))
				.addSection(new Dividers(m_household_or_home, new OrderBy("1,2")))
				.setAccessPolicy(new AccessPolicy())
				.setCenter(false)
				.setFrom(m_events_table);
			String amount = "(end_date - date + 1)*" + getRateColumn();
			if (m_support_multiple_locations)
				amount += "*(SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
			ArrayList<String> column_names_table = new ArrayList<>();
			column_names_table.add(m_start_date_label);
			column_names_table.add(m_end_date_label);
			if ("home/unit".equals(m_household_or_home))
				column_names_table.add("household");
			if (m_events_have_location)
				column_names_table.add("location");
			column_names_table.add("num_nights");
			if (m_charge_for_use) {
				column_names_table.add("rate");
				column_names_table.add("amount_paid");
			}
			view_def.setColumnNamesTable(column_names_table);
			String select_columns = "date AS \"" + m_start_date_label + "\",end_date AS \"" + m_end_date_label + "\"";
			if (m_events_have_location) {
				String locations_table = getLocationsTable();
				select_columns += m_support_multiple_locations
					? ",(SELECT string_agg(text, ', ') FROM " + m_events_table + "_location_links JOIN " + locations_table + " ON " + locations_table + "_id=" + locations_table + ".id WHERE " + m_events_table + "_id=" + m_events_table + ".id GROUP BY " + m_events_table + ".id) AS location"
					: ",(SELECT string_agg(text, ', ') FROM " + locations_table + " WHERE id=" + locations_table + "_id GROUP BY id) AS location";
			}
			select_columns += ",end_date - date + 1 AS num_nights";
			if (m_charge_for_use)
				select_columns += "," + getRateColumn() + " AS rate," + amount + " AS amount_paid";
			String household = "(SELECT name FROM families WHERE id=(SELECT families_id FROM people WHERE people.id=_owner_)) AS household";
			if ("household".equals(m_household_or_home))
				view_def.setSelectColumns(household + "," + select_columns);
			else
				view_def.setSelectColumns("(SELECT number FROM homes WHERE id=(SELECT homes_id FROM families JOIN people ON families_id=families.id WHERE people.id=_owner_)) AS \"home/unit\"," + household + "," + select_columns);
			view_def.setColumn(new CurrencyColumn("amount_paid").setTotal(true))
				.setColumn(new Column("num_nights").setCellStyle("text-align:right").setTotal(true).setTotalIsInt(true))
				.setColumn(new CurrencyColumn("rate"));
			return view_def;
		}
		return super._newViewDef(name, site);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	nightly(LocalDateTime now, Site site, DBConnection db) {
		addInvoices(0, db);
		super.nightly(now, site, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	overlaps(LocalDate date, LocalDate end_date, LocalTime start_time, LocalTime end_time, List<Integer> locations, int event_id, DBConnection db) {
		if (date == null)
			return false;
		Select query = new Select().from(m_events_table);
		if (end_date != null)
			query.where("'" + date.toString() + "' <= end_date AND '" + end_date.toString() + "' >= date");
		else
			query.where("'" + date.toString() + "' = date");
		if (locations != null && locations.size() > 0 && (m_events_have_location || m_calendar_location_id != 0))
			if (m_calendar_location_id != 0) {
				if (!locations.contains(m_calendar_location_id))
					return false;
			} else {
				if (m_support_multiple_locations)
					query.join(m_events_table, m_events_table + "_location_links");
				if (locations.size() == 1)
					query.andWhere(getLocationsTable() + "_id = " + locations.get(0));
				else
					query.andWhere(getLocationsTable() + "_id IN (" + Text.join(",", locations) + ")");
			}
		if (event_id != 0)
			query.andWhere("id != " + event_id);
		return db.exists(query);
	}

	//--------------------------------------------------------------------------

	public GuestRoomEventProvider
	setControls(String controls) {
		m_controls = controls;
		return this;
	}

	//--------------------------------------------------------------------------

	boolean
	showEventOwner() {
		return m_show_event_owner;
	}

	//--------------------------------------------------------------------------

//	boolean
//	showEventOwnerOnMouseOver() {
//		return m_show_event_owner_on_mouse_over;
//	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	supportsPartialDeletion() {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request request) {
		adjustColumns();
		super.updateSettings(request);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	validateTime(LocalTime start_time, LocalTime end_time) {
		return null; // if times are used they are always valid
	}

	//--------------------------------------------------------------------------

	void
	writeAccountingPane(Request request) {
		// find invoices on same day: select * from guest_room_events join transactions on transactions.date=guest_room_events.end_date and accounts_id=66 where guest_room_events.id in (select guest_room_events.id from guest_room_events join transactions on transactions.date=guest_room_events.end_date and accounts_id=66 and type='Invoice' group by guest_room_events.id having count(guest_room_events.id)>1);
		HTMLWriter writer = request.writer;
		writer.h3("Guest Room");
		Table table = writer.ui.table().addStyle("padding", "20px");
		writer.setAttribute("style", "vertical-align: top;");
		table.td();
		ViewState.setBaseFilter("invoices", "type='Invoice' AND accounts_id=" + m_invoice_account_id, request);
		request.site.newView("invoices", request).writeComponent();
		writer.setAttribute("style", "vertical-align: top;");
		table.td();
		if (m_accounting_view_def == null)
			m_accounting_view_def = newPrivateViewDef(m_events_table, request.site)
				.addFeature(new YearFilter("transactions", Location.TOP_LEFT))
				.setAllowQuickEdit(true)
				.setDefaultOrderBy("date")
				.setRecordName("Event")
				.setReplaceOnQuickEdit(true)
				.setColumn(new LookupColumn("_owner_", "people", "first,last"))
				.setColumnNamesTable(new String[] { "date", "end_date", "_owner_", "invoiced" });
		m_accounting_view_def.newView(request).writeComponent();
		writer.setAttribute("style", "vertical-align: top;");
		table.td();
		new MonthView((EventProvider)request.site.getModule("Guest Room"), request).writeComponent(request);
		table.close();
		writer.js("app.subscribe('" + m_accounting_view_def.getName() + "', function(object, message, data){if(message=='update'){net.replace('Accounting');}})");
	}

	//--------------------------------------------------------------------------

	private void
	writeAccountingReport(Request request) {
		String amount = "(end_date - date + 1) * " + getRateColumn();
		if (m_events_have_location && m_support_multiple_locations)
			amount += " * (SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
		amount += " AS amount_paid";
		String date = "date AS \"" + m_start_date_label + "\"";
		String end_date = "end_date AS \"" + m_end_date_label + "\"";
		String household_id = "(SELECT families_id FROM people WHERE people.id=_owner_)";
		String household = "(SELECT name FROM families WHERE id=" + household_id + ") AS household";
		String locations_table = getLocationsTable();
		String locations = m_support_multiple_locations
			? "(SELECT string_agg(text, ', ') FROM " + m_events_table + "_location_links JOIN " + locations_table + " ON " + locations_table + "_id=" + locations_table + ".id WHERE " + m_events_table + "_id=" + m_events_table + ".id GROUP BY " + m_events_table + ".id) AS location"
			: "(SELECT string_agg(text, ', ') FROM " + locations_table + " WHERE id=" + locations_table + "_id GROUP BY id) AS location";
		String num_nights = "end_date - date + 1 AS num_nights";
		String rate = getRateColumn() + " AS rate";
		String view_by = request.getParameter("view_by");
		String detail_columns = date + "," + end_date + "," + household;
		if (m_events_have_location)
			detail_columns += "," + locations;
		detail_columns += "," + num_nights + "," + rate + "," + amount;
		if ("amount paid".equals(view_by))
			new AnnualReport(new Select("date," + amount).from(m_events_table), "Amount Paid", 0, 2, 2)
				.setDetail(null, detail_columns, m_events_table, "(end_date - date + 1) * " + getRateColumn())
				.setItemIsInt(true)
				.setParam("view_by")
				.write("Guest Room by Amount Paid", request);
		else if ("category".equals(view_by))
			new AnnualReport(new Select("rate_category,date," + amount).from(m_events_table), "Rate Category", 0, 1, 3)
				.setDetail(null, detail_columns, m_events_table, "rate_category")
				.setParam("view_by")
				.write("Guest Room by Category", request);
		else if ("home/unit".equals(view_by)) {
			detail_columns = date + "," + end_date + "," + household;
			if (m_events_have_location)
				detail_columns += "," + locations;
			detail_columns += "," + num_nights;
			String homes_id = "(SELECT homes_id FROM families JOIN people ON families_id=families.id WHERE people.id=_owner_)";
			new AnnualReport(new Select("(SELECT number FROM homes WHERE id=" + homes_id + ")," + homes_id + ",date,end_date - date + 1").from(m_events_table), "Home", 2, 1, 4)
				.setDetail(new Select("number").from("homes"), detail_columns, m_events_table + " JOIN people ON people.id=_owner_ JOIN families ON families.id=families_id JOIN homes ON homes.id=homes_id", "homes.id")
				.setParam("view_by")
				.setValueIsDouble(false)
				.write("Guest Room by Home/Unit", request);
		} else if ("household".equals(view_by)) {
			detail_columns = date + "," + end_date;
			if (m_events_have_location)
				detail_columns += "," + locations;
			detail_columns += "," + num_nights;
			new AnnualReport(new Select(household + "," + household_id + ",date,end_date - date + 1").from(m_events_table), "Household", 2, 1, 4)
				.setDetail(new Select("name").from("families"), detail_columns, m_events_table + " JOIN people ON people.id=_owner_ JOIN families ON families.id=families_id", "families.id")
				.setParam("view_by")
				.setValueIsDouble(false)
				.write("Guest Room by Household", request);
		} else if ("location".equals(view_by)) {
			String columns = "(SELECT text FROM " + locations_table + " WHERE id=" + locations_table + "_id)," + locations_table + "_id,date," + (m_charge_for_use ? amount : num_nights);
			String from = m_events_table;
			if (m_support_multiple_locations)
				from += " JOIN " + m_events_table + "_location_links ON " + m_events_table + "_id=" + m_events_table + ".id";
			from += " JOIN " + locations_table + " ON " + locations_table + ".id=" + locations_table + "_id";
			detail_columns = date + "," + end_date + "," + household + "," + num_nights;
			if (m_charge_for_use)
				detail_columns += "," + rate + "," + amount;
			AnnualReport r = new AnnualReport(new Select(columns).from(from), "Location", 2, 1, 4)
				.setDetail(new Select("text").from(locations_table), detail_columns, from, locations_table + ".id")
				.setParam("view_by");
			if (!m_charge_for_use)
				r.setValueIsDouble(false);
			r.write("Guest Room by Location", request);
		} else if ("reservation".equals(view_by)) {
			request.writer.h3("Guest Room by Reservation");
			request.site.newView("reservations", request).writeComponent();
		}
	}

	//--------------------------------------------------------------------------

	void
	writeAccountingReportPicker(Request request) {
		request.site.newHead(request).close();
		ArrayList<String> views = new ArrayList<>();
		if (m_charge_for_use && m_rates == null)
			views.add("amount paid");
		if (m_rate_categories != null)
			views.add("category");
		if ("home/unit".equals(m_household_or_home))
			views.add("home/unit");
		views.add("household");
		if (m_events_have_location)
			views.add("location");
		views.add("reservation");
		request.writer.write("<div class=\"container\" style=\"padding-top:20px;width:intrinsic;\">");
		if (views.size() > 1) {
			request.writer.write("view by ");
			new ui.Select(null, views).setOnChange("net.replace('annual_report',context+'/" + m_name + "/report?view_by='+this.options[this.selectedIndex].text)").setSelectedOption("household", null).write(request);
		}
		request.writer.write("<div id=\"annual_report\" style=\"padding-top:20px;\"></div>")
			.js("net.replace('annual_report',context+'/" + m_name + "/report?view_by=household');")
			.write("</div>");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeAfterView(Request request) {
		if (m_controls != null)
			request.writer.write(m_controls);
		else if (m_support_high_season)
			request.writer.write("<div style=\"padding:10px\"><span class=\"peak\" style=\"border:1px solid black\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> High Season</div>");
		request.writer.aBlank("Reports", request.getContext() + "/Accounting/blocks/" + m_name + "/accounting report");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, Request request) {
		super.writeSettingsForm(new String[] { "m_active", "m_charge_for_use", "m_color", "m_display_item_labels", "m_display_items", "m_display_name", "m_end_date_label", "m_events_have_location", "m_events_have_start_time", "m_fee_description", "m_ical_items", "m_invoice_account_id", "m_nights_per_household_per_year", "m_rate", "m_reminder_subject_items", "m_reserved_by_label", "m_role", "m_show_event_owner", /*"m_show_event_owner_on_mouse_over",*/ "m_show_on_menu", "m_show_on_upcoming", "m_sponsored_guest_rate", "m_start_date_label", "m_support_reminders", "m_support_waiting_list", "m_tooltip_item_labels", "m_tooltip_items", "m_use_guest_field", "m_uuid" }, in_dialog, request);
	}
}
