package mosaic;

import app.Request;
import app.Themes;
import db.Form;
import db.View;
import sitemenu.SiteMenu;

public class AccountForm extends Form {
	public
	AccountForm(View view, Request request) {
		super(view, request);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeBody() {
		for (String column : m_view_def.getColumnNamesForm())
			switch(column) {
			case "additional_emails":
				m_view.getRelationship("additional_emails").writeManyTableRow(1, m_request);
				break;
			case "theme":
				if (m_request.userHasRole("coho"))
					((Themes)m_request.site.getModule("Themes")).writeSettingsInput(null, m_request);
				break;
			case "top menu":
				if (m_request.userHasRole("coho"))
					((SiteMenu)m_request.site.getModule("SiteMenu")).writeSettingsInput(null, m_request);
				break;
			default:
				writeColumnRow(column, m_view.getColumn(column));
			}
	}
}
