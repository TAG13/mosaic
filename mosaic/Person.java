package mosaic;

import java.util.List;
import java.util.TreeSet;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Request;
import app.Text;
import blocks.Blocks;
import db.DBConnection;
import db.Select;
import db.View.Mode;
import db.ViewState;
import db.object.DBField;
import email.MailLists;
import groups.Groups;
import households.Household;
import households.Households;
import web.HTMLWriter;
import web.JS;

public class Person extends app.Person {
	@DBField
	private int			m_families_id;
	private Household	m_household;

	//--------------------------------------------------------------------------

	public
	Person(Request request) {
		super(request);
	}

	//--------------------------------------------------------------------------

	public
	Person(int id, DBConnection db) {
		super(id, db);
	}

	//--------------------------------------------------------------------------

	public Household
	getHousehold(Request request) {
		if (m_household == null) {
			Households households = (Households)request.site.getModule("Households");
			if (households != null && households.isActive())
				m_household = households.getHousehold(m_families_id, request.db);
		}
		return m_household;
	}

	//--------------------------------------------------------------------------

	public int
	getHouseholdId() {
		return m_families_id;
	}

	//--------------------------------------------------------------------------

	public boolean
	isInGroup(String group_name, DBConnection db) {
		return db.rowExists("people_groups", "people_id=" + m_id + " AND groups_id=" + db.lookupInt(new Select("id").from("groups").where("name=" + DBConnection.string(group_name)), -1));
	}

	//--------------------------------------------------------------------------

	public void
	joinGroup(int group_id, String job, DBConnection db) {
		if (job != null)
			db.update("groups", job + '=' + getId(), group_id);
		else if (!db.rowExists("people_groups", "people_id=" + getId() + " AND groups_id=" + group_id))
			db.insert("people_groups", "people_id,groups_id", getId() + "," + group_id);
	}

	//--------------------------------------------------------------------------

	public void
	leaveGroup(int group_id, String job, DBConnection db) {
		if (job != null)
			db.update("groups", job + "=NULL", group_id);
		else
			db.delete("people_groups", "people_id=" + getId() + " AND groups_id=" + group_id, false);
	}

	//--------------------------------------------------------------------------

	public void
	writeInfo(Request request) {
		HTMLWriter writer = request.writer;
		writer.tagOpen("h4");
		String picture = getPicture(request);
		if (picture != null)
			writer.write("<img src=\"").write(picture).write("\" alt=\"\" style=\"max-width:100%\" /> ");
		writer.write(getName());
		writer.addStyle("float:right").ui.buttonOnClick("...", "Person.popup(" + m_id + ")");
		writer.tagClose();
		if (m_email != null)
			writer.ui.icon("envelope").nbsp().write(m_email).br();
		String phone = lookupString("phone", request.db);
		if (phone != null)
			writer.ui.icon("phone").nbsp().write(phone).br();
	}

	//--------------------------------------------------------------------------

	void
	writePage(Request request) {
		boolean is_user = m_id == request.getUser().getId();
		String picture = getPicture(request);
		HTMLWriter writer = request.writer;
		Blocks b = new Blocks(is_user, request).lookup(new Select("*").from("people").whereIdEquals(m_id));

		if (is_user)
			writer.js("Dialog.top().add_header('Profile',true,null,'h3')");
		else {
			String header = "<div style=\"display:inline-block;vertical-align:middle;\"><div>" + getName() + "</div>";
			if (m_pronouns != null)
				header += "<div style=\"font-size:small\">" + m_pronouns + "</div>";
			if (picture != null)
				header = "<img src=\"" + picture + "\" alt=\"\" style=\"max-height:100px\" /> " + header;
			writer.js("Dialog.top().add_header(" + JS.string(header + "</div>") + ",true,null,'h1')");
		}

		JsonObject page = ((People)request.site.getModule("People")).m_page_template;
		JsonArray blocks = page.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			if (b.write(block))
				continue;
			switch(block.getString("type", null)) {
			case "groups":
				Groups groups = (Groups)request.site.getModule("Groups");
				if (groups.isActive()) {
					TreeSet<String> g = new TreeSet<>();
					g.addAll(groups.getGroups(m_id, request.db));
					List<String[]> groups_jobs = groups.getGroupsJobs(m_id, request.db);
					for (String[] gj : groups_jobs) {
						if (g.contains(gj[0]))
							g.remove(gj[0]);
						g.add(gj[0] + " (" + gj[1] + ")");
					}
					if (g.size() > 0) {
						writer.addClass("text-muted").h4(groups.getDisplayName(request));
						writer.tagOpen("p");
						writer.write(Text.join(", ", g));
						writer.tagClose();
					}
				}
				break;
			case "household":
				if (getHousehold(request) != null) {
					String number = request.db.lookupString(new Select("number").from("families JOIN homes ON families.homes_id=homes.id").whereEquals("families.id", m_household.getId()));
					if (number != null)
						writer.write("<div style=\"align-items:center;display:flex;\">").ui.icon("house").nbsp().write(number).write("</div>");
				}
				break;
			case "img":
				b.writeImg(block, picture);
				break;
			case "mail lists":
				MailLists mail_lists = (MailLists)request.site.getModule("MailLists");
				if (mail_lists.isActive()) {
					List<String> lists = mail_lists.getMailLists(m_id, request);
					if (lists.size() > 0) {
						writer.addClass("text-muted").h4("Mail Lists");
						writer.tagOpen("p");
						writer.write(Text.join(", ", lists));
						writer.tagClose();
					}
				}
				break;
			case "pictures":
				if (is_user) {
					writer.addClass("text-muted").h4("Pictures");
					ViewState.setBaseFilter("people_bio_pictures", "people_id=" + m_id, request);
					request.site.getViewDef("people_bio_pictures", request.db).newView(request).setMode(Mode.LIST).writeComponent();
				} else {
					List<String> filenames = request.db.readValues(new Select("filename").from("people_bio_pictures").where("people_id=" + m_id));
					if (filenames.size() > 0) {
						writer.write("<div onclick=\"temp_gallery=new Gallery({dir:'people/").write(m_id).write("'});person_gallery.open(event.target)\">");
						for (String filename : filenames)
							writer.write("\n<img src=\"").write(request.getContext()).write("/people/").write(m_id).write("/thumbs/").write(filename).write("\" style=\"cursor:pointer;max-width:100%;\"/>");
						writer.write("</div>");
					}
				}
				break;
			case "skills":
				if (is_user) {
					writer.addClass("text-muted").h4("Skills");
					ViewState.setBaseFilter("people_skills", "people_id=" + m_id, request);
					request.site.getViewDef("people_skills", request.db).newView(request).setMode(Mode.LIST).writeComponent();
				} else {
					List<String> skills = request.db.readValues(new Select("skill").from("people_skills").whereEquals("people_id", m_id).orderBy("skill"));
					if (skills.size() > 0) {
						writer.addClass("text-muted").h4("Skills");
						writer.tagOpen("p");
						writer.write(Text.join(", ", skills));
						writer.tagClose();
					}
				}
				break;
			}
		}
	}
}
