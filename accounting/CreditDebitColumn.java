package accounting;

import java.util.Map;

import app.Request;
import db.View;
import db.column.CurrencyColumn;

public class CreditDebitColumn extends CurrencyColumn {
	public
	CreditDebitColumn(String name) {
		super(name);
		setCellStyle("text-align:right");
		setTotal(true);
	}

	// --------------------------------------------------------------------------

	@Override
	public double
	getDouble(View view, Request request) {
		if (!view.data.getBoolean("reconciled"))
			return 0;
		return view.data.getDouble(m_name);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View view, Map<String,Object> data, Request request) {
		double d = view.data.getDouble(m_name);
		if (view.data.wasNull())
			return false;
		request.writer.writeCurrency(d);
		return true;
	}
}
