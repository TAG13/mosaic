package accounting;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;

import app.Block;
import app.ClassTask;
import app.Mail;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import app.Text;
import app.Time;
import app.pages.Page;
import db.CSVWriter;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.OneToManyLink;
import db.Options;
import db.OrderBy;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import ui.Form;
import ui.SelectOption;
import ui.Table;
import ui.TaskPane;
import web.HTMLWriter;

public class Accounting extends SiteModule {
	private static final String[]	TYPES = new String[] { "Bank Fee", "Check", "Credit", "Interest", "Invoice", "Payment", "Transfer In", "Transfer Out" };

	private Options					m_accounts;
	private AccountFilter			m_account_filter;
	private final LookupColumn		m_account_column			= new LookupColumn("accounts_id", "accounts", "name").setAllowNoSelection(true).setDisplayName("account");
	private final LookupColumn		m_account_expense_column	= new LookupColumn("accounts_id", "accounts", "name", "(type=2 OR type=3 OR type=5 OR type=9) AND active", "name").setAllowNoSelection(true).setDisplayName("account");
	private final LookupColumn		m_account_income_column		= new LookupColumn("accounts_id", "accounts", "name", "(type=1 OR type=4) AND active", "name").setAllowNoSelection(true).setDisplayName("account");
	private final Column			m_account_type_column		= new Column("type").setInputAndValueRenderers(new SelectRenderer(
																		new String[] { "Income", "Expense", "Capital Improvement", "Ongoing Income", "Ongoing Expense", "Water", "Other" },
																		new String[] { "1", "2", "3", "4", "5", "9", "0" }),
																	false);
	@JSONField(label="account",type="income account")
	private int						m_arrears_account_id;
	@JSONField(after="of each month",label="on day")
	private int						m_arrears_fee_date;
	private final LookupColumn		m_bank_account_column		= new LookupColumn("bank_accounts_id", "bank_accounts", "name").setAllowEditing().setAllowNoSelection(true).setDisplayName("bank account");
	@JSONField(admin_only=true,fields={"m_arrears_account_id","m_arrears_fee_date"})
	private boolean					m_charge_arrears_fees;
	@JSONField(label="create invoices for HOA dues",fields={"m_hoa_invoice_date","m_hoa_account_id"})
	private boolean	 				m_create_hoa_invoices;
	private final Column			m_date_column				= new Column("date").setDefaultToShortDate().setIsRequired(true);
	@JSONField
	int								m_default_bank_account;
	@JSONField(type="income account")
	private int						m_default_interest_account;
	private final LookupColumn		m_household_column			= new LookupColumn("families_id", "families", "name", "active", "name").setAllowNoSelection(true).setDisplayName("household");
	@JSONField(label="account",type="income account")
	private int 					m_hoa_account_id;
	@JSONField(after="of each month",label="on day")
	private int 					m_hoa_invoice_date			= 26;
	@JSONField
	String							m_label						= "Household";
	MonthFilter						m_month_filter				= new MonthFilter("transactions", Location.TOP_LEFT);
	private CurrencyColumn			m_next_year_column			= new CurrencyColumn("next_year"){
																		@Override
																		public void
																		adjustQuery(Select query, Mode mode, Site site) {
																			if (mode == View.Mode.LIST || mode == View.Mode.READ_ONLY_LIST)
																				query.addColumn("(SELECT amount FROM accounts_budgets WHERE accounts_budgets.accounts_id=accounts.id AND year=" + (Time.newDate().getYear() + 1) + ')', m_name);
																		}
																	}.setTotal(true);
	private Column					m_payee_column;
	private CurrencyColumn			m_remaining_column			= new CurrencyColumn("remaining"){
																		@Override
																		public double
																		getDouble(View view, Request request) {
																			double budget = view.data.getDouble("this_year");
																			if (budget == 0)
																				return 0;
																			return budget - getTotalYTD(view.data.getInt("id"), Time.newDate().getYear(), request.db);
																		}
																		@Override
																		public boolean
																		writeValue(View view, Map<String,Object> data, Request request) {
																			double budget = view.data.getDouble("this_year");
																			if (budget != 0)
																				request.writer.writeCurrency(getDouble(view, request));
																			return budget != 0;
																		}
																	}.setCellStyle("text-align:right").setIsReadOnly(true).setTotal(true);
	@JSONField(label="email statements",fields={"m_statement_date","m_statement_from","m_statement_intro","m_statement_negative_balance","m_statement_positive_balance","m_statement_zero_balance"})
	boolean							m_send_statements;
	@JSONField(after="of each month",label="on day")
	private int						m_statement_date			= 27;
	@JSONField(label="from",type="mail list")
	private String					m_statement_from;
	@JSONField(label="Intro")
	private String					m_statement_intro			= "Hello,<br />  this automatically generated email is your monthly household account statement. Your account balance includes one or more of the following: dues, guest room usage, eggs and/or water bills. Payments are due on the first of the month. Checks can be given to Susan Rohrbach or John Palmer, put in the black mailbox in the common house lounge, or mailed to 501 E. Roger Rd.<br /><br />Please check over your household account history on a regular basis and contact the Finance Committee with any concerns.";
	@JSONField(label="Negative Balance Statement")
	private String					m_statement_negative_balance = "Since your balance is negative you have a credit and therefore don't need to pay at this time.";
	@JSONField(label="Positive Balance Statement")
	private String					m_statement_positive_balance = "Please pay off your balance as soon as possible.";
	@JSONField(label="Zero Balance Statement")
	private String					m_statement_zero_balance	= "Since your balance is zero, thank you for being current on your account.";
	private CurrencyColumn			m_this_year_column			= new CurrencyColumn("this_year"){
																	@Override
																	public void
																	adjustQuery(Select query, Mode mode, Site site) {
																		if (mode == View.Mode.LIST || mode == View.Mode.READ_ONLY_LIST)
																			query.addColumn("(SELECT amount FROM accounts_budgets WHERE accounts_budgets.accounts_id=accounts.id AND year=" + Time.newDate().getYear() + ')', m_name);
																	}
																}.setDisplayName("budget").setTotal(true);
	private final OptionsColumn		m_type_column				= new OptionsColumn("type", TYPES);
	YearFilter						m_year_filter				= new YearFilter("transactions", Location.TOP_LEFT);

	// --------------------------------------------------------------------------

	public static void
	addCredit(int accounts_id, int household_id, LocalDate date, String description, double amount, int transactions_id, Site site, DBConnection db) {
		if (amount == 0)
			return;
		if (date == null)
			date = Time.newDate();

		NameValuePairs name_value_pairs = new NameValuePairs()
			.set("amount", amount)
			.set("accounts_id", accounts_id)
			.setDate("date", date)
			.set("description", description)
			.set("families_id", household_id)
			.set("type", "Credit");
		if (transactions_id > 0)
			name_value_pairs.set("transactions_id", transactions_id);

		db.insert("transactions", name_value_pairs);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"date (leave empty for today)"})
	public void
	addHOAInvoices(LocalDate date, Site site, DBConnection db) {
		Rows rows = new Rows(new Select("pays_hoa,hoa_dues,number").from("homes").joinOn("units", "units.id=homes.units_id").where("pays_hoa IS NOT NULL"), db);
		while (rows.next())
			addInvoice(m_hoa_account_id, rows.getInt(1), date, "HOA Dues for " + rows.getString(3), rows.getFloat(2), site, db);
	}

	// --------------------------------------------------------------------------

	public static void
	addInvoice(int accounts_id, int household_id, LocalDate date, String description, double amount, Site site, DBConnection db) {
		addInvoice(accounts_id, household_id, date, description, amount, 0, site, db);
	}

	// --------------------------------------------------------------------------

	public static void
	addInvoice(int accounts_id, int household_id, LocalDate date, String description, double amount, int transactions_id, Site site, DBConnection db) {
		if (amount == 0)
			return;
		if (date == null)
			date = Time.newDate();

		NameValuePairs name_value_pairs = new NameValuePairs();
		name_value_pairs.set("amount", amount);
		name_value_pairs.set("accounts_id", accounts_id);
		name_value_pairs.setDate("date", date);
		name_value_pairs.set("description", description);
		name_value_pairs.set("families_id", household_id);
		name_value_pairs.set("type", "Invoice");
		if (transactions_id > 0)
			name_value_pairs.set("transactions_id", transactions_id);

		db.insert("transactions", name_value_pairs);
	}

	// --------------------------------------------------------------------------

	private void
	addInvoices(Request request) {
		Enumeration<String> parameter_names = request.getParameterNames();
		while (parameter_names.hasMoreElements()) {
			String parameter_name = parameter_names.nextElement();
			if (parameter_name.charAt(0) == 'f')
				addInvoice(request.getInt("accounts_id", 0), Integer.parseInt(parameter_name.substring(1)), request.getDate("date"), request.getParameter("description"), request.getDouble("amount", 0), request.site, request.db);
		}
		request.writer.write("Dialog.alert('','invoices added')");
	}

	// --------------------------------------------------------------------------

	@ClassTask({ "next year" })
	public static void
	addNextYearBudgets(int year, DBConnection db) {
		try {
			ResultSet rs = db.select("SELECT accounts_id,amount FROM accounts JOIN accounts_budgets ON accounts.id=accounts_budgets.accounts_id WHERE type != 3 AND year=" + (year - 1));
			while (rs.next())
				db.insert("accounts_budgets", "accounts_id,year,amount", rs.getInt(1) + "," + year + "," + rs.getString(2));
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceArrears(int household_id, LocalDate date, Site site, DBConnection db) {
		LocalDate ld = date == null ? Time.newDate() : date;
		int month = ld.getMonthValue();
		int year = ld.getYear();
		String where = "families_id=" + household_id;
		if (date != null)
			where += " AND date <='" + date + "'";
		Select query = new Select("type,amount,date").from("transactions").where(where).orderBy("date");
		ResultSet rs = db.select(query.toString());
		double balance = 0;
		try {
			while (rs.next()) {
				String type = rs.getString(1);
				double amount = type.equals("Credit") || type.equals("Payment") ? -rs.getDouble(2) : rs.getDouble(2);
				ld = rs.getDate(3).toLocalDate();
				if (ld.getYear() == year && ld.getMonthValue() == month && ld.getDayOfMonth() < 15 && amount > 0)
					continue;
				balance += amount;
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return Math.round(balance * 100.0) / 100.0;
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceBank(int bank_accounts_id, String date, DBConnection db) {
		String exists = "SELECT * FROM transactions WHERE bank_accounts_id=" + bank_accounts_id;
		if (date != null)
			exists += " AND date<='" + date + "'";
		Select query = new Select("(SELECT starting_balance FROM bank_accounts WHERE id=" + bank_accounts_id + ") + CASE WHEN EXISTS(" + exists + ") THEN sum(CASE WHEN NOT reconciled THEN 0 WHEN type='Deposit' THEN coalesce(amount,0.0)+coalesce((SELECT sum(amount) FROM transactions t2 WHERE transactions.id=t2.transactions_id),0.0) WHEN type='Interest' OR type='Transfer In' THEN amount ELSE -amount END) ELSE 0 END").from("transactions").where("bank_accounts_id=" + bank_accounts_id);
		if (date != null)
			query.andWhere("date<='" + date + "'");
		return db.lookupDouble(query, 0);
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceHousehold(int household_id, LocalDate date, DBConnection db) {
		Select query = new Select("sum(CASE WHEN type='Credit' OR type='Payment' THEN -amount ELSE amount END)").from("transactions").where("families_id=" + household_id);
		if (date != null)
			query.andWhere("date<='" + date + "'");
		return db.lookupDouble(query, 0);
	}

	// --------------------------------------------------------------------------

	public boolean
	createHOAInvoices() {
		return m_create_hoa_invoices;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if (segment_one == null)
			return false;
		HTMLWriter writer = request.writer;
		switch(segment_one) {
		case "Account List":
			writer.h3(segment_one);
			ViewState.setFilter("budgets", "active OR type=4 OR type=5", request);
			m_site.newView("budgets", request).writeComponent();
			return true;
		case "Add Assessment Invoices":
			writer.h3(segment_one);
			writeAddInvoicesForm(request);
			return true;
		case "All Transactions":
			writer.h3(segment_one);
			m_site.newView("transactions", request).writeComponent();
			return true;
		case "Annual Report":
			new AnnualReport().write(request);
			return true;
		case "Arrears Report":
			writer.h3(segment_one);
			writeArrearsReport(request.getDate("date"), request.userHasRole("accounting"), false, m_site, request.db, writer);
			return true;
		case "Automatic Transactions":
			writer.h3(segment_one);
			m_site.newView("automatic_transactions", request).writeComponent();
			return true;
		case "Bank Accounts":
			writer.h3(segment_one);
			m_site.newView("bank_accounts", request).writeComponent();
			return true;
		case "Budget for Next Year":
			writer.h3(segment_one);
			ViewState.setFilter("budget report",
				"EXISTS(SELECT 1 FROM accounts_budgets WHERE (type=1 OR type=2) AND accounts_budgets.accounts_id=accounts.id AND amount>0 AND year>=" + Time.newDate().getYear() + ")", request);
			m_site.newView("budget report", request).writeComponent();
			return true;
		case "Checks":
			writer.h3(segment_one);
			m_site.newView("checks", request).writeComponent();
			return true;
		case "Checks by Account":
			new db.AnnualReport(new Select("(SELECT name FROM accounts WHERE accounts.id=transactions.accounts_id),accounts_id,date,amount").from("transactions").where("type='Check'"), "Account", 2, 1, 4)
				.setDetail(new Select("name").from("accounts"), "date,num,(SELECT text FROM payees WHERE payees.id=transactions.payees_id) AS payee,description,amount", "transactions", "accounts_id")
				.setDetailFilter("type='Check'")
				.write(segment_one, request);
			return true;
		case "Checks by Payee":
			new db.AnnualReport(new Select("(SELECT text FROM payees WHERE payees.id=transactions.payees_id),payees_id,date,amount").from("transactions").where("type='Check'"), "Payee", 2, 1, 4)
				.setDetail(new Select("text").from("payees"), "date,num,description,(SELECT name FROM accounts WHERE accounts.id=transactions.accounts_id) AS account,amount", "transactions", "payees_id")
				.setDetailFilter("type='Check'")
				.write(segment_one, request);
			return true;
		case "Credits":
			writer.h3(segment_one);
			m_site.newView("credits", request).writeComponent();
			return true;
		case "Deposits":
			writer.h3(segment_one);
			m_site.newView("deposits", request).writeComponent();
			return true;
		case "Export Transactions":
			writer.h3(segment_one);
			Form form = new Form(null, request.getContext() + "/Accounting/export_transactions", writer).setKeepSubmit(true).open();
			form.rowOpen("start date");
			writer.dateInput("start", Time.newDate().withDayOfMonth(1));
			form.rowOpen("end date");
			writer.dateInput("end", null);
			form.rowOpen("account");
			new RowsSelect("account", new Select("id,name").from("accounts").orderBy("name"), "name", "id", request).setAllowNoSelection(true).write(request);
			form.rowOpen("type");
			new ui.Select("type", TYPES).setAllowNoSelection(true).write(request);
			form.rowOpen("columns");
			for (String column : new String[] { "date", "num", "description", "amount", "household", "account", "type", "bank account", "payee" })
				writer.ui.checkbox("col_" + column, column, null, true, false, false);
			form.setSubmitText("submit").close();
			return true;
		case "Fees":
			writer.h3(segment_one);
			m_site.newView("fees", request).writeComponent();
			return true;
		case "Household Accounts":
			int household_id = request.getInt("families_id", -1);
			if (household_id != -1) {
				writer.h3(request.db.lookupString(new Select("name").from("families").whereIdEquals(household_id)));
				ViewState view_state = (ViewState)request.getOrCreateState(ViewState.class, "household transactions");
				view_state.setBaseFilter("families_id=" + household_id);
				request.setSessionAttribute("families_id", household_id);
				m_site.newView("household transactions", request).writeComponent();
			} else
				writeHouseholdAccountsPane(request);
			return true;
		case "household accounts":
			writeHouseholdAccounts(new Select("id,name").from("families").where("Active Accounts".equals(request.getPathSegment(2)) ? "active" : "NOT active").orderBy("name"), request);
			return true;
		case "Invoices":
			writer.h3(segment_one);
			m_site.newView("invoices", request).writeComponent();
			return true;
		case "Interest":
			writer.h3(segment_one);
			m_site.newView("interest", request).writeComponent();
			return true;
		case "Other Settings":
			writeSettingsForm(null, true, request);
			return true;
		case "Reconciliation":
			String id = request.getPathSegment(2);
			if (id != null) {
				request.setSessionAttribute("bank_accounts_id", id);
				ViewState.setBaseFilter("reconciliation", "bank_accounts_id=" + id, request);
				ViewState.setFilter("reconciliation", null, request);
				m_site.newView("reconciliation", request).writeComponent();
			} else {
				writer.h3(segment_one);
				RowsSelect bank_accounts = new RowsSelect(null, new Select("id,name").from("bank_accounts").where("reconcile").orderBy("name"), "name", "id", request);
				if (bank_accounts.size() > 1) {
					writer.write("Bank Account: ");
					bank_accounts.setAllowNoSelection(true);
					bank_accounts.setOnChange("var e=dom.$('reconciliation');e.innerHTML='';var id=this.options[this.selectedIndex].value;if(id)net.replace(e, context+'/Accounting/Reconciliation/'+id)");
					bank_accounts.write(request);
					writer.write("<div id=\"reconciliation\"></div>");
				} else if (bank_accounts.size() == 1) {
					SelectOption bank_account = bank_accounts.iterator().next();
					writer.write(bank_account.getText());
					writer.write("<div id=\"reconciliation\"></div>");
					writer.js("net.replace(dom.$('reconciliation'), context+'/Accounting/Reconciliation/'+" + bank_account.getValue() + ");");
				} else
					writer.write("There are no Bank Accounts configured for reconcilliation.");
			}
			return true;
		case "Transfers":
			writer.h3(segment_one);
			m_site.newView("transfers", request).writeComponent();
			return true;
		}
		if (segment_one.equals(m_label + " Payments")) {
			writer.h3(segment_one);
			m_site.newView("payments", request).writeComponent();
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "Add Invoices":
			addInvoices(request);
			return true;
		case "export_transactions":
			exportTransactions(request);
			return true;
		case "Send Statements":
			sendStatements(request.site, request.db);
			return true;
		}
 		return false;
	}

	// --------------------------------------------------------------------------

	private void
	exportTransactions(Request request) {
		ArrayList<String> columns = new ArrayList<>();
		ArrayList<String> heads = new ArrayList<>();
		for (String column : new String[] { "date", "num", "description", "amount", "household", "account", "type", "bank account", "payee" })
			if (request.getParameter("col_" + column) != null) {
				if ("household".equals(column))
					columns.add("(SELECT name FROM families WHERE families.id=families_id)");
				else if ("account".equals(column))
					columns.add("(SELECT name FROM accounts WHERE accounts.id=accounts_id)");
				else if ("bank account".equals(column))
					columns.add("(SELECT name FROM bank_accounts WHERE bank_accounts.id=bank_accounts_id)");
				else if ("payee".equals(column))
					columns.add("(SELECT text FROM payees WHERE payees.id=payees_id)");
				else
					columns.add(column);
				heads.add(column);
			}

		Select query = new Select(Text.join(",", columns)).from("transactions").orderBy("date");
		LocalDate start = request.getDate("start");
		if (start != null)
			query.andWhere("date>='" + start.toString() + "'");
		LocalDate end = request.getDate("end");
		if (end != null)
			query.andWhere("date<='" + end.toString() + "'");
		int account_id = request.getInt("account", 0);
		if (account_id != 0)
			query.andWhere("accounts_id=" + account_id);
		String type = request.getParameter("type");
		if (type.length() > 0)
			query.andWhere("type=" + DBConnection.string(type));

		Rows rows = new Rows(query, request.db);
		CSVWriter w = new CSVWriter("transactions", request);
		for (String column : heads)
			w.column(column);
		while (rows.next()) {
			w.row();
			for (int i=0; i<heads.size(); i++)
				if ("amount".equals(heads.get(i)))
					w.column(rows.getDouble(i + 1));
				else
					w.column(rows.getString(i + 1));
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Accounting system which can optionally interface with the Common Meals module, Guest Room modules and others";
	}

	// --------------------------------------------------------------------------

	public String
	getAccountLabel() {
		return m_label;
	}

	// --------------------------------------------------------------------------

	public static double
	getTotalYTD(int accounts_id, int year, DBConnection db) {
		double total = 0;
		try {
			ResultSet rs = db.select("SELECT sum(amount) " + "FROM transactions " + "WHERE accounts_id=" + accounts_id + " AND date>='1/1/" + year + "'");
			if (rs.next())
				total = rs.getDouble(1);
			rs.getStatement().close();
		} catch (SQLException e) {
		}

		return total;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		m_accounts = new Options(new Select("id,name AS text").from("accounts").orderBy("name"), m_site);
		m_account_filter = new AccountFilter(m_accounts, Location.TOP_LEFT);
		JDBCTable table_def = new JDBCTable("accounts")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("ongoing_account", Types.INTEGER))
			.add(new JDBCColumn("type", Types.INTEGER))
			.add(new JDBCColumn("email", Types.VARCHAR))
			.add(new JDBCColumn("initial_balance", Types.DOUBLE));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("accounts_budgets")
			.add(new JDBCColumn("accounts"))
			.add(new JDBCColumn("year", Types.INTEGER))
			.add(new JDBCColumn("amount", Types.DOUBLE));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("bank_accounts")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("reconcile", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("starting_balance", Types.DOUBLE).setDefaultValue("0"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("transactions")
			.add(new JDBCColumn("accounts_id", Types.INTEGER))
			.add(new JDBCColumn("amount", Types.DOUBLE))
			.add(new JDBCColumn("bank_accounts_id", Types.INTEGER))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("families_id", Types.INTEGER))
			.add(new JDBCColumn("num", Types.VARCHAR))
			.add(new JDBCColumn("payees_id", Types.INTEGER))
			.add(new JDBCColumn("reconciled", Types.BOOLEAN))
			.add(new JDBCColumn("type", Types.VARCHAR))
			.add(new JDBCColumn("transactions_id", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex("transactions", "bank_accounts_id");
		db.createIndex("transactions", "date");
		db.createIndex("transactions", "families_id");
		db.createIndex("transactions", "transactions_id");
		db.createTable("payees", "text VARCHAR");
		m_site.addObjects(new Options(new Select("*").from("payees").orderBy("lower(text)"), m_site).setAllowEditing(true));
		m_payee_column = ((Options)m_site.getObjects("payees")).newColumn("payees_id").setDisplayName("payee");
		table_def = new JDBCTable("automatic_transactions")
				.add(new JDBCColumn("accounts_id", Types.INTEGER))
				.add(new JDBCColumn("amount", Types.DOUBLE))
				.add(new JDBCColumn("bank_accounts_id", Types.INTEGER))
				.add(new JDBCColumn("description", Types.VARCHAR))
				.add(new JDBCColumn("families_id", Types.INTEGER))
				.add(new JDBCColumn("generate_day", Types.INTEGER))
				.add(new JDBCColumn("payees_id", Types.INTEGER))
				.add(new JDBCColumn("transaction_day", Types.INTEGER))
				.add(new JDBCColumn("type", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		Roles.add("accounting");
		Roles.add("hoa history");
		addPage(new Page("Accounting", m_site) {
			@Override
			public void write(Request request) {
				request.site.writePageOpen("Accounting", request);
				TaskPane task_pane = request.writer.ui.taskPane("Accounting").open();
				boolean user_has_accounting_role = request.userHasRole("accounting");

				task_pane.heading("Income");
				ArrayList<String> income_items = new ArrayList<>();
				if (user_has_accounting_role)
					income_items.add("Add Assessment Invoices");
				income_items.add("Invoices");
				income_items.add(m_label + " Payments");
				task_pane.sortedTasks(income_items);

				task_pane.heading("Expense");
				task_pane.task("Checks");
				task_pane.task("Credits");

				if (user_has_accounting_role) {
					task_pane.heading("Other");
					task_pane.task("All Transactions");
					task_pane.task("Automatic Transactions");
					task_pane.task("Export Transactions");
					task_pane.task("Transfers");
				}

				task_pane.heading("Banking");
				task_pane.task("Deposits");
				task_pane.task("Fees");
				task_pane.task("Interest");
				if (user_has_accounting_role)
					task_pane.task("Reconciliation");

				Map<String,Block> blocks = getBlocks();
				if (user_has_accounting_role)
					if (blocks != null) {
						boolean first = true;
						for (Block block : blocks.values())
							if (block.handlesContext("accounting module", request)) {
								if (first) {
									task_pane.heading("Modules");
									first = false;
								}
								task_pane.task(block.getName(), request.getContext() + "/Accounting/blocks/" + block.getName() + "/accounting module");
							}
					}

				task_pane.heading("Reports");
				boolean is_in_group = ((mosaic.Person)request.getUser()).isInGroup("Finance Committee", request.db);
				TreeMap<String,String> reports = new TreeMap<>();
				if (user_has_accounting_role || request.userHasRole("hoa history") || is_in_group)
					reports.put(m_label + (m_label.endsWith("Account") ? "s" : " Accounts"), "Accounting/Household Accounts");
				reports.put("Annual Report", null);
				reports.put("Arrears Report", null);
				reports.put("Checks by Account", null);
				reports.put("Checks by Payee", null);
				if (request.db.countRows(new Select("1").from("accounts_budgets").where("year>" + Time.newDate().getYear())) != 0)
					reports.put("Budget for Next Year", null);
				if (blocks != null)
					for (Block block : blocks.values())
						if (block.handlesContext("accounting report", request))
							reports.put(block.getName(), request.getContext() + "/Accounting/blocks/" + block.getName() + "/accounting report");
				task_pane.tasks(reports);

				if (!m_send_statements) {
					task_pane.heading("Statements");
					task_pane.taskOnClick("Send Statements", "Dialog.confirm('Email accounting statements to community?', function(){net.post(context + '/Accounting/Send Statements', '', function(){Dialog.alert('Statements Sent','The statements have been emailed')})})");
				}

				if (user_has_accounting_role || is_in_group) {
					task_pane.heading("Settings");
					task_pane.task("Account List");
					task_pane.task("Bank Accounts");
					task_pane.taskOnClick("Other Settings", "new Dialog({cancel:true,ok:true,title:'Accounting Settings',url:context+'/Accounting/Other Settings'});return false;");
				}
				task_pane.closeTasks();
				task_pane.close();
				request.close();
			}
		}.setRole(m_site.getDefaultRole()), m_site, db);
		db.update("transactions", "type='Transfer In'", "id IN (SELECT transactions.id FROM transactions JOIN accounts ON accounts.id=accounts_id WHERE transactions.type='Transfer' AND (accounts.type=1 OR accounts.type=4))");
		db.update("transactions", "type='Transfer Out'", "id IN (SELECT transactions.id FROM transactions JOIN accounts ON accounts.id=accounts_id WHERE transactions.type='Transfer' AND (accounts.type=2 OR accounts.type=5))");
		db.update("transactions", "type='Transfer Out'", "type='Transfer'");
	}

	// --------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("accounts"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("name")
				.setRecordName("Account")
				.setColumnNamesForm(new String[] { "name", "type", "initial_balance", "email" })
				.setColumnNamesTable(new String[] { "name", "type" })
				.setColumn(m_account_type_column)
				.addRelationshipDef(new OneToMany("accounts_budgets").setSpanFormCols(false));
		if (name.equals("accounts_budgets"))
			return new ViewDef(name)
				.setDefaultOrderBy("year")
				.setRecordName("Budget");
		if (name.equals("automatic_transactions"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setRecordName("Automatic Transaction")
				.setColumnNamesForm(new String[] { "type", "generate_day", "transaction_day", "payees_id", "description", "amount", "accounts_id", "bank_accounts_id", "families_id" })
				.setColumnNamesTable(new String[] { "generate_day", "transaction_day", "type", "payees_id", "families_id", "description", "accounts_id", "amount", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount")).setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column).setColumn(m_type_column)
				.setColumn(m_payee_column);
		if (name.equals("bank_accounts"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit().view())
				.setDefaultOrderBy("name")
				.setRecordName("Bank Account")
				.setColumnNamesForm(new String[] { "name", "reconcile", "starting_balance" })
				.setColumnNamesTable(new String[] { "name", "reconcile" })
				.setColumn(new Column("name").setIsRequired(true));
		if (name.equals("budget report"))
			return new ViewDef(name)
				.addSection(new Dividers("type", new OrderBy("type")))
				.setAccessPolicy(new AccessPolicy())
				.setDefaultOrderBy("name")
				.setFrom("accounts")
				.setRecordName("Account")
				.setShowGrandTotals(false)
				.setShowPrintLinkTable(true)
				.setColumnNamesTable(new String[] { "name", "this_year", "remaining", "next_year" })
				.setColumn(m_this_year_column)
				.setColumn(m_next_year_column)
				.setColumn(m_remaining_column)
				.setColumn(m_account_type_column);
		if (name.equals("budgets"))
			return new ViewDef(name) {
					@Override
					public void
					afterList(View view, Request request) {
						if (((ViewState)request.getOrCreateState(ViewState.class, m_name)).getFilter() != null)
							request.writer.ui.buttonOnClick("view all accounts", "net.post(context+'/db','db_cmd=clear_filter&db_view_def=budgets',function(){net.replace(C._(this))}.bind(this))");
					}
				}
				.addSection(new Dividers("type", new OrderBy("type")))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("name")
				.setFrom("accounts")
				.setRecordName("Account")
				.setShowGrandTotals(false)
				.setColumnNamesForm(new String[] { "name", "type", "initial_balance", "email", "ongoing_account", "active" })
				.setColumnNamesTable(new String[] { "name", "this_year", "remaining", "next_year" })
				.setColumn(m_this_year_column)
				.setColumn(m_next_year_column)
				.setColumn(new LookupColumn("ongoing_account", "accounts", "name", "type=5", "name").setAllowNoSelection(true))
				.setColumn(m_remaining_column)
				.setColumn(m_account_type_column)
				.addRelationshipDef(new OneToMany("accounts_budgets").setSpanFormCols(false));
		if (name.equals("checks"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.addFeature(m_account_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Check'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Check")
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "num", "payees_id", "description", "accounts_id", "amount", "bank_accounts_id" })
				.setColumn(m_account_expense_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(m_payee_column)
				.setColumn(new Column("type").setDefaultValue("Check").setIsHidden(true));
		if (name.equals("credits"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.addFeature(m_account_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Credit'")
				.setDefaultOrderBy("date,CASE WHEN num~'^\\d+$' THEN num::integer ELSE 0 END")
				.setFrom("transactions")
				.setRecordName("Credit")
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "amount", "accounts_id", "families_id" })
				.setColumnNamesTable(new String[] { "date", "num", "description", "accounts_id", "families_id", "amount" })
				.setColumn(m_account_expense_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("num").setDefaultToSQL(new Select("MAX(CASE WHEN num~'^\\d+$' THEN num::integer ELSE 0 END) + 1").from("transactions").where("type='Credit'")).setIsReadOnly(true))
				.setColumn(new Column("type").setDefaultValue("Credit").setIsHidden(true));
		if (name.equals("deposits"))
			return new ViewDef(name) {
					@Override
					public void
					afterDelete(String where, Request request) {
						request.db.update("transactions", "transactions_id=NULL", "transactions_id=" + where.substring(where.indexOf('=') + 1));
					}
				}
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Deposit'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Deposit")
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "amount", "total", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "num", "description", "amount", "total", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount").setDisplayName("cash"))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new CurrencyColumn("total"){
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						request.writer.writeCurrency(view.data.getDouble("amount") + request.db.lookupDouble(new Select("sum(amount)").from("transactions").whereEquals("transactions_id", view.data.getInt("id")), 0));
						return true;
					}
				}.setIsReadOnly(true))
				.setColumn(new Column("type").setDefaultValue("Deposit").setIsHidden(true))
				.addRelationshipDef(new OneToManyLink("payments for deposit", "num").setMode(LookupColumn.Mode.TABLE));
		if (name.equals("fees"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Bank Fee'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Bank Fee")
				.setColumnNamesForm(new String[] { "type", "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumn(m_account_column)
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new Column("type").setDefaultValue("Bank Fee").setIsHidden(true));
		if (name.equals("household transactions"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transaction")
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "accounts_id", "amount", "families_id" })
				.setColumnNamesTable(new String[] { "date", "type", "description", "accounts_id", "amount", "balance" })
				.setColumn(m_account_column)
				.setColumn(new CurrencyColumn("amount") {
					@Override
					public double
					getDouble(View view, Request request) {
						String type = view.data.getString("type");
						return type.equals("Credit") || type.equals("Payment") ? -view.data.getDouble(m_name) : view.data.getDouble(m_name);
					}
				}.setTotal(true))
				.setColumn(new Column("balance") {
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						MutableDouble balance = (MutableDouble)data.get("balance");
						if (balance == null) {
							balance = new MutableDouble(calcBalanceHousehold(view.data.getInt("families_id"), view.data.getDate("date").minusDays(1), request.db));
							data.put("balance", balance);
						}
						String type = view.data.getString("type");
						double amount = type.equals("Credit") || type.equals("Payment") ? -view.data.getDouble("amount") : view.data.getDouble("amount");
						balance.add(amount);
						request.writer.writeCurrency(balance.value());
						return true;
					}
				}.setCellStyle("text-align:right"))
				.setColumn(m_date_column)
				.setColumn(new LookupColumn("families_id", "families", "name", "active", "name").setDefaultToSessionAttribute().setDisplayName("household"))
				.setColumn(m_type_column);
		if (name.equals("interest"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Interest'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Interest")
				.setColumnNamesForm(new String[] { "type", "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumn(new LookupColumn("accounts_id", "accounts", "name").setAllowNoSelection(true).setDefaultValue(m_default_interest_account).setDisplayName("account"))
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new Column("type").setDefaultValue("Interest").setIsHidden(true));
		if (name.equals("invoices"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.addFeature(m_account_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Invoice'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Invoice")
				.setColumnNamesForm(new String[] { "type", "date", "description", "amount", "accounts_id", "families_id" })
				.setColumnNamesTable(new String[] { "date", "description", "accounts_id", "families_id", "amount" })
				.setColumn(m_account_income_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("type").setDefaultValue("Invoice").setIsHidden(true));
		if (name.equals("payees"))
			return ((Options)m_site.getObjects(name)).newViewDef(name, site)
				.setRecordName("Payee")
				.setColumn(new Column("text").setDisplayName("name").setIsRequired(true));
		if (name.equals("payments"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Payment'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Payment")
				.setColumnNamesForm(new String[] { "type", "date", "amount", "families_id", "num" })
				.setColumnNamesTable(new String[] { "date", "families_id", "amount" })
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("num").setDisplayName("check num"))
				.setColumn(new Column("type").setDefaultValue("Payment").setIsHidden(true));
		if (name.equals("payments for deposit"))
			return new ViewDef(name){
					@Override
					public View
					newView(Request request) {
						return new View(this, request){
							@Override
							public View
							select() {
								if (m_mode == View.Mode.SELECT_LIST)
									m_state.setBaseFilter("type='Payment' AND transactions_id IS NULL");
								else
									m_state.setBaseFilter("type='Payment'");
								return super.select();
							}
						};
					}
				}
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Payment")
				.setColumnNamesForm(new String[] { "type", "date", "amount", "families_id", "num" })
				.setColumnNamesTable(new String[] { "date", "type", "families_id", "amount" })
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("type").setDefaultValue("Payment").setIsReadOnly(true));
		if (name.equals("reconciliation"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting"))
				.setAllowQuickEdit(true)
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transaction")
				.setReplaceOnQuickEdit(true)
				.setSelectColumns("id,accounts_id,CASE WHEN type='Deposit' THEN coalesce(amount,0.0)+coalesce((SELECT sum(amount) FROM transactions t2 WHERE transactions.id=t2.transactions_id),0.0) WHEN type='Interest' OR type='Transfer In' THEN amount ELSE NULL END credit,CASE WHEN type != 'Deposit' AND type != 'Interest' AND type != 'Transfer In' THEN -amount ELSE NULL END debit,bank_accounts_id,date,description,num,payees_id,reconciled,type")
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "reconciled", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "type", "num", "payees_id", "description", "accounts_id", "credit", "debit", "reconciled", "balance" })
				.setColumn(m_account_column)
				.setColumn(new Column("balance") {
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						if (!view.data.getBoolean("reconciled"))
							return false;
						MutableDouble balance = (MutableDouble)data.get("balance");
						if (balance == null) {
							LocalDate date = view.data.getDate("date").minusDays(1);
							balance = new MutableDouble(calcBalanceBank(view.data.getInt("bank_accounts_id"), date.toString(), request.db));
							data.put("balance", balance);
						}
						balance.add(view.data.getDouble("credit") + view.data.getDouble("debit"));
						request.writer.writeCurrency(balance.value());
						return true;
					}
				}.setCellStyle("text-align:right"))
				.setColumn(new LookupColumn("bank_accounts_id", "bank_accounts", "name").setDefaultToSessionAttribute().setDisplayName("bank account").setIsReadOnly(true))
				.setColumn(new CreditDebitColumn("credit"))
				.setColumn(m_date_column)
				.setColumn(new CreditDebitColumn("debit"))
				.setColumn(m_payee_column)
				.setColumn(m_type_column);
		if (name.equals("transactions"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.addFeature(m_account_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type!='Deposit'")
				.setDefaultOrderBy("date")
				.setRecordName("Transaction")
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "families_id", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "type", "num", "payees_id", "description", "accounts_id", "families_id", "amount", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(m_payee_column)
				.setColumn(m_type_column);
		if (name.equals("transfers"))
			return new ViewDef(name)
				.addFeature(m_month_filter)
				.addFeature(m_year_filter)
				.addFeature(m_account_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("(type='Transfer In' OR type='Transfer Out')")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transfer")
				.setColumnNamesForm(new String[] { "type", "date", "accounts_id", "description", "amount", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "type", "description", "accounts_id", "amount", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new OptionsColumn("type", "Transfer In", "Transfer Out"));
		return null;
	}

	// --------------------------------------------------------------------------

	@ClassTask
	@Override
	public void
	nightly(LocalDateTime now, Site site, DBConnection db) {
		int day_of_month = now.getDayOfMonth();
		if (m_create_hoa_invoices && day_of_month == m_hoa_invoice_date)
			addHOAInvoices(now.toLocalDate().plusMonths(1).withDayOfMonth(1), site, db);
		if (m_send_statements && day_of_month == m_statement_date)
			sendStatements(site, db);
		if (m_charge_arrears_fees && day_of_month == m_arrears_fee_date)
			writeArrearsInvoices(m_arrears_account_id, null, site, db);
		NameValuePairs name_value_pairs = new NameValuePairs();
		Rows rows = new Rows(new Select("*").from("automatic_transactions").where("generate_day=" + day_of_month), db);
		while (rows.next()) {
			int accounts_id = rows.getInt("accounts_id");
			if (!rows.wasNull())
				name_value_pairs.set("accounts_id", accounts_id);
			else
				name_value_pairs.remove("accounts_id");
			name_value_pairs.set("amount", rows.getDouble("amount"));
			int bank_accounts_id = rows.getInt("bank_accounts_id");
			if (!rows.wasNull())
				name_value_pairs.set("bank_accounts_id", bank_accounts_id);
			else
				name_value_pairs.remove("bank_accounts_id");
			LocalDate date = now.toLocalDate();
			int transaction_day = rows.getInt("transaction_day");
			if (transaction_day != 0) {
				date = date.withDayOfMonth(transaction_day);
				if (transaction_day < day_of_month)
					date = date.plusMonths(1);
			}
			name_value_pairs.setDate("date", date);
			name_value_pairs.set("description", rows.getString("description"));
			if (!rows.wasNull())
				name_value_pairs.set("families_id", rows.getInt("families_id"));
			else
				name_value_pairs.remove("families_id");
			int payees_id = rows.getInt("payees_id");
			if (!rows.wasNull())
				name_value_pairs.set("payees_id", payees_id);
			else
				name_value_pairs.remove("payees_id");
			name_value_pairs.set("type", rows.getString("type"));
			db.insert("transactions", name_value_pairs);
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	private void
	sendStatement(Mail mail, int household_id, int contact_id, double balance, LocalDate last_payment, Site site, DBConnection db) {
		String email = db.lookupString(new Select("email").from("people").whereIdEquals(contact_id));
		if (email == null)
			//			site.log("sendStatement(): no people record with id " + contact_id);
			return;

		NumberFormat currency_format = new DecimalFormat("$0.00;-$0.00");
		StringBuilder message = new StringBuilder("<html><body>");
		if (m_statement_intro != null) {
			message.append(m_statement_intro);
			message.append("<br /><br />");
		}
		int date = Time.newDate().getDayOfMonth();
		if (m_create_hoa_invoices && date >= m_hoa_invoice_date)
			message.append("The balance on your account will be ").append(currency_format.format(balance)).append(" on the 1st of next month.");
		else
			message.append("Your current balance is ").append(currency_format.format(balance)).append(".");
		if (last_payment != null)
			message.append(" Your last payment was made on ").append(Time.formatter.getDateShort(last_payment)).append(".");
		message.append("<br /><br />");
		if (balance == 0.0) {
			if (m_statement_zero_balance != null)
				message.append(m_statement_zero_balance);
		} else if (balance < 0.0) {
			if (m_statement_negative_balance != null)
				message.append(m_statement_negative_balance);
		} else if (m_statement_positive_balance != null)
			message.append(m_statement_positive_balance);
		message.append("<br /><br /><h3>Recent Transactions</h3><table border=\"1\"><thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Amount</th><th>Balance</th></tr></thead>");
		Rows rows = new Rows(new Select("date,type,description,amount").from("transactions").where("families_id=" + household_id).orderBy("date DESC,id DESC").limit(20), db);
		while (rows.next()) {
			message.append("<tr><td>");
			message.append(rows.getString(1));
			message.append("</td><td>");
			String type = rows.getString(2);
			message.append(type);
			message.append("</td><td>");
			String description = rows.getString(3);
			if (description != null)
				message.append(description);
			message.append("</td><td style=\"text-align:right\">");
			double amount = type.equals("Credit") || type.equals("Payment") ? -rows.getDouble(4) : rows.getDouble(4);
			message.append(currency_format.format(amount));
			message.append("</td><td style=\"text-align:right\">");
			message.append(currency_format.format(balance));
			balance -= amount;
			message.append("</td></tr>");
		}
		rows.close();
		message.append("</table><br /><br />Click <a href=\"");
		message.append(site.getURL("household.jsp"));
		message.append("\">here</a> to check your account history.</body></html>");
		mail.send(m_statement_from, email, "Monthly Statement", message.toString(), true);
	}

	// --------------------------------------------------------------------------

	@ClassTask
	public void
	sendStatements(Site site, DBConnection db) {
		Mail mail = new Mail(site);

		Rows rows = new Rows(new Select("id,contact").from("families").where("active AND contact IS NOT NULL").orderBy("name"), db);
		while (rows.next()) {
			int household_id = rows.getInt(1);
			double balance = calcBalanceHousehold(household_id, null, db);
			LocalDate last_payment = db.lookupDate(new Select("max(date)").from("transactions").where("families_id=" + household_id + " AND type='Payment'"));
			sendStatement(mail, household_id, rows.getInt(2), balance, last_payment, site, db);
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request request) {
		super.updateSettings(request);
		m_bank_account_column.setDefaultValue(m_default_bank_account);
	}

	// --------------------------------------------------------------------------

	private void
	writeAddInvoicesForm(Request request) {
		HTMLWriter writer = request.writer;
		writer.write("<div class=\"panel panel-primary\"><div class=\"panel-body\">This is for adding similar invoices to some or all households for a one-time assessment. Enter the date, description and amount. Select which account to use for the invoices. Check which households to invoice and then click the 'add invoices' button.</div></div>");
		Form form = new Form("add invoices", request.getContext() + "/Accounting/Add Invoices", writer).setSubmitText("add invoices").setButtonsLocation(Form.Location.BOTTOM).open();
		form.rowOpen("date");
		writer.dateInput("date", Time.newDate());
		form.rowOpen("description");
		writer.textInput("description", null, null);
		form.rowOpen("amount");
		writer.textInput("amount", "10", null);
		form.rowOpen("account");
		new LookupColumn("accounts_id", "accounts", "name").writeInput((View)null, null, request);
		form.rowOpen("households");
		ResultSet rs = request.db.select("SELECT id,name FROM families WHERE active ORDER BY name");
		try {
			while (rs.next()) {
				writer.write("<div class=\"checkbox\">");
				writer.ui.checkbox("f" + rs.getInt(1), rs.getString(2), null, false, false, false);
				writer.write("</div>");
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			request.abort(e);
		}
		form.close();
	}

	// --------------------------------------------------------------------------

	@ClassTask({"account id","date (blank for today)"})
	public static void
	writeArrearsInvoices(int account_id, LocalDate date, Site site, DBConnection db) {
		if (date == null)
			date = Time.newDate();

		try {
			ResultSet rs = db.select("SELECT id FROM families");
			while (rs.next()) {
				int household_id = rs.getInt(1);
				double balance = calcBalanceArrears(household_id, date, site, db);
				if (balance > 0) {
					float dues = db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + household_id), 0);
					if (dues > 0 && balance >= dues * 2) {
						double penalty = balance * .005;
						if (!db.lookupBoolean(new Select("hardship").from("families").whereIdEquals(household_id)))
							penalty += 25;
						addInvoice(account_id, household_id, date, "Arrears Penalty", penalty, site, db);
					}
				}
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// --------------------------------------------------------------------------

	public static void
	writeArrearsReport(LocalDate date, boolean show_names, boolean email, Site site, DBConnection db, HTMLWriter writer) {
		if (date == null)
			date = Time.newDate();

		int num_arrears = 0;
		int num_due = 0;
		double total_arrears = 0;
		double total_due = 0;

		if (!email) {
			writer.write("<form class=\"form-inline\">as of ");
			writer.setAttribute("id", "date");
			writer.dateInput(null, date);
			writer.nbsp().nbsp();
			writer.ui.buttonOnClick("go", "net.replace(dom.$('panel'), context+'/Accounting/Arrears Report?date='+dom.$('date').value)");
			writer.write("</form>").br().br();
		}
		writer.write("the <b>due</b> column shows households that owe money but aren't yet in arrears. the <b>arrears</b> column shows households that are in arrears.");
		writer.write("<table class=\"table table-condensed table-bordered table-hover\" style=\"width:auto;\"><tr>");
		if (show_names)
			writer.write("<th>account</th>");
		writer.write("<th>due</th><th>arrears</th><th>last payment/credit date</th></tr>");
		try {
			ResultSet rs = db.select(new Select("id,name").from("families").where("active").orderBy("name"));
			while (rs.next()) {
				int id = rs.getInt(1);
				double balance = calcBalanceArrears(id, date, site, db);
				if (balance > 0) {
					writer.write("<tr>");
					if (show_names)
						writer.write("<td>").write(rs.getString(2)).write("</td>");
					float dues = db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + id), 0);
					if (dues > 0 && balance > dues * 2) {
						writer.write("<td></td><td style=\"text-align:right\">").writeCurrency(balance).write("</td>");
						total_arrears += balance;
						++num_arrears;
					} else {
						writer.write("<td style=\"text-align:right\">").writeCurrency(balance).write("</td><td></td>");
						total_due += balance;
						++num_due;
					}
					writer.write("<td style=\"text-align:right\">");
					writer.writeDate(db.lookupDate(new Select("max(date)").from("transactions").where("families_id=" + id + " AND (type='Payment' OR type='Credit')")));
					writer.write("</td></tr>");
				}
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		writer.write("<tr>");
		if (show_names)
			writer.write("<td><b>total</b></td>");
		writer.write("<td style=\"text-align:right\"><b>");
		writer.writeCurrency(total_due);
		writer.write("</b></td><td style=\"text-align:right\"><b>");
		writer.writeCurrency(total_arrears);
		writer.write("</b></td><td></td></tr>");
		if (show_names) {
			writer.write("<tr><td><b>households</b></td><td style=\"text-align:right\"><b>");
			writer.write(num_due);
			writer.write("</b></td><td style=\"text-align:right\"><b>");
			writer.write(num_arrears);
			writer.write("</b></td><td></td></tr>");
		}
		writer.write("</table>");
	}

	// --------------------------------------------------------------------------

	private void
	writeHouseholdAccounts(Select select, Request request) {
		Rows rows = new Rows(select, request.db);
		if (!rows.isBeforeFirst()) {
			rows.close();
			return;
		}
		HTMLWriter writer = request.writer;
		writer.write("<div class=\"list_border_inner\"><table class=\"table table-condensed table-bordered table-hover\"><thead><tr class=\"db_columnheads\"><th>Household</th><th>Balance</th></tr></thead>");
		LocalDate date = request.getDate("date");
		double total = 0;
		while (rows.next()) {
			writer.write("<tr class=\"db_row\"><td>");
			writer.aOnClick(rows.getString(2), "net.replace('household_account', context+'/Accounting/Household Accounts?families_id=" + rows.getInt(1) + "')");
			writer.write("</td><td>");
			double balance = calcBalanceHousehold(rows.getInt(1), date, request.db);
			total += balance;
			writer.writeCurrency(balance);
			writer.write("</td></tr>");
		}
		rows.close();
		writer.write("<tr class=\"db_row\"><td><strong>Total</strong></td><td>");
		writer.writeCurrency(total);
		writer.write("</td></tr></table></div>");
	}

	// --------------------------------------------------------------------------

	private void
	writeHouseholdAccountsPane(Request request) {
		HTMLWriter writer = request.writer;
		Table table = writer.ui.table();
		writer.setAttribute("style", "vertical-align: top;");
		table.td();
		String on_change = "var s=dom.$('report_select');var d=dom.$('report_date');net.replace('household_accounts',context+'/Accounting/household accounts/'+s.options[s.selectedIndex].text+'?date='+d.firstChild.firstChild.value)";
		writer.setAttribute("id", "report_select")
			.setAttribute("onchange", on_change)
			.select(null, new String[] {"Active Accounts", "Inactive Accounts"}, null, null)
			.br().setAttribute("id", "report_date")
			.setAttribute("style", "padding-top:10px")
			.tagOpen("div");
		writer.setAttribute("onchange", on_change)
			.dateInput(null, Time.newDate())
			.tagClose();
		writer.write("<div id=\"household_accounts\" style=\"padding-top:10px\">");
		writeHouseholdAccounts(new Select("id,name").from("families").where("active").orderBy("name"), request);
		writer.write("</div>")
			.setAttribute("id", "household_account")
			.setAttribute("style", "padding:0 10px;vertical-align: top;");
		table.td();
		table.close();
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request request) {
		if ("m_default_bank_account".equals(field.getName()))
			new RowsSelect("default_bank_account", new Select("name,id").from("bank_accounts").orderBy("name"), "name", "id", request).setSelectedOption(null, Integer.toString(m_default_bank_account)).write(request);
		else
			super.writeSettingsInput(field, request);
	}
}
