package accounting;

public class MutableDouble {
	private double	m_value;

	//--------------------------------------------------------------------------

	public
	MutableDouble(double value) {
		m_value = value;
	}

	//--------------------------------------------------------------------------

	public void
	add(double value) {
		m_value += value;
	}

	//--------------------------------------------------------------------------

	public double
	value() {
		return m_value;
	}
}
