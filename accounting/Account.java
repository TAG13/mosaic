package accounting;

import java.sql.ResultSet;
import java.sql.SQLException;

import db.DBConnection;
import web.HTMLWriter;

public class Account {
	private double		m_budget;
	private boolean		m_has_data;
	private String		m_id;
	private String		m_name;
	private double[]	m_sums = new double[12];
	private int			m_type;

	//--------------------------------------------------------------------------

	public
	Account(String name, int year, DBConnection db) {
		m_name = name;
		ResultSet rs = db.select("SELECT accounts.id,type,amount FROM accounts LEFT JOIN accounts_budgets ON accounts_budgets.accounts_id=accounts.id AND year=" + year + " WHERE name='" + name + "'");
		try {
			rs.next();
			m_id = rs.getString(1);
			m_type = rs.getInt(2);
			m_budget = rs.getDouble(3);
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public void
	add(double amount, int period) {
		m_sums[period] += amount;
		m_has_data = true;
	}

	//--------------------------------------------------------------------------

	public double
	getBudget() {
		return m_budget;
	}

	//--------------------------------------------------------------------------

	public String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public double[]
	getSums() {
		return m_sums;
	}

	//--------------------------------------------------------------------------

	public int
	getType() {
		return m_type;
	}

	//--------------------------------------------------------------------------

	public boolean
	hasData() {
		return m_has_data;
	}

	//--------------------------------------------------------------------------

	public void
	writeRow(int period, int year, boolean show_periods, HTMLWriter writer) {
		writer.write("<tr><td class=\"qr_account\">");
		writer.aOnClick(m_name, "net.replace(dom.$('transactions'), context+'/Accounting/Annual Report?account=" + m_id + "&year=" + year + "')");
		writer.write("</td><td style=\"text-align:right\">");
		if (m_budget > 0)
			writer.writeCurrency(m_budget);
		else
			writer.nbsp();
		double total = 0;
		for (int q=0; q<=period; q++) {
			if (show_periods) {
				writer.write("</td><td style=\"text-align:right\">");
				if (m_sums[q] == 0)
					writer.nbsp();
				else
					writer.writeCurrency(m_sums[q]);
			}
			total += m_sums[q];
		}
		if (period > 0 || !show_periods) {
			writer.write("</td><td style=\"text-align:right\">");
			writer.writeCurrency(total);
		}
		writer.write("</td><td style=\"text-align:right\">");
		if (m_budget > 0)
			writer.writeCurrency(m_budget - total);
		else
			writer.nbsp();
		writer.write("</td><td style=\"text-align:right\">");
		if (m_budget > 0)
			writer.writePercent(1.0 - total / m_budget);
		else
			writer.nbsp();
		writer.write("</td></tr>");
	}
}
