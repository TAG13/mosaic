package accounting;

import java.util.UUID;

import app.Request;
import db.NameValuePairs;
import db.Options;
import db.View;
import db.feature.QuickFilter;
import ui.Select;

public class AccountFilter extends QuickFilter {
	private Options	m_accounts;

	//--------------------------------------------------------------------------

	public
	AccountFilter(Options accounts, Location location) {
		super(location);
		m_accounts = accounts;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(View view, Request request) {
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return "accounts_id";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	set(NameValuePairs name_value_pairs) {
		int accounts_id =  name_value_pairs.getInt("accounts_id", 0);
		return accounts_id == 0 ? null : "accounts_id='" + accounts_id + "'";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	write(View view, Request request) {
		String id = UUID.randomUUID().toString();
		request.writer.setAttribute("id", id);
		Select select = new Select(null, m_accounts.getObjects()).addFirstOption("All Accounts", "all").addFirstOption("No Account", "null");
		String filter = view.getState().getFilter();
		if (filter != null) {
			int index = filter.indexOf("accounts_id");
			if (index != -1)
				if (filter.charAt(index + 11) == '=')
					select.setSelectedOption(null, filter.substring(index + 13, filter.indexOf('\'', index + 14)));
				else
					select.setSelectedOption(null, "null");
		}
		select.write(request);
		return id;
	}
}
