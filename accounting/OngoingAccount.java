package accounting;

import java.time.LocalDate;

import db.DBConnection;
import db.Select;
import ui.Table;
import web.HTMLWriter;

public class OngoingAccount {
	private final double[]	m_expense = new double[12];
	private boolean			m_has_data;
	private final String	m_ids;
	private final double[]	m_income = new double[12];
	private final String	m_name;
	private double			m_starting_balance;
	private final int		m_year;

	//--------------------------------------------------------------------------

	OngoingAccount(String name, int year, DBConnection db) {
		m_name = name;
		m_year = year;
		int income_id = db.lookupInt("id", "accounts", "name='" + DBConnection.escape(name) + " Income'", 0);
		int expense_id = db.lookupInt("id", "accounts", "name='" + DBConnection.escape(name) + " Expense'", 0);
		m_ids = income_id + "," + expense_id;
		m_starting_balance = db.lookupDouble(new Select("initial_balance").from("accounts").whereIdEquals(income_id), 0) + db.lookupDouble(new Select("initial_balance").from("accounts").whereIdEquals(expense_id), 0);
	}

	//--------------------------------------------------------------------------

	void
	addExpense(LocalDate date, LocalDate start_date, int period, double amount) {
		if (date.isBefore(start_date))
			m_starting_balance -= amount;
		else
			m_expense[period] += amount;
		m_has_data = true;
	}

	//--------------------------------------------------------------------------

	void
	addIncome(LocalDate date, LocalDate start_date, int period, double amount) {
		amount = Math.abs(amount);
		if (date.isBefore(start_date))
			m_starting_balance += amount;
		else
			m_income[period] += amount;
		m_has_data = true;
	}

	//--------------------------------------------------------------------------

	public String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public boolean
	hasData() {
		return m_has_data;
	}

	//--------------------------------------------------------------------------

	public double
	totalExpense() {
		float total = 0;
		for (double value : m_expense)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	public double
	totalIncome() {
		float total = 0;
		for (double value : m_income)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	public double
	writeRow(int period, Table table, HTMLWriter writer) {
		table.tr();
		writer.setAttribute("rowspan", "2");
		table.td();
		writer.aOnClick(m_name, "net.replace('transactions',context+'/Accounting/Annual Report?account=" + m_ids + "&year=" + m_year + "')");
		writer.setAttribute("style", "text-align:right");
		writer.setAttribute("rowspan", "2");
		table.td();
		writer.writeCurrency(m_starting_balance);
		table.td("income");
		for (int q=0; q<=period; q++) {
			writer.setAttribute("style", "text-align:right");
			table.td();
			writer.writeCurrency(m_income[q]);
		}
		double total_income = totalIncome();
		double total_expense = totalExpense();
		if (period > 0) {
			writer.setAttribute("style", "text-align:right");
			table.td();
			writer.writeCurrency(total_income);
		}
		writer.setAttribute("style", "text-align:right");
		writer.setAttribute("rowspan", "2");
		table.td();
		writer.writeCurrency(total_income - total_expense);
		writer.setAttribute("style", "text-align:right");
		writer.setAttribute("rowspan", "2");
		table.td();
		writer.writeCurrency(m_starting_balance + total_income - total_expense);
		table.tr().td("expense");
		for (int q=0; q<=period; q++) {
			writer.setAttribute("style", "text-align:right");
			table.td();
			writer.writeCurrency(m_expense[q]);
		}
		if (period > 0) {
			writer.setAttribute("style", "text-align:right");
			table.td();
			writer.writeCurrency(total_expense);
		}
		return m_starting_balance + total_income - total_expense;
	}
}
