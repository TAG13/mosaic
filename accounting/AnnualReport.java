package accounting;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.Request;
import app.Text;
import app.Time;
import db.DBConnection;
import db.Rows;
import db.Select;
import ui.Table;
import web.HTMLWriter;

public class AnnualReport {
	private Map<String, Account>	m_accounts = new TreeMap<>();
	private int						m_num_periods; // 4 quarters or 12 months
	Map<String, OngoingAccount>		m_ongoing_accounts = new TreeMap<>();
	private int						m_period_size; // 1 or 3 months
	private int						m_year;

	//--------------------------------------------------------------------------

	private Account
	getAccount(String name, DBConnection db) {
		Account account = m_accounts.get(name);
		if (account == null) {
			account = new Account(name, m_year, db);
			m_accounts.put(name, account);
		}
		return account;
	}

	//--------------------------------------------------------------------------

	private OngoingAccount
	getOngoingAccount(String name, DBConnection db) {
		String account_name = name.substring(0, name.lastIndexOf(' '));
		OngoingAccount ongoing_account = m_ongoing_accounts.get(account_name);
		if (ongoing_account == null) {
			ongoing_account = new OngoingAccount(account_name, m_year, db);
			m_ongoing_accounts.put(account_name, ongoing_account);
		}
		return ongoing_account;
	}

	//--------------------------------------------------------------------------
	// add accounts that don't have any transactions for the report but do have budgets

	private void
	readAccounts(DBConnection db) {
		Rows rows = new Rows(new Select("name").from("accounts JOIN accounts_budgets ON accounts_budgets.accounts_id=accounts.id").where("active AND year=" + m_year), db);
		while (rows.next()) {
			String name = rows.getString(1);
			if (m_accounts.get(name) == null)
				m_accounts.put(name, new Account(name, m_year, db));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	private int
	readTransactions(LocalDate start_date, LocalDate[] periods, DBConnection db) {
		Rows rows = new Rows(new Select("a.name,a.type,t.amount,t.date,t.type,b.name ongoing").from("transactions t JOIN accounts a ON t.accounts_id=a.id LEFT JOIN accounts b ON a.ongoing_account=b.id").where("a.active AND date<='12/31/" + m_year + "'").orderBy("date"), db);
		int period = 0;
		if (rows.next())
			do {
				String name = rows.getString(1);
				int account_type = rows.getInt(2);
				double amount = rows.getDouble(3);

				LocalDate d = rows.getDate(4);
				while (!d.isBefore(start_date) && period < m_num_periods - 1 && d.compareTo(periods[period]) > 0)
					++period;

				if (account_type == 4)
					getOngoingAccount(name, db).addIncome(d, start_date, period, amount);
				else if (account_type == 5)
					getOngoingAccount(name, db).addExpense(d, start_date, period, amount);
				else {
					if (!d.isBefore(start_date))
						getAccount(name, db).add("Payment".equals(rows.getString(5)) ? -amount : amount, period);
					String ongoing_account = rows.getString(6);
					if (ongoing_account != null)
						getOngoingAccount(ongoing_account, db).addExpense(d, start_date, period, "Payment".equals(rows.getString(5)) ? -amount : amount);
				}
			} while (rows.next());
		rows.close();
		return period;
	}

	//--------------------------------------------------------------------------

	private double[]
	total(int type) {
		double[] totals = new double[m_num_periods];
		for (Account a : m_accounts.values())
			if (a.getType() == type) {
				double[] sums = a.getSums();
				for (int i=0; i<m_num_periods; i++)
					totals[i] += sums[i];
			}

		return totals;
	}

	//--------------------------------------------------------------------------

	private static double
	total(double[] values) {
		float total = 0;
		for (double value : values)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	void
	write(Request request) {
		HTMLWriter writer = request.writer;
		int this_year = Time.newDate().getYear();
		String year = request.getParameter("year");
		m_year = year != null ? Integer.parseInt(year) : this_year;

		String account_id = request.getParameter("account");
		if (account_id != null) {
			String[] ids = account_id.split(",");
			if (ids.length == 1)
				writeTransactions(account_id, request);
			else {
				Table table = writer.ui.table();
				table.tr();
				writer.setAttribute("style", "vertical-align: top;");
				table.td();
				writeTransactions(ids[0], request);
				writer.setAttribute("style", "vertical-align: top;");
				table.td();
				writeTransactions(ids[1], request);
				table.close();
			}
			return;
		}

		if (year == null) {
			writer.h2("Annual Report");
			writer.jsFunction("replace_report", "f",
				"var s=dom.$('year_select');" +
				"dom.empty('transactions');" +
				"net.replace('annual_report',context+'/Accounting/Annual Report?year='+s.options[s.selectedIndex].value+'&period='+(dom.$('period0').checked?'monthly':'quarterly'))");
			writer.write("<div class=\"annual_report\"><form class=\"form-inline\" onchange=\"replace_report(this)\"><select class=\"form-control\" id=\"year_select\" style=\"margin-right:20px;vertical-align:middle;\">");
			for (int i=request.db.lookupInt("extract(year from MIN(date))", "transactions", null, this_year); i<=this_year+1; i++) {
				writer.write("<option");
				if (i == m_year)
					writer.write(" selected=\"selected\"");
				writer.write(">").write(i).write("</option>");
			}
			writer.write("</select> ");
			String[] options = new String[] {"monthly", "quarterly"};
			writer.ui.radioButtons("period", options, options, "monthly");
			writer.write("</form><br/><table><tr><td id=\"annual_report\" style=\"vertical-align: top;\">");
			writeReport(request);
			writer.write("</td><td id=\"transactions\" style=\"padding-left:20px;vertical-align: top;\"></td></tr></table></div>");
		} else
			writeReport(request);
	}

	//--------------------------------------------------------------------------

	private void
	writeReport(Request request) {
		LocalDate today = Time.newDate();
		HTMLWriter writer = request.writer;

		int this_year = today.getYear();
		LocalDate start_date = LocalDate.of(m_year, 1, 1);

		if (request.getParameter("period") == null || request.getParameter("period").equals("monthly")) {
			m_num_periods = 12;
			m_period_size = 1;
		} else {
			m_num_periods = 4;
			m_period_size = 3;
		}
		LocalDate[] periods = new LocalDate[m_num_periods - 1];
		for (int i=0,month=m_period_size; i<m_num_periods-1; i++,month+=m_period_size)
			periods[i] = LocalDate.of(m_year, month + 1, 1).minusDays(1);

		int period = readTransactions(start_date, periods, request.db);
		readAccounts(request.db);

		double total_income = 0;
		double total_expense = 0;
		double ci_balance = 0;

		boolean accounts_have_data = false;
		for (Account account : m_accounts.values())
			if (account.getType() != 3 && account.hasData()) {
				accounts_have_data = true;
				break;
			}
		if (accounts_have_data) {
			writer.setAttribute("class", "table table-condensed table-bordered");
			writer.tagOpen("table");
			writer.write("<thead><tr><th>Account</th><th>Budget</th>");
			for (int p=0; p<=period; p++) {
				writer.write("<th>");
				if (m_period_size == 3) {
					writer.write('Q');
					writer.write(p + 1);
				} else
					writer.write(Time.formatter.getMonthNameShort(p + 1));
				writer.write("</th>");
			}
			if (period > 0)
				if (today.getYear() == m_year)
					writer.write("<th>YTD</th>");
				else
					writer.write("<th>Total</th>");
			writer.write("<th>Remaining</th><th>Percent</th></tr></thead>");
			writer.write("<tr><td colspan=\"");
			writer.write(period > 0 ? period + m_num_periods + 2 : m_num_periods + 1);
			writer.write("\"><b>Income</b></td></tr>");
			for (Account a : m_accounts.values())
				if (a.getType() == 1)
					a.writeRow(period, m_year, true, writer);
			total_income = writeTotals(1, period, true, writer);

			writer.write("<tr><td colspan=\"");
			writer.write(period > 0 ? period + m_num_periods + 2 : m_num_periods + 1);
			writer.write("\"><b>Expense</b></td></tr>");
			for (Account a : m_accounts.values())
				if (a.getType() == 2)
					a.writeRow(period, m_year, true, writer);
			total_expense = writeTotals(2, period, true, writer);

			writer.write("<tr><td colspan=\"2\"><b>Net Income</b>");
			double[] sum_income = total(1);
			double[] sum_expense = total(2);
			for (int p=0; p<=period; p++) {
				writer.write("</td><td style=\"text-align:right\">");
				writer.writeCurrency(sum_income[p] - sum_expense[p]);
			}
			if (period > 0) {
				writer.write("</td><td style=\"text-align:right\">");
				writer.writeCurrency(total(sum_income) - total(sum_expense));
			}
			writer.write("</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
			for (Account a : m_accounts.values())
				if (a.getType() == 0)
					for (double amount : a.getSums())
						if (amount != 0) {
							a.writeRow(period, m_year, true, writer);
							break;
						}
			writer.tagClose();
		}

		accounts_have_data = false;
		for (OngoingAccount account : m_ongoing_accounts.values())
			if (account.hasData()) {
				accounts_have_data = true;
				break;
			}
		if (accounts_have_data) {
			writer.h3("Ongoing Accounts");
			Table table = writer.ui.table().addClass("table table-condensed table-bordered");
			table.th("account").th("starting balance").th("type");
			for (int p=0; p<=period; p++) {
				table.th();
				if (m_period_size == 3) {
					writer.write('Q');
					writer.write(p + 1);
				} else
					writer.write(Time.formatter.getMonthNameShort(p + 1));
			}
			if (period > 0)
				table.th("total");
			table.th("net").th("balance");
			double total_ongoing_balance = 0;
			for (OngoingAccount ongoing_account : m_ongoing_accounts.values()) {
				double balance = ongoing_account.writeRow(period, table, writer);
				total_ongoing_balance += balance;
				if ("Capital Improvements".equals(ongoing_account.getName()))
					ci_balance = balance;
				total_income += ongoing_account.totalIncome();
				total_expense += ongoing_account.totalExpense();
			}
			table.tr();
			writer.setAttribute("style", "text-align:right");
			int cols = period + 5;
			if (period > 0)
				++cols;
			table.td(cols);
			writer.write("total");
			writer.setAttribute("style", "text-align:right");
			table.td();
			writer.writeCurrency(total_ongoing_balance);
			table.close();
		}

		accounts_have_data = false;
		for (Account account : m_accounts.values())
			if (account.getType() != 3 && account.hasData()) {
				accounts_have_data = true;
				break;
			}
		if (accounts_have_data) {
			writer.h3("Capital Improvements");
			writer.setAttribute("class", "table table-condensed table-bordered");
			writer.tagOpen("table");
			writer.write("<thead><tr><th>Account</th><th>Budget</th><th>Spent</th><th>Remaining</th><th>Percent</th></tr></thead>");
			for (Account a : m_accounts.values())
				if (a.getType() == 3 && (a.getBudget() > 0 || total(a.getSums()) > 0))
					a.writeRow(period, m_year, false, writer);
			total_expense += writeTotals(3, period, false, writer);
			writer.tagClose();
			if (m_year == this_year) {
				writer.write("<p>Amount unallocated for Capital Improvements: ");
				double total_budget_capital_improvements = 0;
				for (Account a : m_accounts.values())
					if (a.getType() == 3 && a.getBudget() > 0)
						total_budget_capital_improvements += a.getBudget();
				writer.writeCurrency(ci_balance - (total_budget_capital_improvements - total(total(3))));
				writer.write("</p>");
			}
		}

		writer.write("<hr /><h4>Total Income: ");
		writer.writeCurrency(total_income);
		writer.write("</h4><h4>Total Expense: ");
		writer.writeCurrency(total_expense);
		writer.write("</h4><h4>Net: ");
		writer.writeCurrency(total_income - total_expense);
		writer.write("</h4>");

		writer.h3("Bank Account Balances");
		Table table = writer.ui.table().addClass("table table-condensed table-bordered");
		table.th("account").th("balance");
		Rows balances = new Rows(new Select("id,name").from("bank_accounts").orderBy("name"), request.db);
		while (balances.next()) {
			double balance = Accounting.calcBalanceBank(balances.getInt(1), m_year + 1 + "-1-1", request.db);
			if (balance > 0) {
				table.tr().td(balances.getString(2));
				writer.write("</td><td style=\"text-align:right\">");
				writer.writeCurrency(balance);
			}
		}
		table.close();
		writer.br().br();
	}

	//--------------------------------------------------------------------------

	private double
	writeTotals(int type, int period, boolean show_periods, HTMLWriter writer) {
		double[] sums = total(type);
		double sums_sum = total(sums);
		double total_budget = 0;
		for (Account a : m_accounts.values())
			if (a.getType() == type && a.getBudget() > 0)
				total_budget += a.getBudget();
		writer.write("<tr><td>Total");
		writer.write("</td><td style=\"text-align:right\">");
		writer.writeCurrency(total_budget);
		if (show_periods)
			for (int p=0; p<=period; p++) {
				writer.write("</td><td style=\"text-align:right\">");
				writer.writeCurrency(sums[p]);
			}
		if (period > 0 || !show_periods) {
			writer.write("</td><td style=\"text-align:right\">");
			writer.writeCurrency(sums_sum);
		}
		writer.write("</td><td style=\"text-align:right\">");
		writer.writeCurrency(total_budget - sums_sum);
		writer.write("</td><td style=\"text-align:right\">");
		if (total_budget > 0)
			writer.writePercent(1.0 - sums_sum / total_budget);
		writer.write("</td></tr>");
		return sums_sum;
	}

	//--------------------------------------------------------------------------

	private void
	writeTransactions(String account_id, Request request) {
		String where = "accounts_id=" + account_id;
		HTMLWriter writer = request.writer;

		List<String> ids = request.db.readValues(new Select("id").from("accounts").where("ongoing_account=" + account_id));
		if (ids.size() > 0)
			where = "accounts_id IN (" + account_id + "," + Text.join(",", ids) + ")";
		if (!"all".equals(request.getParameter("years")))
			where += " AND date>='1/1/" + m_year + "' AND date<='12/31/" + m_year + "'";
		writer.h3(request.db.lookupString(new Select("name").from("accounts").whereIdEquals(account_id)));
		writer.write("<table border=\"1\" class=\"table table-condensed table-bordered\"><thead><tr><th>date</th><th>HOA account</th><th>type</th><th>description</th><th>amount</th><th>total</th></tr></thead>");
		double total = 0;
		Rows rows = new Rows(new Select("date,families_id,type,description,amount").from("transactions").where(where).orderBy("date"), request.db);
		while (rows.next()) {
			writer.write("<tr><td>");
			LocalDate date = rows.getDate(1);
			if (date != null)
				writer.writeDate(date);
			writer.write("</td><td>");
			String household_id = rows.getString(2);
			if (household_id != null)
				writer.write(request.db.lookupString(new Select("name").from("families").whereIdEquals(household_id)));
			writer.write("</td><td>");
			String type = rows.getString(3);
			writer.write(type);
			writer.write("</td><td>");
			writer.write(rows.getString(4));
			writer.write("</td><td style=\"text-align:right\">");
			double amount = rows.getDouble(5);
			writer.writeCurrency(amount);
			writer.write("</td><td>");
			total += "Payment".equals(type) ? -amount : amount;
			writer.writeCurrency(total);
			writer.write("</td></tr>");
		}
		rows.close();
		writer.write("</table>");
	}
}
