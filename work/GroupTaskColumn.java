package work;

import java.util.Map;

import app.Request;
import db.Form;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import web.HTMLWriter;

public class GroupTaskColumn extends ColumnBase<GroupTaskColumn> {
	private boolean m_track_hours_by_group_not_task;

	//--------------------------------------------------------------------------

	public
	GroupTaskColumn(String name, boolean track_hours_by_group_not_task) {
		super(name);
		m_track_hours_by_group_not_task = track_hours_by_group_not_task;
		setDisplayName("task");
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderBy() {
		return "(SELECT name FROM groups WHERE groups.id=(SELECT groups_id FROM work_tasks WHERE work_tasks.id=work_tasks_id)),(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id)";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderByDesc() {
		return "(SELECT name FROM groups WHERE groups.id=(SELECT groups_id FROM work_tasks WHERE work_tasks.id=work_tasks_id)) DESC,(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id) DESC";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View view, Form form, Request request) {
		HTMLWriter writer = request.writer;
		writer.setAttribute("id", "group_select");
		RowsSelect rows_select;
		if (m_track_hours_by_group_not_task)
			rows_select = new RowsSelect("work_tasks_id", new Select("work_tasks.id id,name").distinct().from("work_tasks").joinOn("groups", "groups.id=groups_id").where("work_tasks.active").orderBy("name"), "name", "id", request);
		else {
			rows_select = new RowsSelect(null, new Select("id,name").from("groups").where("id IN (SELECT DISTINCT groups_id FROM work_tasks WHERE active)").orderBy("name"), "name", "id", request);
			if (rows_select.size() == 0) {
				new RowsSelect("work_tasks_id", new Select("id,task").from("work_tasks").where("active").orderBy("task"), "task", "id", request).setAllowNoSelection(true).write(request);
				return;
			}
		}
		int work_tasks_id = 0;
		if (view.getMode() == Mode.EDIT_FORM) {
			work_tasks_id = view.data.getInt("work_tasks_id");
			if (m_track_hours_by_group_not_task)
				rows_select.setSelectedOption(null, Integer.toString(work_tasks_id));
			else
				rows_select.setSelectedOption(null, request.db.lookupString(new Select("groups_id").from("work_tasks").whereIdEquals(work_tasks_id)));
		}
		rows_select.setAllowNoSelection(true).write(request);
		if (!m_track_hours_by_group_not_task) {
			writer.nbsp();
			writer.setAttribute("id", "group_tasks");
			writer.select(m_name, (String[])null, null, null);
			Rows tasks = new Rows(new db.Select("id,task,groups_id").from("work_tasks").where("active").orderBy("groups_id,task"), request.db);
			writer.scriptOpen();
			writer.write("dom.$('group_select').tasks = {");
			int current_group_id = 0;
			boolean first_task = true;
			while (tasks.next()) {
				String task = tasks.getString(2);
				if (task == null || task.length() == 0)
					continue;
				int group_id = tasks.getInt(3);
				if (group_id != current_group_id) {
					if (current_group_id != 0)
						writer.write("],");
					writer.write(group_id).write(":[");
					current_group_id = group_id;
					first_task = true;
				}
				if (first_task)
					first_task = false;
				else
					writer.write(",");
				writer.write("[").write(tasks.getString(1)).write(",").jsString(task).write("]");
			}
			tasks.close();
			writer.write("]};" +
				"dom.$('group_select').set_tasks = function(id) {" +
					"var g = dom.$('group_select');" +
					"var s = dom.$('group_tasks');" +
					"dom.empty(s);" +
					"var t = g.tasks[g.options[g.selectedIndex].value];" +
					"if (t) {" +
						"dom.el('option',{parent:s});" +
						"for (var i=0; i<t.length; i++) {" +
							"var e = dom.el('option',{parent:s, text:t[i][1], value:t[i][0]});" +
							"if (id == t[i][0])" +
								"e.selected = 'selected';" +
						"}" +
					"}" +
				"};" +
				"dom.$('group_select').addEventListener(\"change\", dom.$('group_select').set_tasks);" +
				"dom.$('group_select').set_tasks(").write(work_tasks_id).write(");");
			writer.scriptClose();
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View view, Map<String,Object> data, Request request) {
		if (view.data.getInt("link") != 0)
			request.writer.write(view.data.getDouble("hours") > 0 ? "gift received" : "gift given");
		else if (view.data.getInt("work_tasks_id") != 0) {
			Object[] row = request.db.readRowObjects(new Select("task,groups_id").from("work_tasks").whereIdEquals(view.data.getInt("work_tasks_id")));
			if (row[1] != null) {
				request.writer.write(request.db.lookupString(new Select("name").from("groups").whereIdEquals((int)row[1])));
				if (row[0] != null)
					request.writer.write(": ");
			}
			if (row[0] != null)
				request.writer.write(row[0]);
		}
		return true;
	}
}
