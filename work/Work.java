package work;

import java.sql.Types;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Map;

import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import app.Time;
import app.pages.Page;
import db.AnnualReport;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.NumberColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import db.section.Tabs;
import ui.Table;
import ui.TaskPane;
import web.HTMLWriter;

public class Work extends SiteModule {
	@JSONField
	private boolean	m_group_report_sections = true;
	@JSONField
	boolean			m_hours_may_be_gifted;
	@JSONField
	double			m_max_bankable_hours;
	@JSONField
	boolean			m_quarterly;
	@JSONField
	double			m_required_hours_per_period;
	@JSONField
	boolean			m_show_balance = true;
	@JSONField
	boolean			m_show_result_column;
	@JSONField
	private boolean m_track_hours_by_group_not_task;

	// --------------------------------------------------------------------------

	double
	balance(double balance) {
		if (m_max_bankable_hours > 0)
			return Math.min(balance, m_max_bankable_hours);
		return balance;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "Annual Report by Group":
			if (m_track_hours_by_group_not_task)
				new AnnualReport(new Select("(SELECT name FROM groups WHERE groups.id=work_tasks.groups_id),work_tasks.id,task,date,hours").from("work_tasks").join("work_tasks", "work_hours"), "Task", 2, 1, 5)
					.setDetail(new Select("task").from("work_tasks"), "date,(SELECT first || ' ' || last FROM people WHERE people.id=work_hours._owner_) AS person,hours", "work_hours", "work_tasks_id")
					.write(segment_one, request);
			else
				new AnnualReport(new Select("(SELECT name FROM groups WHERE groups.id=work_tasks.groups_id),work_tasks.id,task,date,hours").from("work_tasks").join("work_tasks", "work_hours"), "Task", 2, 3, 5)
				.setDetail(new Select("task").from("work_tasks"), "date,(SELECT first || ' ' || last FROM people WHERE people.id=work_hours._owner_) AS person,hours", "work_hours", "work_tasks_id")
				.setGroup(1)
				.write(segment_one, request);
			return true;
		case "Annual Report by Person":
			new AnnualReport(new Select("(SELECT first || ' ' || last FROM people WHERE people.id=work_hours._owner_),_owner_,date,hours").from("work_hours"), "Person", 2, 1, 4)
				.setDetail(new Select("first || ' ' || last").from("people"), "date,(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id) AS task,hours", "work_hours", "_owner_")
				.write(segment_one, request);
			return true;
		case "balance":
			writeBalance(request);
			return true;
		case "balances":
			writeHistory(request);
			return true;
		case "gift_form":
			writeGiftForm(request);
			return true;
		case "gift_max":
			State state = new State(request.getDate("date"), request.getInt("people_id", 0), this, request.db);
			request.writer.write(state.balance);
			return true;
		case "hours by date":
			request.site.newView("work_hours", request).writeComponent();
			new State(Time.newDate(), request.getUser().getId(), this, request.db).writeGiftButton(request.writer);
			return true;
		case "Jobs":
			request.site.newView("jobs", request).writeComponent();
			return true;
		case "Monthly Report by Group":
			request.site.newView("work_hours monthly group", request).writeComponent();
			return true;
		case "Monthly Report by Person":
			request.site.newView("work_hours monthly person", request).writeComponent();
			return true;
		case "Work Hours":
			writeWorkHours(request);
			return true;
		case "Work Tasks":
			request.site.newView("work_tasks", request).writeComponent();
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		String segment_one = request.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch(segment_one) {
		case "gift hours":
			NameValuePairs nvp = new NameValuePairs();
			int from_id = request.getInt("from", 0);
			if (from_id == 0)
				from_id = request.getUser().getId();
			int to_id = request.getInt("to", 0);
			String hours = request.getParameter("hours");
			LocalDate date_given = request.getDate("date_given");
			if (date_given == null)
				date_given = Time.newDate();
			nvp.setDate("date", date_given)
				.set("hours", "-" + hours)
				.set("_owner_", from_id);
			from_id = request.db.insert("work_hours", nvp);
			nvp.setDate("date", request.getDate("date_received"))
				.set("hours", hours)
				.set("link", from_id)
				.set("_owner_", to_id);
			to_id = request.db.insert("work_hours", nvp);
			request.db.update("work_hours", "link=" + to_id, from_id);
			return true;
		}
		return false;

	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Tracking of community tasks and member contributions by date and number of hours.";
	}

	// --------------------------------------------------------------------------

	static Rows
	getRowsForBalances(int people_id, boolean quarterly, DBConnection db) {
		return new Rows(new Select("extract(year from date),extract(" + (quarterly ? "quarter" : "month") + " from date),SUM(CASE WHEN link IS NULL THEN hours END),-SUM(CASE WHEN link IS NOT NULL AND hours < 0 THEN hours END),SUM(CASE WHEN link IS NOT NULL AND hours > 0 THEN hours END)").from("work_hours").whereEquals("_owner_", people_id).groupBy("extract(year from date),extract(" + (quarterly ? "quarter" : "month") + " from date)").orderBy("1,2"), db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		JDBCTable table_def = new JDBCTable("jobs")
			.add(new JDBCColumn("job", Types.VARCHAR))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("notes", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("jobs_history")
			.add(new JDBCColumn("person", "people"))
			.add(new JDBCColumn("start", Types.DATE))
			.add(new JDBCColumn("end", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("jobs"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("work_tasks")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("groups_id", Types.INTEGER))
			.add(new JDBCColumn("task", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("work_hours")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("hours", Types.DOUBLE))
			.add(new JDBCColumn("link", Types.INTEGER))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("work_tasks"));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex("work_hours", "date");
		addPage(new Page("Work", m_site) {
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Work", request);
				TaskPane task_pane = request.writer.ui.taskPane("Work").open();
				task_pane.task("Work Hours");
				task_pane.task("Work Tasks");
				task_pane.task("Jobs");
				task_pane.task("Annual Report by Group");
				task_pane.task("Annual Report by Person");
				task_pane.task("Monthly Report by Group");
				task_pane.task("Monthly Report by Person");
				task_pane.closeTasks();
				task_pane.selectTask("Work Hours");
				task_pane.close();
				if (m_show_balance)
					request.writer.js("app.subscribe('work_hours',function(){net.replace('balance',context+'/Work/balance')});");
				request.close();
			}
		}, m_site, db);
		Roles.add("work");
	}

	//--------------------------------------------------------------------------

	double
	net(double worked, double given, double received) {
		return worked - m_required_hours_per_period - given + received;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("jobs"))
			return new ViewDef(name)
				.addRelationshipDef(new OneToMany("jobs_history"));
		if (name.equals("jobs_history"))
			return new ViewDef(name)
				.setDefaultOrderBy("start,end")
				.setRecordName("Time Span")
				.setColumnNamesTableAndForm(new String[] { "person", "start", "end", "notes" })
				.setColumn(new LookupColumn("person", "people", "first,last", "active", "first,last"));
		if (name.equals("work_hours"))
			return new ViewDef(name) {
					@Override
					public String
					beforeDelete(String where, Request request) {
						request.db.delete("work_hours", request.db.lookupInt(new Select("link").from("work_hours").whereIdEquals(where.substring(where.indexOf('=') + 1)), 0), false);
						return null;
					}
					@Override
					public String
					beforeUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
						NameValuePairs nvp = new NameValuePairs();
						nvp.set("hours", -name_value_pairs.getDouble("hours", 0));
						request.db.update("work_hours", nvp, request.db.lookupInt(new Select("link").from("work_hours").whereIdEquals(id), 0));
						return null;
					}
				}
				.setAccessPolicy(new Or(new RecordOwnerAccessPolicy() {
					@Override
					protected boolean
					userOwnsRecord(Rows data, Request request) {
						return super.userOwnsRecord(data, request) && (data.getDouble("hours") < 0 || data.getInt("link") == 0);
					}
					@Override
					protected boolean
					userOwnsRecord(String from, int id, Request request) {
						return super.userOwnsRecord(from, id, request) && (request.db.lookupDouble(new Select("hours").from("work_hours").whereIdEquals(id), 0) < 0 || request.db.lookupInt(new Select("link").from("work_hours").whereIdEquals(id), 0) == 0);
					}
				}.add().delete().edit(), new RoleAccessPolicy("work").add().delete().edit()))
				.addFeature(new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.setCenter(false)
				.setDefaultOrderBy("date")
				.setRecordName("Entry")
				.setRowWindowSize(0)
				.setColumnNamesForm(new String[] { "work_tasks_id", "date", "hours", "notes", "_owner_" })
				.setColumnNamesTable(new String[] { "work_tasks_id", "date", "hours", "_owner_" })
				.setColumn(new Column("date").setDefaultToShortDate().setIsRequired(true))
				.setColumn(new NumberColumn("hours").setIsRequired(true).setMin("0.25").setStep("0.25"))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_hours monthly group")) {
			ViewDef view_def = new ViewDef(name)
				.addFeature(new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setFrom("work_tasks")
				.setRecordName("Time")
				.setRowWindowSize(0)
				.setSelectFrom("work_tasks JOIN work_hours ON work_hours.work_tasks_id=work_tasks.id")
				.setShowHead(false)
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"))
				.setColumn(new Column("hours").setTotal(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"));
			if (m_group_report_sections)
				view_def.addSection(new Dividers("groups_id", new OrderBy("groups_id")))
					.setDefaultOrderBy("date,task")
					.setColumnNamesTable(new String[] { "task", "date", "hours", "_owner_", "notes" });
			else
				view_def.setDefaultOrderBy("groups_id,date,task")
					.setColumnNamesTable(new String[] { "groups_id", "task", "date", "hours", "_owner_", "notes" });
			return view_def;
		}
		if (name.equals("work_hours monthly person"))
			return new ViewDef(name)
				.addFeature(new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.addSection(new Dividers("_owner_", new OrderBy("_owner_")))
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("work_hours")
				.setRecordName("Time")
				.setRowWindowSize(0)
				.setSelectFrom("work_hours JOIN work_tasks ON work_hours.work_tasks_id=work_tasks.id")
				.setShowHead(false)
				.setColumnNamesTable(new String[] { "date", "hours", "groups_id", "task", "notes" })
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"))
				.setColumn(new Column("hours").setTotal(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_hours person"))
			return new ViewDef(name)
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.addSection(new Tabs("date", new OrderBy("date")).setFormatMonthNameShort(true))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setAllowSorting(false)
				.setFrom("work_hours")
				.setRecordName("Time")
				.setColumnNamesForm(new String[] { "work_tasks_id", "date", "hours", "notes", "_owner_" })
				.setColumnNamesTable(new String[] { "date", "work_tasks_id", "hours" })
				.setColumn(new Column("date").setDefaultToShortDate())
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_tasks"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("work").add().delete().edit())
				.setDefaultOrderBy("groups_id,task")
				.setRecordName("Task")
				.setColumnNamesTableAndForm(m_track_hours_by_group_not_task ? new String[] { "groups_id", "active" } : new String[] { "groups_id", "task", "active" })
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"));
		return null;
	}

	//--------------------------------------------------------------------------

	private void
	writeBalance(Request request) {
		new State(Time.newDate(), request.getUser().getId(), this, request.db).write(request.writer);
	}

	//--------------------------------------------------------------------------

	private void
	writeHistory(Request request) {
		request.writer.write("<p>");
		String selected_value = request.getParameter("people_id");
		if (selected_value == null)
			selected_value = Integer.toString(request.getUser().getId());
		new RowsSelect(null, new Select("id,first,last").from("people").where("active").orderBy("first,last"), "first,last", "id", request).setOnChange("net.replace(dom.$('button_nav').lastChild,context+'/Work/balances?people_id='+this.options[this.selectedIndex].value)").setSelectedOption(null, selected_value).write(request);
		request.writer.write("</p>");
		Rows rows = getRowsForBalances(request.getInt("people_id", request.getUser().getId()), m_quarterly, request.db);
		if (!rows.next()) {
			rows.close();
			request.writer.write("no hours recorded");
			return;
		}
		int this_period = Time.newDate().getMonthValue();
		if (m_quarterly)
			this_period = (this_period - 1) / 3 + 1;
		int this_year = Time.newDate().getYear();
		DecimalFormat df = new DecimalFormat("0.##");
		ui.Tabs tabs = request.writer.ui.tabs(null).open();
		int year = rows.getInt(1);
		tabs.pane(Integer.toString(year));
		Table table = request.writer.ui.table().addDefaultClasses(true);
		table.th(m_quarterly ? "quarter" : "month").th("worked").th("gave").th("received").th("net hours");
		if (m_show_result_column)
			table.th("result");
		table.th("bank balance");
		double balance = 0;
		int period = rows.getInt(2);
		do {
			while (year < rows.getInt(1)) {
				while (period <= (m_quarterly ? 4 : 12)) {
					balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table);
					period++;
				}
				table.close();
				year++;
				period = 1;
				tabs.pane(Integer.toString(year));
				table.th(m_quarterly ? "quarter" : "month").th("worked").th("gave").th("received").th("net hours").th("balance");
			}
			while (period < rows.getInt(2)) {
				balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table);
				period++;
			}
			balance = writeHistoryRow(balance, rows.getDouble(3), rows.getDouble(4), rows.getDouble(5), period, year, this_period, this_year, df, table);
			period++;
			if (period == (m_quarterly ? 5 : 13)) {
				year++;
				period = 1;
			}
		} while (rows.next());
		rows.close();
		while (year <= this_year && (year < this_year || period <= this_period)) {
			balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table);
			period++;
			if (period == (m_quarterly ? 5 : 13)) {
				year++;
				period = 1;
			}
		}
		table.close();
		tabs.setStartTab(Integer.toString(year));
		tabs.close();
	}

	//--------------------------------------------------------------------------

	private double
	writeHistoryRow(double balance, double worked, double given, double received, int period, int year, int this_period, int this_year, DecimalFormat df, Table table) {
		table.tr().td(m_quarterly ? "Quarter " + period : Time.formatter.getMonthNameLong(period));
		table.td(df.format(worked));
		table.td(df.format(given));
		table.td(df.format(received));
		double net = net(worked, given, received);
		table.td(df.format(net));
		if (m_show_result_column)
			table.td(df.format(balance + net));
		balance = balance(balance + net);
		table.td(year > this_year || year == this_year && period >= this_period ? "" : df.format(balance));
		return balance;
	}

	//--------------------------------------------------------------------------

	private void
	writeGiftForm(Request request) {
		LocalDate today = Time.newDate();
		HTMLWriter writer = request.writer;
		writer.write("<form id=\"gift_form\" action=\"").write(request.getContext()).write("/Work/gift hours\" method=\"post\"><table style=\"border-collapse:separate;border-spacing:10px\">" +
			"<tr><td>give</td><td><input name=\"hours\" max=\"").write(request.getParameter("balance")).write("\" required=\"true\"> hours (max: <span id=\"gift_max_span\">").write(request.getParameter("balance")).write("</span>)</td></tr>");
		if (request.userIsAdmin()) {
			writer.write("<tr><td>from</td><td>");
			new RowsSelect("from", new Select("id,first,last").from("people").where("active").orderBy("lower(first),lower(last)"), "first,last", "id", request).setSelectedOption(null, Integer.toString(request.getUser().getId())).write(request);
			writer.write("</td></tr>");
		}
		writer.write("<tr><td>to</td><td>");
		new RowsSelect("to", new Select("id,first,last").from("people").where("active").orderBy("lower(first),lower(last)"), "first,last", "id", request).write(request);
		writer.write("</td></tr><tr><td>date given</td><td><div style=\"display:inline-block\">").dateInput("date_given", today, "get_gift_max").write("</div>")
			.write("</td></tr><tr><td>date received</td><td><div style=\"display:inline-block\">").dateInput("date_received", today).write("</div>")
			.write("</td></tr></table></form>")
			.jsFunction("get_gift_max", null, "var f=dom.$('gift_form');console.log(f);net.get(context+'/Work/gift_max?date='+f.date_given.value+'&people_id='+" + (request.userIsAdmin() ? "f.from.options[f.from.selectedIndex].value" : request.getUser().getId()) + ",function(t){console.log(t);f.hours.max=t;dom.$('gift_max_span').innerText=t;},true)");
	}

	//--------------------------------------------------------------------------

	private void
	writeWorkHours(Request request) {
		HTMLWriter writer = request.writer;
		writer.write("<div class=\"container\" style=\"padding-top:10px;\"><div style=\"display:table;margin:auto;\">");
		if (m_show_balance) {
			writer.write("<div class=\"alert alert-secondary\" id=\"balance\">");
			writeBalance(request);
			writer.write("</div>");
		}
		writer.write("<div id=\"button_nav\"><div id=\"by date div\">");
		request.site.newView("work_hours", request).writeComponent();
		new State(Time.newDate(), request.getUser().getId(), this, request.db).writeGiftButton(request.writer);
		writer.write("</div></div></div></div>");
		writer.js("new ButtonNav(dom.$('button_nav'),[" +
			"{text:'By Date',icon:'calendar',div:dom.$('by date div'),url:'/Work/hours by date'}," +
			"{text:'History',icon:'card-list',url:'/Work/balances'}])");
	}
}
