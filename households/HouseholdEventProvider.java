package households;

import java.time.LocalDate;

import app.Array;
import app.Request;
import app.Site;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.column.Column;
import db.jdbc.JDBCColumn;
import mosaic.MosaicEventProvider;
import mosaic.Person;

public class HouseholdEventProvider extends MosaicEventProvider {
	public
	HouseholdEventProvider() {
		super("Household Calendar");
		addColumn(new JDBCColumn("families"));
		setColor("#39CCCC");
		setEventsCanRepeat(true);
		setEventsHaveTime(true);
		setEventsTable("families_events");
		setSupportReminders();
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate date, Request request) {
		Person person = (mosaic.Person)request.getUser();
		if (person == null)
			return 0;
		Household household = person.getHousehold(request);
		if (household == null)
			return 0;
		String date_string = date.toString();
		Select query = new Select().from(m_events_table).where("date>='" + date_string + "'");
		query.andWhere("repeat='never'").andWhere("families_id=" + household.getId());
		int count = request.db.countRows(m_events_table, query.getWhere());
		if (count < target_max) {
			Rows rows = new Rows(new Select("date,end_date,repeat").from(m_events_table).where("(end_date IS NULL OR end_date>='" + date_string + "')"), request.db);
			while (rows.next()) {
				LocalDate end_date = rows.getDate(2);
				if (end_date == null)
					return target_max;
				LocalDate d = date;
				while (count < target_max && d.isBefore(end_date)) {
					if (occursOn(d, rows.getDate(1), end_date, rows.getString(3)))
						count++;
					d = d.plusDays(1);
				}
			}
		}
		return count;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDisplayName(Request request) {
		if (request != null) {
			String display_name = ((mosaic.Person)request.getUser()).getHousehold(request).getName();
			if (display_name != null)
				return display_name;
		}
		return "Household Calendar";
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals(m_name)) {
			ViewDef view_def = super._newViewDef(name, site)
				.setColumn(new Column("families_id") {
					@Override
					public String
					getDefaultValue(Request request) {
						return Integer.toString(((mosaic.Person)request.getUser()).getHousehold(request).getId());
					}
				}.setIsHidden(true));
			view_def.setColumnNamesForm(Array.push(view_def.getColumnNamesForm(), "families_id"));
			return view_def;
		}
		return super._newViewDef(name, site);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Select
	selectEvents(LocalDate from, LocalDate to, int category_id, int location_id, int num_events, Request request) {
		Person person = (mosaic.Person)request.getUser();
		if (person == null)
			return null;
		Household household = person.getHousehold(request);
		if (household == null)
			return null;
		Select select = super.selectEvents(from, to, category_id, location_id, num_events, request);
		if (select == null)
			return null;
		return select.andWhere("families_id=" + ((mosaic.Person)request.getUser()).getHousehold(request).getId());
	}

	//--------------------------------------------------------------------------

	@Override
	protected Select
	selectRepeatingEvents(LocalDate from, LocalDate to, int category_id, int location_id, Request request) {
		Person person = (mosaic.Person)request.getUser();
		if (person == null)
			return null;
		Household household = person.getHousehold(request);
		if (household == null)
			return null;
		return super.selectRepeatingEvents(from, to, category_id, location_id, request).andWhere("families_id=" + ((mosaic.Person)request.getUser()).getHousehold(request).getId());
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnMenu(Request request) {
		Person user = (Person)request.getUser();
		return user != null && user.getHousehold(request) != null;
	}
}
