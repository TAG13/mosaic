package households;

import java.util.List;

import app.Text;
import db.DBConnection;
import db.Select;
import db.object.DBField;
import db.object.DBObject;

public class Household {
	private int					m_id;
	private @DBField boolean	m_meal_work_exempt;
	private @DBField String		m_name;
	private List<Integer>		m_people_ids;

	//--------------------------------------------------------------------------

	public boolean
	belongsTo(int people_id) {
		return m_people_ids.contains(people_id);
	}

	//--------------------------------------------------------------------------

	public double
	getDues(DBConnection db) {
		return db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + m_id), 0);
	}

	//--------------------------------------------------------------------------

	public int
	getId() {
		return m_id;
	}

	//--------------------------------------------------------------------------

	public String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public String
	getPeopleIds() {
		return Text.join(",", m_people_ids);
	}

	//--------------------------------------------------------------------------

	public boolean
	isMealWorkExempt() {
		return m_meal_work_exempt;
	}

	//--------------------------------------------------------------------------

	static Household
	load(int id, DBConnection db) {
		Household household = new Household();
		if (DBObject.load(household, "families", id, db)) {
			household.m_id = id;
			household.m_people_ids = db.readValuesInt(new Select("id").from("people").whereEquals("families_id", id));
			return household;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public void
	setName(String name, DBConnection db) {
		m_name = name;
		DBObject.store(this, "families", m_id, db);
	}
}
