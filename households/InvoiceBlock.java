package households;

import java.util.ArrayList;

import accounting.Accounting;
import app.Block;
import app.Request;
import app.Text;
import ui.Form;

public class InvoiceBlock extends Block {
	class Type {
		public
		Type(String description, double cost, int accounts_id) {
			this.description = description;
			this.cost = cost;
			this.accounts_id = accounts_id;
		}
		int		accounts_id;
		double	cost;
		String	description;
	}

	private ArrayList<Type>	m_types = new ArrayList<>();

	//--------------------------------------------------------------------------

	public
	InvoiceBlock(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	public InvoiceBlock
	addType(int accounts_id, double cost, String description) {
		m_types.add(new Type(description, cost, accounts_id));
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	doPost(Request request) {
		int num_invoices = 0;
		for (int i=0; i<m_types.size(); i++) {
			int num = request.getInt("num" + i, 0);
			if (num != 0) {
				Type type = m_types.get(i);
				Accounting.addInvoice(type.accounts_id, ((mosaic.Person)request.getUser()).getHousehold(request).getId(), null, type.description, type.cost * num, request.site, request.db);
				num_invoices++;
			}
		}
		request.addToResponse("message", num_invoices + " " + Text.pluralize("invoice", num_invoices) + " submitted");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	handlesContext(String context, Request request) {
		return context.equals("household accounting");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write(String context, Request request) {
		Form form = new Form(null, request.getContext() + "/" + m_module.getName() + "/blocks/" + m_name, request.writer).open();
		for (int i=0; i<m_types.size(); i++) {
			form.rowOpen(m_types.get(i).description);
			request.writer.numberInput("num" + i, "0", null, "1", null);
			form.rowClose();
		}
		form.setSubmitText("submit").close();
	}
}
