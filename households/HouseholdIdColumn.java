package households;

import app.Request;
import db.column.LookupColumn;
import mosaic.Person;

public class HouseholdIdColumn extends LookupColumn {
	public
	HouseholdIdColumn() {
		super("families_id", "families", "name");
		m_is_hidden = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(Request request) {
		Person user = (Person)request.getUser();
		if (user == null)
			return null;
		Household household = user.getHousehold(request);
		return household == null ? null : Integer.toString(household.getId());
	}
}
