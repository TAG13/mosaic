 package households;

import java.sql.Types;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import accounting.Accounting;
import app.Block;
import app.Module;
import app.Request;
import app.Site;
import app.SiteModule;
import app.Text;
import app.pages.Page;
import blocks.Blocks;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.OneToManyLink;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.UpdateHook;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.PictureColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Tabs;
import mosaic.Contacts;
import mosaic.NoDeleteAccessPolicy;
import mosaic.People;
import ui.Card;
import ui.TaskPane;
import web.HTMLWriter;
import web.JS;

public class Households extends SiteModule {
	@JSONField
	private String[]				m_extra_columns;
	private Map<Integer,Household>	m_households = new HashMap<>();
	@JSONField
	private String					m_hoa_text;
	@JSONField
	private String[]				m_lists = { "contacts", "notes", "recipes", "todos" };
	private JsonObject				m_page_template = Json.parse("{\"blocks\":["
			+ "{\"type\":\"people\"},"
			+ "{\"type\":\"pets\"},"
			+ "{\"type\":\"html\",\"title\":\"Bio\",\"column\":\"bio\",\"view\":\"families\"},"
			+ "{\"type\":\"pictures\"},"
			+ "{\"type\":\"contacts\"},"
			+ "{\"type\":\"notes\"},"
			+ "{\"type\":\"vehicles\"}"
			+ "]}").asObject();

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		if (super.doGet(request))
			return true;
		if ("page".equals(request.getPathSegment(2))) {
			writePage(request.getPathSegmentInt(1), request);
			return true;
		}
		String segment_one = request.getPathSegment(1);
		if ("households".equals(segment_one)) {
			writeHouseholds(request.getPathSegment(2), request);
			return true;
		}
		mosaic.Person user = (mosaic.Person)request.getUser();
		Household household = user.getHousehold(request);
		if (household == null)
			return false;

		int household_id = household.getId();
		if (segment_one == null) {
			request.site.writePageOpen("household", request);
			TaskPane task_pane = request.writer.ui.taskPane("Households").setHead(household.getName() + " Household").open();
			if (m_lists.length > 0) {
				task_pane.heading("Private Lists");
				for (String item : m_lists)
					task_pane.task(Text.capitalize(item));
			}
			Map<String,Block> blocks = getBlocks();
			if (blocks != null) {
				boolean first = true;
				for (Block block : blocks.values())
					if (block.handlesContext("household accounting", request)) {
						if (first) {
							task_pane.heading("Community Accounting");
							first = false;
						}
						task_pane.task(block.getName(), request.getContext() + "/Households/blocks/" + block.getName() + "?id=" + household_id);
					}
			}
			task_pane.close();
			request.close();
			return true;
		}
		switch (segment_one) {
		case "Contacts":
			request.writer.h3("Contacts");
			((Contacts)request.site.getModule("Contacts")).writeHouseholdRecords(household_id, request);
			return true;
		case "Notes":
			request.writer.h3("Notes");
			ViewState.setBaseFilter("notes", "families_id=" + household_id, request);
			request.site.newView("notes", request).writeComponent();
			return true;
		case "pets by household":
			writePets(new Rows(new Select("pets.name,photo,notes,families.id,families.name,number").from("pets").joinOne("pets", "families").leftJoinOne("families", "homes").orderBy("families.name"), request.db), false, request.writer);
			return true;
		case "pets by name":
			writePets(new Rows(new Select("pets.name,photo,notes,families.id,families.name,number").from("pets").joinOne("pets", "families").leftJoinOne("families", "homes").orderBy("pets.name"), request.db), true, request.writer);
			return true;
		case "Recipes":
			request.writer.h3("Recipes");
			ViewState.setBaseFilter("recipes", "_owner_ IN(" + household.getPeopleIds() + ")", request);
			request.site.newView("recipes", request).writeComponent();
			return true;
		case "Todos":
			request.writer.h3("Todos");
			ViewState.setBaseFilter("todos", "families_id=" + household_id, request);
			request.site.newView("todos", request).writeComponent();
			return true;
		case "vehicles by household":
			writeVehicles(new Rows(new Select("description,license_plate,photo,notes,parking_spot,families.id,name,number,available_for_sharing").from("vehicles").joinOne("vehicles", "families").leftJoinOne("families", "homes").orderBy("name"), request.db), true, request.writer);
			return true;
		case "vehicles by parking spot":
			writeVehicles(new Rows(new Select("description,license_plate,photo,notes,parking_spot,families.id,name,number,available_for_sharing").from("vehicles").joinOne("vehicles", "families").leftJoinOne("families", "homes").orderBy("parking_spot"), request.db), false, request.writer);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDescription() {
		return "Organize households and their bios, pets and vehicles, and their private contacts, notes, recipes, and todos.";
	}

	//--------------------------------------------------------------------------

	public Household
	getHousehold(int id, DBConnection db) {
		if (id == 0)
			return null;
		Household household = m_households.get(id);
		if (household == null) {
			household = Household.load(id, db);
			if (household != null)
				m_households.put(id, household);
		}
		return household;
	}

	//--------------------------------------------------------------------------

	public static JDBCTable
	getTable() {
		return new JDBCTable("families")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("homes_id", "homes").setOnDeleteSetNull(true))
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("contact", Types.INTEGER))
			.add(new JDBCColumn("cm_account_active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("meal_work_exempt", Types.BOOLEAN))
			.add(new JDBCColumn("bio", Types.VARCHAR));
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		db.createManyTable("families", "families_pictures", "filename VARCHAR");

		JDBCTable table_def = new JDBCTable("notes")
			.add(new JDBCColumn("families"))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("note", Types.BINARY))
			.add(new JDBCColumn("category", Types.VARCHAR))
			.add(new JDBCColumn("public", Types.BOOLEAN));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("pets")
				.add(new JDBCColumn("families"))
				.add(new JDBCColumn("name", Types.VARCHAR))
				.add(new JDBCColumn("notes", Types.VARCHAR))
				.add(new JDBCColumn("photo", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("vehicles")
				.add(new JDBCColumn("families"))
				.add(new JDBCColumn("available_for_sharing", Types.BOOLEAN))
				.add(new JDBCColumn("description", Types.VARCHAR))
				.add(new JDBCColumn("license_plate", Types.VARCHAR))
				.add(new JDBCColumn("notes", Types.VARCHAR))
				.add(new JDBCColumn("parking_spot", Types.VARCHAR))
				.add(new JDBCColumn("photo", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		addPage(new Page("Pets", m_site){
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Pets", request.site.newHead(request).style(".pets{margin:20px}.pets>div{align-items:flex-start;display:flex;flex-wrap:wrap;margin-top:20px;}.pets>div>div{margin-right:1rem;}"), request);
				HTMLWriter writer = request.writer;
				Rows rows = new Rows(new Select("pets.name,photo,notes,families.id,families.name,number").from("pets").joinOne("pets", "families").leftJoinOne("families", "homes").orderBy("pets.name"), request.db);
				if (!rows.isBeforeFirst())
					writer.write("There are currently no pets in the system. To add a pet: go to the People page, click on \"Households\" and then click on the \"...\" button for your household.");
				else {
					writer.write("<div class=\"pets\" id=\"button_nav\"><div id=\"by name div\">");
					writePets(rows, true, writer);
					writer.write("</div></div>");
					writer.js("new ButtonNav(dom.$('button_nav'),[" +
						"{text:'By Name',display:'flex',div:dom.$('by name div'),url:'/Households/pets by name'}," +
						"{text:'By Household',display:'flex',url:'/Households/pets by household'}])");
				}
				rows.close();
				request.close();
			}
		}.setRole(m_site.getDefaultRole()), m_site, db);
		addPage(new Page("Vehicles", m_site){
			@Override
			public void
			write(Request request) {
				request.site.writePageOpen("Vehicles", request.site.newHead(request).style(".vehicles{margin:20px}.vehicles>div{align-items:flex-start;display:flex;flex-wrap:wrap;margin-top:20px;}.vehicles>div>div{margin-right:1rem;}"), request);
				HTMLWriter writer = request.writer;
				Rows rows = new Rows(new Select("description,license_plate,photo,notes,parking_spot,families.id,name,number,available_for_sharing").from("vehicles").joinOne("vehicles", "families").leftJoinOne("families", "homes").orderBy("name"), request.db);
				if (!rows.isBeforeFirst())
					writer.write("There are currently no vehicles in the system. To add a vehicle: go to the People page, click on \"Households\" and then click on the \"...\" button for your household.");
				else {
					writer.write("<div class=\"vehicles\" id=\"button_nav\"><div id=\"by household div\">");
					writeVehicles(rows, true, writer);
					writer.write("</div></div>");
					writer.js("new ButtonNav(dom.$('button_nav'),[" +
						"{text:'By Household',display:'flex',div:dom.$('by household div'),url:'/Households/vehicles by household'}," +
						"{text:'By Parking Spot',display:'flex',url:'/Households/vehicles by parking spot'}])");
				}
				request.close();
			}
		}.setRole(m_site.getDefaultRole()), m_site, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals("families"))
			return new ViewDef(name)
				.addUpdateHook(new UpdateHook() {
					@Override
					public void
					afterUpdate(int id, NameValuePairs name_value_pairs, Map<String,Object> previous_values, Request request) {
						Module calendar = request.site.getModule("FamilyCalendar" + id);
						if (calendar != null)
							calendar.setDisplayName(name_value_pairs.getString("name"));
						((Households)request.site.getModule("Households")).getHousehold(id, request.db).setName(name_value_pairs.getString("name"), request.db);
					}
				})
				.setAccessPolicy(new NoDeleteAccessPolicy("people").add().edit())
				.setDefaultOrderBy("name")
				.setFilters(new String[] { "Active Households|active", "Inactive Households|NOT active" })
				.setRecordName("Household")
				.setColumnNamesForm(new String[] { "name", "contact", "homes_id", "cm_account_active", "meal_work_exempt", "active" })
				.setColumnNamesTable(new String[] { "name", "homes_id" })
				.setColumn(new Column("active").setDefaultValue(true).setViewRole("people"))
				.setColumn(new LookupColumn("contact", "people", "first,last").setAllowNoSelection(true).setFilter(site.getPeopleFilter()))
				.setColumn(new LookupColumn("homes_id", "homes", "number").setAllowNoSelection(true).setDisplayName("home"))
				.addRelationshipDef(new OneToManyLink("people", "first,last").setManyTableColumn("families_id"))
				.addRelationshipDef(new OneToMany("pets"))
				.addRelationshipDef(new OneToMany("vehicles"));
		if (name.equals("families_pictures")) {
			ViewDef view_def = new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setRecordName("Picture")
				.setShowColumnHeads(false)
				.setColumnNamesTable("filename")
				.setColumn(new Column("families_id").setDefaultToSessionAttribute().setIsHidden(true));
			view_def.setColumn(new PictureColumn("filename", view_def, "families", 200, 1024).setDirColumn("families_id"));
			return view_def;
		}
		if (name.equals("notes"))
			return new ViewDef(name)
				.addSection(new Tabs("category", new OrderBy("category")))
				.setDefaultOrderBy("title")
				.setRecordName("Note")
				.setColumnNamesTable(new String[] { "title", "note", "public" })
				.setColumn(new Column("category").setShowPreviousValuesDropdown(true))
				.setColumn(new HouseholdIdColumn())
				.setColumn(new TextAreaColumn("note").setEncryptionColumn("families_id"))
				.setColumn(new Column("public").setDisplayName("show on public household page"));
		if (name.equals("pets")) {
			ViewDef view_def = new ViewDef(name)
				.setDefaultOrderBy("name")
				.setRecordName("Pet")
				.setColumnNamesForm("families_id", "name", "photo", "notes")
				.setColumnNamesTable("photo", "name", "notes")
				.setColumn(new HouseholdIdColumn())
				.setColumn(new Column("name").setIsRequired(true));
			view_def.setColumn(new PictureColumn("photo", view_def, "families", 200, 1024).setDirColumn("families_id"));
			return view_def;
		}
		if (name.equals("vehicles")) {
			ViewDef view_def = new ViewDef(name)
				.setDefaultOrderBy("parking_spot")
				.setRecordName("Vehicle")
				.setColumnNamesForm("families_id", "description", "license_plate", "photo", "parking_spot", "available_for_sharing", "notes")
				.setColumnNamesTable("photo", "description", "license_plate", "parking_spot", "available_for_sharing", "notes")
				.setColumn(new HouseholdIdColumn())
				.setColumn(new Column("description").setIsRequired(true));
			view_def.setColumn(new PictureColumn("photo", view_def, "families", 200, 1024).setDirColumn("families_id"));
			return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public void
	writeAccountHistory(mosaic.Person user, int household_id, Request request) {
		HTMLWriter writer = request.writer;
		writer.write("<table style=\"margin: 0px auto;\"><tr><td><div class=\"alert alert-secondary\">");
		if (((Accounting)request.site.getModule("Accounting")).createHOAInvoices()) {
			double dues = user.getHousehold(request).getDues(request.db);
			if (dues != 0)
				writer.write("<p>Your HOA dues are ").writeCurrency(dues).write(" per month.</p>");
		}
		double balance = Accounting.calcBalanceHousehold(household_id, null, request.db);
		writer.write("<p>Your current account balance is ").writeCurrency(balance).write(". A negative number means you have a credit.</p>");
		if (m_hoa_text != null)
			writer.space().write(m_hoa_text);
		writer.write("</div></td></tr><tr><td>");
		ViewState.setBaseFilter("household transactions", "families_id=" + household_id, request);
		request.site.newView("household transactions", request).writeComponent();
		writer.write("</td></tr></table>");
	}

	//--------------------------------------------------------------------------

	private void
	writeHouseholds(String view_by, Request request) {
		HTMLWriter writer = request.writer;
		mosaic.Person user = (mosaic.Person)request.getUser();
		int user_pic_size = ((People)request.site.getModule("people")).m_user_pic_size;

		StringBuilder columns = new StringBuilder("families.id,name,number,first,last,people.id pid,picture,length(families.bio)");
		if (m_extra_columns != null)
			for (String column : m_extra_columns)
				columns.append(',').append(column);
		Rows rows = new Rows(new Select(columns.toString())
				.from("families JOIN people ON people.families_id=families.id LEFT JOIN homes ON families.homes_id=homes.id")
				.where("families.active AND people.active")
				.orderBy("home".equals(view_by) ? "number,name,first,last" : "name,first,last"), request.db);
		Card card = null;
		String prev_household_name = null;
		while (rows.next()) {
			String household_name = rows.getString(2);
			if (prev_household_name == null || !prev_household_name.equals(household_name)) {
				if (card != null)
					card.close();
				card = writer.ui.card().headerOpen();
				int id = rows.getInt(1);
				if (id == user.getHouseholdId() || rows.getInt(8) > 0 || request.db.lookupBoolean(new Select("EXISTS (SELECT 1 FROM pets where families_id=" + id + ") OR EXISTS (SELECT 1 FROM vehicles where families_id=" + id + ") OR EXISTS (SELECT 1 FROM contacts where families_id=" + id + " AND public)")))
					writer.addStyle("float:right;").ui.buttonOnClick("...", "new Dialog({click:function(event){RichText.remove(event.currentTarget,event.target)},show_close:true,title:'" + JS.escape(household_name) + " Household',url:context+'/Households/" + id + "/page'})");
				writer.write(household_name);
				String home_number = rows.getString(3);
				if (home_number != null)
					writer.write(" - " + home_number);
				card.bodyOpen();
			}
			writer.write("<p>");
			String people_id = rows.getString(6);
			String filename = rows.getString(7);
			if (filename != null) {
				writer.setAttribute("style", "cursor:pointer;max-height:" + user_pic_size + "px;max-width:" + user_pic_size + "px;");
				writer.setAttribute("onclick", "Img.show(this)");
				writer.img(filename, "people/" + people_id);
			}
			writer.setAttribute("style", "float:right;");
			writer.ui.buttonOnClick("...", "Person.popup(" + people_id + ")");
			writer.nbsp().write(rows.getString(4)).nbsp().write(rows.getString(5));
			if (m_extra_columns != null)
				for (String column : m_extra_columns) {
					int column_index = rows.findColumn(column);
					if (rows.getColumnType(column_index) == Types.DATE) {
						LocalDate date = rows.getDate(column_index);
						if (date != null)
							writer.br().writeDate(date);
					} else {
						String s = rows.getString(column_index);
						if (s != null)
							writer.br().write(s);
					}
				}
			writer.write("</p>");
			prev_household_name = household_name;
		}
		if (card != null)
			card.close();
		rows.close();
	}

	//--------------------------------------------------------------------------

	private void
	writePage(int household_id, Request request) {
		Household household = Household.load(household_id, request.db);
		if (household == null)
			return;
		boolean belongs_to = household.belongsTo(request.getUser().getId());
		HTMLWriter writer = request.writer;
		Blocks b = new Blocks(belongs_to, request).lookup(new Select("*").from("families").whereIdEquals(household_id));
		JsonArray blocks = m_page_template.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			if (b.write(block))
				continue;
			switch(block.getString("type", null)) {
			case "contacts":
				((Contacts)request.site.getModule("Contacts")).writePublicRecords(household_id, request);
				break;
			case "notes":
				writePublicNotes(household_id, request);
				break;
			case "people":
				int user_pic_size = ((People)request.site.getModule("people")).m_user_pic_size;
				Rows rows = new Rows(new Select("id,first,last,picture").from("people").where("families_id=" + household_id + " AND active").orderBy("first"), request.db);
				while (rows.next()) {
					String id = rows.getString(1);
					String filename = rows.getString(4);
					if (filename != null) {
						writer.setAttribute("style", "cursor:pointer;max-height:" + user_pic_size + "px;max-width:" + user_pic_size + "px;");
						writer.setAttribute("onclick", "Img.show(this)");
						writer.img(filename, "people/" + id);
					}
					writer.setAttribute("style", "float:right;");
					writer.ui.buttonOnClick("...", "Person.popup(" + id + ")");
					writer.nbsp().write(rows.getString(2)).nbsp().write(rows.getString(3)).hr();
				}
				rows.close();
				break;
			case "pets":
				if (belongs_to) {
					request.setSessionAttribute("families_id", household_id);
					ViewState.setBaseFilter("pets", "families_id=" + household_id, request);
					request.site.getViewDef("pets", request.db).newView(request).setMode(Mode.LIST).writeComponent();
				} else {
					List<String[]> pets = request.db.readRows(new Select("name,photo,notes").from("pets").whereEquals("families_id", household_id).andWhere("(name IS NOT NULL OR photo IS NOT NULL)"));
					if (pets.size() > 0) {
							writer.write("<div onclick=\"temp_gallery=new Gallery({dir:'families/").write(household_id).write("'});temp_gallery.open(event.target)\">");
							for (String[] pet : pets)
								writer.addStyle("max-width:100px").ui.figure(pet[1] == null ? null : "families/" + household_id + "/" + pet[1], Text.join("<br/>",pet[0], pet[2]), "max-height:100px;max-width:100px");
							writer.write("</div>");
					}
				}
				break;
			case "pictures":
				if (belongs_to) {
					request.setSessionAttribute("families_id", household_id);
					ViewState.setBaseFilter("families_pictures", "families_id=" + household_id, request);
					request.site.getViewDef("families_pictures", request.db).newView(request).setMode(Mode.LIST).writeComponent();
				} else {
					List<String> filenames = request.db.readValues(new Select("filename").from("families_pictures").whereEquals("families_id", household_id));
					if (filenames.size() > 0) {
						writer.write("<div onclick=\"temp_gallery=new Gallery({dir:'families/").write(household_id).write("'});temp_gallery.open(event.target)\">");
						for (String filename : filenames)
							writer.write("\n<img src=\"").write(request.getContext()).write("/families/").write(household_id).write("/thumbs/").write(filename).write("\" style=\"cursor:pointer;max-width:100%;\"/>");
						writer.write("</div>");
					}
				}
				break;
			case "vehicles":
				if (belongs_to) {
					request.setSessionAttribute("families_id", household_id);
					ViewState.setBaseFilter("vehicles", "families_id=" + household_id, request);
					request.site.getViewDef("vehicles", request.db).newView(request).setMode(Mode.LIST).writeComponent();
				} else {
					List<String[]> vehicles = request.db.readRows(new Select("description,license_plate,photo,notes,available_for_sharing").from("vehicles").whereEquals("families_id", household_id));
					if (vehicles.size() > 0) {
							writer.write("<div onclick=\"temp_gallery=new Gallery({dir:'families/").write(household_id).write("'});temp_gallery.open(event.target)\">");
							for (String[] vehicle : vehicles) {
								String caption = vehicle[0];
								if (vehicle[1] != null)
									caption += " - " + vehicle[1];
								if (vehicle[3] != null)
									caption += "<br/>" + vehicle[3];
								if (vehicle[4].equals("true"))
									caption += "<br/>available for sharing";
								writer.addStyle("max-width:100px").ui.figure(vehicle[2] == null ? null : "families/" + household_id + "/" + vehicle[2], caption, "max-height:100px;max-width:100px");
							}
							writer.write("</div>");
					}
				}
				break;
			}
		}
	}

	//--------------------------------------------------------------------------

	void
	writePets(Rows rows, boolean by_name, HTMLWriter writer) {
		while (rows.next()) {
			Card card = writer.ui.card().addStyle("max-width", "500px").headerOpen();
			if (by_name)
				writer.write(rows.getString(1)).write(" - ");
			writer.write(rows.getString(5));
			String number = rows.getString(6);
			if (number != null)
				writer.space().write(number);
			if (!by_name)
				writer.write(" - ").write(rows.getString(1));
			card.bodyOpen();
			String photo = rows.getString(2);
			if (photo != null)
				writer.write("<img src=\"families/").write(rows.getString(4)).write("/").write(photo).write("\" style=\"max-width:100%\" />");
			String notes = rows.getString(3);
			if (notes != null)
				card.text(notes);
			card.close();
		}
	}

	//--------------------------------------------------------------------------

	private void
	writePublicNotes(int household_id, Request request) {
		Rows rows = new Rows(new Select("title,note").from("notes").where("families_id=" + household_id + " AND public"), request.db);
		if (rows.isBeforeFirst()) {
			request.writer.addClass("text-muted").h4("Notes");
			while (rows.next()) {
				request.writer.write("<p><b>");
				request.writer.write(rows.getString(1));
				request.writer.write("</b><br />");
				request.writer.write(Text.scramble(DBConnection.decodeBinary(rows.getString(2)), household_id));
				request.writer.write("</p>");
			}
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	void
	writeVehicles(Rows rows, boolean by_household, HTMLWriter writer) {
		while (rows.next()) {
			Card card = writer.ui.card().addStyle("max-width", "500px").headerOpen();
			String parking_spot = rows.getString(5);
			if (by_household) {
				writer.write(rows.getString(7));
				String number = rows.getString(8);
				if (number != null)
					writer.space().addStyle("vertical-align:baseline").ui.icon("house").space().write(number);
			} else
				writer.write(parking_spot);
			card.bodyOpen();
			writer.write(rows.getString(1));
			String license_plate = rows.getString(2);
			if (license_plate != null)
				writer.write(" - ").write(license_plate);
			writer.br();
			if (!by_household) {
				writer.write(rows.getString(7));
				String number = rows.getString(8);
				if (number != null)
					writer.space().addStyle("vertical-align:baseline").ui.icon("house").space().write(number);
				writer.br();
			}
			if (by_household && parking_spot != null)
				writer.write("Parking Spot: ").write(parking_spot).br();
			if (rows.getBoolean(9))
				writer.write("available for sharing<br/>");
			String photo = rows.getString(3);
			if (photo != null)
				writer.write("<img src=\"families/").write(rows.getString(6)).write("/").write(photo).write("\" style=\"max-width:100%\" />");
			String notes = rows.getString(4);
			if (notes != null)
				card.text(notes);
			card.close();
		}
		rows.close();
	}
}
