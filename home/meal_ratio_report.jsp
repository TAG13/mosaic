<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="meals.MealEventProvider,meals.MealRatioRenderer,app.Request,db.Select,java.util.List" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("", r);
	List<String[]> households = r.db.readRows(new Select("id,name").from("families").orderBy("name"));
	r.writer.write("<table border=\"1\"><tr><th>household</th><th># adult meals / # times worked</th</tr>");
	for (String[] household : households) {
		r.writer.write("<tr><td>");
		r.writer.write(household[1]);
		r.writer.write("</td><td>");
		MealRatioRenderer.writeRatio(Integer.parseInt(household[0]), ((MealEventProvider)r.site.getModule("Common Meals")).getJobs(), true, r);
		r.writer.write("</td></tr>");
	}
	r.writer.write("</table>");
	r.close();
%>
