<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Text,db.Rows,db.Select" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("", r);
%>
	<div class="container">
		<h3>Send Email to Community Members</h3>
		<div style="display:flex;flex-wrap:wrap;">
<%
	Rows rows = new Rows(new Select("first,last,email").from("people").where("active AND email IS NOT NULL").orderBy("first,last"), r.db);
	while (rows.next()) {
		r.writer.write("<div class=\"checkbox\" style=\"margin:0;width:200px;\"><label><input onclick=\"clicked()\" type=\"checkbox\" email=\"").write(rows.getString(3)).write("\">").write(Text.join(" ", rows.getString(1), rows.getString(2))).write("</label></div>");
	}
	rows.close();
%>
		</div>
		<br/>
		<a href="javascript:Dialog.alert(null,'Please select one or more recipients.')" id="compose" class="btn">compose message</a>
	</div>
	<script>
		function clicked() {
			var addresses = '';
			var inputs = document.querySelectorAll("input[type='checkbox']");
			inputs.forEach(function(cb) {
				if (cb.checked) {
					if (addresses)
						addresses += ',';
					addresses += cb.getAttribute('email');
				}
			});
			dom.$('compose').href = addresses ? 'mailto:' + addresses : "javascript:Dialog.alert(null,'Please select one or more recipients.')";
		}
	</script>
<%
	r.close();
%>
