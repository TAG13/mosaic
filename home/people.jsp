<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Text,db.Select,db.ViewState,web.Head" %>
<%
	Request r = new Request(request, response, out);
	Head head = r.site.newHead(r).script("gallery-min").styleSheet("gallery-min").style(".card{margin-right:1em;width:300px;}.panel{margin-right:20px;width:300px;}");
	r.site.writePageOpen("People", head, r);
	r.writer.write("<div id=\"button_nav\" class=\"container\" style=\"padding-top:10px;\">");
	r.writer.write("<div id=\"list\">");
	String base_filter = r.site.getViewDef("people", r.db).getBaseFilter();
	if (base_filter == null)
		base_filter = "active";
	ViewState.setBaseFilter("people", base_filter, r);
	r.site.newView("people", r).writeComponent();
	r.writer.write("</div>");
	if (r.site.getModule("Households").isActive()) {
%>
	<div id="households" style="display:none;">
<%
	if (r.db.countRows(new Select("1").from("homes")) > 0) {
%>
	Order by: <label style="font-weight:normal;cursor:pointer;"><input type="radio" id="view_by_name" name="view_by" onclick="set_view_by('name')"> name</label>&nbsp;&nbsp;&nbsp;&nbsp;<label style="font-weight:normal;cursor:pointer;"><input type="radio" id="view_by_home" name="view_by" onclick="set_view_by('home')"> home</label>
<%
	}
%>
	<div style="display:flex;flex-wrap:wrap;padding-top:10px;"></div>
	</div>
	<script>
	function get_households() {
		var view_by = localStorage.getItem('households view by') || 'name';
		var radio = dom.$('view_by_' + view_by);
		if (radio)
			radio.checked = true;
		net.replace(dom.$('households').lastElementChild, context+'/Households/households/' + view_by);
	}
	function set_view_by(view_by) {
		localStorage.setItem('households view by', view_by);
		get_households();
	}
	gallery = new Gallery();
	new ButtonNav(dom.$('button_nav'), [
		{ text: 'People', div: dom.$('list'), icon: 'person' },
		{ text: '<%= Text.pluralize(r.site.getViewDef("families", r.db).getRecordName()) %>', div: dom.$('households'), icon: 'house-door', onclick: function(){if(dom.$('households').lastElementChild.children.length==0)get_households()} }
	]);
	</script>
<%
	}
	r.close();
%>
<br />
