<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,email.MailLists" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("Email", r.site.newHead(r).style("tr.lists:hover{background-color:#bbb}table.email{margin:0 auto;}"), r);
	if ("show_inactive_lists".equals(r.getParameter("action")))
		r.setSessionAttribute("show inactive lists", true);
%>
<script>
	function toggle_digest(c, list_id) {
		if (c.checked) // just changed to
			net.post(context + '/MailLists/' + list_id + '/digest/true');
		else
			net.post(context + '/MailLists/' + list_id + '/digest/false');
	}
	function toggle_subscription(c, list_id) {
		if (c.checked) // just changed to
			net.post(context + '/MailLists/' + list_id + '/subscribed/true');
		else
			net.post(context + '/MailLists/' + list_id + '/subscribed/false');
	}
</script>
<table class="email">
<%
	if (r.site.getSettings().getBoolean("show send email to community members link")) {
%>
	<tr>
		<td style="border-bottom:1px solid #999;padding:30px;"><% r.writer.ui.aButton("send email to community members", "send_email.jsp", null); %></td>
	</tr>
<% } %>
	<tr>
		<td>
			<%
				((MailLists)r.site.getModule("MailLists")).writeLists(r);
			%>
		</td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #999">
			<%
				if (!r.testSessionFlag("show inactive lists") && r.db.rowExists("mail_lists", "NOT active"))
					r.writer.ui.aButton("show inactive lists", "email.jsp?action=show_inactive_lists");
			%>
		</td>
	</tr>
</table>
<br />
<%
	r.close();
%>
