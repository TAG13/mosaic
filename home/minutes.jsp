<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,db.View,db.ViewState" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("Minutes", r);
	r.writer.write("<div class=\"container\" style=\"display:table;padding-top:10px;\">");
	ViewState.setQuicksearch("minutes", null, null, r);
	ViewState.setBaseFilter("minutes", "show_on_main_minutes_page AND (groups_id IS NULL OR (SELECT active FROM groups WHERE groups.id=groups_id))", r);
	r.site.getViewDef("minutes", r.db).newView(r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
	r.writer.write("</div>");
	r.close();
%>
