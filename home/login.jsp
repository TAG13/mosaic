<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,mosaic.LoginPage" %>
<%
	Request r = new Request(request, response, out);
	LoginPage login_page = (LoginPage)r.site.getModule("LoginPage");
	while (login_page == null) {
		Thread.sleep(1000); // site starting up, wait 1 second
		login_page = (LoginPage)r.site.getModule("LoginPage");
	}
	login_page.doGet(r);
	r.close();
%>
