<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,db.ViewState,mosaic.MOSAIC" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("Docs", r);
%>
<div class="container" style="padding-top:10px;">
<%
	ViewState.setQuicksearch("docs", null, null, r);
	r.writer.setAttribute("style", "display:table");
	r.writer.componentOpen(r.getContext() + "/docs", null);
	((MOSAIC)r.site).writeDocs(r);
	r.writer.tagClose();
%>
</div>
<%
	r.close();
%>
