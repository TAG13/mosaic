<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,calendar.MonthView,calendar.EventProvider" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("Calendars", r);
	new MonthView((EventProvider)r.site.getModule("Main Calendar"), r).writeComponent(r);
	r.close();
%>
