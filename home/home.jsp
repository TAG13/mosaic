<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,mosaic.HomePage" %>
<%
	Request r = new Request(request, response, out);
	try {
		((HomePage)r.site.getModule("HomePage")).doGet(r);
	} finally {
		r.close();
	}
%>