<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,db.ViewState,social.Likes" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("Recipes", r);
	r.writer.write("<div style=\"height:10px\"></div>");
	if (r.db.getTable("recipes").getColumn("coho") != null)
		ViewState.setBaseFilter("recipes", "coho", r);
	r.site.newView("recipes", r).setColumnNamesTable(new String[] { "title", "families_id" }).writeComponent();
	Likes.writeJavascriptFunctions(r.writer);
	r.close();
%>
