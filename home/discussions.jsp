<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,social.Likes" %>
<%
	Request r = new Request(request, response, out);
	r.site.writePageOpen("", r);
	r.site.newView("discussions", r).writeComponent();
	Likes.writeJavascriptFunctions(r.writer);
	r.close();
%>
