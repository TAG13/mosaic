package meals;

import java.time.LocalDate;

import app.Person;
import app.Request;
import app.Site;
import app.Time;
import db.Select;
import db.View;
import db.access.AccessPolicy;

public class MealPeopleAccessPolicy extends AccessPolicy {
	public
	MealPeopleAccessPolicy() {
		add();
		delete();
		edit();
	}

    //--------------------------------------------------------------------------

	private static boolean
	isCookOrNotClosed(View view, Request request) {
		if (request.userHasRole("cm account manager"))
			return true;
		Object[] cook_closed_date = request.db.readRowObjects(new Select("_owner_,closed,date").from("meal_events").whereIdEquals(view.data.getInt("meal_events_id")));
		Person user = request.getUser();
		if (cook_closed_date[0] != null && user != null && ((Integer)cook_closed_date[0]).intValue() == user.getId())
			return true;
		return !isMealClosed((Boolean)cook_closed_date[1], (LocalDate)cook_closed_date[2], request.site);
	}

	//--------------------------------------------------------------------------

	private static boolean
	isMealClosed(Boolean closed, LocalDate date, Site site) {
		if (closed.booleanValue())
			return true;

		LocalDate yesterday = Time.newDate().minusDays(1);
		return date.isBefore(yesterday);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View view, Request request) {
		Person user = request.getUser();
		if (user == null)
			return false;
		if (request.userHasRole("cm account manager"))
			return true;
		if (view.getMode() == View.Mode.ADD_FORM)
			return true;
		int meal_id = view.getRelationship().one.data.getInt("id");
		Object[] cook_closed_date = request.db.readRowObjects(new Select("_owner_,closed,date").from("meal_events").whereIdEquals(meal_id));
		if (cook_closed_date[0] != null && ((Integer)cook_closed_date[0]).intValue() == user.getId())
			return true;
		if (isMealClosed((Boolean)cook_closed_date[1], (LocalDate)cook_closed_date[2], request.site))
			return false;
		int household_id = request.db.lookupInt(new Select("families_id").from("people").whereIdEquals(user.getId()), 0);
		if (household_id == 0)
			return false;
		return !request.db.rowExists("meal_people", "meal_events_id=" + meal_id + " AND families_id=" + household_id);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(View view, Request request) {
		if (request.userHasRole("cm account manager"))
			return true;
		Object[] cook_closed_date = request.db.readRowObjects(new Select("_owner_,closed,date").from("meal_events").whereIdEquals(view.data.getInt("meal_events_id")));
		Person user = request.getUser();
		if (user == null)
			return false;
		if (cook_closed_date[0] != null && ((Integer)cook_closed_date[0]).intValue() == user.getId())
			return true;
		if (isMealClosed((Boolean)cook_closed_date[1], (LocalDate)cook_closed_date[2], request.site))
			return false;
		return request.db.rowExists("people", "id=" + user.getId() + " AND families_id=" + view.data.getString("families_id"));
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(View view, Request request) {
		return isCookOrNotClosed(view, request);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(View view, Request request) {
		if (request.userHasRole("cm account manager"))
			return true;
		Object[] cook_closed_date = request.db.readRowObjects(new Select("_owner_,closed,date").from("meal_events").whereIdEquals(view.data.getInt("meal_events_id")));
		Person user = request.getUser();
		if (user == null)
			return false;
		if (cook_closed_date[0] != null && ((Integer)cook_closed_date[0]).intValue() == user.getId())
			return true;
		if (isMealClosed((Boolean)cook_closed_date[1], (LocalDate)cook_closed_date[2], request.site))
			return false;
		return request.db.rowExists("people", "id=" + user.getId() + " AND families_id=" + view.data.getString("families_id"));
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(View view, Request request) {
		return isCookOrNotClosed(view, request);
	}
}