package meals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import app.Array;
import app.Mail;
import app.Request;
import db.InsertHook;
import db.NameValuePairs;
import db.Rows;
import db.Select;
import db.UpdateHook;

public class MealPeopleInsertUpdateHook implements InsertHook, UpdateHook {
	private String[] m_ages;
	private String[] m_ignore_ages_for_max_count;

	//--------------------------------------------------------------------------

	public
	MealPeopleInsertUpdateHook(String[] ages, String[] ignore_ages_for_max_count) {
		m_ages = ages;
		m_ignore_ages_for_max_count = ignore_ages_for_max_count;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
		updateMeal(name_value_pairs.getInt("meal_events_id", 0), request);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
		updateMeal(request.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0), request);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs name_value_pairs, Request request) {
		return checkMeal(name_value_pairs.getInt("meal_events_id", 0), 0, name_value_pairs, request);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs name_value_pairs, Map<String, Object> previous_values, Request request) {
		int num_people = 0;
		if (m_ages != null) {
			Select query = new Select();
			for (String m_age : m_ages)
				query.addColumn("num_" + m_age + 's');
			query.from("meal_people").whereIdEquals(id);
			Rows rows = new Rows(query, request.db);
			rows.next();
			for (int i=1,n=m_ages.length; i<=n; i++)
				if (m_ignore_ages_for_max_count == null || Array.indexOf(m_ignore_ages_for_max_count, m_ages[i - 1]) == -1)
					num_people += rows.getInt(i);
			rows.close();
		} else
			num_people = 1;
		return checkMeal(request.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0), num_people, name_value_pairs, request);
	}

	//--------------------------------------------------------------------------

	private String
	checkMeal(int meal_id, int previous_people, NameValuePairs name_value_pairs, Request request) {
		Select select = new Select("max_people").from("meal_events").leftJoin("meal_events", "meal_people").whereEquals("meal_events.id", meal_id).groupBy("max_people");
		if (m_ages != null)
			for (String age : m_ages) {
				if (m_ignore_ages_for_max_count == null || Array.indexOf(m_ignore_ages_for_max_count, age) == -1)
					select.addColumn("SUM(num_" + age + "s) num_" + age + 's');
			}
		else
			select.addColumn("COUNT(*)");
		Rows rows = new Rows(select, request.db);
		if (!rows.next()) {
			rows.close();
			return null;
		}
		int max_people = rows.getInt(1);
		if (max_people == 0) {
			rows.close();
			return null;
		}
		int new_people = 0;
		int num_people = 0;
		if (m_ages != null)
			for (String m_age : m_ages) {
				if (m_ignore_ages_for_max_count == null || Array.indexOf(m_ignore_ages_for_max_count, m_age) == -1) {
					String column = "num_" + m_age + "s";
					new_people += name_value_pairs.getInt(column, 0);
					num_people += rows.getInt(column);
				}
			}
		else {
			new_people = 1;
			num_people = rows.getInt(2);
		}
		rows.close();
		if (num_people - previous_people + new_people > max_people) {
			int num_openings = max_people - num_people;
			if (num_openings <= 0)
				return "There aren't any openings available for this meal. Please check with the cook to see if they can increase the number of openings.";
			if (num_openings == 1)
				return "There is only one opening available for this meal. Please reduce the number of people you are signing up or ask the cook to increase the number of openings.";
			return "There are only " + num_openings + " openings available for this meal. Please reduce the number of people you are signing up or ask the cook to increase the number of openings.";
		}
		return null;
	}

	//--------------------------------------------------------------------------

	private void
	updateMeal(int meal_id, Request request) {
		try {
			StringBuilder sql = new StringBuilder("SELECT max_people,closed,_owner_,menu");
			if (m_ages != null)
				for (String age : m_ages) {
					sql.append(",SUM(num_");
					sql.append(age);
					sql.append("s)");
				}
			else
				sql.append(",COUNT(*)");
			sql.append(" FROM meal_events LEFT JOIN meal_people ON (meal_people.meal_events_id=meal_events.id) WHERE meal_events.id=");
			sql.append(meal_id);
			sql.append(" GROUP BY max_people,closed,_owner_,menu");
			ResultSet rs = request.db.select(sql.toString());
			rs.next();
			int max_people = rs.getInt(1);
			int num_people = 0;
			if (m_ages != null)
				for (int i=1,n=m_ages.length; i<=n; i++)
					num_people += rs.getInt(i + 4);
			else
				num_people += rs.getInt(5);
			if (max_people > 0 && num_people >= max_people && !rs.getBoolean(2)) {
				int cook = rs.getInt(3);
				if (!rs.wasNull()) {
					String email = request.db.lookupString("email", "people", cook);
					if (email != null)
						new Mail(request.site).send(
							email,
							"meal closed",
							Integer.toString(num_people) + " people have signed up for your meal (" + rs.getString(4).trim() + ") and so it has been closed. You may reopen it and increase the value of 'max people' if you wish.",
							false);
				}
				request.db.update("meal_events", "closed=true", meal_id);
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
