package meals;

import java.time.LocalDate;
import java.util.ArrayList;

import app.Person;
import app.Request;
import app.Time;
import calendar.Event;
import db.DBConnection;
import db.Rows;
import db.Select;

public class MealEvent extends Event {
	private boolean	m_closed;
	private boolean	m_invoiced;
	private String	m_menu;
	private String	m_title;
	private int[]	m_workers;

	//--------------------------------------------------------------------------

	public
	MealEvent(MealEventProvider event_provider, Rows rows) {
		super(event_provider, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendEventHTML(LocalDate date, StringBuilder s, boolean black_text, Request request) {
		super.appendEventHTML(date, s, black_text, request);
		if (request.getUser() != null && "Accounting".equals(request.getSessionAttribute("current page")))
			if (m_invoiced)
				if (((MealEventProvider)getEventProvider()).mealHasInvoices(m_id, request.db))
					s.append("<br />").append(request.writer.ui.button("delete invoices")
						.setOnClick("net.post(context+'/Common Meals/" + m_id + "/invoices/delete',null,function(){Calendar.$().update();net.replace(dom.$('invoices').firstChild);net.replace(dom.$('events').firstChild);});event.stopPropagation();return false;")
						.toString());
				else
					s.append("<br />invoiced");
			else
				s.append("<br />").append(request.writer.ui.button("invoice")
					.setOnClick("net.post(context+'/Common Meals/" + m_id + "/invoices/add',null,function(){Calendar.$().update();net.replace(dom.$('invoices').firstChild);net.replace(dom.$('events').firstChild);});event.stopPropagation();return false;")
					.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request request) {
		switch (item) {
		case "cook":
//			if (m_owner != 0)
				appendItem(((MealEventProvider)m_event_provider).m_cook_label, m_owner == 0 ? null : request.site.lookupName(m_owner, request.db), null, black_text, html, ics, s);
			return;
		case "menu":
			if (m_menu != null)
				appendItem(item, m_menu, null, black_text, html, ics, s);
			return;
		case "jobs":
			Select workers_query = ((MealEventProvider)m_event_provider).m_workers_query;
			String worker_columns = workers_query.getColumns().substring(3); // assume columns start with "id,"
			String[] jobs = ((MealEventProvider)m_event_provider).m_jobs;
			for (int i=0; i<m_workers.length; i++)
				appendItem(jobs[i].replace('_', ' '), request.db.lookupString(worker_columns, workers_query.getFirstTable(), m_workers[i]), null, black_text, html, ics, s);
			return;
		case "open/closed":
			if (html)
				s.append("<div>- ");
			else if (s.length() > 0)
				s.append(ics ? "\\n" : "\n");
			s.append(m_closed || m_start_date.isBefore(Time.newDate().minusDays(1))? "closed" : "open");
			if (html)
				s.append(" -</div>");
			return;
		case "title":
			if (m_title != null)
				appendItem("", m_title, null, black_text, html, ics, s);
			return;
		}
		super.appendItem(item, black_text, html, ics, date, s, request);
	}

	//--------------------------------------------------------------------------

	@Override
	public String getEventText() {
		if (m_title != null)
			return m_title;
		else if (m_menu != null)
			return m_menu;
		return null;
	}

	//--------------------------------------------------------------------------

	public boolean
	isClosed(Request request) {
		Person user = request.getUser();
		boolean is_cook = user != null && m_owner == user.getId();
		return !is_cook && !request.userHasRole("cm account manager") && !request.userIsAdmin() && (m_closed || m_start_date.isBefore(Time.newDate().minusDays(1)));
	}

	//--------------------------------------------------------------------------

	public boolean
	isPotluck() {
		return isPotluck(m_menu);
	}

	//--------------------------------------------------------------------------

	public static boolean
	isPotluck(String menu) {
		if (menu == null)
			return false;
		menu = menu.toLowerCase();

		return menu.indexOf("potluck") != -1 || menu.indexOf("pot luck") != -1;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	load(Rows rows) {
		super.load(rows);
		m_closed = rows.getBoolean("closed");
		m_invoiced = rows.getBoolean("invoiced");
		m_menu = rows.getString("menu");
		if (((MealEventProvider)getEventProvider()).m_use_title_field)
			m_title = rows.getString("title");
		if (isPotluck())
			if (m_menu != null)
				m_event_text = m_menu.trim();
			else
				m_event_text = "potluck";
		else if (m_menu != null && m_menu.indexOf("eating circle") != -1)
			m_event_text = m_menu.trim();
		String[] jobs = ((MealEventProvider)m_event_provider).m_jobs;
		m_workers = new int[jobs.length];
		for (int i=0; i<jobs.length; i++)
			m_workers[i] = rows.getInt(jobs[i]);
	}

	//--------------------------------------------------------------------------

	@Override
	public String[]
	lookupEmail(String email, DBConnection db) {
		if ("cook".equals(email))
			return new String[] { db.lookupString(new Select("email").from("people").whereIdEquals(m_owner)) };
		if ("diners".equals(email))
			return ((MealEventProvider)m_event_provider).getDinerEmails(m_id, db);
		if ("workers".equals(email)) {
			ArrayList<String> emails = new ArrayList<>();
			emails.add(db.lookupString(new Select("email").from("people").whereIdEquals(m_owner)));
			for (int worker : m_workers)
				if (worker != 0) {
					boolean workers_are_households = ((MealEventProvider)m_event_provider).m_workers_are_families;
					email = db.lookupString(new Select("email").from(workers_are_households ? "families JOIN people ON (families.contact=people.id)" : "people").whereEquals(workers_are_households ? "families.id" : "people.id", worker));
					if (email != null)
						emails.add(email);
				}
			return emails.toArray(new String[emails.size()]);
		}
		return super.lookupEmail(email, db);
	}
}
