package meals;

import app.Request;
import app.Site;
import db.column.LookupColumn;

public class MealPeoplePersonColumn extends LookupColumn {
	public
	MealPeoplePersonColumn(Site site) {
		super("people_id", "people", "first,last");
		setDisplayName("person");
		setFilter(site.getPeopleFilter());
	}

    //--------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(Request request) {
		return Integer.toString(request.getUser().getId());
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	isReadOnly(db.View.Mode mode, Request request) {
		return !((Boolean)request.getSessionAttribute("iscook")).booleanValue() && !request.userIsAdministrator();
	}
}