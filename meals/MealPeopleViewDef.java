package meals;

import java.util.ArrayList;

import accounting.Accounting;
import app.Person;
import app.Request;
import app.Site;
import db.FormHook;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.Column;
import households.Household;

public class MealPeopleViewDef extends ViewDef {
	private String[]	m_ages;
	private boolean		m_register_individuals;
	boolean				m_show_balance_message;

	//--------------------------------------------------------------------------

	public
	MealPeopleViewDef(boolean register_individuals, boolean show_balance_message, String[] ages, String[] display_names, String[] ignore_ages_for_max_count, Site site) {
		super("meal_people");
		m_register_individuals = register_individuals;
		m_show_balance_message = show_balance_message;
		m_ages = ages;
		setAccessPolicy(new MealPeopleAccessPolicy());
		setAddButtonText("sign up for meal");
		setDefaultOrderBy(m_register_individuals ? "people_id" : "families_id");
		MealPeopleInsertUpdateHook insert_update_hook = new MealPeopleInsertUpdateHook(register_individuals ? null : m_ages, ignore_ages_for_max_count);
		addInsertHook(insert_update_hook);
		addUpdateHook(insert_update_hook);
		setRecordName(m_register_individuals ? "Person" : "Household");
		setColumn(m_register_individuals ? new MealPeoplePersonColumn(site) : new MealPeopleHouseholdColumn());
		String meal_work_ratio = site.getSettings().getString("meal work ratio");
		if (meal_work_ratio != null)
			setColumn(new Column("adult eating/work ratio"){
				@Override
				public boolean isHidden(Request request) {
					return ((mosaic.Person)request.getUser()).getHousehold(request).isMealWorkExempt();
				}
			}.setInputRenderer(new MealRatioRenderer()).setPostText("<div style=\"font-size:9pt;width:300px\">(current policy is " + meal_work_ratio + " times eating per 1 time working. Higher ratios should work more, lower ratios should eat more :-)</span>"));
		if (!m_register_individuals)
			for (int i=0,n=ages.length; i<n; i++)
				setColumn(new Column("num_" + ages[i] + "s").setDisplayName(display_names[i]));
		addFormHook(new FormHook() {
				@Override
				public void
				afterForm(View view, Mode mode, boolean printer_friendly, Request request) {
					if (!m_show_balance_message)
						return;
					Person user = request.getUser();
					if (user == null)
						return;
					Household household = ((mosaic.Person)user).getHousehold(request);
					if (household == null)
						return;
					double balance = Accounting.calcBalanceHousehold(household.getId(), null, request.db);
					if (balance <= 0)
						return;
					request.writer.write("<tr><td><div class=\"alert alert-danger\">You have an outstanding balance of ").writeCurrency(balance).write(".<br/>Please submit a payment as soon as possible to bring your credit back up<br/>(we suggest maintaining a 50-100 dollar credit).</div></td></tr>");
				}
			});
	}

	//--------------------------------------------------------------------------

	@Override
	public View
	newView(Request request) {
		View view = new View(this, request);
		Boolean is_pot_luck = (Boolean)request.getSessionAttribute("ispotluck");
		if (is_pot_luck != null && is_pot_luck.booleanValue()) {
			ArrayList<String> column_names = new ArrayList<>();
			if (m_register_individuals)
				column_names.add("people_id");
			else {
				column_names.add("families_id");
				for (String age : m_ages)
					column_names.add("num_" + age + "s");
			}
			column_names.add("dish");
			column_names.add("notes");
			String[] columnnames = column_names.toArray(new String[column_names.size()]);
			view.setColumnNamesForm(columnnames);
			view.setColumnNamesFormTable(columnnames);
			view.setColumnNamesTable(columnnames);
		} else {
			ArrayList<String> column_names = new ArrayList<>();
			if (m_register_individuals)
				column_names.add("people_id");
			else {
				column_names.add("families_id");
				for (String age : m_ages)
					column_names.add("num_" + age + "s");
			}
			column_names.add("notes");
			view.setColumnNamesFormTable(column_names.toArray(new String[column_names.size()]));
			String meal_work_ratio = request.site.getSettings().getString("meal work ratio");
			if (meal_work_ratio != null)
				column_names.add("adult eating/work ratio");
			view.setColumnNamesForm(column_names.toArray(new String[column_names.size()]));
		}
		return view;
	}
}
