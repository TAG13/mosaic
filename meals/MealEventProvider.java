package meals;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;

import accounting.Accounting;
import app.Array;
import app.Block;
import app.Person;
import app.Request;
import app.Roles;
import app.Site;
import app.Text;
import calendar.Event;
import calendar.EventProvider;
import calendar.MonthView;
import db.DBConnection;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import db.section.Tabs;
import households.Household;
import mosaic.MosaicEventProvider;
import ui.Table;
import web.HTMLWriter;
import web.JS;
import web.Stringify;

@Stringify
public class MealEventProvider extends MosaicEventProvider {
	@JSONField(label="age groups")
	String[]		m_ages = new String[] { "adult", "kid", "under_five" };
	@JSONField
	boolean			m_automatic_signup;
	@JSONField(label="age group budgets")
	float[]			m_budgets = new float[] { 4, 2, 0 };
	@JSONField
	String			m_cook_label = "cook";
	@JSONField(type="expense account")
	private int		m_credit_account_id;
	@JSONField(label="age group names")
	String[]		m_display_names = new String[] { "adults (13+)", "kids (5-12)", "under_fives" };
	@JSONField
	private boolean	m_event_text_show_jobs = true;
	@JSONField(label="age group extras")
	float[]			m_extras = new float[] { (float)0.4, (float)0.2, 0 };
	@JSONField
	String[]		m_ignore_ages_for_max_count;
	@JSONField(fields={"m_invoice_account_id"})
	private boolean	m_invoice_nightly;
	@JSONField(type="income account")
	int				m_invoice_account_id;
	@JSONField
	String[]		m_jobs = new String[] { "additional_cooks", "assist", "setup", "clean1", "clean2" };
	@JSONField
	String			m_max_people_label = "max people";
	@JSONField
	String[]		m_option_columns;
	@JSONField
	String[]		m_other_columns;
	@JSONField
	String			m_print_text;
	@JSONField(type="mail list")
	String			m_send_announcement_to;
	@JSONField
	boolean			m_show_balance_message;
	@JSONField
	boolean			m_signup_individuals;
	@JSONField
	boolean			m_use_title_field;
	@JSONField
	boolean			m_workers_are_families;
	Select			m_workers_query;

	//--------------------------------------------------------------------------

	public
	MealEventProvider(boolean workers_are_households) {
		super("Common Meals");
		m_workers_are_families = workers_are_households;
		m_workers_query = workers_are_households ? new Select("id,name").from("families").where("active AND cm_account_active").orderBy("name") : new Select("id,first,last").from("people").where("active").orderBy("first,last");
		m_display_item_labels = true;
		m_display_items = new String[] { "menu", "cook", "jobs", "open/closed" };
		m_events_table = "meal_events";
		m_events_have_event = false;
	}

	//--------------------------------------------------------------------------

	public void
	addCredit(int household_id, LocalDate date, double amount, int meal_event_id, Site site, DBConnection db) {
		if (amount == 0.0)
			return;
		if (m_credit_account_id == 0)
			throw new RuntimeException("credit account not set");
		Accounting.addCredit(m_credit_account_id, household_id, date, "Common Meal", amount, meal_event_id, site, db);
	}

	//--------------------------------------------------------------------------

	protected void
	addInvoice(int household_id, LocalDate date, double amount, int meal_event_id, Site site, DBConnection db) {
		if (amount == 0.0)
			return;
		if (m_invoice_account_id == 0)
			throw new RuntimeException("invoice account not set");
		Accounting.addInvoice(m_invoice_account_id, household_id, date, "Common Meal", amount, meal_event_id, site, db);
	}

	//--------------------------------------------------------------------------

	public void
	addInvoices(int meal_event_id, DBConnection db) {
		float[] prices = new float[m_ages.length];
		Select query = new Select("id,date");
		for (String age : m_ages)
			query.addColumn(age + "_price");
		query.from("meal_events");
		if (meal_event_id == 0)
			query.where("NOT invoiced AND date<CURRENT_DATE-1");
		else
			query.whereIdEquals(meal_event_id);
		Rows meal_events = new Rows(query, db);
		query = new Select("families_id");
		for (String age : m_ages)
			query.addColumn("num_" + age + 's');
		query.from("meal_people");
		while (meal_events.next()) {
			for (int i=0,n=prices.length; i<n; i++)
				prices[i] = meal_events.getFloat(i + 3);
			int meal_events_id = meal_events.getInt(1);
			Rows meal_people = new Rows(query.where("meal_events_id=" + meal_events_id), db);
			while (meal_people.next()) {
				double amount = 0;
				for (int i=0,n=prices.length; i<n; i++)
					amount += prices[i] * meal_people.getInt(i + 2);
				addInvoice(meal_people.getInt(1), meal_events.getDate(2), amount, meal_events_id, m_site, db);
			}
			meal_people.close();
			db.update("meal_events", "invoiced=TRUE", meal_events_id);
		}
		meal_events.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_events_table)
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("closed", Types.BOOLEAN))
			.add(new JDBCColumn("max_people", Types.INTEGER))
			.add(new JDBCColumn("menu", Types.VARCHAR))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("start_time", Types.TIME))
			.add(new JDBCColumn("end_time", Types.TIME))
			.add(new JDBCColumn("invoiced", Types.BOOLEAN));
		if (m_use_title_field)
			table_def.add(new JDBCColumn("title", Types.VARCHAR));
		for (String age : m_ages) {
			table_def.add(new JDBCColumn(age + "_budget_amount", Types.REAL));
			table_def.add(new JDBCColumn(age + "_price", Types.REAL));
		}
		if (m_jobs != null)
			for (String job : m_jobs)
				table_def.add(new JDBCColumn(job, Types.INTEGER));
		if (m_option_columns != null)
			for (String option_column : m_option_columns)
				table_def.add(new JDBCColumn(option_column, Types.BOOLEAN));
		if (m_other_columns != null)
			for (String other_column : m_other_columns)
				table_def.add(new JDBCColumn(other_column, Types.REAL));
		adjustLocation(table_def, db);
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex(m_events_table, "date");
		adjustReminders(db);
		table_def = new JDBCTable("meal_people")
			.add(new JDBCColumn("meal_events"))
			.add(new JDBCColumn("dish", Types.VARCHAR))
			.add(new JDBCColumn(m_signup_individuals ? "people" : "families"))
			.add(new JDBCColumn("notes", Types.VARCHAR));
		if (!m_signup_individuals)
			for (String age : m_ages)
				table_def.add(new JDBCColumn("num_" + age + "s", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	public void
	deleteInvoicesAndCredits(int meal_event_id, DBConnection db) {
		db.delete("transactions", "type='Invoice' AND accounts_id=" + m_invoice_account_id + " AND transactions_id=" + meal_event_id, false);
		db.delete("transactions", "type='Credit' AND accounts_id=" + m_credit_account_id + " AND transactions_id=" + meal_event_id, false);
		db.update("meal_events", "invoiced=false", meal_event_id);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request request) {
		String segment_one = request.getPathSegment(1);
		if (segment_one != null)
			switch (segment_one) {
			case "worker emails":
				String ids = request.getParameter("ids");
				if (ids.length() == 0)
					return true;
				int comma = ids.indexOf(',');
				String owner = comma == -1 ? ids : ids.substring(0, comma);
				request.writer.write(request.db.lookupString(new Select("email").from("people").whereIdEquals(Integer.parseInt(owner))));
				if (comma != -1) {
					ids = ids.substring(comma + 1);
					Rows emails = new Rows(new Select("email").from(m_workers_are_families ? "families JOIN people ON (people.id=families.contact)" : "people").where((m_workers_are_families ? "families" : "people") + ".id IN(" + ids + ")").andWhere("email IS NOT NULL"), request.db);
					while (emails.next())
						request.writer.comma().write(Text.escapeDoubleQuotes(emails.getString(1)));
					emails.close();
				}
				return true;
			}
		return super.doGet(request);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request request) {
		if (super.doPost(request))
			return true;
		switch(request.getPathSegment(2)) {
		case "invoices":
			int meal_event_id = request.getPathSegmentInt(1);
			switch(request.getPathSegment(3)) {
			case "add":
				addInvoices(meal_event_id, request.db);
				return true;
			case "delete":
				deleteInvoicesAndCredits(meal_event_id, request.db);
				return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public String[]
	getAges() {
		return m_ages;
	}

	//--------------------------------------------------------------------------

	String[]
	getDinerEmails(int meal_events_id, DBConnection db) {
		if (m_signup_individuals) {
			ArrayList<String> emails = db.readValues(new Select("email").from("meal_people JOIN people ON (people.id=meal_people.people_id)").whereEquals("meal_events_id", meal_events_id).andWhere("email IS NOT NULL"));
			return emails.toArray(new String[emails.size()]);
		}
		ArrayList<String> emails = db.readValues(new Select("email").from("meal_people JOIN families ON (families.id=meal_people.families_id) JOIN people ON (people.id=families.contact)").whereEquals("meal_events_id", meal_events_id).andWhere("email IS NOT NULL"));
		return emails.toArray(new String[emails.size()]);
	}

	//--------------------------------------------------------------------------

	String[]
	getWorkerEmails(View view, DBConnection db) {
		ArrayList<Integer> job_ids = new ArrayList<>();
		for (String job : m_jobs) {
			int job_id = view.data.getInt(job);
			if (job_id != 0)
				job_ids.add(job_id);
		}
		if (job_ids.size() == 0)
			return new String[] { db.lookupString(new Select("email").from("people").whereIdEquals(view.data.getInt("_owner_"))) };
		ArrayList<String> emails = new ArrayList<>();
		if (m_workers_are_families) {
			emails = db.readValues(new Select("email").from("people JOIN families ON (families.contact=people.id)").where("families.id IN (" + Text.join(",",  job_ids) + ")").andWhere("email IS NOT NULL"));
			emails.add(db.lookupString(new Select("email").from("people").whereIdEquals(view.data.getInt("_owner_"))));
		} else {
			job_ids.add(view.data.getInt("_owner_"));
			emails = db.readValues(new Select("email").from("people").where("id IN (" + Text.join(",",  job_ids) + ")").andWhere("email IS NOT NULL"));
		}
		return emails.toArray(new String[emails.size()]);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getEventCrumb() {
		return "meal";
	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request request) {
		Person user = request.getUser();
		if (user == null)
			return null;
		return "Calendar.$().edit(" + JS.string(m_name) + "," + JS.string(getDisplayName(request)) + "," + event.getID() + ",'" + date.toString() + "'," + (((MealEvent)event).isClosed(request) ? "true" : "false") + ")";
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		if ("m_reminder_subject_items".equals(field_name))
			return getFieldOptions(new String[] { "cook", "date", "menu", "location" });
		if (m_use_title_field)
			return getFieldOptions(new String[] { "cook", "jobs", "location", "menu", "open/closed", "time", "title" });
		return getFieldOptions(new String[] { "cook", "jobs", "location", "menu", "open/closed", "time" });
	}

	//--------------------------------------------------------------------------

	public String[]
	getJobs() {
		return m_jobs;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		super.init(db);
		Roles.add("cm account manager");
		((Accounting)m_site.getModule("Accounting")).addBlock(new Block(m_name) {
			@Override
			public boolean
			handlesContext(String context, Request request) {
				if (context.equals("accounting module"))
					return isActive() && m_invoice_account_id != 0;
				return isActive();
			}
			@Override
			public void
			write(String context, Request request) {
				switch(context) {
				case "accounting module":
					writeAccountingPane(request);
					return;
				case "accounting report":
					request.site.newView("meal_events list", request).writeComponent();
					return;
				}
			}
		});
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isValidEmail(String email) {
		if ("cook".equals(email) || "diners".equals(email) || "workers".equals(email))
			return true;
		for (String job : m_jobs)
			if (job.replace('_', ' ').equals(email))
				return true;
		return super.isValidEmail(email);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new MealEvent(this, rows);
	}

	//--------------------------------------------------------------------------

	boolean
	mealHasInvoices(int meal_event_id, DBConnection db) {
		return db.rowExists("transactions", "type='Invoice' AND accounts_id=" + m_invoice_account_id + " AND transactions_id=" + meal_event_id);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name, Site site) {
		if (name.equals(m_name))
			return new MealViewDef(name, this, site);
		if (name.equals("meal_events list")) {
			ViewDef view_def = new MealViewDef(name, this, site)
				.addFeature(new MonthFilter("meal_events", Location.TOP_LEFT))
				.addFeature(new YearFilter("meal_events", Location.TOP_LEFT))
				.setDefaultOrderBy("date")
				.setFrom("meal_events");
			for (String age : m_ages)
				view_def.setColumn(((Column)view_def.cloneColumn("num_" + age + "s")).setTotal(true));
			view_def.setColumn(((CurrencyColumn)view_def.cloneColumn("total")).setTotal(true))
				.setColumn(((CurrencyColumn)view_def.cloneColumn("total budget")).setTotal(true));
			return view_def;
		}
		if (name.equals("meal_people"))
			return new MealPeopleViewDef(m_signup_individuals, m_show_balance_message, m_ages, m_display_names, m_ignore_ages_for_max_count, site);
		if (name.equals("meal history")) {
			ViewDef view_def = new ViewDef(name)
				.addSection(new Tabs("date", new OrderBy("date", false)).setFormatYear(true))
				.setAccessPolicy(new AccessPolicy())
				.setFrom("meal_events LEFT JOIN meal_people ON meal_people.meal_events_id=meal_events.id")
				.setRecordName("Meal");
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("date");
			column_names.add("menu");
			column_names.add("jobs");
			for (String age : m_ages)
				column_names.add("num_" + age + "s");
			column_names.add("cost");
			view_def.setColumnNamesTable(column_names)
				.setColumn(new CurrencyColumn("cost"){
					@Override
					public double
					getDouble(View view, Request request) {
						double cost = 0;
						for (String age : m_ages)
							cost += view.data.getDouble(age + "_price") * view.data.getDouble("num_" + age + "s");
						return cost;
					}
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						request.writer.writeCurrency(getDouble(view, request));
						return true;
					}
				}.setCellStyle("text-align:right").setTotal(true))
				.setColumn(new Column("jobs"){
					@Override
					public double
					getDouble(View view, Request request) {
						double num = 0;
						boolean workers_are_households = m_workers_query.getFirstTable().equals("families");
						Household household = ((mosaic.Person)request.getUser()).getHousehold(request);
						String[] household_members = household.getPeopleIds().split(",");
						if (Array.indexOf(household_members, view.data.getString("_owner_")) != -1)
							num++;
						for (String job : m_jobs)
							if (workers_are_households) {
								if (household.getId() == view.data.getInt(job))
									num++;
							} else
								if (Array.indexOf(household_members, view.data.getString(job)) != -1)
									num++;
						return num;
					}
					@Override
					public boolean
					writeValue(View view, Map<String,Object> data, Request request) {
						StringBuilder jobs = new StringBuilder();
						Household household = ((mosaic.Person)request.getUser()).getHousehold(request);
						String[] household_members = household.getPeopleIds().split(",");
						if (Array.indexOf(household_members, view.data.getString("_owner_")) != -1)
							jobs.append("cook");
						for (String job : m_jobs)
							if (m_workers_are_families) {
								if (household.getId() == view.data.getInt(job)) {
									if (jobs.length() > 0)
										jobs.append(", ");
									jobs.append(job.replaceAll("_", " "));
								}
							} else
								if (Array.indexOf(household_members, view.data.getString(job)) != -1) {
									if (jobs.length() > 0)
										jobs.append(", ");
									jobs.append(job.replaceAll("_", " "));
								}
						if (jobs.length() > 0) {
							request.writer.write(jobs.toString());
							return true;
						}
						return false;
					}
				}.setTotal(true).setTotalIsInt(true));
			return view_def;
		}
		if (name.equals("report invoices"))
			return new ViewDef(name)
				.addFeature(new MonthFilter("transactions", Location.TOP_LEFT))
				.addFeature(new YearFilter("transactions", Location.TOP_LEFT))
				.addSection(new Dividers("date", new OrderBy("date")).setCollapseRows(true))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("families_id")
				.setFrom("transactions")
				.setRecordName("Invoice")
				.setColumnNamesTable(new String[] { "families_id", "amount" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(new LookupColumn("families_id", "families", "name", "active", "name").setDisplayName("household"));
		return super._newViewDef(name, site);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	nightly(LocalDateTime now, Site site, DBConnection db) {
		if (m_invoice_nightly && m_invoice_account_id != 0)
			addInvoices(0, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request request) {
		String[] option_columns = m_option_columns;
		super.updateSettings(request);
		if (option_columns != null)
			for (String option_column : option_columns)
				if (Array.indexOf(m_option_columns, option_column) == -1)
					request.db.dropColumn("meal_events", option_column);
	}

	// --------------------------------------------------------------------------

	void
	writeAccountingPane(Request request) {
		HTMLWriter writer = request.writer;
		writer.h3(getName());
		Table table = writer.ui.table().addStyle("padding", "20px");
		table.tr();
		writer.setAttribute("id", "invoices").setAttribute("style", "min-width:250px;vertical-align:top;");
		table.td();
		ViewState.setBaseFilter("report invoices", "type='Invoice' AND accounts_id=" + m_invoice_account_id, request);
		request.site.newView("report invoices", request).writeComponent();
//		writer.setAttribute("id", "events").setAttribute("style", "min-width:250px;vertical-align:top;");
//		table.td();
//		if (m_view_def == null)
//			m_view_def = newPrivateViewDef("meal_events", request.site)
//				.addFeature(new MonthFilter("transactions", Location.TOP_LEFT))
//				.addFeature(new YearFilter("transactions", Location.TOP_LEFT))
//				.setAllowQuickEdit(true)
//				.setDefaultOrderBy("date")
//				.setRecordName("Event")
//				.setColumnNamesTable(new String[] { "date", "_owner_", "invoiced" })
//				.setColumn(new LookupColumn("_owner_", "people", "first,last"));
//		m_view_def.newView(request).writeComponent();
		writer.setAttribute("style", "vertical-align: top;");
		table.td();
		new MonthView((EventProvider)request.site.getModule("Common Meals"), request).writeComponent(request);
		table.close();
	}

	//--------------------------------------------------------------------------

	public void
	writeMealHistory(Request request) {
		mosaic.Person user = (mosaic.Person)request.getUser();
		int household_id = user.getHousehold(request).getId();
		if (household_id == 0)
			return;

		StringBuilder where = new StringBuilder();
		if (m_signup_individuals) {
			where.append("(people_id=");
			where.append(user.getId());
			where.append(" OR people_id IS NULL)");
		} else {
			where.append("(families_id=");
			where.append(household_id);
			where.append(" OR families_id IS NULL)");
		}
		where.append(" AND (_owner_ IN(");
		String people_ids = user.getHousehold(request).getPeopleIds();
		where.append(people_ids);
		where.append(')');
		boolean workers_are_households = m_workers_query.getFirstTable().equals("families");
		for (String job : m_jobs) {
			where.append(" OR ");
			where.append(job);
			if (workers_are_households) {
				where.append('=');
				where.append(household_id);
			} else {
				where.append(" IN(");
				where.append(people_ids);
				where.append(')');
			}
		}
		if (m_signup_individuals) {
			where.append(" OR people_id=");
			where.append(user.getId());
		} else {
			where.append(" OR families_id=");
			where.append(household_id);
		}
		where.append(')');
		request.site.newView("meal history", request)
			.setWhere(where.toString())
			.writeComponent();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, Request request) {
		super.writeSettingsForm(new String[] { "m_active", "m_ages", "m_budgets", "m_color", "m_display_item_labels", "m_display_items", "m_display_name", "m_display_names", "m_events_have_location", "m_events_have_start_time", "m_event_text_show_jobs", "m_extras", "m_ical_items", "m_invoice_nightly", "m_jobs", "m_option_columns", "m_print_text", "m_reminder_subject_items", "m_role", "m_send_announcement_to", "m_show_on_menu", "m_show_on_upcoming", "m_start_date_label", "m_support_reminders", "m_tooltip_item_labels", "m_tooltip_items","m_use_title_field", "m_uuid" }, in_dialog, request);
	}
}
