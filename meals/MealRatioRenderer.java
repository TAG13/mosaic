package meals;

import java.text.NumberFormat;

import app.Request;
import db.DBConnection;
import db.Form;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import mosaic.Person;

public class MealRatioRenderer implements ColumnInputRenderer {
	public static void
	writeRatio(int household_id, String[] jobs, boolean show_math, Request request) {
		DBConnection db = request.db;
		int num_adults = db.lookupInt("SUM(num_adults)", "meal_people", "families_id=" + household_id, 0);
		int times_worked = db.lookupInt("COUNT(*)", "meal_events JOIN people ON people.id=meal_events._owner_", "families_id=" + household_id, 0);
		for (String job : jobs)
			times_worked += db.lookupInt("COUNT(*)", "meal_events", job + "=" + household_id, 0);
		float ratio = (float)num_adults / times_worked;
		if (Float.isInfinite(ratio)) {
			request.writer.write("# adult meals: ");
			request.writer.write(num_adults);
			request.writer.write(", times worked: 0");
		} else if (Float.isNaN(ratio))
			request.writer.write("never worked or eaten");
		else {
			if (show_math) {
	//			request.writer.write("(# adult meals: ");
				request.writer.write(num_adults);
				request.writer.write(" / ");
	//			request.writer.write(") / (# times household worked: ");
				request.writer.write(times_worked);
	//			request.writer.write(") = ");
				request.writer.write(" = ");
			}
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			request.writer.write(nf.format(ratio));
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeInput(View view, Form form, ColumnBase<?> column, Request request) {
		writeRatio(((Person)request.getUser()).getHousehold(request).getId(), ((MealEventProvider)request.site.getModule("Common Meals")).getJobs(), false, request);
		return true;
	}
}
