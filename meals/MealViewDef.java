package meals;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.Array;
import app.Mail;
import app.Person;
import app.Request;
import app.Site;
import app.Text;
import app.Time;
import calendar.EventViewDef;
import db.LinkValueRenderer;
import db.NameValuePairs;
import db.OneToMany;
import db.Rows;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.column.Column;
import db.column.ColumnInputRenderer;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.column.MultiColumnRenderer;
import db.jdbc.JDBCColumn;
import web.HTMLWriter;
import web.Stringify;

@Stringify
public class MealViewDef extends EventViewDef {
	public
	MealViewDef(String name, MealEventProvider event_provider, Site site) {
		super(event_provider);
		setAccessPolicy(new MealAccessPolicy());
		setDefaultOrderBy("date DESC");
		setFormHeadText("common meals calendar");
		setFrom("meal_events");
		setRecordName("Meal");
		setShowPrintLinkForm(true);

		// form column names
		ArrayList<String> column_names = new ArrayList<>();
		column_names.add("date");
		event_provider.setLocationColumn(this, column_names);
		column_names.add("start_time");
		if (event_provider.m_use_title_field)
			column_names.add("title");
		column_names.add("menu");
		column_names.add("notes");
		if (event_provider.m_option_columns != null)
			for (String option_column : event_provider.m_option_columns)
				column_names.add(option_column);
		for (float budget : event_provider.m_budgets)
			if (budget != 0) {
				column_names.add("price");
				break;
			}
		for (float extra : event_provider.m_extras)
			if (extra != 0) {
				column_names.add("budget");
				break;
			}
		column_names.add("_owner_");
		for (String job : event_provider.m_jobs)
			column_names.add(job);
		column_names.add("max_people");
		column_names.add("closed");
		if (!event_provider.m_signup_individuals)
			column_names.add("total people");
		column_names.add("total");
		for (float extra : event_provider.m_extras)
			if (extra != 0) {
				column_names.add("total budget");
				break;
			}
		if (event_provider.m_other_columns != null)
			for (String other_column : event_provider.m_other_columns)
				column_names.add(other_column);
		setColumnNamesForm(column_names);

		column_names.clear();
		column_names.add("date");
		column_names.add("start_time");
		column_names.add("menu");
		column_names.add("_owner_");
		for (String job : event_provider.m_jobs)
			column_names.add(job);
		for (String age : event_provider.m_ages)
			column_names.add("num_" + age + "s");
		column_names.add("total");
		for (float extra : event_provider.m_extras)
			if (extra != 0) {
				column_names.add("total budget");
				break;
			}
		setColumnNamesTable(column_names);

		setColumn(new Column("action").setValueRenderer(new LinkValueRenderer().setNamesAndValuesColumns(new String[] { "link", "id" }, new String[] { "action", "id" }).setText("add to accounts"), false));
		String worker_columns = event_provider.m_workers_query.getColumns().substring(3); // assume columns start with "id,"
		ColumnInputRenderer worker_input_renderer = new SelectRenderer(event_provider.m_workers_query, worker_columns, "id").setAllowNoSelection(true).setOnChange("update_workers()");
		for (String job : event_provider.m_jobs)
			setColumn(new Column(job).setInputAndValueRenderers(worker_input_renderer, false));
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		for (int i=0,n=event_provider.m_ages.length; i<n; i++) {
			setColumn(new CurrencyColumn(event_provider.m_ages[i] + "_price").setDefaultValue(nf.format(event_provider.m_budgets[i] + event_provider.m_extras[i])).setDisplayName(event_provider.m_display_names[i]));
			setColumn(new CurrencyColumn(event_provider.m_ages[i] + "_budget_amount").setDefaultValue(nf.format(event_provider.m_budgets[i])).setDisplayName(event_provider.m_display_names[i]));
			setColumn(new Column("num_" + event_provider.m_ages[i] + "s"){
				@Override
				public double getDouble(View view, Request request) {
					return request.db.lookupInt("SUM(" + m_name + ")", "meal_people", "meal_events_id=" + view.data.getString("id"), 0);
				}
				@Override
				public boolean
				writeValue(View view, Map<String,Object> data, Request request) {
					request.writer.write((int)getDouble(view, request));
					return true;
				}
			}.setDisplayName("num " + event_provider.m_display_names[i]).setIsReadOnly(true).setTotalIsInt(true));
		}
		setColumn(new Column("budget").setDisplayName("cook's budget").setInputAndValueRenderers(new MealBudgetRenderer(event_provider.m_ages, event_provider.m_extras), false));
		setColumn(new Column("date").setDisplayName(event_provider.getStartDateLabel()).setIsRequired(true));
		setColumn(new Column("max_people").setDisplayName(event_provider.m_max_people_label));
		setColumn(new LookupColumn("_owner_", "people", "first,last").setAllowNoSelection(true).setFilter(site.getPeopleFilter()).setDisplayName(event_provider.m_cook_label).setDefaultToUserId());
		String[] prices = new String[event_provider.m_ages.length];
		for (int i=0,n=event_provider.m_ages.length; i<n; i++)
			prices[i] = event_provider.m_ages[i] + "_price";
		setColumn(new Column("price").setInputAndValueRenderers(new MultiColumnRenderer(prices, true, true), false));
		setColumn(new Column("start_time").setDefaultValue(event_provider.getDefaultStartTime()));
		setColumn(new CurrencyColumn("total"){
			@Override
			public double
			getDouble(View view, Request request) {
				float total = 0;
				if (event_provider.m_signup_individuals) {
					List<String> ids = request.db.readValues(new Select("people_id").from("meal_people").where("meal_events_id=" + view.data.getString("id")));
					total = request.db.lookupFloat(new Select("SUM(meal_price)").from("people").where("id IN(" + Text.join(",", ids) + ")"), 0);
				} else
					for (String age : event_provider.m_ages) {
						int num = request.db.lookupInt("SUM(num_" + age + "s)", "meal_people", "meal_events_id=" + view.data.getString("id"), 0);
						total += view.data.getFloat(age + "_price") * num;
					}
				return total;
			}
			@Override
			public boolean
			writeValue(View view, Map<String,Object> data, Request request) {
				View.Mode mode = view.getMode();
				if (mode != View.Mode.ADD_FORM)
					request.writer.writeCurrency(getDouble(view, request));
				return true;
			}
		}.setCellStyle("text-align:right").setIsReadOnly(true));
		setColumn(new CurrencyColumn("total budget"){
			@Override
			public double getDouble(View view, Request request) {
				float total = 0;
				for (String age : event_provider.m_ages) {
					int num = request.db.lookupInt("SUM(num_" + age + "s)", "meal_people", "meal_events_id=" + view.data.getString("id"), 0);
					total += view.data.getFloat(age + "_budget_amount") * num;
				}
				return total;
			}
			@Override
			public boolean
			writeValue(View view, Map<String,Object> data, Request request) {
				View.Mode mode = view.getMode();
				if (mode != View.Mode.ADD_FORM)
					request.writer.writeCurrency(getDouble(view, request));
				return true;
			}
		}.setCellStyle("text-align:right").setDisplayName("total cook's budget").setIsReadOnly(true));
		if (!event_provider.m_signup_individuals)
			setColumn(new Column("total people").setIsReadOnly(true).setValueRenderer(new TotalsRenderer(event_provider.m_ages, event_provider.m_display_names), false));
		if (event_provider.m_other_columns != null)
			for (String other_column : event_provider.m_other_columns)
				setColumn(new CurrencyColumn(other_column));
		addRelationshipDef(new OneToMany("meal_people").setSpanFormCols(false));
		if (event_provider.supportsReminders())
			addRelationshipDef(new OneToMany(m_name + "_reminders").setSpanFormCols(false));

	}

	//--------------------------------------------------------------------------
	// FormHook

	@Override
	public void
	afterForm(View view, View.Mode mode, boolean printer_friendly, Request request) {
		MealEventProvider meal_event_provider = (MealEventProvider)m_event_provider;
		HTMLWriter writer = request.writer;

		if (printer_friendly && meal_event_provider.m_print_text != null) {
			writer.write("<tr><td><div style=\"width:500px;\"><ol><li style=\"padding-bottom:20px;\">Final totals:");
			for (String display_name : meal_event_provider.m_display_names)
				writer.write("  ______ ").write(display_name);
			writer.write("</li>");
			writer.write(meal_event_provider.m_print_text);
			writer.write("</ol></div></td></tr>");
		} else if (mode == View.Mode.EDIT_FORM || mode == View.Mode.READ_ONLY_FORM) {
			int meal_events_id = view.data.getInt("id");
			writer.write("<tr><td>");
			writer.ui.aButton("email diners", "mailto:" + Text.join(",", meal_event_provider.getDinerEmails(meal_events_id, request.db))).space();
			int owner_id = view.data.getInt("_owner_");
			Person user = request.getUser();
			boolean is_cook = user != null && owner_id == user.getId();
			boolean closed = !is_cook && !request.userHasRole("cm account manager") && !request.userIsAdmin() && (view.data.getBoolean("closed") || view.data.getDate("date").isBefore(Time.newDate().minusDays(1)));
			if (!closed) {
				writer.scriptOpen();
				writer.write("function update_workers(){" +
					"var f=dom.$('Meal');" +
					"var workers=[");
				for (int i=0; i<meal_event_provider.m_jobs.length; i++) {
					if (i > 0)
						writer.comma();
					writer.jsString(meal_event_provider.m_jobs[i]);
				}
				writer.write("];" +
					"var ids='").write(owner_id).write("';" +
					"for (var i=0; i<workers.length; i++){" +
						"var s=f[workers[i]];" +
						"if (s) {" +
							"var id=s.options[s.selectedIndex].value;" +
							"if(id)" +
								"ids += ','+id;" +
						"}" +
					"}" +
					"net.get(context+'/" + meal_event_provider.getName() + "/worker emails?meal_events_id=" + meal_events_id + "&ids=' + ids, function(text){dom.$('email_workers').href='mailto:'+text});" +
				"}\n" +
				"update_workers();");
				writer.scriptClose();
				writer.setAttribute("id", "email_workers").ui.aButton("email workers", "#");
			} else
				writer.ui.aButton("email workers", "mailto:" + Text.join(",", meal_event_provider.getWorkerEmails(view, request.db))).space();
			writer.write("</td></tr>");

			if (closed)
				request.writer.js("document.querySelector('.modal-footer').style.display='none';");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs name_value_pairs, Request request) {
		super.afterInsert(id, name_value_pairs, request);

		MealEventProvider meal_event_provider = (MealEventProvider)m_event_provider;
		if (meal_event_provider.m_automatic_signup) {
			JDBCColumn automatic_meal_signup = request.db.getTable("families").getColumn("automatic_meal_signup");
			Rows households = new Rows(new Select("id").distinct().from("families").where(automatic_meal_signup == null ? "active" : "active AND automatic_meal_signup"), request.db);
			 while (households.next()) {
				int[] counts = new int[meal_event_provider.m_ages.length];
				int household_id = households.getInt(1);
				Rows people = new Rows(new Select("meal_age").from("people").whereEquals("families_id", household_id), request.db);
				while (people.next()) {
					String age = people.getString(1);
					if (age != null && age.length() > 0)
						counts[Array.indexOf(meal_event_provider.m_ages, age)]++;
				}
				people.close();
				int num = 0;
				NameValuePairs nvp = new NameValuePairs();
				for (int i=0; i<counts.length; i++)
					if (counts[i] > 0) {
						nvp.set("num_" + meal_event_provider.m_ages[i] + "s", counts[i]);
						++num;
					}
				if (num > 0) {
					nvp.set("meal_events_id", id);
					nvp.set("families_id", household_id);
					request.db.insert("meal_people", nvp);
				}
			}
			households.close();
		}

		if (((MealEventProvider)m_event_provider).m_send_announcement_to != null)
			sendAnnouncement(id, request);
	}

	//--------------------------------------------------------------------------

	@Override
	public View
	newView(Request request) {
		View view = new View(this, request);
		View.Mode mode = request.getEnum("db_mode", View.Mode.class);

		if (request.getParameter("db_relationship") == null)
			if (mode == View.Mode.ADD_FORM) {
				request.setSessionAttribute("iscook", true);
				request.setSessionAttribute("ispotluck", false);
			} else if (mode == View.Mode.EDIT_FORM || mode == View.Mode.READ_ONLY_FORM) {
				MealEventProvider meal_event_provider = (MealEventProvider)m_event_provider;
				int meal_id = view.getID();
				if (meal_id == 0)
					return null;
				if (!view.selectByID(meal_id)) {
					request.writer.write("The meal you are trying to view was not found. It may have been deleted.");
					return null;
				}
				request.setSessionAttribute("ispotluck", MealEvent.isPotluck(view.data.getString("menu")));
				int owner_id = view.data.getInt("_owner_");
				Person user = request.getUser();
				boolean is_cook = user != null && owner_id == user.getId();
				request.setSessionAttribute("iscook", is_cook);
				if (!getAccessPolicy().showEditButtonForRow(view, request)) {
					view.setColumn(new Column("budget").setIsHidden(true));
					view.setColumn(new Column("date").setIsReadOnly(true));
					view.setColumn(new Column("menu").setIsReadOnly(true));
					view.setColumn(new Column("notes").setIsReadOnly(true));
					view.setColumn(new Column("start_time").setIsReadOnly(true));
					view.setColumn(new Column("title").setIsReadOnly(true));
					view.setColumn(new Column("total").setIsHidden(true));
					view.setColumn(new Column("total budget").setIsHidden(true));
					for (int i=0; i<meal_event_provider.m_ages.length; i++)
						view.setColumn(new CurrencyColumn(meal_event_provider.m_ages[i] + "_price").setDisplayName(meal_event_provider.m_display_names[i]).setIsReadOnly(true));
					if (owner_id != 0)
						view.setColumn(new LookupColumn("_owner_", "people", "first,last").setIsReadOnly(true).setDisplayName(meal_event_provider.m_cook_label));
					view.setColumn(new Column("max_people").setIsReadOnly(true));
					view.setColumn(new Column("closed").setIsReadOnly(true));
					if (meal_event_provider.m_option_columns != null)
						for (String option_column : meal_event_provider.m_option_columns)
							view.setColumn(new Column(option_column).setIsReadOnly(true));
					if (meal_event_provider.m_other_columns != null)
						for (String other_column : meal_event_provider.m_other_columns)
							view.setColumn(new Column(other_column).setIsHidden(true));
				}
				boolean closed = !is_cook && !request.userHasRole("cm account manager") && !request.userIsAdmin() && (view.data.getBoolean("closed") || view.data.getDate("date").isBefore(Time.newDate().minusDays(1)));
				if (closed) {
					String worker_columns = meal_event_provider.m_workers_query.getColumns().substring(3); // assume columns start with "id,"
					ColumnInputRenderer worker_input_renderer = new SelectRenderer(meal_event_provider.m_workers_query, worker_columns, "id").setAllowNoSelection(true).setOnChange("update_workers()");
					for (String job : meal_event_provider.m_jobs)
						view.setColumn(new Column(job).setInputAndValueRenderers(worker_input_renderer, false).setIsReadOnly(true));
				}
			}
		return view;
	}

	// --------------------------------------------------------------------------

	private void
	sendAnnouncement(int id, Request request) {
		Object[] o = request.db.readRowObjects(new Select("date, start_time, menu, _owner_").whereIdEquals(id));
		StringBuilder message = new StringBuilder("<p>A meal has been added to the common meal calendar with the following details:</p><table border=\"0\" padding=\"2px\"><tr><th>date:</th><td>");
		message.append(Time.formatter.getWeekdayMonthDate((LocalDate)o[0]))
			.append("</td></tr><tr><th>time:</th><td>")
			.append((String)o[1]);
		String menu = (String)o[2];
		if (menu != null && menu.length() > 0)
			message.append("</td></tr><tr><th>menu:</th><td>").append(menu);
		message.append("</td></tr><tr><th>cook:</th><td>")
			.append(request.site.lookupName((Integer)o[3], request.db))
			.append("</td></tr></table><p>Click <a href=\"")
			.append(request.site.getURL("calendars.jsp?edit='Common Meals'," + id))
			.append("\">here</a> to sign up for the meal.</p>");

		String address = ((MealEventProvider)m_event_provider).m_send_announcement_to + "@" + request.site.getDomain();
		new Mail(request.site).send(address, address, "common meal announcement", message.toString(), true);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	validate(int id, Request request) {
		if (request.getParameter("date") == null && id != 0) // date is read-only
			return null;
		return super.validate(id, request);
	}
}
