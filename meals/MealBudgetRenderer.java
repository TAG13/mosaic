package meals;

import app.Request;
import db.Form;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import db.column.ColumnValueRenderer;
import web.HTMLWriter;

public class MealBudgetRenderer implements ColumnInputRenderer, ColumnValueRenderer {
	private String[]	m_ages;
	private float[]		m_extras;

	// --------------------------------------------------------------------------

	public
	MealBudgetRenderer(String[] ages, float[] extras) {
		m_ages = ages;
		m_extras = extras;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	writeInput(View view, Form form, ColumnBase<?> column, Request request) {
		HTMLWriter writer = request.writer;

		writer.write("<div style=\"vertical-align:baseline;\">");
		for (int i=0,n=m_ages.length; i<n; i++) {
			String column_name = m_ages[i] + "_budget_amount";
			ColumnBase<?> c = view.getColumn(column_name);

			if (i > 0)
				writer.space();
			c.writeLabel(request);
			writer.write(": ");
			if (c.isReadOnly(view.getMode(), request))
				view.writeColumnHTML(column_name);
			else
				form.writeColumnInput(column_name);
		}
		if (!view.isPrinterFriendly() && m_ages.length > 0 && !view.getColumn(m_ages[0] + "_budget_amount").isReadOnly(view.getMode(), request)) {
			writer.space();
			StringBuilder script = new StringBuilder();
			script.append("var f=this.closest('form');");
			for (int i=0,n=m_ages.length; i<n; i++) {
				script.append("var b=parseFloat(f.");
				script.append(m_ages[i]);
				script.append("_budget_amount.value);f.");
				script.append(m_ages[i]);
				script.append("_price.value=b?(parseFloat(f.");
				script.append(m_ages[i]);
				script.append("_budget_amount.value)+");
				script.append(m_extras[i]);
				script.append(").toFixed(2):0;");
			}
			writer.write("<span style=\"display:inline-block;width:5px;\"></span>");
			writer.ui.buttonOnClick("update price", script.toString());
		}
		writer.write("</div>");
		return true;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeValue(View view, ColumnBase<?> column, Request request) {
		HTMLWriter writer = request.writer;

		for (int i=0,n=m_ages.length; i<n; i++) {
			String column_name = m_ages[i] + "_budget_amount";
			ColumnBase<?> c = view.getColumn(column_name);

			if (i > 0)
				writer.space();
			c.writeLabel(request);
			writer.write(": ");
			view.writeColumnHTML(column_name);
		}
	}
}
