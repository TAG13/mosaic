package meals;

import java.sql.ResultSet;
import java.sql.SQLException;

import app.Person;
import app.Request;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import web.HTMLWriter;

public class TotalsRenderer implements ColumnValueRenderer {
	private String[]	m_ages;
	private String[]	m_display_names;

	//--------------------------------------------------------------------------

	public
	TotalsRenderer(String[] ages, String[] display_names) {
		m_ages = ages;
		m_display_names = display_names;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View view, ColumnBase<?> column, Request request) {
		int[] nums = new int[m_ages.length];
		HTMLWriter	writer = request.writer;

		if (m_ages.length == 0)
			return;
		try {
			StringBuilder sql = new StringBuilder("SELECT ");
			for (int i=0,n=m_ages.length; i<n; i++) {
				if (i > 0)
					sql.append(',');
				sql.append("SUM(num_");
				sql.append(m_ages[i]);
				sql.append("s)");
			}
			sql.append(" FROM meal_people WHERE meal_events_id=");
			sql.append(view.data.getString("id"));
			ResultSet people = request.db.select(sql.toString());
			people.next();
			for (int i=0,n=nums.length; i<n; i++)
				nums[i] = people.getInt(i + 1);
			people.getStatement().close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		int total = 0;
		for (int i=0,n=m_ages.length; i<n; i++) {
			if (i > 0)
				writer.write(" + ");
			total += nums[i];
			writer.write(nums[i]);
			writer.space();
			writer.write(m_display_names[i]);
		}
		writer.write(" = ");
		writer.write(total);
		writer.write(" people");
		Person user = request.getUser();
		if (!view.isPrinterFriendly() && user != null && view.data.getInt("_owner_") == user.getId()) {
			writer.space();
			writer.ui.buttonOnClick("recalc budget", "top.calc_totals(this.form)");
			writer.scriptOpen();
			writer.write("var ages=");
			writer.jsArray(m_ages);
			writer.write(";\nnums=");
			writer.jsArray(nums);
			writer.write(";\n");
			writer.jsFunction("calc_totals", "f", "var b=0;var t='';for(var i=0;i<ages.length;i++){var a=parseFloat(f.elements[ages[i]+'_budget_amount'].value);if(nums[i]>0){b+=a*nums[i];if(t)t+=' + ';t+=nums[i]+' '+ages[i]+'s x $'+a.toFixed(2);}}Dialog.alert('','budget is '+t+' = $'+b.toFixed(2));");
			writer.scriptClose();
		}
	}
}
