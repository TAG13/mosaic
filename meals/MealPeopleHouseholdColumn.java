package meals;

import app.Request;
import db.column.LookupColumn;
import households.Household;
import mosaic.Person;

public class MealPeopleHouseholdColumn extends LookupColumn {
	public
	MealPeopleHouseholdColumn() {
		super("families_id", "families", "name", "active", "name");
		setDisplayName("household");
	}

    //--------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(Request request) {
		Person user = (Person)request.getUser();
		if (user == null)
			return "";
		Household household = user.getHousehold(request);
		return household != null ? Integer.toString(household.getId()) : "";
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	isReadOnly(db.View.Mode mode, Request request) {
		return request.getUser() != null || !((Boolean)request.getSessionAttribute("iscook")).booleanValue() && !request.userIsAdministrator();
	}
}