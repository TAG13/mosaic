package meals;

import app.Person;
import app.Request;
import db.View;
import db.access.AccessPolicy;

public class MealAccessPolicy extends AccessPolicy {
	public
	MealAccessPolicy() {
		add();
		delete();
		edit();
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(View view, Request request) {
		Person user = request.getUser();
		int cook = view.data.getInt("_owner_");
		return user != null && (cook == 0 || cook == user.getId() || request.userHasRole("cm account manager"));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(View view, Request request) {
		Person user = request.getUser();
		int cook = view.data.getInt("_owner_");
		return user != null && (cook == 0 || cook == user.getId() || request.userHasRole("cm account manager"));
	}
}
