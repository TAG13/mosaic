import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.UUID;

public class ReceiveMail {
	private static String
	getDomain() {
		String domain = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader("url.txt"));
			domain = br.readLine();
			br.close();
		} catch (IOException e) {
			log(e.toString());
		}
		if ("demo.zenlunatics.org".equals(domain))
			domain = "demo.cohousing.site";
		return domain;
	}

	//--------------------------------------------------------------------------

	public static void
	main(String[] args) {
		try {
			String file_path = "inbox/" + args[0] + "/" + UUID.randomUUID().toString();
			File file = writeToFile(file_path);
			File file2 = new File(file_path + ".txt");
			Files.copy(file.toPath(), file2.toPath());
//			file.renameTo(file2);
//			File old_file = new File(file_path);
//			if (old_file.exists())
//				old_file.delete();
			file2.setReadable(true, false);
			pingSite(args[0]);
		} catch (IOException e) {
			log(e.toString());
		}
	}

	//--------------------------------------------------------------------------

	private static void
	log(String string) {
		try {
			FileWriter fw = new FileWriter("mail.log", true);
			fw.write(string + "\n");
			fw.close();
		} catch (IOException e) {
		}
	}

	//--------------------------------------------------------------------------

	private static void
	pingSite(String list) {
		try {
			URL url = new URL("https://" + getDomain() + "/MailLists/" + list + "/receive_mail");
			URLConnection connection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null)
			    log(line);
			in.close();
		} catch (IOException e) {
			log(e.toString());
		}
	}

	//--------------------------------------------------------------------------

	private static File
	writeToFile(String file_path) {
		File file = new File(file_path);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			int b = System.in.read();
			while (b != -1) {
				fos.write(b);
				b = System.in.read();
			}
			fos.close();
		} catch (IOException e) {
			log(e.toString());
		}
		file.setReadable(true, false);
		return file;
	}
}
